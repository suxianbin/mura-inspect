#include "MCExport.h"
#include "MCLib.h"

class MC_EXPORT::MC_EXPORTPrivate
{
public:
	MC_EXPORTPrivate(){};
	MCLib lib;
};


MC_EXPORT::MC_EXPORT()
{
	m_private = new MC_EXPORTPrivate();
	connect(m_private->lib.getSocket(), &QTcpSocket::stateChanged, [=](QAbstractSocket::SocketState state)
	{
		emit stateChanged(state);
	});
	connect(m_private->lib.getSocket(), &QTcpSocket::connected, [=]()
	{
		emit connected();
	});
	connect(m_private->lib.getSocket(), &QTcpSocket::disconnected, [=]()
	{
		emit disconnected();
	});

	connect(m_private->lib.getSocket(), SIGNAL(error(QAbstractSocket::SocketError error)), this, SIGNAL(error(QAbstractSocket::SocketError error)));

}

MC_EXPORT::~MC_EXPORT()
{
	
}



void MC_EXPORT::closeConnect()
{
	m_private->lib.closeConnect();
}

int MC_EXPORT::mc_init(const QString& _address, int _port, int _type, int _reconnectTime)
{
	if(m_private->lib.init(_address, _port, _type, _reconnectTime)) {
		return 1;
	}else {
		return 0;
	}
}

void MC_EXPORT::mc_close()
{
	m_private->lib.closeConnect();
}

int MC_EXPORT::mc_write_bits(int _addr, QVector<bool> _data)
{
	return m_private->lib.write_m(_addr, _data);
}


int MC_EXPORT::mc_write_registers(int _addr, QByteArray _values, bool _isBig)
{
	QByteArray data;
	if (!_isBig)
	{
		int i = 0;
		while(true)
		{
			if(i+1<_values.size())
			{
				data.append(_values[i+1]);
			}
			if(i<_values.size())
			{
				data.append(_values[i]);
			}else
			{
				break;
			}
			i += 2;
		}
	}else
	{
		data = _values;
	}

	return m_private->lib.write_d(_addr, data);
}

int MC_EXPORT::mc_read_bits(int _addr, int _nb, QVector<bool>& _values)
{
	QVector<bool> data(_nb);
	int rlt = m_private->lib.read_m(_addr, data);
	if(rlt ==1) {
		if(_values.size()<_nb) {
			_values.resize(_nb);
		}
		for(int i=0;i<_nb;i++) {
			if(data[i]) {
				_values[i] = 1;
			}else {
				_values[i] = 0;
			}
		}
	}
	return rlt;
}

int MC_EXPORT::mc_read_registers(int _addr, int _nb, QByteArray& _values, bool _isBig)
{
	QByteArray data;
	int rlt = m_private->lib.read_d(_addr, data, _nb);

	if (!_isBig)
	{
		int i = 0;
		while (true)
		{
			if (i + 1 < data.size())
			{
				_values.append(data[i + 1]);
			}
			if (i < data.size())
			{
				_values.append(data[i]);
			}
			else
			{
				break;
			}
			i += 2;
		}
	}
	else
	{
		_values = data;
	}
	return rlt;
}


QAbstractSocket::SocketState MC_EXPORT::state() const
{
	return m_private->lib.getSocket()->state();
}

QAbstractSocket::SocketError MC_EXPORT::error() const
{
	return m_private->lib.getSocket()->error();
}
