#pragma once
#pragma execution_character_set("utf-8")   // 防止乱码

#include <QObject>
#include <QDebug>
#include <QTcpSocket>
#include <QTimer>
#include <QtMath>

class MCLib :public QObject
{
	Q_OBJECT

signals:
	void sig_reConnect();
	void sig_timerStart();
public:
	MCLib(QObject* parent = Q_NULLPTR);
	~MCLib() override;

	QTcpSocket* getSocket();
	bool init(const QString& _address, int _port, int _type = 0, int _reconnectTime = 1000);
	void setAddress(const QString& address, int _port);
	void closeConnect();
	int read_m(int _add, QVector<bool>& _data);
	int write_m(int _add, QVector<bool> _data);
	int read_d(int _add, QByteArray& _data, int len = 1);
	int write_d(int _add, QByteArray _data);

private:

	//creat int main thread
	QTcpSocket* m_socket = Q_NULLPTR;			//socket链接客户端
	QString m_address ="";
	int m_port = 0;
	int m_reConnectTime;
	int m_type = -1;                            //发送格式：0-ASCII  1-BIN
	QTimer* m_timer = Q_NULLPTR;				//重连定时器
private:
	bool start();
	/**
	 * 读M寄存器_Ascii
	 * _add寄存器地址
	 * _data读取到的数据
	 * 成功返回1，失败返回0
	 */
	int read_m_Ascii(int _add, QVector<bool>& _data);

	/**
	 * 写M寄存器_Ascii
	 * _add寄存器地址
	 * _data写入的数据
	 * 成功返回1，失败返回0
	 */
	int write_m_Ascii(int _add,QVector<bool> _data);

	/**
	 * 读D寄存器_Ascii
	 * _add寄存器地址
	 * _data读取到的值
	 * len读取到数据的长度
	 * 成功返回1，失败返回0
	 */
	int read_d_Ascii(int _add, QByteArray& _data,int len = 1);

	/**
	 * 写D寄存器_Ascii
	 * _add寄存器地址
	 * _data写入的数据
	 * _len 写入数据的长度
	 * 成功返回1，失败返回0
	 */
	int write_d_Ascii(int _add, QByteArray _data);



	/**
	 * 读M寄存器_Bin
	 * _add寄存器地址
	 * _data读取到的数据
	 * 成功返回1，失败返回0
	 */
	int read_m_Bin(int _add, QVector<bool>& _data);

	/**
	 * 写M寄存器_Bin
	 * _add寄存器地址
	 * _data写入的数据
	 * 成功返回1，失败返回0
	 */
	int write_m_Bin(int _add, QVector<bool> _data);

	/**
	 * 读D寄存器_Bin
	 * _add寄存器地址
	 * _data读取到的值
	 * len读取到数据的长度
	 * 成功返回1，失败返回0
	 */
	int read_d_Bin(int _add, QByteArray& _data,int len = 1);

	/**
	 * 写D寄存器_Bin
	 * _add寄存器地址
	 * _data写入的数据
	 * _len 写入数据的长度
	 * 成功返回1，失败返回0
	 */
	int write_d_Bin(int _add, QByteArray _data);

	/**
	 * 功能 发送数据
	 * 输入 _data
	 * 返回
	 */
	bool sendData(const QByteArray& _data, QByteArray& outData);

private slots:

	void slot_error(QAbstractSocket::SocketError error);

	void slot_timeOut();
};
