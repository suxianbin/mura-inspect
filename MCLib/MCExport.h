#pragma once

#include <QObject>
#include <QTcpSocket>
#include "mclib_global.h"

class MCLIB_SHARED_EXPORT MC_EXPORT : public QObject
{
	Q_OBJECT
signals:
	/********************************
	 * 功能 已经成功链接信号
	 ********************************/
	void connected();
	/********************************
	 * 功能 已经断开链接信号
	 ********************************/
	void disconnected();
	/********************************
	 * 功能 链接状态改变信号
	 ********************************/
	void stateChanged(QAbstractSocket::SocketState);
	/********************************
	 * 功能 链接错误信息
	 ********************************/
	void error(QAbstractSocket::SocketError);

public:
	MC_EXPORT();
	~MC_EXPORT() override;

	/**
	 * 关闭连接
	 */
	void closeConnect();

	/*
	  *初始化方法
	  *_address 目的地址
	  *_port 目的端口号
	  *_type 类型 0-ascii  1-BIN
	  *_reconnectTime 自动重连时间 ms
	  */
	int mc_init(const QString& _address, int _port, int _type = 0, int _reconnectTime = 1000);

	/**
	 * 关闭连接
	 */
	void mc_close();
	
	
	/********************************
	 * 功能 写M寄存器
	 * 输入 _addr 地址
	 * 输入 _data 数据
	 * 返回 
	 ********************************/
	int mc_write_bits(int _addr, QVector<bool> _data);

	/********************************
	 * 功能 写D寄存器
	 * 输入 _addr 地址
	 * 输入 _data 数据
	 * 输入 _isBig 是否是大端序，高位字节放在内存的低地址端
	 * 返回 
	 ********************************/
	int mc_write_registers(int _addr, QByteArray _data ,bool _isBig = true);
	
	/********************************
	 * 功能 读多个M寄存器
	 * 输入 _addr  地址
	 * 输入 _nb 点数
	 * 输入 _values 
	 * 返回 
	 ********************************/
	int mc_read_bits(int _addr, int _nb, QVector<bool>& _values);

	/********************************
	 * 功能 
	 * 输入 _addr  地址
	 * 输入 _nb  D寄存器数量
	 * 输入 _values 
	 * 返回 
	 ********************************/
	int mc_read_registers(int _addr, int _nb, QByteArray& _values, bool _isBig = true);

	/********************************
	 * 功能 链接状态
	 * 返回 
	 ********************************/
	QAbstractSocket::SocketState state() const;
	/********************************
	 * 功能 链接错误信息
	 * 返回 
	 ********************************/
	QAbstractSocket::SocketError error() const;

private:
	class MC_EXPORTPrivate;
	MC_EXPORTPrivate* m_private = Q_NULLPTR;
};

 
