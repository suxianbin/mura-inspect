#include "MCLib.h"

#include <QThread>
#include <QtWidgets/QApplication>
#include <QRegExpValidator>

#pragma execution_character_set("utf-8")   // 防止乱码

char ConvertHexChar(char ch)
{
	if ((ch >= '0') && (ch <= '9'))
		return ch - 0x30;
	else if ((ch >= 'A') && (ch <= 'F'))
		return ch - 'A' + 10;
	else if ((ch >= 'a') && (ch <= 'f'))
		return ch - 'a' + 10;
	else return (-1);
}

QByteArray QString2Hex(QString str)
{
	QByteArray senddata;
	int hexdata, lowhexdata;
	int hexdatalen = 0;
	int len = str.length();
	senddata.resize(len / 2);
	char lstr, hstr;
	for (int i = 0; i < len;)
	{
		hstr = str[i].toLatin1(); //字符型
		if (hstr == ' ')
		{
			i++;
			continue;
		}
		i++;
		if (i >= len)
			break;
		lstr = str[i].toLatin1();
		hexdata = ConvertHexChar(hstr);
		lowhexdata = ConvertHexChar(lstr);
		if ((hexdata == 16) || (lowhexdata == 16))
			break;
		else
			hexdata = hexdata * 16 + lowhexdata;
		i++;
		senddata[hexdatalen] = (char)hexdata;
		hexdatalen++;
	}
	senddata.resize(hexdatalen);
	return senddata;
}

bool validatorIpAddress(QString _ipAddress)
{
	// IP地址的合法检测
	QRegExp rx("((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)");
	QRegExpValidator validator(rx);

	int pos = 0;
	QRegExpValidator::State state = validator.validate(_ipAddress, pos);
	if (QRegExpValidator::State::Acceptable == state)
	{
		return true;
	}
	return false;
}

MCLib::MCLib(QObject* parent): QObject(parent)
{
	m_socket = new QTcpSocket();
	m_socket->setSocketOption(QAbstractSocket::LowDelayOption, 1);
	m_socket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this,
	        SLOT(slot_error(QAbstractSocket::SocketError)));


	m_timer = new QTimer();
	m_timer->setInterval(1000);
	connect(m_timer, &QTimer::timeout, this, &MCLib::slot_timeOut);
	connect(m_socket, SIGNAL(connected()), m_timer, SLOT(stop()));
	connect(m_socket, SIGNAL(disconnected()), m_timer, SLOT(start()));
}


MCLib::~MCLib()
{
	if (m_timer != Q_NULLPTR)
	{
		m_timer->deleteLater();
		m_timer = Q_NULLPTR;
	}
	if(m_socket!=Q_NULLPTR)
	{
		m_socket->deleteLater();
		m_socket = Q_NULLPTR;
	}
}

QTcpSocket* MCLib::getSocket()
{
	return m_socket;
}

bool MCLib::init(const QString& _address, int _port, int _type, int _reconnectTime)
{
	setAddress(_address, _port);
	m_reConnectTime = _reconnectTime;
	m_type = _type;
	m_timer->setInterval(m_reConnectTime);
	return start();
}

void MCLib::setAddress(const QString& address, int _port)
{
	m_address = address;
	m_port = _port;
	closeConnect();
	start();
}

void MCLib::closeConnect()
{
	m_socket->disconnectFromHost(); //断开链接
	m_socket->abort(); // 断开所有连接
}

#pragma region 读写操作
int MCLib::read_m(int _add, QVector<bool>& _data)
{
	if (m_type == 0)
	{
		return read_m_Ascii(_add, _data);
	}
	else if (m_type == 1)
	{
		return read_m_Bin(_add, _data);
	}
	else
	{
		return 0;
	}
}

int MCLib::write_m(int _add, QVector<bool> _data)
{
	if (m_type == 0)
	{
		return write_m_Ascii(_add, {_data});
	}
	else if (m_type == 1)
	{
		return write_m_Bin(_add, {_data});
	}
	else
	{
		return 0;
	}
}

int MCLib::read_d(int _add, QByteArray& _data, int len)
{
	_data.clear();
	if (m_type == 0)
	{
		return read_d_Ascii(_add, _data, len);
	}
	else if (m_type == 1)
	{
		return read_d_Bin(_add, _data, len);
	}
	else
	{
		return 0;
	}
}

int MCLib::write_d(int _add, QByteArray _data)
{
	if (m_type == 0)
	{
		return write_d_Ascii(_add, _data);
	}
	else if (m_type == 1)
	{
		return write_d_Bin(_add, _data);
	}
	else
	{
		return 0;
	}
}


bool MCLib::start()
{
	if (!validatorIpAddress(m_address))
	{
		//qDebug() << "ip地址错误";
		return false;
	}
	m_timer->start();
	return true;
}

//ASCII十进制即可
int MCLib::read_m_Ascii(int _add, QVector<bool>& _data) //读M寄存器ASCII
{
	QString sendStr;
	sendStr.clear();
	sendStr += "500000FF03FF000018100004010000M*";
	//拼接寄存器地址
	QString addStr = QString::number(_add);
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}
	sendStr.append(addStr);

	//拼接寄存器长度
	//读取长度
	int registerLen = qCeil(_data.size() / 16.0);
	QString lenStr = QString::number(registerLen, 16);
	int i0 = 4 - lenStr.size();
	for (; i0 > 0; i0--)
	{
		lenStr.insert(0, "0");
	}
	sendStr.append(lenStr);

	QByteArray returned;
	returned.clear();
	int rlt = sendData(sendStr.toLocal8Bit(), returned);
	////qDebug() << "ASCII格式读M寄存器发送数据为：" << sendStr;
	////qDebug() << "ASCII格式读M寄存器返回数据为：" << returned;
	////qDebug() << "读取到的数据为：" << returned.at(22);
	if (rlt == 1)
	{
		QString resStr = returned.right(returned.size() - 22);
		for (int n = 0; n < _data.size(); n++)
		{
			//每个字符抵4位   每4个字符为一段
			//第几段字符中
			int nStr = n / 16;

			int i = n % 16;
			//第几个字符
			int nChar = i / 4;
			//第几位
			int nBit = i % 4;
			QString str = resStr.mid(nStr * 4, 4);
			QString cStr = str.mid(3 - nChar, 1);
			bool f1 = false;
			short num = cStr.toShort(&f1, 16);
			if (f1)
			{
				short bit = (num >> nBit) & 1;
				if (bit == 1)
				{
					_data[n] = true;
				}
				else
				{
					_data[n] = false;
				}
			}
			else
			{
				_data[n] = false;
			}
		}
	}
	return rlt;
}

int MCLib::write_m_Ascii(int _add, QVector<bool> _data) //写M寄存器Ascii
{
	QString sendStr;
	sendStr.clear();
	sendStr += "500000FF03FF00";

	//数据长度
	int dataLen = 24 + _data.size();
	QString dataLenStr = QString::number(dataLen, 16);
	int i0 = 4 - dataLenStr.size();
	for (; i0 > 0; i0--)
	{
		dataLenStr.insert(0, "0");
	}
	sendStr.append(dataLenStr);

	sendStr += "100014010001M*";

	//寄存器地址
	QString addStr = QString::number(_add);
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}
	sendStr.append(addStr);

	//寄存器长度
	QString len = QString::number(_data.size(), 16);
	i0 = 4 - len.size();
	for (; i0 > 0; i0--)
	{
		len.insert(0, "0");
	}
	sendStr.append(len);

	//拼接数据
	for (int i = 0; i < _data.size(); i++)
	{
		if (_data[i] == false)
		{
			sendStr.append("0");
		}
		else
		{
			sendStr.append("1");
		}
	}

	QByteArray returned;
	returned.clear();
	int rlt = sendData(sendStr.toLocal8Bit(), returned);
	//qDebug() << "ASCII格式写M寄存器发送数据为：" << sendStr;
	//qDebug() << "ASCII格式写M寄存器返回数据为：" << returned;
	if (rlt)
	{
		return 1;
	}
	return 0;
}

int MCLib::write_d_Ascii(int _add, QByteArray _data) //写D寄存器Ascii
{
	//寄存器数量
	int count = ceil((double)_data.size() / 2); //向上取整
	if (_add + count > 0x001F3F)
	{
		//qDebug() << "写入寄存器地址超出范围！！！！";
		return 0;
	}
	QString sendStr;
	sendStr.clear();
	QString headStr = "500000FF03FF00";

	//请求长度
	int size = 14 + 6 + 4 + count * 4; //整包请求长度数据字长 
	QString sizeStr = QString::number(size, 16); //转换为16进制
	for (; sizeStr.length() < 4;)
	{
		//字长 4位 16进制 补全
		sizeStr.insert(0, '0');
	}

	//其他及命令字符串  14位
	QString orderStr = "100014010000D*";

	//寄存器地址 6位
	QString addStr = QString::number(_add);
	//地址 6位 10进制补全 
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}

	//寄存器数量 4位
	QString countStr = QString::number(count);
	for (; countStr.length() < 4;)
	{
		countStr.insert(0, '0');
	}

	sendStr = headStr + sizeStr + orderStr + addStr + countStr;

	//数据补零  在末尾
	QString dataStr = _data.toHex();
	int lostN = count * 4 - dataStr.length();
	for (int i = 0; i < lostN; i++)
	{
		dataStr.append('0');
	}
	sendStr += dataStr;

	QByteArray returned;
	returned.clear();
	int rlt = sendData(sendStr.toLocal8Bit(), returned);
	if (rlt)
	{
		return 1;
	}
	else
	{
		//qDebug() << "ASCII格式写D寄存器发送数据为：" << sendStr;
		//qDebug() << "ASCII格式写D寄存器返回数据为：" << returned;
	}
	return 0;
}

int MCLib::read_d_Ascii(int _add, QByteArray& _data, int len) //读D寄存器Ascii
{
	_data.clear();
	QString sendStr;
	sendStr.clear();
	sendStr += "500000FF03FF000018100004010000D*";
	//寄存器地址
	QString addStr = QString::number(_add);
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}
	sendStr.append(addStr);

	//寄存器长度
	QString lenStr = QString::number(len, 16);
	int i0 = 4 - lenStr.size();
	for (; i0 > 0; i0--)
	{
		lenStr.insert(0, "0");
	}
	sendStr.append(lenStr);

	QByteArray returned;
	returned.clear();
	bool ok;
	int rlt = sendData(sendStr.toLocal8Bit(), returned);
	if (rlt == 1)
	{
		QByteArray byteData = QByteArray::fromHex(returned.mid(22));
		_data.append(byteData);
	}
	else
	{
		//qDebug() << "ASCII格式读D寄存器发送数据为：" << sendStr;
		//qDebug() << "ASCII格式读D寄存器返回数据为：" << returned;
	}

	return rlt;
}

//BIN寄存器地址和数据需为十六进制,发送数据和接收的数据也都是十六进制的
int MCLib::read_m_Bin(int _add, QVector<bool>& _data) //读M寄存器Bin
{
	QString sendStr;
	sendStr.clear();
	sendStr += "500000FFFF03000C00100001040000";

	//寄存器地址
	QString addStr = QString::number(_add, 16);
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}
	QString addStr_Reverse = NULL; //反转数据
	addStr_Reverse += addStr[4];
	addStr_Reverse += addStr[5];
	addStr_Reverse += addStr[2];
	addStr_Reverse += addStr[3];
	addStr_Reverse += addStr[0];
	addStr_Reverse += addStr[1];
	sendStr.append(addStr_Reverse);
	sendStr.append("90");

	//拼接寄存器长度
	QString len = QString::number(qCeil(_data.size() / 16.0), 16);
	int i0 = 4 - len.size();
	for (; i0 > 0; i0--)
	{
		len.insert(0, "0");
	}
	QString lenReverse;
	lenReverse += len[2];
	lenReverse += len[3];
	lenReverse += len[0];
	lenReverse += len[1];
	sendStr.append(lenReverse);


	QByteArray sendStr_Byte; //转换发送数据为16进制
	bool ok;
	for (int i = 0; i < sendStr.length(); i += 2)
	{
		char data = sendStr.mid(i, 2).toInt(&ok, 16);
		sendStr_Byte.append(data);
	}

	QByteArray returned; //接收返回的数据
	returned.clear();

	int rlt = sendData(sendStr_Byte, returned);
	//qDebug() << "Bin格式读M寄存器发送数据为：" << sendStr_Byte.toHex();
	//qDebug() << "Bin格式读M寄存器返回数据为：" << returned.toHex();

	if (rlt == 1)
	{
		QString resStr = returned.toHex().mid(22);
		//高低置位
		for (int n = 0; n < resStr.size(); n += 4)
		{
			QString s = resStr.mid(n, 4);
			resStr[n] = s[2];
			resStr[n + 1] = s[3];
			resStr[n + 2] = s[0];
			resStr[n + 3] = s[1];
		}


		for (int n = 0; n < _data.size(); n++)
		{
			//每个字符抵4位   每2个字符为一段
			//第几段字符中
			int nStr = n / 16;

			int i = n % 16;
			//第几个字符
			int nChar = i / 4;
			//第几位
			int nBit = i % 4;
			QString str = resStr.mid(nStr * 4, 4);
			QString cStr = str.mid(3 - nChar, 1);
			bool f1 = false;
			short num = cStr.toShort(&f1, 16);
			if (f1)
			{
				short bit = (num >> nBit) & 1;
				if (bit == 1)
				{
					_data[n] = true;
				}
				else
				{
					_data[n] = false;
				}
			}
			else
			{
				_data[n] = false;
			}
		}
	}
	return rlt;
}

int MCLib::write_m_Bin(int _add, QVector<bool> _data) //写M寄存器Bin
{
	QString sendStr;
	sendStr.clear();
	sendStr += "500000FFFF0300";

	//请求长度
	int size = 12 + qCeil(_data.size() / 2.0); //整包请求长度数据字长 
	QString sizeStr = QString::number(size, 16); //转换为16进制
	for (; sizeStr.length() < 4;)
	{
		//字长 4位 16进制 补全
		sizeStr.insert(0, '0');
	}
	QString sizeStr_Reverse; //反转数据
	sizeStr_Reverse.clear();
	sizeStr_Reverse += sizeStr[2];
	sizeStr_Reverse += sizeStr[3];
	sizeStr_Reverse += sizeStr[0];
	sizeStr_Reverse += sizeStr[1];
	sendStr += sizeStr_Reverse;
	sendStr += "100001140100";

	//地址
	QString addStr = QString::number(_add, 16);
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}
	QString addStr_Reverse; //反转数据
	addStr_Reverse.clear();
	addStr_Reverse += addStr[4];
	addStr_Reverse += addStr[5];
	addStr_Reverse += addStr[2];
	addStr_Reverse += addStr[3];
	addStr_Reverse += addStr[0];
	addStr_Reverse += addStr[1];
	sendStr.append(addStr_Reverse);
	sendStr.append("90");

	//拼接寄存器长度
	QString len = QString::number(_data.size(), 16);
	int i0 = 4 - len.size();
	for (; i0 > 0; i0--)
	{
		len.insert(0, "0");
	}
	QString lenReverse;
	lenReverse += len[2];
	lenReverse += len[3];
	lenReverse += len[0];
	lenReverse += len[1];
	sendStr.append(lenReverse);

	QString dataStr = "";
	for (int i = 0; i < _data.size(); i++)
	{
		if (_data[i] == false)
		{
			dataStr.append("0");
		}
		else
		{
			dataStr.append("1");
		}
	}
	if (_data.size() % 2 != 0)
	{
		dataStr.append("0");
	}
	sendStr.append(dataStr);

	QByteArray sendStr_Byte = 0; //转换发送数据为16进制
	bool ok;
	for (int i = 0; i < sendStr.length(); i += 2)
	{
		char data = sendStr.mid(i, 2).toInt(&ok, 16);
		sendStr_Byte.append(data);
	}

	QByteArray returned; //发送和返回数据
	returned.clear();
	int rlt = sendData(sendStr_Byte, returned);
	//qDebug() << "Bin格式写M寄存器发送数据为：" << sendStr_Byte.toHex();
	//qDebug() << "Bin格式写M寄存器返回数据为：" << returned.toHex();
	if (rlt)
	{
		return 1;
	}
	return 0;
}

int MCLib::write_d_Bin(int _add, QByteArray _data) //写D寄存器Bin
{
	int count = ceil((double)_data.size() / 2); //寄存器数量，向上取整
	// if (_add + count > 0x001F3F) {
	// 	//qDebug() << "写入寄存器地址超出范围！！！！";
	// 	return 0;
	// }

	QString sendStr; //存储发送数据
	sendStr.clear();

	//数据头
	QString headStr = "500000FFFF0300";

	//请求长度
	int size = 6 + 4 + 2 + count * 2; //整包数据字长 一个字符两个字  一个寄存器两个字符
	QString sizeStr = QString::number(size, 16).toUpper(); //转换为16进制
	for (; sizeStr.length() < 4;) //字长 4位 16进制 补全
	{
		sizeStr.insert(0, '0');
	}

	QString sizeStr_Reverse;
	sizeStr_Reverse.clear();
	sizeStr_Reverse += sizeStr[2];
	sizeStr_Reverse += sizeStr[3];
	sizeStr_Reverse += sizeStr[0];
	sizeStr_Reverse += sizeStr[1];

	//其他及命令字符串  14位
	QString orderStr = "100001140000";

	//寄存器地址 6位 转为16进制
	QString addStr = QString::number(_add, 16).toUpper();
	for (; addStr.length() < 6;)
	{
		//地址 6位 10进制补全 
		addStr.insert(0, '0');
	}
	QString addStr_Reverse; //反转数据
	addStr_Reverse.clear();
	addStr_Reverse += addStr[4];
	addStr_Reverse += addStr[5];
	addStr_Reverse += addStr[2];
	addStr_Reverse += addStr[3];
	addStr_Reverse += addStr[0];
	addStr_Reverse += addStr[1];
	addStr_Reverse += "A8";

	//寄存器数量 4位
	QString countStr = QString::number(count, 16).toUpper();
	for (; countStr.length() < 4;)
	{
		countStr.insert(0, '0');
	}
	QString countStr_Reverse;
	countStr_Reverse.clear();
	countStr_Reverse += countStr[2];
	countStr_Reverse += countStr[3];
	countStr_Reverse += countStr[0];
	countStr_Reverse += countStr[1];

	sendStr = headStr + sizeStr_Reverse + orderStr + addStr_Reverse + countStr_Reverse;
	bool ok;
	//数据部分
	QString dataStr; // 切换发送格式 十进制/十六进制   char转为QString
	dataStr.clear();
	dataStr = _data.toHex();
	int lostN = count * 4 - dataStr.length();
	for (int i = 0; i < lostN; i++)
	{
		dataStr.append('0');
	}
	sendStr.append(dataStr);

	QByteArray sendStr_Byte = 0; //转换发送数据为16进制
	for (int i = 0; i < sendStr.length(); i += 2)
	{
		char data = sendStr.mid(i, 2).toInt(&ok, 16);
		sendStr_Byte.append(data);
	}

	QByteArray returned;
	returned.clear();
	int rlt = sendData(sendStr_Byte, returned);

	if (rlt)
	{
		return 1;
	}
	else
	{
		//qDebug() << "Bin格式写D寄存器发送数据为：" << sendStr_Byte.toHex();
		//qDebug() << "Bin格式写D寄存器返回数据为：" << returned.toHex();
	}
	return 0;
}

int MCLib::read_d_Bin(int _add, QByteArray& _data, int len) //读D寄存器Bin
{
	QString sendStr;
	sendStr.clear();
	sendStr += "500000FFFF03000C00100001040000";
	QString addStr = QString::number(_add, 16); //寄存器地址   
	for (; addStr.length() < 6;)
	{
		addStr.insert(0, '0');
	}
	QString addStr_Reverse; //反转数据
	addStr_Reverse.clear();
	addStr_Reverse += addStr[4];
	addStr_Reverse += addStr[5];
	addStr_Reverse += addStr[2];
	addStr_Reverse += addStr[3];
	addStr_Reverse += addStr[0];
	addStr_Reverse += addStr[1];
	sendStr.append(addStr_Reverse + "A8");

	//拼接寄存器长度
	QString lenstr = QString::number(len, 16);
	int i0 = 4 - lenstr.size();
	for (; i0 > 0; i0--)
	{
		lenstr.insert(0, "0");
	}
	QString lenReverse;
	lenReverse += lenstr[2];
	lenReverse += lenstr[3];
	lenReverse += lenstr[0];
	lenReverse += lenstr[1];
	sendStr.append(lenReverse);


	QByteArray sendStr_Byte = 0; //转换发送数据为16进制
	bool ok;
	for (int i = 0; i < sendStr.length(); i += 2)
	{
		char data = sendStr.mid(i, 2).toInt(&ok, 16);
		sendStr_Byte.append(data);
	}

	QByteArray returned; //返回的所有数据
	returned.clear();
	int rlt = sendData(sendStr_Byte, returned);

	QByteArray returnedData; //返回数据中想要读取部分,临时存储
	QByteArray returnedDataFinal; //返回数据中想要读取部分


	if (rlt == 1)
	{
		returnedData = returned.mid(11);
		_data.append(returnedData);
	}
	else
	{
		//qWarning() << "Bin格式读D寄存器发送数据为：" << sendStr_Byte.toHex();
		//qWarning() << "Bin格式读D寄存器返回数据为：" << returned.toHex();
	}

	return rlt;
}
bool MCLib::sendData(const QByteArray& _data, QByteArray& outData) //发送数据函数
{
	if (m_socket != Q_NULLPTR && m_socket->state() == QAbstractSocket::ConnectedState)
	{
		if (m_socket->thread() != thread())
		{
			m_socket->moveToThread(thread());
		}

		m_socket->write(_data);
		if (m_socket->flush())
		{
			//等待返回值
			if (m_socket->waitForReadyRead(1000))
			{
				outData = m_socket->readAll();
				if (m_type == 0)
				{
					if (outData.mid(18, 4) == "0000")
					{
						//ASCII格式接收返回值
						return true;
					}
					else
					{
						////qDebug() << "发送命令为：" << _data;
						////qDebug() << "收到错误返回值：" << outData;
						////qDebug() << "收到错误返回值：" << outData;

						return false;
					}
				}
				else if (m_type == 1)
				{
					if (outData.toHex().mid(18, 4) == "0000")
					{						
						return true;
					}
					else
					{
						////qDebug() << "发送命令为：" << _data.toHex('-');
						////qDebug() << "收到错误返回值：" << outData.toHex('-');
						return false;
					}
				}
				else
				{
					////qDebug() << "没有发送格式";
					return false;
				}
			}
			else
			{
				//qDebug() << "socket返回超时,写出的数据为" + _data.toHex();
				//qDebug() << "socket返回超时，错误：" + m_socket->errorString();
			}
		}
		else
		{
			//qDebug() << "flush出错！" + m_socket->errorString();
		}
	}
	else
	{
		//qDebug() << "发送错误，Socket未连接！";
	}
	return false;
}

#pragma endregion

void MCLib::slot_error(QAbstractSocket::SocketError error)
{
	//sqCritical() << "MC Socket错误：" << m_socket->errorString();
}



void MCLib::slot_timeOut()
{
	if (m_socket->state() != QAbstractSocket::ConnectedState || m_socket->state() != QAbstractSocket::ConnectingState)
	{
		m_socket->abort();
		m_socket->connectToHost(m_address, m_port, QIODevice::ReadWrite); //设置连接的地址、端口号和读写方式
	}
}

