#ifndef MCLIB_GLOBAL_H
#define MCLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MCLIB_LIB)
#  define MCLIB_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define MCLIB_SHARED_EXPORT Q_DECL_IMPORT
#endif



#endif // QCANPOOL_GLOBAL_H
