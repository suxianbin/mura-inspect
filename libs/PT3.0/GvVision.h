#pragma once

#include <QWidget>
#include <vector>

#include "GvVisionData.h"

#define GVVISIONSHARED_LIBRARY

#if defined(GVVISIONSHARED_LIBRARY)
#  define GVVISIONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GVVISIONSHARED_EXPORT Q_DECL_IMPORT
#endif


class VisionInspect;
class GVVISIONSHARED_EXPORT GvVision : public QWidget
{
	Q_OBJECT

public:
	GvVision(QWidget *parent = nullptr,bool bComServer = false);
	//单例接口
	static GvVision* Instance();
	//单例释放接口
	static void ReleaseInstance();

signals:
	void sigExeCuteLog(QString);   //执行日志
	void sigCheckResult(s_CheckResult);  //检测结果
	void sigMapSpecAll(std::map<int, QString>);
	void sigVisualMode(bool);  //true 仿真模式 false 相机模式
	void sigProductId(int);   //当前流程执行产品Id
	void sigEndMsg(QString);  //结束信息
	void sigTrapCamera(std::vector<s_TrapCameraImage>);  //看图模式下发送图像

signals:
	void sigLoadTre(int);		 //底层加载Tre文件状态  1---OK  0---NG
	void sigLoadConfig(int);     //底层加载Config文件状态 1---OK  0---NG
	void sigEnvironment(int);    //底层执行环境状态 1---正在执行  0---停止执行

public:
	/**************************************************************
	* @brief  新建配方
	* @param [in]  strRecipePath 配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeName 配方名称   例如 test.tre
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool CreateRecipe(QString strRecipePath, QString strRecipeName);

	/**************************************************************
	* @brief  获取配方信息 路径 [2023/8/11 9:24 zaiyuan.li]
	* @param [in]  strRecipePath 配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeName 配方名称   例如 test.tre
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool GetRecipeInfo(QString& strRecipePath, QString& strRecipeName);


	/**************************************************************
	* @brief  加载配方
	* @param [in]  strRecipePath 配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeName 配方名称   例如 test.tre
	* @param [in]  bLoadConfig 是否加载config 
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool LoadRecipe(QString strRecipePath, QString strRecipeName, bool bLoadConfig);

	/**************************************************************
	* @brief  加载Tre文件
	* @param [in]  strRecipePath 配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeName 配方名称   例如 test.tre
	* @param [in]  bLoadConfig 是否加载config
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool LoadTreFile(QString strRecipePath, QString strRecipeName);

	/**************************************************************
	* @brief  加载Config文件
	* @param [in]  strRecipePath 配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeName 配方名称   例如 test.tre
	* @param [in]  bLoadConfig 是否加载config
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool LoadConfigFile(QString strRecipePath, QString strConfigName);

	/**************************************************************
	* @brief  保存配方
	* @param [in]  strRecipePath 配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeName 配方名称   例如 test.tre
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SaveRecipe(QString strRecipePath, QString strRecipeName);

	/**************************************************************
	* @brief  另存为配方
	* @param [in]  strRecipePath 旧的配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeSrcName 旧的配方名称   例如 test.tre
	* @param [in]  strRecipeDstPath 新的配方路径   例如 E:/Model/Model-侧面相机
	* @param [in]  strRecipeDstName 新的配方名称   例如 testDst.tre
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SaveRecipeAs(QString strRecipePath, QString strRecipeSrcName, QString strRecipeDstPath, QString strRecipeDstName);

	/**************************************************************
	* @brief  运行Tre脚本
	* @param  nMode nMode=0单次执行,  nMode=1 连续执行 nMode=2 停止执行
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool RunTre(int nMode);

	/**************************************************************
	* @brief  获取算子块列表
	* @return 返回算子块列表
	**************************************************************/
	QStringList GetBlockList();

	/**************************************************************
	* @brief  获取算子结构
	* @return 返回算子结构
	**************************************************************/
	void GetAlgList(std::vector<s_AlgNode>& vecAlgNode);

	/**************************************************************
	* @brief  获取图像源算子列表
	* @return 返回图像源算子列表
	**************************************************************/
	void GetTrapCameraList(std::vector<QString>& vecAlgNode);

	/**************************************************************
	* @brief  设置要获取图像算子
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SetGetImage(QString strAlg);

	/**************************************************************
	* @brief  获取算子界面UI指针
	* @param [in]  strBlockName 算子名称
	* @param [out] bModelPtr=true 返回定制界面指针,否则为普通界面指针
	* @param [out] pParam Widget界面指针
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool GetParamPtr(QString strBlockName,bool &bModelPtr, QWidget*& pParam);

	/**************************************************************
	* @brief  保存算子UI指针对应的参数
	* @param [in]  strBlockName 算子名称
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SaveParam(QWidget* pParam);

	/**************************************************************
	* @brief  获取算子输入图像
	* @param [in]  strName 算子名称
	* @return 返回QImage图像
	**************************************************************/
	QImage GetImage(QString strName);

	/**************************************************************
	* @brief  获取算子输入特征
	* @param [in]  strName 算子名称
	* @return 返回特征Spec表格
	**************************************************************/
	QWidget* GetSpec(QString strBlockName, QWidget* pParam);

	/**************************************************************
	* @brief  获取分类器界面
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool GetFlawClassify();

	/**************************************************************
	* @brief  获取分选界面
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool GetFeatureSort();

	/**************************************************************
	* @brief  获取所有可显示窗口列表
	* @return 成功返回true， 失败返回false
	**************************************************************/
	QStringList GetDlgShowList();

	/**************************************************************
	* @brief  设置指定界面显示
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SetDlgShow(QString strObject);

	/**************************************************************
	* @brief  设置GvGraphicsWindow指针,实现控制ROI显示，操作ROI
	* @param [in]  pGvGraphics GvGraphicsWindow指针
	* @param [in]  strAlgName 算子名称
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SetAlgRoi(void *pGvGraphics,QString strAlgName);

	/**************************************************************
	* @brief 算子单步执行
	* @param [in]  strAlgName 算子名称
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool Step_Run(QString strAlgName);

	/**************************************************************
	* @brief  设置算子工具箱是否显示
	* @param [in]  bShow bShow=true显示算子工具箱 bShow = false隐藏算子工具箱
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SetAlgToolBoxShow(bool bShow);

	/**************************************************************
	* @brief  设置绘制ROI图元信息
	* @param [in]  vecRoiDraw 需要绘制ROI集合
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool SetDrawRoi(const std::vector<s_Roi_Draw> vecRoiDraw);

	/**************************************************************
	* @brief  获取算子执行结果
	* @param [in]  strAlg 算子名称
	* @param [out]  pResult 算子运算后输出结构体指针
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool GetAlgResult(QString strAlg,int nParamIndex,QString strParamType, void *&pResult);

	/**************************************************************
	* @brief  修改算子参数
	* @param [in]  strAlg 算子名称
	* @param [in]  s_AlgInParam 需要修改的参数集合
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool ModifyAlgParam(QString strAlg, s_AlgNode sAlgNode);

	/**************************************************************
	* @brief  修改算子Roi参数
	* @param [in]  strAlg 算子名称
	* @param [in]  s_Roi_Draw 需要修改的ROI集合
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool ModifyAlgRoi(QString strAlg, std::vector<s_Roi_Draw> vecAlgParam);
 
	/**************************************************************
	* @brief  获取算子关联算子
	* @param [in]  strAlg 算子名称
	* @param [in]  strAlg 关联算子类型
	* @param [Out] strAlgOut 获取到的关联算子
	* @return 成功返回true， 失败返回false
	**************************************************************/
	bool GetConnectAlg(QString strAlg, QString strAlgType, QString &strAlgOut);

	/**************************************************************
	* @brief  输入算子类型，会通过信号将算子对应的结果发出来
	**************************************************************/
	void SetAlgType(QStringList strAlgTypelist);

	/**************************************************************
	* @brief  获取当前是相机模式还是仿真模式
	* @return 仿真模式返回true， 相机模式返回false
	**************************************************************/
	bool GetVisualMode();

	/**************************************************************
	* @brief  设置当前是相机模式还是仿真模式
	* @param [in]  bVisual bVisual=true 仿真模式  bVisual=false 相机模式
	* @return 仿真模式返回true， 相机模式返回false
	**************************************************************/
	void SetVisualMode(bool bVisual);

	/**************************************************************
	* @brief  获取底层执行状态
	**************************************************************/
	void Get_TRE_Environment();

	/**************************************************************
	* @brief  写InfoLog
	* @param [in] strInfo 文本字符串
	**************************************************************/
	void WriteInfoLog(QString strInfo);

	/**************************************************************
	* @brief  写WarinLog
	* @param [in] strInfo 文本字符串
	**************************************************************/
	void WriteWarinLog(QString strInfo);

	/**************************************************************
	* @brief  写WarinLog
	* @param [in] strInfo 文本字符串
	**************************************************************/
	void WriteErrorLog(QString strInfo);

	/**************************************************************
	* @brief  获取相机状态
	* @return 返回Json格式相机状态
	**************************************************************/
	QString GetCameraStatus();

	/**************************************************************
	* @brief  设定内部是否刷新结果
	**************************************************************/
	void SetRefreshData(bool bRefesh);

	/**************************************************************
	* @brief  测试从外部开始添加算子或者算子块
	* 
	**************************************************************/
	bool AddAlg();

	/**************************************************************
	* @brief  设置当前为分布式模式
	* @return 设置成功返回true， 设置失败返回false
	**************************************************************/
	bool SetDistributedMode(bool bSet);

	/**************************************************************
	* @brief  设置为返回图像模式
	* @param [in] strAlglist 需要返回图像的算子列表
	* @param [in] bUse 设置是否启用当前模式
	* @return 设置成功返回true， 设置失败返回false
	**************************************************************/
	bool SetTrapCameraMode(QStringList strAlglist, bool bUse);
	
	/**************************************************************
	* @brief  设置分布式当前IP
	* @param [in] strThriftIP IP地址
	* @return 设置成功返回true， 设置失败返回false
	**************************************************************/
	bool Set_Thrift_IP(QString strThriftIP);

	/**************************************************************
	* @brief  设置分布式从机IP
	* @param [in] strThriftIP IP地址
	* @return 设置成功返回true， 设置失败返回false
	**************************************************************/
	bool Set_Master_IP(QString strMasterIP);
	

	/**************************************************************
	* @brief  获取版本号 
	* @return 版本号
	**************************************************************/
	QString getVision();

private:
	VisionInspect* m_pVisionInspect;
	static GvVision* m_pGvVision;
	~GvVision();
};
