﻿#pragma once
#include <vector>
#include <qpoint.h>
#include <opencv2/opencv.hpp>


//算子运行参数
struct s_TreRunParam
{
	QString strParam;
	QString strValue;
public:
	bool operator ==(const QString& strFind)
	{
		return (this->strParam == strFind);
	}
};

struct s_AlgNode;
typedef struct s_BlockNode
{
	struct s_BlockNode* pBlockNext = nullptr;
	QString strBlockName;  //算子块名称
	std::vector<s_AlgNode> vecAlgList;  //算子列表

}s_BlockNode;


struct s_AlgNode
{
	QString strNodeName;      //结点标识
	QString strDisplayName;   //结点显示名称
	QString strCmdType;       //结点类型

	bool bBlcok;			 //是否包含算子块
	bool bShowNode;			 //结点隐藏或者显示
	bool bEnable;			 //是否启用算子

	std::vector<s_TreRunParam> vecBasicParam; //运行参数

	s_BlockNode sBlockNode; //算子块数据

	s_AlgNode()
	{
		bBlcok = false;
		bShowNode = true;
	}

	bool operator == (const QString& strFind)
	{
		return (this->strNodeName == strFind);
	}
};

//外接矩形
struct s_BoundingBox
{
	std::vector<QPointF> centrePoint;  //缺陷中心点
	std::vector<std::vector<QPointF>> vecPts;//外接矩形的5个点
	std::vector<std::vector<QPointF>> vecContour;  //轮廓

};


//算子自定义结构
struct s_ExtendData
{
	//中文特征以及对应值
	std::vector<std::pair<QString, double>> vecFeature;
	//英文特征以及对应值
	std::vector<std::pair<QString, double>> vecFeature_En;
	//一维点集
	std::vector<cv::Point2f> Vec_Point2f;
	//二维点集
	std::vector<std::vector<cv::Point2f>> Vec_Vec_Point2f;
};

//
struct s_DefectItem
{
	int nDefcetIndex;    //缺陷列表索引
	QString strChildType;   //子分类 #Check_1_
	QString strChildDisplayName; //子节点名称
	QString strFatherDisplayName; //父节点名称

};

//检测结果
struct s_DefectResult
{
	QString strAlgId;  //算子Id

	QString strAlgDisplayName; //算子备注
	QString strBlockDisplayName; //算子块备注
	QString strProcessDisplayName; //算子流程备注--20240105jyj

	QImage qImageData;  //图像数据

	std::vector<s_ExtendData> vecExtendData;  //自定义特征
	std::vector<s_DefectItem> vecDefectItem;  //筛选结果

	bool operator == (const QString& strFind)
	{
		return (this->strAlgId == strFind);
	}
};



//基础特征
struct s_FeatureBasic
{
	float   flawWidth;                    //缺陷长度
	float   flawHeight;                   //缺陷高度
	float   flawArea;                     //缺陷面积
	float	angle;                        //缺陷角度

};

//缺陷特征
struct s_Feature
{
	QString strFeature; //特征名称
	QString strDisplayName; //特征中文名称
	std::vector<double> vecFeatureValue; //特征数据

	bool operator == (const QString& strFind)
	{
		return (this->strFeature == strFind);
	}
};


//分类器输出结果
struct s_CheckBack
{
	QString strAlgId;            //算子名称
	QString strAlgDisplayName;   //算子备注
	QString strBlockDisplayName; //算子块备注

	QString strFatherDisplayName; //父节点名称
	QString strChildDisplayName;  //子节点名称

	int nDefectIndex;  //缺陷索引

};

//检测结果
struct s_CheckResult
{
	int nProductId;   //产品Id

	std::vector<QImage> qImageShow;  //图像数据
	std::map<int, std::vector<s_Feature>> mapFeature;  //产品Id 产品结果

	std::vector<s_CheckBack>  vecClassCheck; //分类器输出检测结果
	std::vector<std::vector<QPointF>> vecPts;//外接矩形的5个点

	std::vector<s_DefectResult> vecDefectResult;  //算子对应特征信息

};

//相机图像
struct s_TrapCameraImage
{
	int nProductId;   //产品Id

	QString strAlgId; //算子Id
	QString strIP;   //IP信息
	QString strStation; //工位信息

	QImage qImage;  //图像信息
};


//外部传入需要绘制ROI
struct s_Roi_Draw
{
	QString strRoiType;  //ROI类型 ROI_RECT(矩形)  GATHER_LINE(扫描矩形) POS_VEC_POINT(多边形) ROI_CIRCLE(圆形)
	QString strRoiValue; //ROI数据 
	QString strRoiLabel; //ROI标签(ROI图元显示使用)

	QString strObjectName; //ROI属性(修改ROI时使用)

	int nLineWidth;		 //线宽
	bool bShow;			 //是否显示
	bool bShieldRegion;  //是否为屏蔽区域
	QColor qColor;		 //显示颜色

	s_Roi_Draw()
	{
		nLineWidth = 1;
		bShow = false;
		bShieldRegion = false;
		qColor = QColor(255, 0, 0);
	}

};

//算子参数
struct s_AlgInParam
{
	QString strObjectName;  //参数名称
	QString strValue;  //参数数值  
};
