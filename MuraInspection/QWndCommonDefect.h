#pragma once

#include <QWidget>
#include "ui_QWndCommonDefect.h"

class QWndCommonDefect : public QWidget
{
	Q_OBJECT

public:
	QWndCommonDefect(QWidget *parent = nullptr);
	~QWndCommonDefect();

private:
	Ui::QWndCommonDefectClass ui;
};
