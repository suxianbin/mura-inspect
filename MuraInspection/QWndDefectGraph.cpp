﻿#pragma execution_character_set("utf-8")
#include "QWndDefectGraph.h"
#include <QStandardItem>
#include <QRandomGenerator>
#include <QMenu>

QWndDefectGraph::QWndDefectGraph(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	SetStyleSheet();

	m_pCustomPlot = new QCustomPlot(this);

	m_pCustomPlot->plotLayout()->insertRow(0);
	m_pPlotTitle = new QCPTextElement(m_pCustomPlot, "Defect History (Type)", QFont("sans", 14, QFont::Bold));
	m_pCustomPlot->plotLayout()->addElement(0, 0, m_pPlotTitle);

	m_pCustomPlot->setGeometry(ui.frame->geometry());
	m_pCustomPlot->setInteractions(QCP::iSelectLegend | QCP::iSelectPlottables);
	
	InitPlot();

	m_pCustomPlot->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_pCustomPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));
	connect(m_pCustomPlot, SIGNAL(legendClick(QCPLegend*, QCPAbstractLegendItem*, QMouseEvent*)), this, SLOT(on_LegendClicked(QCPLegend*, QCPAbstractLegendItem*, QMouseEvent*)));

	connect(ui.btnType, SIGNAL(clicked()), this, SLOT(on_btnTypeClicked()));
	connect(ui.btnSize, SIGNAL(clicked()), this, SLOT(on_btnSizeClicked()));
	connect(ui.btnOption, SIGNAL(clicked()), this, SLOT(on_btnOptionClicked()));
	connect(ui.btnJudgement, SIGNAL(clicked()), this, SLOT(on_btnJudgementClicked()));
	connect(ui.btnStack, SIGNAL(clicked()), this, SLOT(on_btnStackClicked()));
}

QWndDefectGraph::~QWndDefectGraph()
{}

void QWndDefectGraph::InitPlot()
{
	QCPAxis* xAxis = m_pCustomPlot->xAxis;//x轴
	QCPAxis* yAxis = m_pCustomPlot->yAxis;//y轴
	QPen pen;
	pen.setColor(Qt::black);
	QFont f;
	f.setPointSize(18);
	pen.setWidth(2);
	xAxis->setBasePen(pen);
	xAxis->setTickPen(pen);
	xAxis->setTickLength(10);
	xAxis->setSubTickLength(6);
	xAxis->setSubTickPen(pen);
	xAxis->setLabelColor(Qt::black);
	xAxis->setTickLabelColor(Qt::black);
	xAxis->setLabelFont(f);
	yAxis->setLabelFont(f);
	xAxis->setLabel("Axis X");

	f.setPointSize(10);
	f.setFamily(u8"微软雅黑");
	xAxis->setTickLabelFont(f);

	yAxis->setBasePen(pen);
	yAxis->setTickPen(pen);
	yAxis->setTickLength(10);
	yAxis->setSubTickLength(6);
	yAxis->setSubTickPen(pen);
	yAxis->setLabelColor(Qt::black);
	yAxis->setTickLabelColor(Qt::black);

	yAxis->setLabel("Axis Y");
	m_pCustomPlot->legend->setVisible(true);
	QFont legendFont = font();
	legendFont.setPointSize(10);
	m_pCustomPlot->legend->setFont(legendFont);
	m_pCustomPlot->legend->setSelectedFont(legendFont);
	m_pCustomPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items
	m_pCustomPlot->setBackground(QColor(221, 221, 221));
	// 设置图例
	m_pCustomPlot->legend->setVisible(true);
	m_pCustomPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop | Qt::AlignRight);

	QVector<double> ticks;
	QVector<QString> labels;
	for (int i = 0; i < 50; i++)
	{
		ticks.push_back(i);
		labels.push_back("ND303" + QString::number(i + 1));
	}
	QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
	textTicker->addTicks(ticks, labels);
	xAxis->setTicker(textTicker);        // 设置为文字轴
	xAxis->setTickLabelRotation(90);     // 轴刻度文字旋转60度
	xAxis->setSubTicks(true);           // 不显示子刻度
	xAxis->setTickLength(0, 4);          // 轴内外刻度的长度分别是0,4,也就是轴内的刻度线不显示
	xAxis->setRange(0, 55);             // 设置x轴范围
	yAxis->setRange(0, 30);

	AddPlotGraph(QColor(147, 68, 255), u8"TB");
	AddPlotGraph(QColor(33, 103, 251), u8"TW");
	AddPlotGraph(QColor(53, 217, 255), u8"RB");
	AddPlotGraph(QColor(111, 192, 55), u8"RW");
	AddPlotGraph(QColor(252, 233, 45), u8"MD");
	AddPlotGraph(QColor(254, 138,  1), u8"CD");
	//AddPlotGraph(QColor(255, 12, 2), u8"");
	m_pCustomPlot->replot();

}

void QWndDefectGraph::SetStyleSheet()
{
	QString style = "background-color:rgb(87, 153, 239);color:rgb(221, 221, 221);border-radius:3px;border:0px;}\
					QPushButton:pressed{ background-color:rgb(87, 153, 239) ; }\
					QPushButton:hover{ background-color:rgb(13,55,147);}";

	ui.btnType->setStyleSheet(style);
	ui.btnSize->setStyleSheet(style);
	ui.btnOption->setStyleSheet(style);
	ui.btnJudgement->setStyleSheet(style);
	ui.btnStack->setStyleSheet(style);
}

void QWndDefectGraph::AddPlotGraph(QColor color, QString text)
{
	QCPGraph* graph = m_pCustomPlot->addGraph();
	QPen p = QPen(QColor(color));
	p.setWidth(2);
	graph->setPen(p);
	graph->setName(text);
	graph->setLineStyle(QCPGraph::lsLine);
	graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, QColor(color), 6));
	QVector<double> keys1;
	QVector<double> keys2;
	QVector<double> values1;
	QVector<double> values2;
	int numValues = 50; // 要生成的值的数量
	int A = 2, B = 28;
	for (int i = 0; i < numValues; ++i) {
		int n = QRandomGenerator::global()->generateDouble() * (B - A) + A;
		keys1.push_back(i);
		keys2.push_back(i);
		values1.push_back(n);
	}
	// 设置数据并显示图表
	graph->setData(keys1, values1);
}

void QWndDefectGraph::contextMenuRequest(QPoint pos)
{
	QMenu* menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QString style = "QMenu{background-color: rgb(200,200,200);border: 0px solid white;}";
	menu->setStyleSheet(style);
	int idx = m_pCustomPlot->legend->selectTest(pos, false);
	if (idx >= 0) // context menu on legend requested
	{
		
		QAction*act = menu->addAction("显 示", this, [=]()
			{
				if (m_pSelectedLegendItem)
				{
					m_pSelectedLegendItem->plottable()->setVisible(true);
					m_pCustomPlot->replot();
				}
	
			});
		act->setIcon(QIcon(":/Icons/ic_homepage_side_pause.png"));
		menu->addAction("隐 藏", this, [=]()
			{
				if (m_pSelectedLegendItem)
				{
					m_pSelectedLegendItem->plottable()->setVisible(false);
					m_pCustomPlot->replot();
				}
			});
	}
	else  // general context menu on graphs requested
	{
		if (m_pCustomPlot->selectedGraphs().size() > 0)
			menu->addAction("隐藏选中图形", this, SLOT(hideSelectedGraph()));
	}

	menu->popup(m_pCustomPlot->mapToGlobal(pos));
}

void QWndDefectGraph::graphClicked(QCPAbstractPlottable* plottable, int dataIndex)
{

}

void QWndDefectGraph::hideSelectedGraph()
{
	if (m_pCustomPlot->selectedGraphs().size() > 0)
	{
		m_pCustomPlot->selectedGraphs().first()->setVisible(false);
		m_pCustomPlot->replot();
	}
}

void QWndDefectGraph::on_LegendClicked(QCPLegend* legend, QCPAbstractLegendItem* item, QMouseEvent*)
{
	Q_UNUSED(legend)
	if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
	{
		QCPPlottableLegendItem* plItem = qobject_cast<QCPPlottableLegendItem*>(item);
		if (plItem)
		{
			m_pSelectedLegendItem = plItem;
			//plItem->plottable()->setVisible(true);
			//m_pCustomPlot->replot();
		}	
	}
}

void QWndDefectGraph::on_btnTypeClicked()
{
	m_pPlotTitle->setText("Defect History (Type)");
	m_pCustomPlot->replot();
}

void QWndDefectGraph::on_btnStackClicked()
{
	m_pPlotTitle->setText("Defect History (Stack)");
	m_pCustomPlot->replot();
}

void QWndDefectGraph::on_btnJudgementClicked()
{
	m_pPlotTitle->setText("Defect History (Judgement)");
	m_pCustomPlot->replot();
}

void QWndDefectGraph::on_btnOptionClicked()
{
	m_pPlotTitle->setText("Defect History (Option)");
	m_pCustomPlot->replot();
}

void QWndDefectGraph::on_btnSizeClicked()
{
	m_pPlotTitle->setText("Defect History (Size)");
	m_pCustomPlot->replot();
}
