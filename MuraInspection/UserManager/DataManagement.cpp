#include "DataManagement.h"
static DataManagement* gInstance = nullptr;
DataManagement* DataManagement::Instance()
{
	if (gInstance == nullptr) {
		if (gInstance == nullptr) {
			gInstance = new DataManagement;
		}
	}
	return gInstance;
}

int DataManagement::login(st_userInfo& _userInfo)
{
	if (confirmSuperUser(_userInfo)) {
		return 999;
	}
	return confirmCommonUser(_userInfo);
}

QVector<st_userInfo> DataManagement::getUserList()
{
	// 查询数据库中的数据
	QVector<QMap<QString, QString>> userInfo = m_pDBManager->queryAllUser();
	QVector<st_userInfo> res;
	for(int i =0;i<userInfo.size();i++)
	{
		bool f;
		st_userInfo u = map2User(userInfo[i], f);
		if(f)
		{
			res << u;
		}

	}
	return res;
}

QString DataManagement::getSuperPwd()
{
	QString currentDate = QDateTime::currentDateTime().toString("yyyyMMdd");
	int result = 0;
	for (int i = 0; i < currentDate.count(); i++) {
		int j = currentDate.mid(i, 1).toInt();
		result += j * j;
	}
	// 放大倍数
	result *= m_magnificationTimes;
	return QString::number(result);
}

DataManagement::DataManagement()
{
	m_pDBManager = new DBManagement;

	QSettings settings(SETTINGS_PATH, QSettings::IniFormat);
	settings.setIniCodec("UTF-8");
	// 放大倍数不能为0
	m_magnificationTimes = settings.value("Settings/times", 1).toInt();
	m_magnificationTimes = m_magnificationTimes == 0 ? 1 : m_magnificationTimes;
}

int DataManagement::confirmSuperUser(st_userInfo& _userInfo)
{
	bool flag = true;
	flag &=	confirmIsEqual(_userInfo.user, SUPER_USER);
	flag &= confirmIsEqual(_userInfo.pwd, getSuperPwd());
	if (flag) {
		_userInfo.roleLabel ="超级权限";
		_userInfo.roleLevel = 999;
	}
	return flag;
}

int DataManagement::confirmCommonUser(st_userInfo& _userInfo)
{
	// 查询数据库中的数据
	QVector<QMap<QString, QString>> userInfo = m_pDBManager->queryAllUser();
	// 比对数据
	bool userExist = false, pwdExist = false;
	for (QMap<QString, QString> var : userInfo) {
		QString user = var.value("userName");
		if (confirmIsEqual(user, _userInfo.user)) {
			userExist = true;
			QString pwd = var.value("pwd");
			if (confirmIsEqual(pwd, _userInfo.pwd)) {
				pwdExist = true;
				_userInfo.roleLevel = var.value("roleLevel").toInt();
				_userInfo.roleLabel = var.value("roleLabel");
			}
		}
	}
	if (!userExist) {
		_userInfo.errorMsg = "用户名错误";
		return -1;
	}
	if (!pwdExist) {
		_userInfo.errorMsg = "密码错误";
		return 0;
	}
	return 1;
}

bool DataManagement::confirmIsEqual(const QString& _data1, const QString& _data2)
{
	return _data1 == _data2;
}

void DataManagement::dataConvert(const st_userInfo& _userInfo, QMap<QString, QString>& _data)
{
	_data.clear();
	_data.insert("id", _userInfo.id);
	_data.insert("userName", _userInfo.user);
	_data.insert("pwd", _userInfo.pwd);
	_data.insert("roleLevel", QString::number(_userInfo.roleLevel));
	_data.insert("roleLabel", _userInfo.roleLabel);

}
st_userInfo DataManagement::map2User(const QMap<QString, QString>& data, bool& rlt)
{
	
	st_userInfo res;
	bool f = false;
	if (data.contains("id"))
	{
		res.id = data["id"];
		f = true;
	}

	if (data.contains("userName"))
	{
		res.user = data["userName"];
		f = true;
	}

	if (data.contains("pwd"))
	{
		res.pwd = data["pwd"];
		f = true;
	}

	if (data.contains("roleLevel"))
	{
		res.roleLevel = data["roleLevel"].toInt();
		f = true;
	}

	if (data.contains("roleLabel"))
	{
		res.roleLabel = data["roleLabel"];
		f = true;
	}
	rlt = f;
	return res;
}




void DataManagement::getAllUserInfos(QVector<QMap<QString, QString>>& _data)
{
	_data.clear();
	_data = m_pDBManager->queryAllUser();
}

void DataManagement::queryAllUserName(QVector<QString>& _data, const QString& _notQueryId)
{
	_data.clear();
	_data = m_pDBManager->queryAllUserName(_notQueryId);
}

int DataManagement::delSingleUserInfo(const QMap<QString, QString>& _data)
{
	return m_pDBManager->delSingleUserInfo(_data);
}

int DataManagement::delSingleUserInfoById(const QString& id)
{
	return	m_pDBManager->delSingleUserInfoById(id);
}


int DataManagement::insertSingleUserInfo(const st_userInfo& _userInfo)
{
	QMap<QString, QString> data;
	dataConvert(_userInfo, data);
	return m_pDBManager->insertSingleUserInfo(data);
}

int DataManagement::insertOp(const OperatingRecord& o)
{
	return m_pDBManager->insertOperatingRecord(o);
}

QVector<OperatingRecord> DataManagement::queryOp(const QString& start, const QString& end)
{
	return m_pDBManager->queryOp(start, end);
}

int DataManagement::updateSingleUserInfo(const st_userInfo& _userInfo)
{
	QMap<QString, QString> data;
	dataConvert(_userInfo, data);
	return m_pDBManager->updateSingleUserInfo(data);
}

bool DataManagement::confirmUserIsExists(const st_userInfo& _userInfo)
{
	QVector<QString> userInfo;
	queryAllUserName(userInfo, _userInfo.id);
	return userInfo.contains(_userInfo.user);
}

bool DataManagement::doesDisplayUserSys()
{
	bool flag = doesDisplayUserSys(SUPER_KEY_PATH_1) | doesDisplayUserSys(SUPER_KEY_PATH_2);
	return flag;
}



bool DataManagement::doesDisplayUserSys(const QString& _superKeyPath)
{
	QFile file(_superKeyPath);
	if (!file.exists()) return false;
	QSettings settings(_superKeyPath, QSettings::IniFormat);
	QString  flag0 = settings.value("Time/flag0").toString();
	//如果当前的时间和flag0的时间相差5天以上，则需要用户登录
	QDateTime flag0DataTime = QDateTime::fromString(flag0, "yyyyMMdd");
	if (flag0DataTime.daysTo(QDateTime::currentDateTime()) > 5) {
		return false;//显示用户管理系统
	}
	//
	QStringList tempNumList = (flag0.remove(QString("0"))).split("");//移除flag0中0字符
	int rlt = 1;
	for (int i = 0; i < tempNumList.size(); i++)
	{
		if (tempNumList.at(i).isEmpty()) { continue; }
		rlt = rlt * (tempNumList.at(i).toInt());//每一位字符数字相乘
	}
	int flag1 = settings.value("Time/flag1").toInt();
	return rlt == flag1;
}
