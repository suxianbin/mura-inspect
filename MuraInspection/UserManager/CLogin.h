#pragma once
#ifndef _CLOGIN_FILE_H_
#define _CLOGIN_FILE_H_
#pragma execution_character_set("utf-8")
#include <QTimer>
#include <QDialog>
#include <QPushButton>
#include <QMessageBox>
#include <QAction>
#include "ui_CLogin.h"
#include "DataManagement.h"

class CLogin : public QDialog
{
	Q_OBJECT
public:
	explicit CLogin(QWidget *parent = Q_NULLPTR);
	~CLogin() override;
	st_userInfo getLogUser();
private:
	Ui::CLogin ui;
	DataManagement* m_manage = Q_NULLPTR;
	st_userInfo m_logUser;
private:


	/**
	 * @brief 登录按钮点击触发函数
	 */
	void onLoginBtnClicked();

	/**
	 * @brief 登录表单的校验器
	 */
	bool loginChecker(const QString& username, const QString& password);


};
#endif