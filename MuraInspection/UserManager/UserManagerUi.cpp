#include "UserManagerUi.h"

UserManagerUi::UserManagerUi(QWidget *parent)
	: QDialog(parent)
{
	m_dataMangeer = DataManagement::Instance();
	ui.setupUi(this);
	this->setWindowTitle("用户管理");
	init();
	ui.pb_exit->hide();
	ui.tw_op->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.tb_userList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

UserManagerUi::~UserManagerUi()
{
	if (m_puserTable != Q_NULLPTR) {
		delete m_puserTable;
		m_puserTable = Q_NULLPTR;
	}
}

QStringList UserManagerUi::user2StringList(st_userInfo _userInfo)
{
	QStringList list;
	list << _userInfo.id << _userInfo.user << _userInfo.roleLabel;
	return list;
}

QWidget* UserManagerUi::userGroup()
{
	return ui.groupBox;
}

QWidget* UserManagerUi::queryBtn()
{
	return ui.pb_queryLog;
}

QWidget* UserManagerUi::loginBtn()
{
	return ui.pb_login;
}


QWidget* UserManagerUi::exitBtn()
{
	return ui.pb_exit;
}


void UserManagerUi::init()
{
	initUserTable();		
	initUserDialog();
	initConnect();
	initLogin();
}

void UserManagerUi::initUserDialog()
{
	m_userDialog = new QDialog;
	dialog_Ui.setupUi(m_userDialog);
	m_userDialog->setModal(true);

	dialog_Ui.edit_username->setPlaceholderText("请输入账户名");
	dialog_Ui.edit_password->setPlaceholderText("请输入密码");
	dialog_Ui.edit_confirm->setPlaceholderText("请重新输入密码");
	dialog_Ui.edit_password->setEchoMode(QLineEdit::Password);
	dialog_Ui.edit_confirm->setEchoMode(QLineEdit::Password);
	UserType ut;
	m_pGroup = new QButtonGroup(m_userDialog);
	int i = 0;
	for(auto it = ut.m_data.begin();it!=ut.m_data.end();++it)
	{
		QRadioButton* radio = new QRadioButton(m_userDialog);
		radio->setText(it.i->t().second);
		dialog_Ui.hl->addWidget(radio);
		m_pGroup->addButton(radio, i);
		i++;
		m_radios.insert(radio, it.i->t().first);
	}
}

void UserManagerUi::initUserTable()
{
	//查询数据
	queryUser();
	QStringList heardList;
	heardList <<"用户id"<< "用户名" << "角色";
	
	m_puserTable = new RowTableDelegate<st_userInfo>(ui.tb_userList);
	m_puserTable->init(heardList, (&user2StringList));
	m_puserTable->refreshData(m_userInfoList);

	heardList.clear();
	heardList << "用户" << "操作内容" << "时间";
	ui.tw_op->setColumnCount(heardList.size());

	ui.tw_op->setHorizontalHeaderLabels(heardList);
	//ui.de_start->setDateTime(QDateTime::currentDateTime());
	//ui.de_end->setDateTime(QDateTime::currentDateTime());

	ui.de_start->setDisplayFormat("yyyy-MM-dd hh:mm:ss");
	ui.de_end->setDisplayFormat("yyyy-MM-dd hh:mm:ss");
	ui.de_start->setDate(QDate::currentDate().addDays(-1));

}

void UserManagerUi::initConnect()
{
	connect(ui.btn_add, &QPushButton::clicked, this, &UserManagerUi::slot_addUser);
	connect(ui.btn_del, &QPushButton::clicked, this, &UserManagerUi::slot_delUser);
	connect(ui.btn_edit, &QPushButton::clicked, this, &UserManagerUi::slot_editUser);
	connect(dialog_Ui.btn_sure, &QPushButton::clicked, this, &UserManagerUi::slot_sureClicked);
	connect(dialog_Ui.btn_cancel, &QPushButton::clicked, [=]()
	{
		m_userDialog->reject();
	});
	connect(ui.pb_login, &QPushButton::clicked, this, &UserManagerUi::slot_login);
	connect(ui.pb_login, &QPushButton::clicked, this, &UserManagerUi::slot_query);
	connect(ui.pb_exit, &QPushButton::clicked, this, &UserManagerUi::slot_exit);
	connect(ui.pb_queryLog, &QPushButton::clicked, this, &UserManagerUi::slot_query);
}

void UserManagerUi::initLogin()
{
	m_login = new CLogin(this);
}

void UserManagerUi::queryUser()
{
	m_userInfoList.clear();
	m_userInfoList = DataManagement::Instance()->getUserList();
}

void UserManagerUi::setCheckRadioAble()
{
	for (auto it = m_radios.begin(); it != m_radios.end(); ++it) {

		if (100 == m_currentUser.roleLevel)
		{
			it.key()->setDisabled(false);
		}
		else
		{
			if (it.value() < m_currentUser.roleLevel)
			{
				it.key()->setDisabled(false);
			}
			else
			{
				it.key()->setDisabled(true);
			}
		}
	}
}

void UserManagerUi::slot_addUser()
{
	m_dialogType = 0;
	m_userDialog->setWindowTitle("新增用户");
	setCheckRadioAble();
	dialog_Ui.edit_username->setText("");
	dialog_Ui.edit_username->setDisabled(false);
	dialog_Ui.edit_password->setText("");
	dialog_Ui.edit_confirm->setText("");
	((QRadioButton*)m_pGroup->button(0))->click();
	if (m_userDialog->exec() == QDialog::Accepted) {
		st_userInfo info;
		info.user = dialog_Ui.edit_username->text();
		info.pwd = dialog_Ui.edit_password->text();
		QAbstractButton* btn = m_pGroup->checkedButton();
		info.roleLevel = m_radios[btn];
		info.roleLabel = btn->text();

		if(m_dataMangeer!=Q_NULLPTR)
		{
			int rlt = m_dataMangeer->insertSingleUserInfo(info);
			if(rlt>0)
			{

				OperatingRecord o;
				o.user = m_currentUser.user;
				o.context = "增加用户:" + info.user + " 权限:" + info.roleLabel;
				o.dt = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
				m_dataMangeer->insertOp(o);
				slot_query();
				//更新表格
				queryUser();
				m_puserTable->refreshData(m_userInfoList);
				QMessageBox::information(Q_NULLPTR, "成功", "保存成功！");

				return;
			}

		}
		QMessageBox::warning(Q_NULLPTR, "错误", "保存失败！");
	}
}

void UserManagerUi::slot_editUser()
{
	m_dialogType = 1;
	int row = ui.tb_userList->currentRow();
	if (row >= 0 && row < m_userInfoList.size())
	{
		st_userInfo info = m_userInfoList[row];
		if (100 != m_currentUser.roleLevel)
		{
			if (m_currentUser.roleLevel <= info.roleLevel)
			{
				QMessageBox::warning(NULL, "警告", "您没有权限修改该用户！");
				return;
			}
		}
		
		m_userDialog->setWindowTitle("修改用户信息");
		setCheckRadioAble();

		dialog_Ui.edit_username->setText(info.user);
		dialog_Ui.edit_username->setDisabled(true);
		dialog_Ui.edit_password->setText(info.pwd);
		dialog_Ui.edit_confirm->setText(info.pwd);
		if(m_radios.values().contains(info.roleLevel))
		{
			QAbstractButton* btn  = m_radios.key(info.roleLevel, Q_NULLPTR);
			if(btn!=Q_NULLPTR)
			{
				btn->click();
			}
		}
		if (m_userDialog->exec() == QDialog::Accepted)
		{
			info.pwd = dialog_Ui.edit_password->text();
			QAbstractButton* btn = m_pGroup->checkedButton();
			info.roleLevel = m_radios[btn];;
			info.roleLabel = btn->text();
			if (m_dataMangeer != Q_NULLPTR)
			{
				int i = m_dataMangeer->updateSingleUserInfo(info);
				//更新表格
				if (i > 0)
				{
					OperatingRecord o;
					o.user = m_currentUser.user;
					o.context = "修改用户:" + info.user + " 权限:" + info.roleLabel;
					o.dt = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
					m_dataMangeer->insertOp(o);
					slot_query();

					m_userInfoList[row] = info;
					//更新表格
					m_puserTable->refreshData(m_userInfoList);
					QMessageBox::information(Q_NULLPTR, "成功", "保存成功！");
					return;
				}
			}
			QMessageBox::warning(Q_NULLPTR, "错误", "保存失败！");
		}
	}
	else
	{

		QMessageBox::warning(NULL, "警告", "请选择一条记录！");
	}
}

void UserManagerUi::slot_delUser()
{
	int row = ui.tb_userList->currentRow();
	if (row >= 0 && row < m_userInfoList.size()) {
		st_userInfo info = m_userInfoList[row];
		if (100 != m_currentUser.roleLevel)
		{
			if (m_currentUser.roleLevel <= info.roleLevel)
			{
				QMessageBox::warning(NULL, "警告", "您没有权限删除该用户！");
				return;
			}
		}

		if(info.id!=1) {
			QMessageBox::StandardButton result = QMessageBox::information(NULL, "请确认", "是否删除此条记录", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
			if (result == QMessageBox::Yes) {
				if (m_dataMangeer != Q_NULLPTR)
				{
					int i = m_dataMangeer->delSingleUserInfoById(info.id);
					//更新表格
					if (i > 0) {

						OperatingRecord o;
						o.user = m_currentUser.user;
						o.context = "删除用户:" + info.user + " 权限:" + info.roleLabel;
						o.dt = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
						m_dataMangeer->insertOp(o);
						slot_query();

						m_userInfoList.removeAt(row);
						m_puserTable->refreshData(m_userInfoList);
						QMessageBox::information(Q_NULLPTR, "成功", "删除成功！");
						return;
					}
				}
				QMessageBox::warning(Q_NULLPTR, "错误", "删除失败！");
			}
		}else {
			QMessageBox::warning(NULL, "警告", "默认管理员用户不能删除！");
		}
	}
	else {
		QMessageBox::warning(NULL, "警告", "请选择一条记录！");
	}
}

void UserManagerUi::slot_sureClicked()
{
	QString username = dialog_Ui.edit_username->text();
	username = username.trimmed();
	//用户名 不能为空
	if(username.isEmpty()) {
		QMessageBox::warning(NULL, "警告", "用户名不能为空！");
		return;
	}
	//用户名不能重名
	if(m_dialogType==0) {
		for (int i = 0; i < m_userInfoList.size(); i++) {
			QString str = m_userInfoList[i].user;
			if (str.trimmed() == username) {
				QMessageBox::warning(NULL, "警告", "用户名重复！");
				return;
			}
		}
	}
	
	//密码确认
	QString pass = dialog_Ui.edit_password->text();
	pass = pass.trimmed();
	if (pass.isEmpty()) {
		QMessageBox::warning(NULL, "警告", "密码不能为空！");
		return;
	}
	QString confirm = dialog_Ui.edit_confirm->text();
	confirm = confirm.trimmed();
	if(pass!=confirm) {
		QMessageBox::warning(NULL, "警告", "确认密码错误！");
		return;
	}
	m_userDialog->accept();
}

void UserManagerUi::slot_login()
{
	if (m_login->exec() == QDialog::Accepted)
	{
		m_currentUser = m_login->getLogUser();
		emit sig_currentUserChanged(m_currentUser);
		ui.pb_login->hide();
		ui.pb_exit->show();
	}
	else {
		ui.pb_login->setText("登录");
	}
}

void UserManagerUi::slot_exit()
{
	if (ui.pb_exit->text() == "登出")
	{
		m_currentUser = m_login->getLogUser();
		m_currentUser.roleLevel = 0;
		emit sig_currentUserChanged(m_currentUser);
		ui.pb_exit->hide();
		ui.pb_login->show();
		return;
	}
}

void UserManagerUi::slot_query()
{
	ui.tw_op->clearContents();

	ui.de_end->setDateTime(QDateTime::currentDateTime());
	QString start = ui.de_start->dateTime().toString("yyyy-MM-dd hh:mm:ss");
	QString end = ui.de_end->dateTime().toString("yyyy-MM-dd hh:mm:ss");
	QVector<OperatingRecord> list = m_dataMangeer->queryOp(start, end);
	ui.tw_op->setRowCount(list.size());
	std::reverse(list.begin(), list.end());
	for (int r = 0; r < list.size(); r++)
	{
		int c = 0;
		OperatingRecord record = list[r];
		ui.tw_op->setItem(r, c++, new QTableWidgetItem(record.user));
		ui.tw_op->setItem(r, c++, new QTableWidgetItem(record.context));
		ui.tw_op->setItem(r, c++, new QTableWidgetItem(record.dt));
	}
}
