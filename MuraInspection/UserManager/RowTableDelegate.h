#pragma once
#ifndef _ROW_TABLE_Delegate_FILE_H_
#define _ROW_TABLE_Delegate_FILE_H_
#pragma execution_character_set("utf-8")

#include <QObject>
#include <QTableWidget>
#include <QHeaderView>

template<class T>
class RowTableDelegate
{
	typedef QStringList(*MyFuncPtrType)(T);
public:

	
	
	/********************************
	 * 功能 行表委托类
	 * 输入 parent 被委托的边表格
	 * 输入 _heardList  头list
	 * 输入 _func 数据转化函数指针
	 ********************************/
	RowTableDelegate(QTableWidget *_table);
	void init(const QStringList& _heardList, MyFuncPtrType _func);

	/********************************
	 * 功能 清除数据
	 ********************************/
	void clearData();
	/********************************
	 * 功能 刷新数据
	 * 输入 _t 
	 ********************************/
	void refreshData(const QList<T>& _t);
	/********************************
	 * 功能 刷新数据
	 * 输入 _t 
	 ********************************/
	void refreshData(const QVector<T>& _t);

	/********************************
	 * 功能 添加数据
	 * 输入 _t 
	 ********************************/
	void addData(const T& _t);
	/********************************
	 * 功能 添加数据
	 * 输入 _t 
	 ********************************/
	void addData(const QVector<T>& _t);
private:
	QTableWidget* m_table;
	QVector<T> m_data;
	MyFuncPtrType my_func_ptr = NULL;
private:
	void addRow(const int& _row,const QStringList& strList);
};


#include "RowTableDelegate.cpp"

#endif
