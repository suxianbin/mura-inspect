#pragma once

#include <QObject>
#include <QMap>

class SqlManager : public QObject
{
	Q_OBJECT

public:

	static QByteArray createSql(const QString& _tableName, const QMap<QString, QString>& _tableParam);
	static QByteArray queryAllUserSql();
	static QByteArray queryAllUserName(const QString& _notQueryId);
	static QByteArray delSingleUserByIdSql(const QString& _id);
	static QByteArray insertSingleUserSql(const QString& _tableName, const QMap<QString, QString>& _userInfo);
	static QByteArray updateSingleUserByIdSql(const QString& _tableName, const QMap<QString, QString>& _userInfo);
};
