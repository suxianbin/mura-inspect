#include "CLogin.h"

CLogin::CLogin( QWidget* parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	m_manage = DataManagement::Instance();
	// 设置登录时的图标样式
	QAction* usernameAction = new QAction(ui.usernameEdit);
	usernameAction->setIcon(QIcon(":/Image/用户名.png"));
	ui.usernameEdit->addAction(usernameAction, QLineEdit::LeadingPosition);
	ui.usernameStatusIcon->setFixedSize(QSize(30, 30));
	QAction* passwordAction = new QAction(ui.passwordEdit);
	passwordAction->setIcon(QIcon(":/Image/密码.png"));
	ui.passwordEdit->addAction(passwordAction, QLineEdit::LeadingPosition);
	ui.passwordStatusIcon->setFixedSize(QSize(30, 30));


	// //设置背景图片
	QPalette palette;
	QString str = ":/Image/loginbackground.jpg";

	QPixmap pixmap(str);
	palette.setBrush(backgroundRole(), QBrush(pixmap));
	this->setPalette(palette);


	//设置图标
	setWindowIcon(QApplication::windowIcon());

	setWindowFlags(Qt::Dialog | Qt::WindowCloseButtonHint); //去掉标题栏中的问号

	connect(ui.loginBtn, &QPushButton::clicked, this, &CLogin::onLoginBtnClicked); //登录

}

CLogin::~CLogin()
{
}

st_userInfo CLogin::getLogUser()
{
	return m_logUser;
}

/**
 * @brief 登录按钮点击触发函数
 */
void CLogin::onLoginBtnClicked()
{
	// 清空表单数据
	ui.usernameStatusIcon->clear();
	ui.passwordStatusIcon->clear();
	ui.usernameCheckerError->clear();
	ui.passwordCheckerError->clear();

	const auto username = ui.usernameEdit->text();
	const auto password = ui.passwordEdit->text();
	
	//// 表单数据校验
	if (!loginChecker(username, password)) return;

	//数据库校验
	m_logUser.user = username;
	m_logUser.pwd = password;
	int rlt = m_manage->confirmSuperUser(m_logUser);
	if(rlt<=0)
	{
		rlt = m_manage->confirmCommonUser(m_logUser);
		if(rlt<=0)
		{
			ui.passwordStatusIcon->setPixmap(QPixmap(":/resources/icon/错误.png"));
			ui.passwordCheckerError->setText("账号密码错误");
			ui.passwordCheckerError->setStyleSheet("color:red");
			return;
		}
	}
	ui.passwordStatusIcon->clear();
	ui.passwordEdit->clear();
	accept();
	
}

/**
 * @brief 登录表单的校验器
 */
bool CLogin::loginChecker(const QString& username, const QString& password)
{
	if (username.isEmpty() || username == "")
	{
		ui.usernameStatusIcon->setPixmap(QPixmap(":/Image/错误.png"));
		ui.usernameCheckerError->setText("账号不能为空");
		ui.passwordCheckerError->setStyleSheet("color:red");
		return false;
	}
	ui.usernameCheckerError->clear();
	ui.usernameStatusIcon->setPixmap(QPixmap(":/Image/正确.png"));

	if (password.isEmpty() || password == "")
	{
		ui.passwordStatusIcon->setPixmap(QPixmap(":/Image/错误.png"));
		ui.passwordCheckerError->setText("密码不能为空");
		ui.passwordCheckerError->setStyleSheet("color:red");
		return false;
	}
	ui.passwordCheckerError->clear();
	ui.passwordStatusIcon->setPixmap(QPixmap(":/Image/正确.png"));


	return true;
}

