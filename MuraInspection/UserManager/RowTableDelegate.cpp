#include "RowTableDelegate.h"
#ifndef _ROW_TABLE_Delegate_FILE_CPP_
#define _ROW_TABLE_Delegate_FILE_CPP_
template <class T>
RowTableDelegate<T>::RowTableDelegate(QTableWidget* _table)
	:m_table(_table)
{
	//行头隐藏
	m_table->verticalHeader()->setHidden(true);
	//最后一列 占据剩余所有空间
	m_table->horizontalHeader()->setStretchLastSection(false);
	//选择模式为行 单选
	m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
	m_table->setSelectionMode(QAbstractItemView::SingleSelection);//单行选中

	//不可编辑
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);

	//行交替颜色
	m_table->setAlternatingRowColors(true);
	//表格宽度 自动适应
	m_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
}

template <class T>
void RowTableDelegate<T>::init(const QStringList& _heardList, MyFuncPtrType _func)
{
	m_table->setColumnCount(_heardList.size());
	m_table->setHorizontalHeaderLabels(_heardList);
	my_func_ptr = _func;
}


template <class T>
void RowTableDelegate<T>::clearData()
{
	m_data.clear();
	m_table->clearContents();
	m_table->setRowCount(0);
}

template <class T>
void RowTableDelegate<T>::refreshData(const QList<T>& _t)
{
	m_data = _t.toVector();
	clearData();
	int s = _t.size();
	m_table->setRowCount(s);
	int row = 0;
	foreach(T t,_t)
	{
		QStringList strList = my_func_ptr(t);
		addRow(row, strList);
		row++;
	}
}

template <class T>
void RowTableDelegate<T>::refreshData(const QVector<T>& _t)
{
	m_data = _t;
	clearData();
	int s = _t.size();
	m_table->setRowCount(s);
	for (int i = 0; i < _t.size(); i++) {
		T t = _t[i];
		QStringList strList = my_func_ptr(t);
		addRow(i, strList);
		
	}
}
template <class T>
void RowTableDelegate<T>::addData(const T& _t)
{
	int row = m_table->rowCount();
	m_table->insertRow(row);
	m_data.append(_t);
	this->addRow(row, _t);
}

template <class T>
void RowTableDelegate<T>::addData(const QVector<T>& _t)
{
	int row = m_table->rowCount();
	for(int i=0;i<_t.size();i++) {
		T t = _t[i];
		m_table->insertRow(row+t);
		m_data.append(t);
		this->addRow(row, t);
		
	}
}

template <class T>
void RowTableDelegate<T>::addRow(const int& _row, const QStringList& strList)
{
	int columnCount = m_table->columnCount();
	for(int c =0;c<columnCount;c++) {
		
		if(c<strList.size()) {
			QTableWidgetItem* item = new QTableWidgetItem();
			item->setText(strList[c]);
			
			m_table->setItem(_row, c, item);
		}
	}
	
}


#endif



