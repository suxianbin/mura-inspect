#include "DBManagement.h"
#include <QDebug>
#include <qcoreapplication.h>

DBManagement::DBManagement(QObject *parent)
	: QObject(parent)
{
	createDatabase();
}

DBManagement::~DBManagement()
{
}

int DBManagement::createDatabase()
{
	createFile(DB_FILE_PATH);
	createTable();

	return 1;
}

QVector<QMap<QString, QString>> DBManagement::queryAllUser()
{
	QString sql = SqlManager::queryAllUserSql();
	return queryBatch("queryAllUser", sql);
}

QVector<QString> DBManagement::queryAllUserName(const QString& _notQueryId)
{
	QString sql = SqlManager::queryAllUserName(_notQueryId);
	return queryValue("queryAllUserName", sql);
}

QVector<OperatingRecord> DBManagement::queryOp(const QString& startDt, const QString& _endDt)
{
	QString sql = "select * from operationalLogbook where opDateTime >='" + startDt + "' and opDateTime <= '" + _endDt + "';";
	QVector<QMap<QString, QString>> list =  queryBatch("queryOp", sql);
	QVector<OperatingRecord> res;
	for(int i=0;i< list.size();i++)
	{
		OperatingRecord o;
		o.user = list[i]["userName"];
		o.context = list[i]["context"];
		o.dt = list[i]["opDateTime"];
		res << o;
	}
	return res;
}

int DBManagement::delSingleUserInfo(const QMap<QString, QString>& _data)
{
	QString sql = SqlManager::delSingleUserByIdSql(_data.value("id"));
	return execSingleSql("delSingleUserInfo", sql);
}

int DBManagement::delSingleUserInfoById(const QString& id)
{
	QString sql = SqlManager::delSingleUserByIdSql(id);
	return execSingleSql("delSingleUserInfo", sql);
}

int DBManagement::insertSingleUserInfo(const QMap<QString, QString>& _userInfo)
{
	QString sql = SqlManager::insertSingleUserSql("userInfo", _userInfo);
	return execSingleSql("insertSingleUserInfo", sql);
}

int DBManagement::insertOperatingRecord(const OperatingRecord& data)
{
	QString sql;
	sql = "INSERT INTO operationalLogbook (userName,context,opDateTime) values ('"
	+ data .user+"','"
	+data.context+"','"
	+data.dt+"');";
	return execSingleSql("insertOp", sql);
}

int DBManagement::updateSingleUserInfo(const QMap<QString, QString>& _userInfo)
{
	QString sql = SqlManager::updateSingleUserByIdSql("userInfo", _userInfo);
	return execSingleSql("updateSingleUserInfo", sql);
}

void DBManagement::createFile(const QString& _dbPath)
{
	QDir dir(DB_PATH);
	if (!dir.exists()) dir.mkpath(DB_PATH);
	QFile file(_dbPath);
	if (!file.exists()) {
		file.open(QIODevice::ReadWrite);
	}
	file.close();
}

void DBManagement::createTable()
{
	QMap<QString, QString> userInfoMap;
	int rlt = 0;

	//用户表
	userInfoMap.clear();
	userInfoMap.insert("id", "INTEGER PRIMARY KEY AUTOINCREMENT");
	userInfoMap.insert("userName", "CHAR");
	userInfoMap.insert("pwd", "CHAR");
	userInfoMap.insert("roleLevel", "INTEGER");
	userInfoMap.insert("roleLabel", "CHAR");
	create("userInfo", userInfoMap);


	//操作日志
	QMap<QString, QString> userOperateLog;
	userOperateLog.insert("id", "INTEGER PRIMARY KEY AUTOINCREMENT");
	userOperateLog.insert("userName", "CHAR");
	userOperateLog.insert("context", "CHAR");
	userOperateLog.insert("opDateTime", "datetime");
	userOperateLog.insert("remark", "CHAR");
	create("operationalLogbook", userOperateLog);

}

int DBManagement::create(const QString& tableName, const QMap<QString, QString>& params)
{
	// 创建数据库表SQL
	QByteArray insertSql = SqlManager::createSql(tableName, params);
	// 执行
	// return getExecNum("createTable", insertSql, 0, 0);
	return execSingleSql("createTable", insertSql);
}

int DBManagement::getExecNum(const QString& connectName, const QString& execSql, void* params, void(*pFun)(QSqlQuery*, void*))
{
	// 连接数据库
	QSqlDatabase database = getConnect(connectName);
	// 数据库队列
	QSqlQuery sqlQuery(database);
	// 将仅转发模式转换为转发模式，只允许正值的next()和seek()出现
	sqlQuery.setForwardOnly(true);
	sqlQuery.prepare(execSql);
	if (params != 0) {
		(*pFun)(&sqlQuery, params);
	}
	// 执行SQL
	if (!sqlQuery.exec()) {
		// 执行SQL失败
		QString err = sqlQuery.lastError().text();
		//qDebug() << connectName + "Error: " + err;
		// 关闭数据库
		database.close();
		QSqlDatabase::removeDatabase(database.connectionName());

		return -1;
	}
	// 关闭数据库
	database.close();

	return 0;
}

QMap<QString, QString> DBManagement::query(const QString& _connectName, const QString& _sql)
{
	QSqlDatabase database = getConnect(_connectName);
	QSqlQuery query(database);
	query.prepare(_sql);
	query.setForwardOnly(true);

	QMap<QString, QString> hash = QMap<QString, QString>();
	if (!query.exec()) {
		QString err = query.lastError().text();
		qDebug() << database.connectionName() + "Error: " + err;
		// 关闭数据库
		database.close();
		QSqlDatabase::removeDatabase(database.connectionName());
		return hash;
	}

	QSqlRecord record = query.record();
	while (query.next()) {
		for (int i = 0; i < record.count(); ++i) {
			hash.insert(record.fieldName(i), query.value(i).toString());
		}
	}
	database.close();
	return hash;
}

QVector<QString> DBManagement::queryValue(const QString& _connectName, const QString& _sql)
{
	QSqlDatabase database = getConnect(_connectName);
	QSqlQuery query(database);
	query.prepare(_sql);
	query.setForwardOnly(true);

	QVector<QString> hash = QVector<QString>();
	if (!query.exec()) {
		QString err = query.lastError().text();
		qDebug() << database.connectionName() + "Error: " + err;
		// 关闭数据库
		database.close();
		QSqlDatabase::removeDatabase(database.connectionName());
		return hash;
	}

	QSqlRecord record = query.record();
	while (query.next()) {
		for (int i = 0; i < record.count(); ++i) {
			hash.push_back(query.value(i).toString());
		}
	}
	database.close();
	return hash;
}

QVector<QMap<QString, QString>> DBManagement::queryBatch(const QString& _connectName, const QString& _sql)
{
	QSqlDatabase database = getConnect(_connectName);
	QSqlQuery query(database);
	query.prepare(_sql);
	query.setForwardOnly(true);

	QVector<QMap<QString, QString>> hashs = QVector<QMap<QString, QString>>();
	if (!query.exec()) {
		QString err = query.lastError().text();
		qDebug() << database.connectionName() + "Error: " + err;
		// 关闭数据库
		database.close();
		QSqlDatabase::removeDatabase(database.connectionName());
		return hashs;
	}

	QSqlRecord record = query.record();
	while (query.next()) {
		QMap<QString, QString> hash;
		for (int i = 0; i < record.count(); ++i) {
			hash.insert(record.fieldName(i), query.value(i).toString());
		}
		hashs.push_back(hash);
	}
	database.close();
	return hashs;
}

int DBManagement::execSingleSql(const QString& _connectName, const QString& _sql)
{
	QSqlDatabase database = getConnect(_connectName);
	QSqlQuery query(database);
	query.prepare(_sql);
	if (!query.exec()) {
		QString err = query.lastError().text();
		qDebug() << database.connectionName() + "Error: " + err;
		// 关闭数据库
		database.close();
		QSqlDatabase::removeDatabase(database.connectionName());
		return 0;
	}
	database.close();
	return 1;
}

QSqlDatabase DBManagement::getConnect(const QString& _connectName)
{
	QString connectStr = _connectName;

	if (QSqlDatabase::contains(connectStr)) {
		QSqlDatabase database = QSqlDatabase::database(connectStr);
		database.open();
		return database;
	}

	// 添加数据库驱动、设置数据库名称、数据库登录用户名、密码
	QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE", connectStr);
	database.setDatabaseName(DB_FILE_PATH);

	// 设置超时等待时间500ms
	// database.setConnectOptions("QSQLITE_BUSY_TIMEOUT = 500");

	if (!database.open()) {
		QString err = database.lastError().text();
		qDebug() << QString("%1 -- getConnectError: %2").arg(connectStr).arg(err);	// 数据库连接打开失败
	}

	/*database.exec("PRAGMA synchronous = OFF");
	database.exec("PRAGMA journal_mode = MEMORY");
	database.exec("PRAGMA SQLITE_CONFIG_SINGLETHREAD");
	database.exec("PRAGMA SQLITE_CONFIG_SERIALIZED");*/

	return database;
}
