﻿#include "SqlManager.h"
#include <QByteArray>
#include <QString>

QByteArray SqlManager::createSql(const QString& _tableName, const QMap<QString, QString>& _tableParam)
{
	QByteArray insertSqlField = "", insertSql;
	foreach(QString key, _tableParam.keys()) {
		insertSqlField += QString(key + " " + _tableParam[key] + ", ").toUtf8();
	}
	insertSqlField = insertSqlField.left(insertSqlField.size() - 2);
	insertSql = "CREATE TABLE IF NOT EXISTS " + _tableName.toLocal8Bit() + " (" + insertSqlField + ");";

	return insertSql;
}

QByteArray SqlManager::queryAllUserSql()
{
	QByteArray sql;
	sql.clear();

	sql.append("SELECT * FROM userInfo ORDER BY id;");
	return sql;
}

QByteArray SqlManager::queryAllUserName(const QString& _notQueryId)
{
	QByteArray sql = "";
	sql.append(QString("SELECT id, userName FROM userInfo WHERE id != '" + _notQueryId + "';").toUtf8());
	return sql;
}

QByteArray SqlManager::delSingleUserByIdSql(const QString& _id)
{
	QByteArray sql;
	sql.clear();

	sql.append(QString("DELETE FROM userInfo where id = '" + _id + "';").toUtf8());
	return sql;
}

QByteArray SqlManager::insertSingleUserSql(const QString& _tableName, const QMap<QString, QString>& _userInfo)
{
	QByteArray sql = "", insertField = "", insertValue = "";

	sql.append("INSERT INTO " + _tableName.toLocal8Bit());
	for (QString key : _userInfo.keys()) {
		if (key != "id") {
			insertField += QString(key + ",").toUtf8();
			insertValue += QString("'" + _userInfo.value(key) + "',").toUtf8();
		}
	}
	insertField = insertField.left(insertField.length() - 1);
	insertValue = insertValue.left(insertValue.length() - 1);
	sql.append("(" + insertField + ")" + "VALUES(" + insertValue + ");");
	return sql;
}

QByteArray SqlManager::updateSingleUserByIdSql(const QString& _tableName, const QMap<QString, QString>& _userInfo)
{
	QByteArray sql = "", setField = "";

	sql.append(QString("UPDATE " + _tableName.toLocal8Bit() + " SET ").toUtf8());
	for (QString key : _userInfo.keys()) {
		if (key != "id") {
			setField += QString(key + " = '" + _userInfo.value(key) + "',").toUtf8();
		}
	}
	setField = setField.left(setField.length() - 1);
	sql.append(QString(setField + " WHERE id = '" + _userInfo.value("id") + "';").toUtf8());
	return sql;
}
