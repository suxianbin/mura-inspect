#pragma once

#include <QObject>
#include <QMap>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDir>

#include "SqlManager.h"
#include "DataManagement.h"

#define DB_PATH QCoreApplication::applicationDirPath() + "/UserManagement"
#define DB_FILE_PATH QCoreApplication::applicationDirPath() + "/UserManagement/userData.db"

struct OperatingRecord;

class DBManagement : public QObject
{
	Q_OBJECT

public:
	DBManagement(QObject *parent = nullptr);
	~DBManagement();

	int createDatabase();
	QVector<QMap<QString, QString>> queryAllUser();
	QVector<QString> queryAllUserName(const QString& _notQueryId);

	QVector<OperatingRecord> queryOp(const QString& startDt,const QString& _endDt);

	int delSingleUserInfo(const QMap<QString, QString>& _data);
	int delSingleUserInfoById(const QString& id);

	int insertSingleUserInfo(const QMap<QString, QString>& _userInfo);
	int insertOperatingRecord(const OperatingRecord & data);

	int updateSingleUserInfo(const QMap<QString, QString>& _userInfo);

private:
	void createFile(const QString& _dbPath);
	void createTable();
	int create(const QString& tableName, const QMap<QString, QString>& params);
	int getExecNum(const QString& connectName, const QString& execSql, void* params, void(*pFun)(QSqlQuery*, void*));
	QMap<QString, QString> query(const QString& _connectName, const QString& _sql);
	QVector<QString> queryValue(const QString& _connectName, const QString& _sql);
	QVector<QMap<QString, QString>> queryBatch(const QString& _connectName, const QString& _sql);
	int execSingleSql(const QString& _connectName, const QString& _sql);
	QSqlDatabase getConnect(const QString& _connectName);
};
