#pragma once
#pragma execution_character_set("UTF-8")

#include <QObject>
#include <QDateTime>
#include <QSettings>
#include <QVector>
#include <QMap>
#include <QCoreApplication>
#include "DBManagement.h"

#define SUPER_USER "cjmm"
#define SETTINGS_PATH QCoreApplication::applicationDirPath() + "/UserManagement/Settings.ini"
#define SUPER_KEY_PATH_1 "D:/超级KEY.ini"
#define SUPER_KEY_PATH_2 "D:/丹丹是社会哥.ini"

/**
 * \brief 默认一打开软件的权限等级
 */
#define DEFAULT_USER_ROLE_LEVEL 0

 /**
  * \brief 超级用户权限等级
  */
#define SUPPER_USER_ROLE_LEVLE 999

struct UserType
{
	QList<QPair<int, QString>> m_data;
	UserType()
	{
		QPair<int, QString> data;
		//0  未登录
		data.first = 1;
		data.second = "操作员";
		m_data << data;
		data.first = 10;
		data.second = "工程师";
		m_data << data;
		data.first = 100;
		data.second = "管理员";
		m_data << data;
		sortData();
	}


	QString getLabelByLevel(int level)
	{
		QString res;
		if (level >= 0)
		{
			for (int i = 0; i < m_data.size(); i++)
			{

			}
		}
		return res;
	}

private:
	void sortData()
	{
		std::sort(m_data.begin(), m_data.end(), [](const QPair<int, QString>& infoA, const QPair<int, QString>& infoB) {return infoA.first < infoB.first; });
	}

};


struct st_userInfo
{
	QString id;				// 数据库使用
	QString user = "未登录";			// 用户名
	QString pwd;			// 密码
	int roleLevel = 0;			// 权限 数字越大 权限越高
	QString roleLabel;		// 角色名称
	QString errorMsg;		// 错误信息

	st_userInfo()
	{
		id = "";
		user = "";
		pwd = "";
		errorMsg = "";
	}
};
Q_DECLARE_METATYPE(st_userInfo);

struct OperatingRecord
{
	QString user;
	QString context;
	QString dt;
};

class DBManagement;

class DataManagement : public QObject
{
	Q_OBJECT

public:
	/**
	 * @brief 获取该类的单例对象
	 * @retval 单例对象
	 */
	static DataManagement* Instance();
	/**
	 * @brief 用户登录
	 * @param _userInfo 用户信息
	 * @retval -1 用户名错误
	 * @retval 0 密码错误
	 * @retval 1 登录成功
	 */
	int login(st_userInfo& _userInfo);

	QVector<st_userInfo> getUserList();
	/**
	 * @brief 获取超级密码
	 * @return 超级密码
	 */
	QString getSuperPwd();
	// /**
	//  * @brief 获取类型说明
	//  * @param _type 类型
	//  * @return 说明
	//  */
	// QString getTypeDes(const UserType& _type);
	/**
	 * @brief 获取所有用户信息
	 * @param _data 用户信息
	 */
	void getAllUserInfos(QVector<QMap<QString, QString>>& _data);

	/**
	 * @brief 查询所有用户名
	 * @param _data 用户名
	 */
	void queryAllUserName(QVector<QString>& _data, const QString& _notQueryId);

	/**
	 * @brief 删除某一条数据
	 * @param _data 单条数据
	 * @retval 0 删除失败
	 * @retval 1 删除成功
	 */
	int delSingleUserInfo(const QMap<QString, QString>& _data);
	int delSingleUserInfoById(const QString& id);


	/**
	 * @brief 插入一条数据
	 * @param _userInfo 单条数据
	 * @retval 0 插入失败
	 * @retval 1 插入成功
	 */
	int insertSingleUserInfo(const st_userInfo& _userInfo);
	int insertOp(const OperatingRecord& o);
	QVector<OperatingRecord> queryOp(const QString& start, const QString& end);

	/**
	 * @brief 更新一条数据
	 * @param _userInfo 单条数据
	 * @retval 0 更新失败
	 * @retval 1 更新成功
	 */
	int updateSingleUserInfo(const st_userInfo& _userInfo);

	/**
	 * @brief 判断用户是否已经存在
	 * @param _userInfo 用户信息
	 * @retval false 不存在
	 * @retval true 存在
	 */
	bool confirmUserIsExists(const st_userInfo& _userInfo);

	/**
	 * @brief 判断是否有超级Key
	 * @retval false 不存在
	 * @retval true 存在
	 */
	bool doesDisplayUserSys();

	int confirmSuperUser(st_userInfo& _userInfo);
	int confirmCommonUser(st_userInfo& _userInfo);
	bool confirmIsEqual(const QString& _data1, const QString& _data2);

	/**
	 * \brief map数据转user
	 * \param data 
	 * \param res 
	 * \return 
	 */
	st_userInfo map2User(const QMap<QString, QString>& data, bool& rlt);


private:
	DataManagement();
	
	void dataConvert(const st_userInfo& _userInfo, QMap<QString, QString>& _data);
	/**
	 * @brief 判断是否有超级Key
	 * @param _superKeyPath 超级Key
	 * @retval false 不存在
	 * @retval true 存在
	 */
	bool doesDisplayUserSys(const QString& _superKeyPath);



private:
	DBManagement* m_pDBManager = nullptr;
	int m_magnificationTimes = 1;
};
