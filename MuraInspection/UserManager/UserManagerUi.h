#pragma once
#ifndef _USER_MANAGER_FILE_H_
#define _USER_MANAGER_FILE_H_
#pragma execution_character_set("UTF-8")
#include <QWidget>
#include "ui_UserManagerUi.h"
#include "ui_UserDialog.h"
#include <QButtonGroup>
#include <QMessageBox>
#include <QRadioButton>
#include "CLogin.h"
#include "RowTableDelegate.h"
#include "DataManagement.h"

/********************************
	 * 功能 用户数据转stringlist 用户表格显示
	 ********************************/
class UserManagerUi : public QDialog
{
	Q_OBJECT
signals:
	void sig_currentUserChanged(st_userInfo user);
public:
	UserManagerUi(QWidget* parent = Q_NULLPTR);
	~UserManagerUi() override;
	static QStringList user2StringList(st_userInfo _userInfo);

	QWidget* userGroup();
	QWidget* queryBtn();
	QWidget* loginBtn();
	QWidget* exitBtn();

	void slot_query();
private:
	Ui::UserManagerUi ui;														//主界面
	Ui::UserDialog dialog_Ui;												//新增用户弹窗界面

	QVector<st_userInfo> m_userInfoList;											//用户列表
	RowTableDelegate<st_userInfo>* m_puserTable = Q_NULLPTR;					//用户表格代理
	QDialog* m_userDialog                    = Q_NULLPTR;					//用户弹窗
	QButtonGroup* m_pGroup                   = Q_NULLPTR;					//新增 修改用户角色 单选按钮
	int m_dialogType = 0;													//小弹窗类型 0新增 1修改
	st_userInfo m_currentUser;										//当前用户等级
	DataManagement* m_dataMangeer = Q_NULLPTR;
	CLogin* m_login = Q_NULLPTR;
	//key radio，  value 是用户权限等级
	QMap<QAbstractButton*, int> m_radios;

private:
	void init();
	/********************************
	 * 功能 初始化用户管理弹窗
	 ********************************/
	void initUserDialog();
	/********************************
	 * 功能 初始化用户列表
	 ********************************/
	void initUserTable();
	/********************************
	 * 功能 初始化链接
	 ********************************/
	void initConnect();

	void initLogin();
	/********************************
	 * 功能 从数据库中查询数据
	 ********************************/
	void queryUser();

	/********************************
	 * 功能 根据角色控制新增或修改用户权限的使能
	 * 输入 role 
	 ********************************/
	void setCheckRadioAble();
private slots:
	void slot_addUser();
	void slot_editUser();
	void slot_delUser();
	void slot_sureClicked();
	void slot_login();
	void slot_exit();

};
#endif
