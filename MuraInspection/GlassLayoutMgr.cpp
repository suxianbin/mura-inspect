#include "GlassLayoutMgr.h"
#include <QJsonArray>

GlassLayoutMgr::GlassLayoutMgr()
{
}

GlassLayoutMgr::~GlassLayoutMgr()
{
}

bool GlassLayoutMgr::Load(std::string name)
{
	QString path = QString::fromStdString(m_path);
	QFile file(path + QString::fromStdString(name));
	if (file.open(QFile::ReadOnly))
	{
		QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
		if (!doc.isNull())
		{
			QJsonObject json = doc.object();
			QJsonArray dataArrays = json["GlassLayoutMgr"].toArray();
			if (dataArrays.size() == 0)
			{
				return false;
			}
			else
			{
				int i = 0;
				for (auto dataArr : dataArrays)
				{
					GlassLayout* prm = new GlassLayout("");
					QJsonObject obj = dataArr.toObject();
					prm->read(obj);
					GlassLayoutMgr::Instance()->Add(prm);
				}
				return true;
			}
		}
	}
	return false;
}

bool GlassLayoutMgr::Save(std::string name)
{
	QJsonArray dataArray;
	for (std::map<QString, Base*>::iterator itr = m_map.begin(); itr != m_map.end(); ++itr)
	{
		QJsonObject obj;
		itr->second->write(obj);
		dataArray.append(obj);
	}
	QJsonObject json;
	json["GlassLayoutMgr"] = dataArray;
	QJsonDocument doc(json);
	QString path = QString::fromStdString(m_path);
	QDir dir(path);
	if (!dir.exists())
	{
		dir.mkpath(path);
	}
	QFile file(path + QString::fromStdString(name));
	file.open(QFile::WriteOnly);
	file.write(doc.toJson());
	file.close();
	return true;
}


