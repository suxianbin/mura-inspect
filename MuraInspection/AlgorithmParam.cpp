#include "AlgorithmParam.h"

AlgorithmParam::AlgorithmParam(QString key) : Base(key)
{
}

AlgorithmParam::~AlgorithmParam()
{
}

void AlgorithmParam::write(QJsonObject& json) const
{
	Base::write(json);
	json["AlgoType"] = (int)AlgoType;
	json["Remark"] = Remark;
	json["CreateTime"] = CreateTime;
}

void AlgorithmParam::read(const QJsonObject& json)
{
	Base::read(json);
	AlgoType = (eAlgType)json["AlgoType"].toInt();
	Remark = json["Remark"].toString();
	CreateTime = json["CreateTime"].toString();
}
