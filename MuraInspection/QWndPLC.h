#pragma once

#include <QWidget>
#include "ui_QWndPLC.h"
#include "CommandData.h"
#include <QTimer>

class QWndPLC : public QWidget
{
	Q_OBJECT

public:
	QWndPLC(QWidget *parent = nullptr);
	~QWndPLC();

	void refreshJobData();
private:
	Ui::QWndPLCClass ui;
	QTimer timer50;

private slots:
	void on_timertimeout();
	void on_chk_refreshJobDataClicked();
	void on_pb_ResetStation1Clicked();
	void on_pb_ResetStation2Clicked();
	void on_pb_TransDataClicked();
};
