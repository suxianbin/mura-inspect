#pragma once
#include "Defect.h"
#include "JobData.h"
#include <QVector>
#include <QColor>
#include <QImage>
#include "PanelLayout.h"
#include <opencv2/opencv.hpp>

struct GlassMaskArea
{
	eMaskType type = eMaskType::Circle;
	double x = 0;          // 中心点 x 坐标（用于矩形和圆形）
	double y = 0;          // 中心点 y 坐标（用于矩形和圆形）
	double width = 0;      // 宽度（用于矩形）
	double height = 0;     // 高度（用于矩形）
	double radius = 0;     // 半径（用于圆形）
	PolygonVertex polygon; // 多边形（用于多边形屏蔽区域）
};

class Glass
{	
private:
	Glass() {};
	~Glass() {};
public:
	static Glass* Instance()
	{
		static Glass instance;
		return &instance;
	}
	int productIdToVision3 = 100;		//发送给vision3的产品id
	QString glassId;					//id
	QString productName = "";			//产品名称
	QString startdateTime = "";			//检测时间 yyyy-MM-dd hh:mm:ss
	QString enddateTime = "";			//检测时间 yyyy-MM-dd hh:mm:ss
	int recipeID = -1;
	QString recipeName = "";			//配方名称
	QString layoutName = "";			//布局名称
	int level = -1;						//glass Warning等级	0:good 1:warning 2:stop报警等级  glass AutoJudged的等级0:GD 1:RP 2:PD 3:NG
	QString levelLabel;					//等级字符
	QString WarninglevelLabel;			//等级字符 缓存warning 判级字符
	QColor levelColor;					//等级对应的颜色
	QVector<PanelLayout> panelRlts;		//panel结果列表
	int station1DefectCount = 0;
	int station2DefectCount = 0;
	int	defectTotal = 0;				//缺陷总数
	JobData jobData;					//工作数据
	QVector<QString> imgPaths;			//图片路径
	double pixel = 1;					//像素当量
	int operatorJudge = -1;				//人工评判  -1人工没有操作 0:GD 1:RP 2:PD 3:NG
	QString operatorJudgeReson = "";	//人工评判原因 	//添加 弹窗 N 的警告信息
	QString IntJobJudge;				// 长度1 上游设备判定的JobJudge
	QString VCDSLot;					// 长度1
	QString HPSlot;						// 长度1
	QString CPSlot;						// 长度1
	QString StepId;						//
	bool IsCommonDefWarnAlarm = false;		//共通缺陷warning报警
	bool IsCommonDefMuraAlarm = false;		//共通缺陷mura stop报警
	bool IsCommonDefCoaterAlarm = false;	//共通缺陷 coater stop报警
	bool IsmaxDefcetCountAlarm = false;		//缺陷超出overflow报警

	int orgMeanGray = 0;				//原图平均灰度值
	int preMeanGray = 0;				//预处理后平均灰度值
	cv::Mat Station1CombindImg;			//工站1拼接图像
	cv::Mat Station2CombindImg;			//工站2拼接图像
	QList<GlassMaskArea> glassMaskAreaList;
	QVector<Defect>Station1OrgDefect;
	QVector<Defect>Station1CrackDefect;
	QVector<Defect>Station1MuraDefect;
	QVector<Defect>Station2OrgDefect;
	QVector<Defect>Station2CrackDefect;
	QVector<Defect>Station2MuraDefect;
	void clear()
	{
		productName = "";
		startdateTime = "";
		enddateTime = "";
		recipeName = "";
		level = -1;
		panelRlts.clear();
		operatorJudge = -1;
		operatorJudgeReson = "";
		orgMeanGray = 0;
		preMeanGray = 0;
		VCDSLot = "";
		HPSlot = "";
		CPSlot = "";
		StepId = "";
		jobData = JobData();
		Station1OrgDefect.clear();
		Station1CrackDefect.clear();
		Station1MuraDefect.clear();
		Station2OrgDefect.clear();
		Station2CrackDefect.clear();
		Station2MuraDefect.clear();
	}
};

