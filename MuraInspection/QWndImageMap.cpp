#include "QWndImageMap.h"
#include "SystemConfig.h"
#include "Glass.h"

QWndImageMap::QWndImageMap(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	setAllButtonDefaultStyleSheet();

	m_pMainDispFrameLayout = new QVBoxLayout(0);
	m_pMainDispFrameLayout->setMargin(0);
	ui.frame->setLayout(m_pMainDispFrameLayout);
	m_pStation1 = new ShowLayoutWidget(this);
	m_pStation1->resize(700, 460);
	m_pMainDispFrameLayout->addWidget(m_pStation1);
	m_pStation1->setHidden(false);
	setButtonStyleSheet(ui.pb_Station1);

	m_pStation2 = new ShowLayoutWidget(this);
	m_pMainDispFrameLayout->addWidget(m_pStation2);
	m_pStation2->setHidden(true);

	//ui.pb_Station1->setVisible(QWndSystemParam::Instance()->SystemParam.Station1ImageDisp);
	//ui.pb_Station2->setVisible(QWndSystemParam::Instance()->SystemParam.Station2ImageDisp);

	m_pStation1->setLayoutChecked(SystemConfig::Instance()->Station1GlassLayoutChecked);
	m_pStation1->setPanelChecked(SystemConfig::Instance()->Station1GlassPanelChecked);
	m_pStation1->setDefectChecked(SystemConfig::Instance()->Station1GlassDefectChecked);
	m_pStation2->setLayoutChecked(SystemConfig::Instance()->Station2GlassLayoutChecked);
	m_pStation2->setPanelChecked(SystemConfig::Instance()->Station2GlassPanelChecked);
	m_pStation2->setDefectChecked(SystemConfig::Instance()->Station2GlassDefectChecked);

	connect(ui.pb_Station1, SIGNAL(clicked()), this, SLOT(on_pb_Station1Clicked()));
	connect(ui.pb_Station2, SIGNAL(clicked()), this, SLOT(on_pb_Station2Clicked()));
	connect(m_pStation1, SIGNAL(sig_LayoutChecked(bool)), this, SLOT(on_Station1sig_LayoutChecked(bool)));
	connect(m_pStation1, SIGNAL(sig_PanelChecked(bool)), this, SLOT(on_Station1sig_PanelChecked(bool)));
	connect(m_pStation1, SIGNAL(sig_DefectChecked(bool)), this, SLOT(on_Station1sig_DefectChecked(bool)));
	connect(m_pStation2, SIGNAL(sig_LayoutChecked(bool)), this, SLOT(on_Station2sig_LayoutChecked(bool)));
	connect(m_pStation2, SIGNAL(sig_PanelChecked(bool)), this, SLOT(on_Station2sig_PanelChecked(bool)));
	connect(m_pStation2, SIGNAL(sig_DefectChecked(bool)), this, SLOT(on_Station2sig_DefectChecked(bool)));
}

QWndImageMap::~QWndImageMap()
{}

void QWndImageMap::updateImageMap(int station)
{
	m_pStation1->loadImage(Glass::Instance()->Station1CombindImg);
	m_pStation1->showLayoutByName("B12_2");
	m_pStation1->showFlaws(Glass::Instance()->Station1OrgDefect);
}

void QWndImageMap::selectedDefect(int station, int index)
{
}

void QWndImageMap::on_pb_Station1Clicked()
{
	if (m_pStation1 && m_pStation2)
	{
		m_pStation1->setHidden(false);
		m_pStation2->setHidden(true);
		setAllButtonDefaultStyleSheet();
		setButtonStyleSheet(ui.pb_Station1);

	}
}

void QWndImageMap::on_pb_Station2Clicked()
{
	if (m_pStation1 && m_pStation2)
	{
		m_pStation1->setHidden(true);
		m_pStation2->setHidden(false);
		setAllButtonDefaultStyleSheet();
		setButtonStyleSheet(ui.pb_Station2);
	}
}

void QWndImageMap::on_Station1sig_LayoutChecked(bool checked)
{
	SystemConfig::Instance()->Station1GlassLayoutChecked = checked;
	SystemConfig::Instance()->writeToFile();
}

void QWndImageMap::on_Station1sig_PanelChecked(bool checked)
{
	SystemConfig::Instance()->Station1GlassPanelChecked = checked;
	SystemConfig::Instance()->writeToFile();
}

void QWndImageMap::on_Station1sig_DefectChecked(bool checked)
{
	SystemConfig::Instance()->Station1GlassDefectChecked = checked;
	SystemConfig::Instance()->writeToFile();
}

void QWndImageMap::on_Station2sig_LayoutChecked(bool checked)
{
	SystemConfig::Instance()->Station2GlassLayoutChecked = checked;
	SystemConfig::Instance()->writeToFile();
}

void QWndImageMap::on_Station2sig_PanelChecked(bool checked)
{
	SystemConfig::Instance()->Station2GlassPanelChecked = checked;
	SystemConfig::Instance()->writeToFile();
}

void QWndImageMap::on_Station2sig_DefectChecked(bool checked)
{
	SystemConfig::Instance()->Station2GlassDefectChecked = checked;
	SystemConfig::Instance()->writeToFile();
}

void QWndImageMap::hiddenAllWidget()
{
	m_pStation1->setHidden(true);
	m_pStation2->setHidden(true);
}

void QWndImageMap::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);color:rgb(20, 20, 20);border-radius:5px;border:0px;}\
					QPushButton:pressed {background-color:rgba(13,55,147,0.85);}\
					QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_Station1->setStyleSheet(style);
	ui.pb_Station2->setStyleSheet(style);
}

void QWndImageMap::setButtonStyleSheet(QPushButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(87, 153, 239);border-radius:5px;border:0px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}


