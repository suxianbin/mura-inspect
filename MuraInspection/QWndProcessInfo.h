#pragma once
#include <QWidget>
#include "ui_QWndProcessInfo.h"

class QWndProcessInfo : public QWidget
{
	Q_OBJECT

public:
	QWndProcessInfo(QWidget *parent = nullptr);
	~QWndProcessInfo();
	void updateRecipe();
	void updateRuntimeData();
	void updateStatus(int pcStatus, int plcStatus);
private:
	Ui::QWndProcessInfoClass ui;



};
