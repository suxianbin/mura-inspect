#pragma once

#include <QWidget>
#include <QVBoxLayout>
#include "ui_QWndRecipe.h"
#include "QWndGlassCrack.h"
#include "QWndGlassJudge.h"
#include "QWndPanelJudge.h"
#include "QWndCommonDefect.h"
#include "QWndDefectClassification.h"

class RecipeInfo;

class QWndRecipe : public QWidget
{
	Q_OBJECT

public:
	QWndRecipe(QWidget *parent = nullptr);
	~QWndRecipe();
	void loadConfig();
	void setStyleSheet();
	void refreshSpmeParamListTable();

	void refreshParam(RecipeInfo*recipeParam);

	void initParamComboBox();

	void initRecipeBindingList();

	void initRightWidget();
private:
	Ui::QWndRecipeClass ui;
private slots:
	void on_pb_ModifyBatchClicked();
	void on_pb_RecipeNewClicked();
	void on_pb_RecipeEditClicked();
	void on_pb_RecipeDeleteClicked();
	void on_pb_RecipeSearchClicked();
	void on_pb_RecipeSaveClicked();
	void on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

	void on_pb_BindingModifyBatchClicked();
	void on_pb_BindingRecipeClicked();
	void on_pb_BindingSearchClicked();

private:
	QWndGlassCrack* m_pGlsCrackPage = nullptr;
	QWndGlassJudge* m_pGlsJudgePage = nullptr;
	QWndPanelJudge* m_pPnlJudgePage = nullptr;
	QWndCommonDefect* m_pCommonDftPage = nullptr;
	QWndDefectClassification* m_pDftClsifyPage = nullptr;
	//������
	QVBoxLayout* m_pMainDispFrameLayout = nullptr;

	void hiddenAllWidget();
	void setAllButtonDefaultStyleSheet();
	void setButtonStyleSheet(QPushButton* button);
private slots:
	void on_pb_DefectClassClicked();
	void on_pb_GlassJudgeClicked();
	void on_pb_PanelJudgeClicked();
	void on_pb_CommonDefectClicked();
	void on_pb_GlassCrackClicked();
};
