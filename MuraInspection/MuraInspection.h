#pragma once

#include <QtWidgets/QWidget>
#include "ui_MuraInspection.h"
#include "QWndHomePage.h"
#include <QVBoxLayout>
#include <QTimer>
#include "MyMenu.h"
#include "QWndRecipe.h"
#include "QWndGlassLayout.h"
#include "QWndHardware.h"
#include "QWndSystemSet.h"
#include "UserManagerUi.h"
#include "SplashScreen.h"
#include "QWndHistoryData.h"
#include "QWndAlgorithmDebug.h"

class MuraInspection : public QWidget
{
    Q_OBJECT

public:
    MuraInspection(QWidget *parent = nullptr);
    ~MuraInspection();

    void GetSoftVersion();
    std::string GetReleaseTime(const char* c_date, const char* c_time);
private:
    Ui::MuraInspectionClass ui;
    SplashScreen* m_splash = Q_NULLPTR;							//加载显示界面

    QWndHomePage* m_pHomePage = nullptr;
    QWndHardware* m_pHardwarePage = nullptr;
    QWndRecipe* m_pRecipePage = nullptr;
    QWndGlassLayout* m_pLayoutPage = nullptr;
    QWndAlgorithmDebug* m_pAlgrithmPage = nullptr;
    UserManagerUi* m_pUserManagePage = nullptr;
    QWidget* m_pSpmePage = nullptr;
    QWndSystemSet* m_pSystemSetting = nullptr;
    QLabel* m_pUserLabel = nullptr;

    QWndHistoryData* m_pHistoryData;
	//主界面布局
	QVBoxLayout* m_pMainDispFrameLayout = nullptr;


    MyMenu* m_mainMenu = nullptr;

    QTimer timer100;
    QTimer autoTestTimer;

    void LoadRecipe();
    void SaveRecipe();
    void LoadHardwareConfig();
    void InitWindow();
    void InitWindowHeader();
    void InitSignalSlot();
    void InitMenu();

    void StartPLCLogic();
    void StopPLCLogic();
    void InitHardware();
private slots:
    void pb_closeClickedSlot();
    void pb_minClickedSlot();
    void pb_HomePageClickedSlot();
    void pb_HardwareDebugClickedSlot();
    void pb_AlgParamClickedSlot();
    void pb_RecipeClickedSlot();
    void pb_LayoutClickedSlot();
    void pb_UserMgrClickedSlot();
    void pb_SystemClickedSlot();
    void pb_SpmeClickedSlot();
    void pb_HistoryDataClickedSlot();

    void slot_runStepChanged(int station,int step);
    void slot_currentRecipeChanged();
    void slot_resultPanelJugdeEdit();
    void on_Timertimeout();
    void on_autoTestTimerTimeout();
    void on_pushButtonClicked();

    void on_sig_acqCompleted();
protected:
    virtual void closeEvent(QCloseEvent* event) override;//重写
    void hiddenAllWidget();
    void setAllButtonDefaultStyleSheet();
    void setButtonStyleSheet(QToolButton*button);
};
