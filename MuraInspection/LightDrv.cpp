#include "LightDrv.h"
#include "OPTLight.h"
#include "RuiShiLight.h"

LightDrv::LightDrv(eLightVendor vendor)
{
	switch (vendor)
	{
	case eLightVendor::OPT:
		lightDrv = new OPTLight();
		break;
	case eLightVendor::RUISHI:
		lightDrv = new RuiShiLight();
		break;
	default:
		lightDrv = new OPTLight();
		break;
	}
}

LightDrv::~LightDrv()
{
}

void LightDrv::SetIntensity(int ch, int intensity)
{
	lightDrv->SetIntensity(ch, intensity);
}

void LightDrv::TurnOff(int ch)
{
	lightDrv->TurnOff(ch);
}
