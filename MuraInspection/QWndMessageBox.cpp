#include "QWndMessageBox.h"
#pragma execution_character_set("utf-8")

QWndMessageBox::QWndMessageBox(QString msg, QColor textColor, QWidget *parent)
	: QDialog(parent)
{
	setAttribute(Qt::WA_TranslucentBackground);
	this->setObjectName("QWndMessageBox");
	this->setWindowFlags(Qt::FramelessWindowHint);
	ui.setupUi(this);
	this->setStyleSheet("QWidget#QWndMessageBox{ border: 3px solid; border-color:rgba(2,2,2,70); border-radius: 13px;background-color: rgba(2,2,2,50);}");
	connect(ui.btnClose, SIGNAL(clicked()), this, SLOT(on_btnClose_clicked()));
	connect(ui.btnOK, SIGNAL(clicked()), this, SLOT(on_btnOK_clicked()));
	connect(ui.btnCancel, SIGNAL(clicked()), this, SLOT(on_btnCancel_clicked()));
	
	QString close_style = "color:rgba(54, 54, 54);background-color:rgba(223,228,255,0.15);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(225, 0, 0);}\
					 QPushButton:hover {background-color:rgb(225, 0, 0);}";
	ui.btnClose->setStyleSheet(close_style);

	QString style = "color:rgb(221, 221, 221);background-color:rgba(13,55,147,0.65);border-radius:5px;}\
					 QPushButton:pressed {background-color:rgba(13,55,147,0.65);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.btnOK->setStyleSheet(style);
	ui.btnCancel->setStyleSheet(style);
	ui.pushButton->setIcon(QIcon(":/img/query.png"));

	//ui.btnCancel->setFocus(); //设置默认焦点
	//ui.btnCancel->setShortcut(QKeySequence::InsertParagraphSeparator); //设置快捷键为键盘的“回车”键
	//ui.btnCancel->setShortcut(Qt::Key_Enter); //设置快捷键为enter键
	//ui.btnCancel->setShortcut(Qt::Key_Return); //设置快捷键为小键盘上的enter键
	ui.label->setText(msg);
	ui.btnCancel->setFocus();
	ui.btnCancel->setDefault(true);
	QString str = QStringLiteral("background-color:rgba(%1, %2, %3,0.15);border:0px;border-radius:22px;").arg(textColor.red()).arg(textColor.green()).arg(textColor.blue());
	ui.pushButton->setStyleSheet(str);
}

QWndMessageBox::~QWndMessageBox()
{
}

void QWndMessageBox::mousePressEvent(QMouseEvent * qevent)
{
	if (qevent->button() == Qt::LeftButton)
	{
		mouse_press = true;
		//鼠标相对于窗体的位置（或者使用event->globalPos() - this->pos()）
		move_point = qevent->pos();;
	}
}

void QWndMessageBox::mouseReleaseEvent(QMouseEvent * qevent)
{
	//设置鼠标为未被按下
	mouse_press = false;
}

void QWndMessageBox::mouseMoveEvent(QMouseEvent * qevent)
{
	//若鼠标左键被按下
	if (mouse_press)
	{
		//鼠标相对于屏幕的位置
		QPoint move_pos = qevent->globalPos();

		//移动主窗体位置
		this->move(move_pos - move_point);
	}
}

void QWndMessageBox::on_btnOK_clicked()
{
	//qDebug()<<1;
	this->accept();
	this->close();
}

void QWndMessageBox::on_btnCancel_clicked()
{
	this->reject();
	this->close();
}

void QWndMessageBox::on_btnClose_clicked()
{
	close();
}
