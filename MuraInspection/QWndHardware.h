#pragma once

#include <QWidget>
#include "ui_QWndHardware.h"
#include "QWndSPME.h"
#include "QWndPLC.h"
#include "QWndCamera.h"
#include "QWndLight.h"
#include <QVBoxLayout>

class QWndHardware : public QWidget
{
	Q_OBJECT

public:
	QWndHardware(QWidget *parent = nullptr);
	~QWndHardware();

private:
	Ui::QWndHardwareClass ui;

    QWndSPME* m_pSpmePage = nullptr;
	QWndCamera* m_pCameraPage = nullptr;
	QWndPLC* m_pPLCPage = nullptr;
	QWndLight* m_pLightPage = nullptr;

	//������
	QVBoxLayout* m_pMainDispFrameLayout = nullptr;

	void hiddenAllWidget();
	void setAllButtonDefaultStyleSheet();
	void setButtonStyleSheet(QPushButton* button);
private slots:
	void on_pb_PLCClicked();
	void on_pb_CameraClicked();
	void on_pb_LightClicked();
	void on_pb_SPMEClicked();
};
