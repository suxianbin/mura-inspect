#include "QWndSPME.h"
#include "SpmeParamMgr.h"
#include "QWndMessageBox.h"
#pragma execution_character_set("utf-8")

QWndSPME::QWndSPME(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	//ui.pb_SaveMajorPoint->setVisible(false);
	//ui.pb_SaveMinorPoint->setVisible(false);

	InitTable();
	refreshSpmeParamListTable();
	setStyleSheet();

	connect(ui.pb_SpmePrmNew, SIGNAL(clicked()), this, SLOT(on_pb_SpmePrmNewClicked()));
	connect(ui.pb_SpmePrmEdit, SIGNAL(clicked()), this, SLOT(on_pb_SpmePrmEditClicked()));
	connect(ui.pb_SpmePrmDelete, SIGNAL(clicked()), this, SLOT(on_pb_SpmePrmDeleteClicked()));
	connect(ui.pb_SpmePrmSearch, SIGNAL(clicked()), this, SLOT(on_pb_SpmePrmSearchClicked()));
	connect(ui.pb_ModifyBatch, SIGNAL(clicked()), this, SLOT(on_pb_ModifyBatchClicked()));
	connect(ui.pb_SpmePrmSave, SIGNAL(clicked()), this, SLOT(on_pb_SpmePrmSaveClicked()));
	connect(ui.btn_RecipeSet, SIGNAL(clicked()), this, SLOT(on_btn_RecipeSetClicked()));
	connect(ui.btn_AnalyzeSet, SIGNAL(clicked()), this, SLOT(on_btn_AnalyzeSetClicked()));
	connect(ui.btn_StartReference, SIGNAL(clicked()), this, SLOT(on_btn_StartReferenceClicked()));
	connect(ui.btn_StartOD, SIGNAL(clicked()), this, SLOT(on_btn_StartODClicked()));
	connect(ui.btn_Analyze, SIGNAL(clicked()), this, SLOT(on_btn_AnalyzeClicked()));
	connect(ui.btn_ODReferencePos, SIGNAL(clicked()), this, SLOT(on_btn_ODReferencePosClicked()));
	connect(ui.pb_ColorReferencePos, SIGNAL(clicked()), this, SLOT(on_pb_ColorReferencePosClicked()));
	connect(ui.pb_AddMinorPoint, SIGNAL(clicked()), this, SLOT(on_pb_AddMinorPointClicked()));
	connect(ui.pb_DeleteMinorPoint, SIGNAL(clicked()), this, SLOT(on_pb_DeleteMinorPointClicked()));
	connect(ui.pb_SaveMinorPoint, SIGNAL(clicked()), this, SLOT(on_pb_SaveMinorPointClicked()));
	connect(ui.pb_AddMajorPoint, SIGNAL(clicked()), this, SLOT(on_pb_AddMajorPointClicked()));
	connect(ui.pb_DeleteMajorPoint, SIGNAL(clicked()), this, SLOT(on_pb_DeleteMajorPointClicked()));
	connect(ui.pb_SaveMajorPoint, SIGNAL(clicked()), this, SLOT(on_pb_SaveMajorPointClicked()));

	connect(ui.tw_SpmeParamList, &QTableWidget::currentCellChanged, this, &QWndSPME::on_ParamTableSeletedChanged);
}

QWndSPME::~QWndSPME()
{}

void QWndSPME::InitTable()
{
	QStringList headerList = { "序号","X(mm)","Y(mm)","OOC Min","OOC Max","OOS Min","OOS Max"};
	ui.table_MarjorPoint->setColumnCount(headerList.size());
	ui.table_MarjorPoint->setHorizontalHeaderLabels(headerList);
	ui.table_MarjorPoint->setAlternatingRowColors(true); // 隔行变色
	ui.table_MarjorPoint->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.table_MarjorPoint->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.table_MarjorPoint->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.table_MarjorPoint->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.table_MarjorPoint->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	QHeaderView* header = ui.table_MarjorPoint->verticalHeader();
	header->setHidden(true);

	ui.table_MinorPoint->setColumnCount(headerList.size());
	ui.table_MinorPoint->setHorizontalHeaderLabels(headerList);
	ui.table_MinorPoint->setAlternatingRowColors(true); // 隔行变色
	ui.table_MinorPoint->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.table_MinorPoint->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.table_MinorPoint->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.table_MinorPoint->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.table_MinorPoint->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	header = ui.table_MinorPoint->verticalHeader();
	header->setHidden(true);

	for (int i = 0; i < headerList.size(); i++)
	{
		ui.table_MarjorPoint->setColumnWidth(i, 126);
		ui.table_MinorPoint->setColumnWidth(i, 126);
	}
}

void QWndSPME::loadConfig()
{
}

void QWndSPME::refreshSpmeParamListTable()
{
	QStringList headers;
	headers << "参数名称" << "更新时间" << "备注信息";
	ui.tw_SpmeParamList->setColumnCount(headers.size());
	ui.tw_SpmeParamList->setHorizontalHeaderLabels(headers);
	ui.tw_SpmeParamList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_SpmeParamList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_SpmeParamList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_SpmeParamList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_SpmeParamList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_SpmeParamList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_SpmeParamList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_SpmeParamList->verticalHeader()->hide();
	ui.tw_SpmeParamList->setRowCount(SpmeParamMgr::Instance()->Size());

	int i = 0;
	for (auto var : SpmeParamMgr::Instance()->FindAll())
	{
		SpmeParam* spme = dynamic_cast<SpmeParam*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(spme->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_SpmeParamList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(spme->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_SpmeParamList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(spme->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_SpmeParamList->setItem(i, 2, item2);
		i++;
	}
}

void QWndSPME::setStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(13,55,147);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_SpmePrmNew->setStyleSheet(style);
	ui.pb_SpmePrmEdit->setStyleSheet(style);
	ui.pb_SpmePrmDelete->setStyleSheet(style);
	ui.pb_SpmePrmSearch->setStyleSheet(style);
	ui.pb_ModifyBatch->setStyleSheet(style);
	ui.pb_SpmePrmSave->setStyleSheet(style);
	ui.btn_RecipeSet->setStyleSheet(style);
	ui.btn_AnalyzeSet->setStyleSheet(style);
	ui.btn_TestSpme->setStyleSheet(style);
	ui.btn_StartReference->setStyleSheet(style);
	ui.btn_StartOD->setStyleSheet(style);
	ui.btn_Analyze->setStyleSheet(style);
	ui.btn_ODReferencePos->setStyleSheet(style);
	ui.pb_ColorReferencePos->setStyleSheet(style);
	ui.pb_AddMinorPoint->setStyleSheet(style);
	ui.pb_DeleteMinorPoint->setStyleSheet(style);
	ui.pb_SaveMinorPoint->setStyleSheet(style);
	ui.pb_AddMajorPoint->setStyleSheet(style);
	ui.pb_DeleteMajorPoint->setStyleSheet(style);
	ui.pb_SaveMajorPoint->setStyleSheet(style);
}

void QWndSPME::refreshParam(SpmeParam* param)
{
	ui.le_SpmeRemark->setText(param->Remark);
	ui.le_SpmeName->setText(param->Key);
	ui.comboBox->setCurrentIndex((int)param->SpmeType);

	setMajorTableData(param->MajorPoints);


}

QVector<SpmeMeasPoint> QWndSPME::getMajorTableData()
{
	QVector<SpmeMeasPoint> data;
	// 遍历表格的每一行数据
	int kk = ui.table_MarjorPoint->rowCount();
	for (int row = 0; row < ui.table_MarjorPoint->rowCount(); row++)
	{
		SpmeMeasPoint info;
		int c = 1;
		// 读取每一列数据
		//info.Index = ui.table_MarjorPoint->item(row, c++)->text().toInt();
		info.X = ui.table_MarjorPoint->item(row, c++)->text().toDouble();
		info.Y = ui.table_MarjorPoint->item(row, c++)->text().toDouble();
		info.OOCMin = ui.table_MarjorPoint->item(row, c++)->text().toDouble();
		info.OOCMax = ui.table_MarjorPoint->item(row, c++)->text().toDouble();
		info.OOSMin = ui.table_MarjorPoint->item(row, c++)->text().toDouble();
		info.OOSMax = ui.table_MarjorPoint->item(row, c++)->text().toDouble();
		//info.measureType = ui.table_MarjorPoint->item(row, c++)->text();
		// 将数据添加到 QVector 中
		data.append(info);
	}
	return data;
}

void QWndSPME::setMajorTableData(QVector<SpmeMeasPoint>& data)
{
	// 清空现有表格数据
	for (int row = ui.table_MarjorPoint->rowCount() - 1; row >= 0; row--) 
	{
		ui.table_MarjorPoint->removeRow(row);
	}

	// 添加新的表格数据
	for (int i = 0; i < data.size(); ++i) {
		const SpmeMeasPoint& info = data.at(i);
		int r = ui.table_MarjorPoint->rowCount();
		ui.table_MarjorPoint->insertRow(r);
		int c = 0;
		QTableWidgetItem* item = new QTableWidgetItem(QString::number(r + 1)); // 行号从1开始
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		item = new QTableWidgetItem(QString::number(info.X));
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		item = new QTableWidgetItem(QString::number(info.Y));
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		item = new QTableWidgetItem(QString::number(info.OOCMin));
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		item = new QTableWidgetItem(QString::number(info.OOCMax));
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		item = new QTableWidgetItem(QString::number(info.OOSMin));
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		item = new QTableWidgetItem(QString::number(info.OOSMax));
		item->setTextAlignment(Qt::AlignCenter);
		ui.table_MarjorPoint->setItem(r, c++, item);

		//item = new QTableWidgetItem(info.measureType);
		//ui.tableWidget->setItem(r, c++, item);
	}
}

void QWndSPME::on_pb_SpmePrmNewClicked()
{
}

void QWndSPME::on_pb_SpmePrmEditClicked()
{
}

void QWndSPME::on_pb_SpmePrmDeleteClicked()
{
}

void QWndSPME::on_pb_SpmePrmSearchClicked()
{
}

void QWndSPME::on_pb_ModifyBatchClicked()
{
}

void QWndSPME::on_pb_SpmePrmSaveClicked()
{
	if (ui.le_SpmeName->text().isEmpty())
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未选中有效配方!");
		int ret = messageBox->exec();
		return;
	}
	SpmeParam* spmeParam = dynamic_cast<SpmeParam*>(SpmeParamMgr::Instance()->FindBy(ui.le_SpmeName->text()));
	if (!spmeParam)
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未找到配方!" + ui.le_SpmeName->text());
		int ret = messageBox->exec();
		return;
	}
	QVector<SpmeMeasPoint>datas = getMajorTableData();
	spmeParam->MajorPoints = datas;
	spmeParam->MajorPointCount = datas.size();
	spmeParam->Remark = ui.le_SpmeRemark->text();
	spmeParam->CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
	spmeParam->Key = ui.le_SpmeName->text();
	spmeParam->SpmeType = (eSpmeType)ui.comboBox->currentIndex();
	SpmeParamMgr::Instance()->Save("SpmeMgr.json");

	QWndMessageBox* messageBox = new QWndMessageBox("分光计参数保存成功!");
	int ret = messageBox->exec();
}

void QWndSPME::on_btn_RecipeSetClicked()
{
}

void QWndSPME::on_btn_AnalyzeSetClicked()
{
}

void QWndSPME::on_btn_StartReferenceClicked()
{
}

void QWndSPME::on_btn_StartODClicked()
{
}

void QWndSPME::on_btn_AnalyzeClicked()
{
}

void QWndSPME::on_btn_ODReferencePosClicked()
{
}

void QWndSPME::on_pb_ColorReferencePosClicked()
{
}

void QWndSPME::on_pb_AddMinorPointClicked()
{
}

void QWndSPME::on_pb_DeleteMinorPointClicked()
{
}

void QWndSPME::on_pb_SaveMinorPointClicked()
{
	on_pb_SpmePrmSaveClicked();
}

void QWndSPME::on_pb_AddMajorPointClicked()
{
	SpmeMeasPoint _SpmeInfo;
	int r = ui.table_MarjorPoint->rowCount();
	ui.table_MarjorPoint->insertRow(r);
	int c = 0;
	QTableWidgetItem* item = new QTableWidgetItem(QString::number(r + 1));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	item = new QTableWidgetItem(QString::number(_SpmeInfo.X));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	item = new QTableWidgetItem(QString::number(_SpmeInfo.Y));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	item = new QTableWidgetItem(QString::number(_SpmeInfo.OOCMin));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	item = new QTableWidgetItem(QString::number(_SpmeInfo.OOCMax));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	item = new QTableWidgetItem(QString::number(_SpmeInfo.OOSMin));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	item = new QTableWidgetItem(QString::number(_SpmeInfo.OOSMax));
	item->setTextAlignment(Qt::AlignCenter);
	ui.table_MarjorPoint->setItem(r, c++, item);

	//item = new QTableWidgetItem(_SpmeInfo.measureType);
	//ui.table_MarjorPoint->setItem(r, c++, item);
}

void QWndSPME::on_pb_DeleteMajorPointClicked()
{
	if (m_currentRow >= 1)
	{
		ui.table_MarjorPoint->removeRow(m_currentRow);
	}
}

void QWndSPME::on_pb_SaveMajorPointClicked()
{
	on_pb_SpmePrmSaveClicked();
}

void QWndSPME::on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tw_SpmeParamList->rowCount())
	{
		if (currentRow != previousRow)
		{
			m_currentRow = currentRow;
			QTableWidgetItem* item = ui.tw_SpmeParamList->item(currentRow, 0);
			QString Name = item->text();
			SpmeParam* spmeParam = dynamic_cast<SpmeParam*>(SpmeParamMgr::Instance()->FindBy(Name));
			refreshParam(spmeParam);
		}
	}
	else
	{

	}
	

}
