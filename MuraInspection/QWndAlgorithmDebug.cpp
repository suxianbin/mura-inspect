#include "QWndAlgorithmDebug.h"
#include "AlgorithmParam.h"
#include "AlgorithmMgr.h"
#include "DevLog.h"

#pragma execution_character_set("utf-8")

QWndAlgorithmDebug::QWndAlgorithmDebug(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.m_pGvVision->setContentsMargins(0, 0, 0, 0);
	InitPT3();
	setStyleSheet();
	InitTable();
	loadConfig();
	refreshAlgoParamListTable();
	connect(ui.pb_ExecSingle, SIGNAL(clicked()), this, SLOT(on_pb_ExecSingleClicked()));
}

QWndAlgorithmDebug::~QWndAlgorithmDebug()
{}

void QWndAlgorithmDebug::InitTable()
{
}

void QWndAlgorithmDebug::loadConfig()
{
}

void QWndAlgorithmDebug::refreshAlgoParamListTable()
{
	QStringList headers;
	headers << "参数名称" << "更新时间" << "备注信息";
	ui.tw_AlgoParamList->setColumnCount(headers.size());
	ui.tw_AlgoParamList->setHorizontalHeaderLabels(headers);
	ui.tw_AlgoParamList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_AlgoParamList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_AlgoParamList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_AlgoParamList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_AlgoParamList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_AlgoParamList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_AlgoParamList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_AlgoParamList->verticalHeader()->hide();
	ui.tw_AlgoParamList->setRowCount(AlgorithmMgr::Instance()->Size());

	int i = 0;
	for (auto var : AlgorithmMgr::Instance()->FindAll())
	{
		AlgorithmParam* spme = dynamic_cast<AlgorithmParam*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(spme->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_AlgoParamList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(spme->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_AlgoParamList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(spme->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_AlgoParamList->setItem(i, 2, item2);
		i++;
	}
}

void QWndAlgorithmDebug::InitPT3()
{
	QObject::connect(ui.m_pGvVision, &GvVision::sigCheckResult, this, &QWndAlgorithmDebug::slot_GvVision_CheckResult);
	QObject::connect(ui.m_pGvVision, &GvVision::sigProductId, this, &QWndAlgorithmDebug::slot_GvVision_sigProductId);
	QObject::connect(ui.m_pGvVision, &GvVision::sigExeCuteLog, this, &QWndAlgorithmDebug::slot_GvVision_sigExeCuteLog);

	QString path = QCoreApplication::applicationDirPath() + "/RecipeLibrary/RecipeFiles/CameraTestCCF/";
	bool ret = ui.m_pGvVision->LoadRecipe(path,"CameraTestCCF.tre",true);
	if (ret)
	{
		QStringList strlist;
		strlist << "GetDefectFeatures";
		ui.m_pGvVision->SetAlgType(strlist);
		bool bRect = ui.m_pGvVision->SetGetImage("#ImgShow_1_");
		int a = 0 + 9;
	}
	else
	{

	}
}

QList<QString> QWndAlgorithmDebug::GetRecipeFileList()
{
	//保存tre
	QString PathFile = QCoreApplication::applicationDirPath() + "/RecipeLibrary/RecipeFiles";
	QDir dir(PathFile);
	QStringList filter;//清单过滤器，可以获取过滤后所得到的文件夹下的文件信息列表，
	QFileInfoList list = dir.entryInfoList(QDir::Files | QDir::CaseSensitive | QDir::Dirs | QDir::NoDotAndDotDot);
	QList<QString> strList;
	dir.setSorting(QDir::DirsFirst);
	for (auto Item : list)//list可以是多个字符串的集合
	{
		if (Item.fileName().indexOf(".") == -1)//”.”是表示当前目录，“..”用于表示上一级目录。
		{
			strList.push_back(Item.fileName());
		}
	}
}



void QWndAlgorithmDebug::setStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(13,55,147);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_AlgoPrmNew->setStyleSheet(style);
	ui.pb_AlgoPrmEdit->setStyleSheet(style);
	ui.pb_AlgoPrmDelete->setStyleSheet(style);
	ui.pb_AlgoPrmSearch->setStyleSheet(style);
	ui.pb_ModifyBatch->setStyleSheet(style);
	ui.pb_AlgoPrmSave->setStyleSheet(style);
	ui.pb_ExecSingle->setStyleSheet(style);
	ui.pb_ExecContinue->setStyleSheet(style);
	ui.pb_StopExec->setStyleSheet(style);
}

void QWndAlgorithmDebug::on_pb_AlgoPrmNewClicked()
{
}

void QWndAlgorithmDebug::on_pb_AlgoPrmEditClicked()
{
}

void QWndAlgorithmDebug::on_pb_AlgoPrmDeleteClicked()
{
}

void QWndAlgorithmDebug::on_pb_AlgoPrmSearchClicked()
{
}

void QWndAlgorithmDebug::on_pb_ModifyBatchClicked()
{
}

void QWndAlgorithmDebug::on_pb_AlgoPrmSaveClicked()
{
}

void QWndAlgorithmDebug::on_pb_ExecSingleClicked()
{
	ui.m_pGvVision->RunTre(0);
	
}

void QWndAlgorithmDebug::on_pb_ExecContinueClicked()
{
}

void QWndAlgorithmDebug::on_pb_StopExecClicked()
{
}

void QWndAlgorithmDebug::on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
}

void QWndAlgorithmDebug::slot_GvVision_CheckResult(s_CheckResult rltdata)
{
	cv::Mat imagemat1;
	cv::Mat imagemat2;
	ImageData2Mat(rltdata.qImageShow[0], imagemat1);
	ImageData2Mat(rltdata.qImageShow[1], imagemat2);
	Glass::Instance()->Station1CombindImg = imagemat1.clone();
	Glass::Instance()->Station2CombindImg = imagemat2.clone();

	QVector<Defect> vecDefect;
	std::vector<s_DefectResult> vecDefectResult = rltdata.vecDefectResult;  //算子对应特征信息
	int defIndex = 0;
	//1.获取检测结果
	try {
		for (auto Item : vecDefectResult)
		{
			if (Item.vecExtendData.size() < 0)
				return;
			for (auto itemDefect : Item.vecExtendData)
			{

				Defect d;
				QString panelId = "Na";
				QString panelModel = "Na";

				d.defectId = defIndex++;
				d.DefectType = eDefectType::null;
				d.DefectLevel = eDefectLevel::null;
				d.type = "Na";

				d.size = findDoubleBySubstring(itemDefect.vecFeature_En, "Area");
				d.length = findDoubleBySubstring(itemDefect.vecFeature_En, "Height");
				d.width = findDoubleBySubstring(itemDefect.vecFeature_En, "Width");

				//中心坐标点
				QPointF centerPoint = getCentroid(itemDefect.Vec_Point2f);
				d.imgPosX = centerPoint.x();
				d.imgPosY = centerPoint.y();

				d.linearity = findDoubleBySubstring(itemDefect.vecFeature_En, "regionlinearity");
				//平均灰度
				d.meangraydiff2 = findDoubleBySubstring(itemDefect.vecFeature_En, "meangraydiff2");
				d.area2smallestRectRatio = findDoubleBySubstring(itemDefect.vecFeature_En, "area2smallestRectRatio");
				d.roundness = findDoubleBySubstring(itemDefect.vecFeature_En, "roundness");
				d.rect2Phi = findDoubleBySubstring(itemDefect.vecFeature_En, "rect2Phi");
				d.grayDeviation = findDoubleBySubstring(itemDefect.vecFeature_En, "grayDeviation");//灰度方差
				d.grayNeiRegionRingMean = findDoubleBySubstring(itemDefect.vecFeature_En, "grayNeiRegionRingMean"); //缺陷邻域背景平均灰度
				d.thicknessDev = findDoubleBySubstring(itemDefect.vecFeature_En, "thicknessDev"); //区域主方向宽度标准差
				d.grayMean = findDoubleBySubstring(itemDefect.vecFeature_En, "grayMean"); //平均灰度

				d.pts = itemDefect.Vec_Point2f;
				d.contours = itemDefect.Vec_Vec_Point2f;

				d.panelId = panelId;
				d.panelModel = panelModel;
				Glass::Instance()->Station1OrgDefect << d;
			}
		}
	}
	catch (const cv::Exception& ex) {
		// 处理异常，例如打印错误消息
		qDebug() << "缺陷结果获取异常：" << ex.what();
	}
	QVector<Defect>dft = Glass::Instance()->Station1OrgDefect;
	emit sig_acqCompleted();
}

void QWndAlgorithmDebug::slot_GvVision_sigProductId(int id)
{
}

void QWndAlgorithmDebug::slot_GvVision_sigExeCuteLog(QString log)
{
}

bool QWndAlgorithmDebug::ImageData2Mat(const QImage& ImageData, cv::Mat& MatImage)
{
	if (ImageData.isNull()) {
		qDebug() << "Error: Image data is null.";
		return false;
	}

	try
	{
		cv::Mat temp;
		QImage::Format format = ImageData.format();

		switch (format)
		{
		case QImage::Format_Indexed8:
		case QImage::Format_Grayscale8:
			temp = cv::Mat(ImageData.height(), ImageData.width(), CV_8UC1,
				(void*)ImageData.constBits(), ImageData.bytesPerLine());
			break;

		case QImage::Format_RGB888:
			temp = cv::Mat(ImageData.height(), ImageData.width(), CV_8UC3,
				(void*)ImageData.constBits(), ImageData.bytesPerLine());
			cv::cvtColor(temp, MatImage, cv::COLOR_RGB2BGR);
			return true;

		case QImage::Format_RGB32:
		case QImage::Format_ARGB32:
			temp = cv::Mat(ImageData.height(), ImageData.width(), CV_8UC4,
				(void*)ImageData.constBits(), ImageData.bytesPerLine());
			break;

		default:
			qDebug() << "Error: Unsupported image format.";
			return false;
		}

		temp.copyTo(MatImage);
		return true;
	}
	catch (const cv::Exception& e)
	{
		// 处理OpenCV异常
		qDebug() << "OpenCV Exception: " << e.what();
		return false;
	}
	catch (const std::exception& ex)
	{
		// 处理其他异常
		qDebug() << "Exception: " << ex.what();
		return false;
	}
}

double QWndAlgorithmDebug::findDoubleBySubstring(const std::vector<std::pair<QString, double>>& vec, const QString& searchString)
{
	for (const auto& pair : vec) {
		if (pair.first.contains(searchString)) {
			return pair.second;
		}
	}
	return -1; // 默认返回值为 -1
}

QPointF QWndAlgorithmDebug::getCentroid(std::vector<cv::Point2f> vecQPointF)
{
	qreal x = 0;
	qreal y = 0;
	for (auto& temp : vecQPointF)
	{
		x += temp.x;
		y += temp.y;
	}
	x = x / vecQPointF.size();
	y = y / vecQPointF.size();
	return QPointF(x, y);
}
