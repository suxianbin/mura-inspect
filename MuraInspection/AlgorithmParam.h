#pragma once
#include "Base.h"

enum class eAlgType
{
	null,

};

class AlgorithmParam : public Base
{
public:
	AlgorithmParam(QString key);
	~AlgorithmParam() override;
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;

	QString CreateTime = "";
	QString Remark = "";
	QString AlgoName;
	QString AlgoPath;
	eAlgType AlgoType = eAlgType::null;

};

