#include "MyMenu.h"
#include <QIcon>

MyMenu::MyMenu(QWidget *parent)
	: QMenu(parent)
{
	//setAttribute(Qt::WA_TranslucentBackground);
}

MyMenu::~MyMenu()
{
}

void MyMenu::addCustomMenu(const QString& qsName, const QString& icon, const QString& zhName)
{
	QAction *pAction = addAction(QIcon(icon), zhName);
	m_menuActionMap.insert(qsName, pAction);
}

void MyMenu::addMyAction(QAction& action,const QString zhName)
{
	m_menuActionMap.insert(zhName, &action);
	this->insertAction(nullptr, &action);
}

QAction* MyMenu::getAction(const QString& qsName)
{
	return m_menuActionMap[qsName];
}