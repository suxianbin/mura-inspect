#pragma once
#include "EntityMgr.h"
#include "PanelModel.h"

class PanelModelMgr : public EntityMgr
{
	//�����ṹ
	PanelModelMgr();
	~PanelModelMgr();
public:
	static PanelModelMgr* Instance()
	{
		static PanelModelMgr instance;
		return &instance;
	}
	bool Load(std::string name) override;
	bool Save(std::string name) override;
};

