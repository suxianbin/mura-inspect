#pragma once

#include <QWidget>
#include "ui_QWndSpmeResult.h"
//#include "SPMETestFinishedEvent.h"
//#include "EventCenter.h"

class QWndSpmeResult : public QWidget
{
	Q_OBJECT

public:
	QWndSpmeResult(QWidget *parent = nullptr);
	~QWndSpmeResult();
protected:
	bool event(QEvent* ev) override;
private:
	Ui::QWndSpmeResultClass ui;

	void InitTable();
};
