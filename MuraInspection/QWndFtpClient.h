#pragma once

#include <QWidget>
#include "ui_QWndFtpClient.h"
#include "FtpClient.h"

class QWndFtpClient : public QWidget
{
	Q_OBJECT

public:
	QWndFtpClient(QWidget* parent = nullptr);
	~QWndFtpClient();

private:
	Ui::QWndFtpClientClass ui;

	FtpClient* client;

private slots:
	void onClientSig(eFtpResult val);
	void on_doneSignal(bool);
	void on_connectClicked();
	void on_disconnectClicked();
	void on_CDClicked();
	void on_CDParentClicked();
	void on_ListClicked();
	void on_UploadClicked();
	void on_donwloadClicked();
	void on_MKDirClicked();
	void on_deleteFileCliced();
	void on_deleteDirClicked();
	void on_CreateDirClicked();
	void on_ExistPathClicked();
};
