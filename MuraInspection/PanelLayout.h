#pragma once
#include <QString>
#include <QPointF>
#include <QVector>
#include <QList>
#include <QJsonObject>

// 屏蔽区域类型
enum class eMaskType
{
	Circle,
	Polygon,
	Rectangle
};

struct PolygonVertex 
{
	QVector<QPointF> vertices; // 多边形的顶点
	QVector<double>  vecRadius; //多边形圆角
	void clear()
	{
		vertices.clear();
		vecRadius.clear();
	}
};

struct PanelMask
{
	void write(QJsonObject& json) const
	{
		json["type"] = (int)type;
		json["x"] = x;
		json["y"] = y;
		json["width"] = width;
		json["height"] = height;
		json["radius"] = radius;
	}
	void read(const QJsonObject& json)
	{
		type = (eMaskType)json["type"].toInt() ;
		x = json["x"].toDouble();
		y = json["y"].toDouble();
		width = json["width"].toDouble();
		height = json["height"].toDouble();
		radius = json["radius"].toDouble();
	}
	eMaskType type = eMaskType::Circle;
	double x = 0;          // 中心点 x 坐标（用于矩形和圆形）
	double y = 0;          // 中心点 y 坐标（用于矩形和圆形）
	double width = 0;      // 宽度（用于矩形）
	double height = 0;     // 高度（用于矩形）
	double radius = 0;     // 半径（用于圆形）
	PolygonVertex polygon; // 多边形（用于多边形屏蔽区域）
};

typedef QList<PanelMask> PanelMaskList;
class PanelLayout
{
public:
	void write(QJsonObject& json) const;
	void read(const QJsonObject& json);

	int serial = 0;					//id序号
	QString panelName;				//panel名称
	QString panelModel;				//panel类型
	double x = 0;				    //x 图像值mm
	double y = 0;				    //y 图像值mm
	double width = 0;			    //宽 图像值mm
	double height = 0;			    //长 图像值mm
	double contract = 0;			//内缩图像值mm
	double radius = 0;
	double equivalent = 0.1;
	//内缩尺寸
	double topDst = 0;
	double botDst = 0;
	double leftDst = 0;
	double rightDst = 0;
	PanelMaskList panelMaskList;
};

