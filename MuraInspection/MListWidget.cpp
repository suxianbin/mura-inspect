#include "MListWidget.h"
#include <QApplication>

MListWidget::MListWidget(QWidget *parent)
	: QListWidget(parent)
{
	m_vscrollBar = verticalScrollBar(); // 保持垂直滚动条
	
}

MListWidget::~MListWidget()
{

}

void MListWidget::wheelEvent(QWheelEvent *event)
{
	// 检测 Ctrl 键状态
	if (QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
		event->ignore();
		return;
	}
	int para = event->angleDelta().y();//获得鼠标滚轮的滚动距离para，para<0向下滚动，>0向上滚动  

	if (para<0)
	{
		emit SignalSliderDown();
		//向下滚动，设定鼠标滚轮每滚动一个单位，滑块就移动20个单位  
		//加入此判断的理由是，若当时滑块处于90的位置上，一旦继续下滑20个单位，就会超过最大范围100，show界面就会移动超出范围。  
		if (m_vscrollBar->value() + 20 <= 100)
		{
			//发射verticalScrollBar的信号valueChange（value+20），连接到了我们之前写的slot_ScrollWidget(int)槽上  
			emit m_vscrollBar->valueChanged(m_vscrollBar->value() + 20);
			//设定滚动之后的滑块位置  
			m_vscrollBar->setSliderPosition(m_vscrollBar->value() + 20);
		}
		else
		{
			emit m_vscrollBar->valueChanged(m_vscrollBar->maximum());
			m_vscrollBar->setSliderPosition(m_vscrollBar->maximum());
		}


	}
	else
	{
		//向上滚动  

		if (m_vscrollBar->value() - 20 >= 0)
		{
			emit m_vscrollBar->valueChanged(m_vscrollBar->value() - 20);
			m_vscrollBar->setSliderPosition(m_vscrollBar->value() - 20);
		}
		else
		{
			emit m_vscrollBar->valueChanged(m_vscrollBar->minimum());
			m_vscrollBar->setSliderPosition(m_vscrollBar->minimum());
		}
		emit SignalSliderUp();

	}

}