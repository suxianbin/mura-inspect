#include "RecipeInfo.h"

RecipeInfo::RecipeInfo(QString key) : Base(key)
{
	RecipeName = Key;
}

RecipeInfo::~RecipeInfo()
{
}

void RecipeInfo::write(QJsonObject& json) const
{
	Base::write(json);
	json["CreateTime"] = CreateTime;
	json["Remark"] = Remark;
	json["RecipeName"] = RecipeName;
	json["RecipeType"] = RecipeType;
	json["ProcessId"] = ProcessId;
	json["MachineStepID"] = MachineStepID;
	json["AlgorithmName"] = AlgorithmName;
	json["GlassLayoutName"] = GlassLayoutName;
	json["AxisInfoName"] = AxisInfoName;
	json["LightInfoName"] = LightInfoName;
	json["SpmeInfoName"] = SpmeInfoName;
	json["CameraInfoName"] = CameraInfoName;	
}

void RecipeInfo::read(const QJsonObject& json)
{
	Base::read(json);
	CreateTime = json["CreateTime"].toString();
	Remark = json["Remark"].toString();
	RecipeName = json["RecipeName"].toString();
	RecipeType = json["RecipeType"].toString();
	ProcessId = json["ProcessId"].toInt();
	MachineStepID = json["MachineStepID"].toString();
	AlgorithmName = json["AlgorithmName"].toString();
	GlassLayoutName = json["GlassLayoutName"].toString();
	AxisInfoName = json["AxisInfoName"].toString();
	LightInfoName = json["LightInfoName"].toString();
	SpmeInfoName = json["SpmeInfoName"].toString();
	CameraInfoName = json["CameraInfoName"].toString();
}



