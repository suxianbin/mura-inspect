#pragma once
#include "CameraParam.h"
#include "EntityMgr.h"

class CameraParamMgr : public EntityMgr
{
	//�����ṹ
	CameraParamMgr();
	~CameraParamMgr();
public:
	static CameraParamMgr* Instance()
	{
		static CameraParamMgr instance;
		return &instance;
	}
	bool Load(std::string name) override;
	bool Save(std::string name) override;
};

