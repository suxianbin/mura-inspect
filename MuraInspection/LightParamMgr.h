#pragma once
#include "LightParam.h"
#include "EntityMgr.h"

class LightParamMgr : public EntityMgr
{
	//�����ṹ
	LightParamMgr();
	~LightParamMgr();
public:
	static LightParamMgr* Instance()
	{
		static LightParamMgr instance;
		return &instance;
	}
	bool Load(std::string name) override;
	bool Save(std::string name) override;
};

