#pragma once

#include <QWidget>
#include "ui_QWndDefectClassification.h"

class QWndDefectClassification : public QWidget
{
	Q_OBJECT

public:
	QWndDefectClassification(QWidget *parent = nullptr);
	~QWndDefectClassification();

private:
	Ui::QWndDefectClassificationClass ui;
};
