﻿#include "QTableViewDelegate.h"
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QLineEdit>
#include <QApplication>

#pragma execution_character_set("utf-8")

QTableViewDelegate::QTableViewDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{
	
}

QTableViewDelegate::~QTableViewDelegate()
{
}

void QTableViewDelegate::setEditor(eColumnType eType, bool bEdit)
{
	m_eType    = eType;
	m_bEditEna = bEdit;
}

void QTableViewDelegate::setStringList(const QStringList &strList)
{
	m_strList = strList;
}

void QTableViewDelegate::setTextAlignment(Qt::AlignmentFlag eAlign)
{
	m_eAlign = eAlign;
}

QWidget *QTableViewDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (!m_bEditEna) { return nullptr; }

	if (eColumnType::ComboBox == m_eType)
	{
		QComboBox *pCombo = new QComboBox(parent);
		pCombo->addItems(m_strList);
		return pCombo;
	}

	if (eColumnType::Button == m_eType)
	{
		QPushButton *pButton = new QPushButton(parent);
		pButton->setText(m_strList[0]);
		return pButton;
	}

	if (eColumnType::CheckBox == m_eType)
	{
		QCheckBox *pCheckBox = new QCheckBox(parent);
		return pCheckBox;
	}

	if (eColumnType::Color == m_eType)
	{
		//return pCheckBox;
	}

	return QStyledItemDelegate::createEditor(parent, option, index);
}

void QTableViewDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (!m_bEditEna) { return; }
	
	if (eColumnType::NormalEdit == m_eType)
	{
		QLineEdit *pEditor = qobject_cast<QLineEdit *>(editor);
		pEditor->setText(index.data().toString());
	}
	else if (eColumnType::ComboBox == m_eType)
	{
		QComboBox *pCombo = qobject_cast<QComboBox *>(editor);
		pCombo->setCurrentText(index.data().toString());
	}
	else if (eColumnType::Button == m_eType)
	{
		QPushButton *pBtn = qobject_cast<QPushButton *>(editor);
		pBtn->setText(m_strList[0]);
	}
	else if (eColumnType::CheckBox == m_eType)
	{
		//QPushButton *pBtn = new QPushButton(parent);
		//pBtn->setText(m_strList[0]);
	}
	else if (eColumnType::Color == m_eType)
	{
		QLineEdit *pEditor = qobject_cast<QLineEdit *>(editor);
		pEditor->setText("Test");
	}
}

void QTableViewDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyleOptionViewItem temp(option);
	temp.displayAlignment = m_eAlign;

	QStyledItemDelegate::paint(painter, temp, index);
}



//QDelegateCheckBox

QDelegateCheckBox::QDelegateCheckBox(QObject *parent)
	: QStyledItemDelegate(parent)
{

}

QDelegateCheckBox::~QDelegateCheckBox()
{
}

void QDelegateCheckBox::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyleOptionButton cbOption;
	QRect rc = QApplication::style()->subElementRect(QStyle::SE_CheckBoxIndicator, &cbOption);

	cbOption.rect = option.rect;
	cbOption.rect.setLeft(option.rect.left() + (option.rect.width() - rc.width()) / 2);
	cbOption.rect.setTop(option.rect.top() + (option.rect.height() - rc.height()) / 4);
	cbOption.state = index.data(Qt::UserRole).toInt() ? QStyle::State_On : QStyle::State_Off;
	cbOption.state |= QStyle::State_Enabled;

	QApplication::style()->drawControl(QStyle::CE_CheckBox, &cbOption, painter);
}

bool QDelegateCheckBox::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
	if (event->type() == QEvent::MouseButtonDblClick) { return true; } //禁止双击编辑	

	//过滤鼠标松开
	if (event->type() == QEvent::MouseButtonRelease) { return false; }

	bool checked = index.data(Qt::UserRole).toBool();
	model->setData(index, !checked, Qt::UserRole);

	return QStyledItemDelegate::editorEvent(event, model, option, index);
}