#pragma once
#include <QWidget>
#include "ui_QWndDefectTable.h"
#include <QStandardItemModel>
#include "Defect.h"

enum class TABLE_TYPE
{
	ScreenOutTable = 0,
	OrgDefectTable,
};

class QWndDefectTable : public QWidget
{
	Q_OBJECT

public:
	QWndDefectTable(QWidget *parent = nullptr, TABLE_TYPE type = TABLE_TYPE::ScreenOutTable);
	~QWndDefectTable();

	void updateTable(QVector<Defect>& defects);
signals:
	void DefectSelected(int row);
private:
	Ui::QWndDefectTableClass ui;

	QStandardItemModel* m_pModelCheckInfo = nullptr;

	void initTable(TABLE_TYPE type);

	//QVector<DefectRlt> m_showDefects;

	TABLE_TYPE m_type;
private slots:
	void on_currentRowSelected(int currentRow, int currentColumn, int previousRow, int previousColumn);
};
