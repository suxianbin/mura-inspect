#include "FtpClient.h"
#include <QFile>
#include "DevLog.h"
#include <QDebug>
#include <QThread>
#include <QTimer>

FtpClient::FtpClient()
{
	ftp = new QFtp(this);
	connect(ftp, &QFtp::commandFinished, this, &FtpClient::ftpCommandFinished);
	connect(ftp, &QFtp::listInfo, this, &FtpClient::addToList);
	//connect(ftp, &QFtp::done, this, &FtpClient::doFinished);
	connect(ftp, &QFtp::stateChanged, this, &FtpClient::ftpStateChanged);
	connect(ftp, &QFtp::dataTransferProgress, this, &FtpClient::updateDataTransferProgress);
}

FtpClient::~FtpClient()
{
}

bool FtpClient::connectServer(QString ip, QString user, QString pwd)
{
	m_ip = ip;
	m_userName = user;
	m_pwd = pwd;

	return connectServer();
}

void FtpClient::disconect()
{
	ftp->close();
}

void FtpClient::cdToParent()
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
	if (currentPath.isEmpty() || currentPath == "/") {
		return;
	}
	else {
		ftp->cd("../");
		tmpCdPath = "../";
	}
}

bool FtpClient::cd(QString path)
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
	ExecSuccess = false;
	ftp->cd(path);
	QEventLoop eventloop;
	connect(ftp, SIGNAL(done()), &eventloop, SLOT(quit()));
	QTimer::singleShot(100, &eventloop, &QEventLoop::quit);
	eventloop.exec();
	return ExecSuccess;
}

bool FtpClient::uploadFile(QString filePath, QString remotePath, QString remotefileName)
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
	if (!QFile::exists(filePath))
	{
		glog << "FtpClient Intent Upload File not exist:" << filePath << gendl;
		//FtpClientSig(eFtpResult::UPLOADFILENOTEXIST);
		return false;
	}
	if (!existMulDir(remotePath))
	{
		if (!mkDir(remotePath))
		{
			glog << "uploadFile: Romote Path Create Fail!" << gendl;
			return false;
		}
	}
	file = new QFile(filePath);
	if (!file->open(QIODevice::ReadWrite))
	{
		delete file;
		glog << "uploadFile: Open File Fail!" << gendl;
		return false;
	}
	ExecSuccess = false;
	ftp->put(file, remotePath + '/' + remotefileName);
	QEventLoop eventloop;
	connect(ftp, SIGNAL(done()), &eventloop, SLOT(quit()));
	QTimer::singleShot(10000, &eventloop, &QEventLoop::quit);
	eventloop.exec();
	return ExecSuccess;
}

bool FtpClient::downloadFile(QString fileName, QString localPath)
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		if (!connectServer())
		{
			return false;
		}
	}
	if (QFile::exists(fileName))
	{
		return false;
	}
	file = new QFile(fileName);
	if (!file->open(QIODevice::ReadWrite))
	{
		delete file;
		return false;
	}
	ftp->get(fileName, file);
	return true;
}

void FtpClient::ftpCommandFinished(int commandId, bool error)
{
	if (ftp->currentCommand() == QFtp::ConnectToHost)
	{
		lastCommand = "ConnectToHost";
		if (error)
		{
			glog << "Connect to Host Error:"<< m_ip << gendl;
			ExecSuccess = false;
			return;
		}
		return;
	}
	if (ftp->currentCommand() == QFtp::Login)
	{
		lastCommand = "Login";
		qDebug() << "Login";
		currentPath = '/';
		if (error)
		{
			glog << "Login to Host Error:" << m_userName << "," << m_pwd << gendl;
		}
		FtpClientSig(eFtpResult::CONNECT_SUCCESS);
		ExecSuccess = true;
	}

	if (ftp->currentCommand() == QFtp::Get)
	{
		lastCommand = "Get";
		if (error)
		{
			file->close();
			file->remove();
			FtpClientSig(eFtpResult::DOWNLOAD_FAIL);
		}
		else
		{
			file->close();
			FtpClientSig(eFtpResult::DOWNLOAD_SUCCESS);
		}
		delete file;

	}
	if (ftp->currentCommand() == QFtp::Put)
	{
		lastCommand = "Put";
		if (error)
		{
			if (file->isOpen())
			{
				file->close();
			}
			ExecSuccess = false;
			//FtpClientSig(eFtpResult::UPLOAD_FAIL);
		}
		else
		{
			ExecSuccess = true;
			//file->close();
			//FtpClientSig(eFtpResult::UPLOAD_SUCCESS);
		}
		//delete file;
	}
	else if (ftp->currentCommand() == QFtp::List)
	{
		qDebug() << "List Command";
		lastCommand = "List";
		FtpClientSig(eFtpResult::LISTFINISHED);

		qDebug() << "commandFinished List=" << ftp->errorString() << ftp->error();
		if (ftp->error() == 0)
		{
		}
	}
	if (ftp->currentCommand() == QFtp::Mkdir)
	{
		lastCommand = "Mkdir";
		if (error)
		{
			ExecSuccess = false;
			qDebug() << "Make Dir Fail!";
			return;
		}
		ExecSuccess = true;
		return;
	}
	if (ftp->currentCommand() == QFtp::Cd)
	{
		lastCommand = "Cd";
		if (error)
		{
			ExecSuccess = false;
			qDebug() << "cd dir fail!";
			return;
		}
		else
		{
			qDebug() << "cd dir success!" << "Last Command:";
			ExecSuccess = true;
			if (tmpCdPath == "../")
			{
				currentPath = currentPath.left(currentPath.lastIndexOf('/'));
			}
			else
			{
				if (tmpCdPath.startsWith('/'))
				{
					currentPath.clear();
					currentPath = tmpCdPath;
				}
				else
				{
					currentPath = currentPath + "/" + tmpCdPath;
					currentPath = currentPath.replace("//", "/");
					currentPath = currentPath.replace("///", "/");
				}
			}
		}
	}
}

void FtpClient::addToList(const QUrlInfo& urlInfo)
{
	qDebug() << "current Command:" << ftp->currentCommand();
	qDebug() << "add url:" << urlInfo.name();
	isDirectory[urlInfo.name()] = urlInfo.isDir();
	fileList.insert(std::make_pair(urlInfo.name(), urlInfo.isDir()));
}

//void FtpClient::doFinished()
//{
//	qDebug() << "Do Finished..."  << "current command:" << ftp->currentCommand();
//	qDebug() << "Current Path:" << currentPath << "Last Command:" << lastCommand;
//	if (lastCommand == "List")
//	{
//		loop.quit();
//	}
//	else if(lastCommand == "Get")
//	{
//		//ExecSuccess = !error;
//		loop.quit();
//	}
//	else if (lastCommand == "Put")
//	{
//		//ExecSuccess = !error;
//		loop.quit();
//	}
//	else if (lastCommand == "Cd")
//	{
//		//ExecSuccess = !error;
//		loop.quit();
//	}
//	else if (lastCommand == "Mkdir")
//	{
//		//ExecSuccess = !error;
//		loop.quit();
//	}
//	emit doneSignal();
//}

void FtpClient::ftpStateChanged(int)
{

}

bool FtpClient::connectServer()
{
	currentPath.clear();
	isDirectory.clear();
	int rtn = ftp->connectToHost(m_ip, 21);
	rtn = ftp->login(m_userName, m_pwd);
	ExecSuccess = false;
	QEventLoop eventloop;
	connect(ftp, SIGNAL(done()), &eventloop, SLOT(quit()));
	QTimer::singleShot(1000, &eventloop, &QEventLoop::quit);
	eventloop.exec();
	return ExecSuccess;
}

void FtpClient::deleteFile(QString filename)
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
}

void FtpClient::deleteDir(QString dir)
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
}

void FtpClient::deleteMulDir()
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
}

bool FtpClient::mkDir(QString dir)
{
	if (ftp->state() == QFtp::State::Unconnected)
	{
		connectServer();
	}
	ExecSuccess = false;
	ftp->mkdir(dir);
	QEventLoop eventloop;
	connect(ftp, SIGNAL(done()), &eventloop, SLOT(exit()));
	QTimer::singleShot(100, &eventloop, &QEventLoop::quit);
	eventloop.exec();
	return ExecSuccess;
}

//bool FtpClient::existDir(QString dir)
//{
//	fileList = listFile();
//	for each(auto var in fileList)
//	{
//		if (dir == var.first)
//		{
//			return true;
//		}
//	}
//	return false;
//}

bool FtpClient::existMulDir(QString mulDir)
{
	return cd(mulDir);
}

void FtpClient::updateDataTransferProgress(qint64 readBytes, qint64 totalBytes)
{
}

std::map<QString, bool> FtpClient::listFile()
{
	fileList.clear();
	ftp->list();
	ExecSuccess = false;
	QEventLoop eventloop;
	connect(ftp, SIGNAL(done()), &eventloop, SLOT(quit()));
	QTimer::singleShot(1000, &eventloop, &QEventLoop::quit);
	eventloop.exec();
	return fileList;
}

QFtp::State FtpClient::State()
{
	return ftp->state();
}