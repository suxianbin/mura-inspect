#include "InteractiveUtil.h"
#include "CommandData.h"
#include "HardwareConfig.h"

InteractiveUtil::InteractiveUtil(QObject*parrent)
{
	PLC_Address = HardwareConfig::Instance()->plcCommonAddress;
	PC_Address = HardwareConfig::Instance()->pcCommonAddress;
}

InteractiveUtil::~InteractiveUtil()
{
}

bool InteractiveUtil::readPlcCommandData()
{
	if (state() == QAbstractSocket::ConnectedState)
	{
		int rlt = 0;
		QByteArray data;
		{
			rlt = mc_read_registers(PLC_Address, 100, data);
		}
		if (rlt != 1) 
		{
			return false;
		}
		if (ArrayToVec(data, *CommandData::getPLCommand()))
		{
			return true;
		}
	}
	return false;
}

bool InteractiveUtil::sendPcCommandData()
{
	if (state() == QAbstractSocket::ConnectedState)
	{
		QByteArray data;
		if (VecToArray(*CommandData::getPCommand(), data)) {
			int rlt = 0;
			rlt = mc_write_registers(PC_Address, data, true);
			if (rlt != 1) 
			{
				qDebug() << "����PC����ToPlcʧ�ܣ�";
			}
			else
			{
				return true;
			}
		}
		else {
			qDebug() << "����PC����ת��ʧ�ܣ�";
		}
	}
	return false;
}

bool InteractiveUtil::readPLCJobData(JobData& jobdata)
{
	if (state() != QAbstractSocket::ConnectedState)
	{
		return false;
	}
	else {
		QByteArray data;
		int rlt = 0;
		rlt = mc_read_registers(PLC_Address + 100, 128, data, true);
		if (rlt == 1) 
		{
			ArrayToJobData(data, jobdata);
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

bool InteractiveUtil::sendPCJobData(JobData& jobdata)
{
	if (state() != QAbstractSocket::ConnectedState) 
	{
		qCritical() << "����jobDataʧ�ܣ���������";
		return false;
	}
	else 
	{
		QByteArray data;
		JobDataToArray(jobdata, data);
		int rlt = 0;
		rlt = mc_write_registers(PC_Address + 100, data, true);
		if (rlt == 1) {
			qInfo() << "����jobData�ɹ�����������";
			return true;
		}
		else {
			qCritical() << "����jobDataʧ�ܣ���������";
			return false;
		}
	}
}

bool InteractiveUtil::readPLCCurrentPos(double& x, double& y)
{
	if (state() != QAbstractSocket::ConnectedState)
	{
		return false;
	}
	else 
	{
		QByteArray data;
		int rlt = 0;
		rlt = mc_read_registers(PLC_Address + 450, 8, data, true);
		if (rlt == 1) {
			int val_x = 0;
			int val_y = 0;
			Bytes2Int(data, 0, val_x);
			Bytes2Int(data.mid(4, 4), 4, val_y);
			x = val_x / 1000.0;
			y = val_y / 1000.0;
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

int InteractiveUtil::readPLCLocalRecipeId(bool& f)
{
    return 0;
}

bool InteractiveUtil::sendPCLocalRecipeId(int& nLocalRecipe)
{
	if (state() != QAbstractSocket::ConnectedState) 
	{
		qCritical() << "����recipeIdʧ�ܣ���������";
		return false;
	}
	else
	{
		int rlt = 0;

		QByteArray data;
		data.append(UShort2Bytes(nLocalRecipe));

		rlt = mc_write_registers(PC_Address + 230, data, true);
		if (rlt == 1) 
		{
			return true;
		}
		else
		{
			qCritical() << "����recipeIdʧ�ܣ���������";
			return false;
		}
	}
}

bool InteractiveUtil::sendPCRecipeIdState(int& nState)
{
    return false;
}

bool InteractiveUtil::sendRecipeList(QVector<QString>& vecRecipeList)
{
	int rlt = 0;
	if (state() != QAbstractSocket::ConnectedState)
	{
		qCritical() << "����RecipeListʧ�ܣ���������";
		return false;
	}
	else {
		QByteArray data;
		RecipeListDataToArray(vecRecipeList, data);
		rlt = mc_write_registers(PC_Address + 200, data, true);
		if (rlt == 1) 
		{
			return true;
		}
		else {
			qCritical() << "����RecipeListʧ�ܣ���������";
			return false;
		}
	}
}

bool InteractiveUtil::sendSpmeMeasurePoints(QVector<QPointF>& vecMeasurePoint)
{
	int rlt = 0;
	if (state() != QAbstractSocket::ConnectedState)
	{
		qCritical() << "����SpmeMeasurePointsʧ�ܣ�PLCδ����";
		return false;
	}
	else
	{
		QByteArray data;
		MeasurePointListToArray(vecMeasurePoint, data);
		rlt = mc_write_registers(PC_Address + 1000, data, true);
		if (rlt == 1) {
			//qInfo() << "����SpmeMeasurePoints�ɹ�����������";
			return true;
		}
		else {
			qCritical() << "����SpmeMeasurePointsʧ�ܣ���������";
			return false;
		}
	}
}

int InteractiveUtil::readPCAlarmStatus(bool& nSate)
{
	return false;
}

bool InteractiveUtil::sendPCAlarmStatus(int& nSate)
{
	if (state() != QAbstractSocket::ConnectedState)
	{
		qCritical() << "PC ����״̬ʧ�ܣ���������";
		return false;
	}
	else
	{
		int rlt = 0;
		QByteArray data;
		data.append(UShort2Bytes(nSate));
		rlt = mc_write_registers(PC_Address, data, true);
		if (rlt == 1)
		{
			return true;
		}
		else {
			qCritical() << "PC ����״̬ʧ�ܣ���������";
			return false;
		}
	}
}

bool InteractiveUtil::sendPCPRUNDelay(int& nSate)
{
    return false;
}


bool InteractiveUtil::VecToArray(const QVector<quint16>& srcData, QByteArray& disData)
{
	disData.clear();
	for (int i = 0; i < srcData.size(); i++)
	{
		disData.append(UShort2Bytes(srcData[i]));
	}
	return true;
}

bool InteractiveUtil::ArrayToVec(const QByteArray& srcData, QVector<quint16>& disData)
{
	QByteArray bytes;
	for (int i = 0; i < disData.size(); i++) {
		if (i * 2 < srcData.size()) {
			quint16 i16 = 0;
			Bytes2UShort(srcData, i * 2, i16);
			disData[i] = i16;
		}
	}
	return true;
}

bool InteractiveUtil::ArrayToJobData(const QByteArray& srcData, JobData& disData)
{
	bool f = true;
	int p = 0;
	disData.PRODID = srcData.mid(p, 10);
	p += 20;
	disData.OperID = srcData.mid(p, 5);
	p += 10;
	disData.LotID = srcData.mid(p, 10);
	p += 20;
	disData.PPID01_1 = srcData.mid(p, 10);
	p += 104;
	f = f & Bytes2UShort(srcData, p, disData.JobType);
	p += 2;
	disData.JobID = srcData.mid(p, 10);
	p += 20;
	f = f & Bytes2UShort(srcData, p, disData.LotSequenceNumber);
	p += 2;

	f = f & Bytes2UShort(srcData, p, disData.SlotSequenceNumber);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.PropertyCode);
	p += 2;
	disData.JobJudgeCode = srcData.mid(p, 2);
	p += 2;
	disData.JobGradeCode = srcData.mid(p, 2);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.SubstrateType);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.ProcessingFlag);
	p += 8;
	f = f & Bytes2UShort(srcData, p, disData.InspectionFlag);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.SkipFlag);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.JobSize);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.GlassThickness);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.JobAngle);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.JobFlip);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.CuttingGlassType);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.InspectionJudgeData);
	p += 6;
	f = f & Bytes2UShort(srcData, p, disData.PairLotSequenceNumber);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.PairSlotSequenceNumber);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.PortNumber);
	f = f & Bytes2UShort(srcData, p, disData.InputMode);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.EVRouteRecipe);
	p += 2;
	f = f & Bytes2UShort(srcData, p, disData.ENRoutePathInfo);
	p += 4;
	f = f & Bytes2UShort(srcData, p, disData.JobChangeflag);
	p += 2;
	disData.OptionValue = srcData.mid(p, 10);
	p += 20;
	disData.Reserved = srcData.mid(p, 1);
	p += 2;
	return f;
}

bool InteractiveUtil::JobDataToArray(const JobData& srcData, QByteArray& disData)
{
	disData.append(QString2Bytes(srcData.PRODID, 20));
	disData.append(QString2Bytes(srcData.OperID, 10));
	disData.append(QString2Bytes(srcData.LotID, 20));
	disData.append(QString2Bytes(srcData.PPID01_1, 104));
	disData.append(UShort2Bytes(srcData.JobType));
	disData.append(QString2Bytes(srcData.JobID, 20));
	disData.append(UShort2Bytes(srcData.LotSequenceNumber));
	disData.append(UShort2Bytes(srcData.SlotSequenceNumber));
	disData.append(UShort2Bytes(srcData.PropertyCode));
	disData.append(QString2Bytes(srcData.JobJudgeCode, 2));
	disData.append(QString2Bytes(srcData.JobGradeCode, 2));
	disData.append(UShort2Bytes(srcData.SubstrateType));
	disData.append(UShort2Bytes(srcData.ProcessingFlag));
	disData.append(UShort2Bytes(0));
	disData.append(UShort2Bytes(0));
	disData.append(UShort2Bytes(0));
	disData.append(UShort2Bytes(srcData.InspectionFlag));
	disData.append(UShort2Bytes(srcData.SkipFlag));
	disData.append(UShort2Bytes(srcData.JobSize));
	disData.append(UShort2Bytes(srcData.GlassThickness));
	disData.append(UShort2Bytes(srcData.JobAngle));
	disData.append(UShort2Bytes(srcData.JobFlip));
	disData.append(UShort2Bytes(srcData.CuttingGlassType));
	disData.append(UShort2Bytes(srcData.InspectionJudgeData));
	disData.append(UShort2Bytes(0));
	disData.append(UShort2Bytes(0));
	disData.append(UShort2Bytes(srcData.PairLotSequenceNumber));
	disData.append(UShort2Bytes(srcData.PairSlotSequenceNumber));
	disData.append(UShort2Bytes(srcData.PortNumber));
	disData.append(UShort2Bytes(srcData.InputMode));
	disData.append(UShort2Bytes(srcData.EVRouteRecipe));
	disData.append(UShort2Bytes(srcData.ENRoutePathInfo));
	disData.append(UShort2Bytes(srcData.JobChangeflag));
	disData.append(QString2Bytes(srcData.OptionValue, 20));
	return true;
}

bool InteractiveUtil::RecipeListDataToArray(const QVector<QString>& VecData, QByteArray& disData)
{
	bool f = false;
	int Dn = 0;
	int nbit = 0;
	for (int i = 0; i < VecData.size(); i++)
	{
		if (VecData[i].length() <= 0 || VecData[i] == " ")//��ǰλΪ 0
		{
			QString kk = VecData[i];
			f = false;
		}
		else
		{
			f = true;
		}

		Dn = i / 16;
		nbit = i % 16;
		CommandData::setPCStatusStream(f, Dn, nbit);
	}

	VecToArray(*CommandData::getPCRecipeListData(), disData);
	return true;
}

bool InteractiveUtil::MeasurePointListToArray(const QVector<QPointF>& VecData, QByteArray& disData)
{
	for (const auto& point : VecData)
	{
		int x = static_cast<int>(point.x() * 1000);
		int y = static_cast<int>(point.y() * 1000);
		QByteArray xData, yData;
		IntToByte(x, xData);
		IntToByte(y, yData);
		disData.append(xData).append(yData);
	}
	return true;
}

bool InteractiveUtil::Bytes2UShort(const QByteArray& srcData, int position, quint16& res)
{
	res = 0;
	bool ret = false;
	if (srcData.size() > position + 1) {
		QByteArray ba = srcData.mid(position + 1, 1).toHex();
		int c = QString(ba).toInt(&ret, 16);
		if (ret)
		{
			c = c << 8;
			res += c;
			ba = srcData.mid(position + 0, 1).toHex();
			c = QString(ba).toInt(&ret, 16);
			if (ret)
			{
				res += c;
			}
		}
		return ret;
	}
	return false;
}

bool InteractiveUtil::Bytes2Int(const QByteArray& srcData, int position, int& res)
{
	int outVal = 0;
	memcpy(&outVal, srcData, 4);
	res = outVal;
	return true;
}

QByteArray InteractiveUtil::UShort2Bytes(quint16 i)
{
	QByteArray bytes;
	char cs[2];
	cs[0] = i & 0xFF;
	cs[1] = (i >> 8) & 0xFF;

	bytes.append(cs[0]);
	bytes.append(cs[1]);

	return bytes;
}

QByteArray InteractiveUtil::UShort2Bytes(quint16 i, int nsize)
{
	QByteArray bytes;
	if (nsize == 2)
	{
		char cs[2];
		cs[0] = i & 0xFF;
		cs[1] = (i >> 8) & 0xFF;

		bytes.append(cs[0]);
		bytes.append(cs[1]);
	}
	else if (nsize == 4)
	{
		char cs[4];
		cs[0] = i & 0xFF;
		cs[1] = (i >> 8) & 0xFF;

		bytes.append(cs[0]);
		bytes.append(cs[1]);
		bytes.append(cs[2]);
		bytes.append(cs[3]);
	}

	return bytes;
}

QByteArray InteractiveUtil::QString2Bytes(const QString& str, int len)
{
	QByteArray res;
	res.append(str.left(len).toLatin1());
	for (; res.size() < len;) {
		res.append((char)0x00);
	}
	return res;
}

bool InteractiveUtil::IntToByte(const int number, QByteArray& disData)
{
	QByteArray abyte0;
	abyte0.resize(4);
	abyte0[0] = (uchar)(0x000000ff & number);
	abyte0[1] = (uchar)((0x0000ff00 & number) >> 8);
	abyte0[2] = (uchar)((0x00ff0000 & number) >> 16);
	abyte0[3] = (uchar)((0xff000000 & number) >> 24);
	disData = abyte0;
	return true;
}
