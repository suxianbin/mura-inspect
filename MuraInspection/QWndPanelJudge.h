#pragma once

#include <QWidget>
#include "ui_QWndPanelJudge.h"

class QWndPanelJudge : public QWidget
{
	Q_OBJECT

public:
	QWndPanelJudge(QWidget *parent = nullptr);
	~QWndPanelJudge();

private:
	Ui::QWndPanelJudgeClass ui;
};
