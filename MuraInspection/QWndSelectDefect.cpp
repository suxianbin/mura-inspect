#include "QWndSelectDefect.h"
#include <QSettings>
#include <algorithm>
#include "QTableViewDelegate.h"
#include <QStandardItemModel>
#pragma execution_character_set("utf-8")

QWndSelectDefect::QWndSelectDefect(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ReadParam();
	InitTable();
}

QWndSelectDefect::~QWndSelectDefect()
{}

void QWndSelectDefect::updateCheckSum()
{

}

void QWndSelectDefect::InitTable()
{
	QStringList strHeader;
	strHeader << u8"序 号" << u8"操 作" << u8"缺陷类型" << u8"备注";
	m_pItemModel = new  QStandardItemModel(0, 4, this);
	m_pItemModel->setHorizontalHeaderLabels(strHeader);

	ui.tableView->setModel(m_pItemModel);
	ui.tableView->setEditTriggers(QTableView::DoubleClicked);				//使其双击可进行编辑
	ui.tableView->setSelectionBehavior(QTableView::SelectRows);			//一次选中整行
	ui.tableView->setSelectionMode(QTableView::SingleSelection);			//单选
	ui.tableView->horizontalHeader()->setStretchLastSection(true);        //最后一列自适应宽度
	ui.tableView->setAlternatingRowColors(true); // 隔行变色
	ui.tableView->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色

	ui.tableView->setColumnWidth(0, 80);
	ui.tableView->setColumnWidth(1, 100);
	ui.tableView->setColumnWidth(2, 150);
	//ui.tableView->setColumnWidth(3, 200);
	//1、3列不允许编辑
	QTableViewDelegate* pDelegate = new QTableViewDelegate();
	pDelegate->setTextAlignment(Qt::AlignCenter);
	pDelegate->setEditor(eColumnType::NormalEdit, false);
	ui.tableView->setItemDelegateForColumn(0, pDelegate);
	ui.tableView->setItemDelegateForColumn(2, pDelegate);

	QDelegateCheckBox* pDelegate1 = new QDelegateCheckBox();
	ui.tableView->setItemDelegateForColumn(1, pDelegate1);
	ui.tableView->verticalHeader()->hide();

	connect(ui.tableView, SIGNAL(clicked(QModelIndex)), this, SLOT(on_tableSelectDefect_clicked(QModelIndex)));

	QString style = "QTableView{background-color: rgb(230, 230, 230);alternate-background-color: rgb(180, 180, 180);gridline-color: rgb(80, 80, 80); gridline-width: 2px;}\
	 QHeaderView{\
	 color: rgb(130, 130, 130);\
	 font: 12pt;\
	 background-color: rgb(150, 150, 150);\
	 border: 0px solid rgb(144, 144, 144);\
	 border:0px solid rgb(191,191,191);\
	 border-left-color: rgba(255, 255, 255, 0);\
	 border-top-color: rgba(255, 255, 255, 0);\
	 border-radius:0px;\
	 min-height:50px;\
	 }";

	ui.tableView->setStyleSheet(style);

	for (int n = 0;n< DefectTypeList.size();n++)
	{
		m_pItemModel->insertRow(n);
		m_pItemModel->setItem(n, 0, new QStandardItem(QString::number(n + 1)));
		m_pItemModel->setItem(n, 2, new QStandardItem(DefectTypeList[n]));
		m_pItemModel->setData(m_pItemModel->index(n, 1), true, Qt::UserRole);
	}
}

void QWndSelectDefect::ReadParam()
{
	QString path = QCoreApplication::applicationDirPath() + "/MuraSystem/RecipePlug.ini";
	QSettings* setting = new QSettings(path, QSettings::IniFormat);
	setting->setIniCodec("UTF-8");

	DefectLevels.clear();
	DefectTypeList.clear();

	setting->beginGroup("DefectTypeList");
	QList<int> keys;
	// 将keys从字符串转换为整数并进行排序
	for (const QString& key : setting->childKeys()) {
		keys.append(key.toInt());
	}
	std::sort(keys.begin(), keys.end());
	// 使用排序后的整数作为key获取对应的值
	for (int key : keys) {
		DefectTypeList.append(setting->value(QString::number(key)).toString());
	}
	setting->endGroup();


	setting->beginGroup("DefectLevels");
	QList<int> keys_levels;
	// 将keys从字符串转换为整数并进行排序
	for (const QString& key : setting->childKeys()) 
	{
		keys_levels.append(key.toInt());
	}
	std::sort(keys_levels.begin(), keys_levels.end());
	// 使用排序后的整数作为key获取对应的值
	DefectLevels.append(u8"类 型");
	for (int key : keys_levels)
	{
		DefectLevels.append(setting->value(QString::number(key)).toString());
	}
	setting->endGroup();

	delete setting;
}

void QWndSelectDefect::on_tableSelectDefect_clicked(QModelIndex index)
{
	if (1 != index.column()) { return; }

	int row = index.row();
	bool b = index.data(Qt::UserRole).toString() == "true" ? true : false;
	emit sig_DefectSelectChanged(DefectTypeList[row], b);

}
