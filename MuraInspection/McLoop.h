#pragma once
#include <QThread>
#include <QTimer>
#include "InteractiveUtil.h"

enum class eSpmeStep
{
	WaitGlassReady = 0, //等待玻璃就绪
	GlassReadyReply,    //回复玻璃就绪信号
	SendStartMeasure,   //发送开始测量信号
	WaitStartMeasureReply,
	SendStartReference, //发送开始测量基准
	WaitStartReferenceReply,
	SendSkipGlass,		//发送跳过测量
	WaitSkipGlassReply,
	WaitReachPoint,		//等待到达测量点
	ReachPointReply,		//到达点回应
	SendSPMEMeasure,	//SPME发送测量指令
	WaitSPMEMeasFinished,//等待SPME测量完成
	SendSPMEAnalyze,	//SPME发送分析指令
	WaitSPMEAnalyzeFinished,//等待SPME分析完成
	SendSPMEMeasureFinished,	//发送SPEME测量完成给PLC
	WaitThisPointMeasureFinishedReply,	//等待点检测完成回应
	SendUnloadGlass,
	WaitUnloadGlassReply,
};

enum class eScanStep
{
	WaitGlassLoadDone = 0, //玻璃装载完成
	GlassLoadDoneReply,//玻璃装载完成回应
	ScanRequst,   //扫描请求PC
	WaitScanRequstReply,
	WaitScanDone, //等待扫描完成
	ScanDoneReply,
	GlassChecking,
	UnLoadRequst,//卸载请求
	WaitUnloadFinished,
};

class McLoop : public QThread
{
	Q_OBJECT
signals:
	void sig_revicedRecipeId(int isAutoChange, int recipeId);
public:
	static McLoop* Instance()
	{
		static McLoop instance;
		return &instance;
	}
	JobData* GetJobData();
	bool ReadJobData = false;
	bool SyncJobData = false;
	void ResetStation1(); //重置流程状态
	void ResetStation2(); //重置流程状态
	bool IsConnecting();
	void SendSpmeMeasurePoints(QVector<QPointF>points);
	void SendJobData();

private:
	McLoop(QObject* parent = Q_NULLPTR);
	~McLoop() override;

	void Init();

private:
	QTimer* m_hbTimer; //心跳定时器
	bool m_hbf = false;
	InteractiveUtil* m_util;

	JobData m_jobData;

	eSpmeStep SpmeStep;
	eScanStep Station1Step;
	eScanStep Station2Step;

	QString SpmeRunStepStr;
	QString Station1RunStepStr;
	QString Station2RunStepStr;
	QString ScaneStartTime = "";

private:
	void run() override;

	void generalPolling(); //轮询

	//自动切换配方流程
	void SwitchRecipe();

	//扫描流程动作
	void Station1ScanLoop(); //工站1扫描
	void Station2ScanLoop(); //工站2扫描

	//SPME流程动作
	void SPMELoop();
	void WaitGlassReadyAct();
	void GlassReadyReplyAct();
	void SendStartMeasureAct();
	void WaitStartMeasureReplyAct();
	void SendStartReferenceAct();
	void WaitStartReferenceReplyAct();
	void SendSkipGlassAct();
	void WaitSkipGlassReplyAct();
	void WaitReachPointAct();
	void SendSPMEMeasureAct();
	void WaitSPMEMeasFinishedAct();
	void SendSPMEAnalyzeAct();
	void WaitSPMEAnalyzeFinishedAct();
	void SendSPMEMeasureFinishedAct();
	void WaitThisPointMeasureFinishedReplyAct();
	void SendUnloadGlassAct();
	void WaitUnloadGlassReplyAct();
};
