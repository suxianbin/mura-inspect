#pragma once
#include "Base.h"
#include <QVector>

enum class eSpmeType
{
	null,
	OD,   //Ĥ�ܶ�
	Color,//ɫ��
	VIS,  //Ĥ��
};

struct SpmeMeasPoint
{
	SpmeMeasPoint() { X = 0; Y = 0; }
	SpmeMeasPoint(double _x, double _y)
	{
		X = _x;
		Y = _y;
	}
	double X = 0.0;
	double Y = 0.0;
	double OOCMin = 0.0;
	double OOCMax = 0.0;
	double OOSMin = 0.0;
	double OOSMax = 0.0;
	void write(QJsonObject& json) const
	{
		json["X"] = X;
		json["Y"] = Y;
		json["OOCMin"] = OOCMin;
		json["OOCMax"] = OOCMax;
		json["OOSMin"] = OOSMin;
		json["OOSMax"] = OOSMax;
	}
	void read(const QJsonObject& json)
	{
		X = json["X"].toDouble();
		Y= json["Y"].toDouble();
		OOCMin = json["OOCMin"].toDouble();
		OOCMax = json["OOCMax"].toDouble();
		OOSMin = json["OOSMin"].toDouble();
		OOSMax = json["OOSMax"].toDouble();
	}
};


class SpmeParam : public Base
{
public:
	SpmeParam(QString key);
	~SpmeParam() override;
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;
	eSpmeType SpmeType = eSpmeType::null;
	QString CreateTime = "";
	QString Remark = "";
	int MajorPointCount = 0;
	int MinorPointCount = 0;
	QVector<SpmeMeasPoint>MajorPoints; //glass ����
	QVector<SpmeMeasPoint>MinorPoints;


};

