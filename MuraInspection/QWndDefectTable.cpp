﻿#pragma execution_character_set("utf-8")
#include "QWndDefectTable.h"
#include <QStandardItem>
#include "Glass.h"

QWndDefectTable::QWndDefectTable(QWidget *parent, TABLE_TYPE type)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_type = type;
	initTable(type);
	connect(ui.tableWidget, &QTableWidget::currentCellChanged, this, &QWndDefectTable::on_currentRowSelected);
}

QWndDefectTable::~QWndDefectTable()
{}

void QWndDefectTable::updateTable(QVector<Defect>&defects)
{
	int rowNum = ui.tableWidget->rowCount();
	for (int row = 0; row < rowNum; row++) 
	{
		ui.tableWidget->removeRow(0);
	}
	if (m_type == TABLE_TYPE::OrgDefectTable)
	{
		ui.tableWidget->setRowCount(defects.size());
		for (int i = 0; i < defects.size(); i++)
		{
			int c = 0;
			const Defect dfr = defects[i];
			QTableWidgetItem* item0 = new QTableWidgetItem(QString::number(dfr.defectId));
			item0->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item0);
			QTableWidgetItem* item1 = new QTableWidgetItem(dfr.type);
			item1->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item1);
			QTableWidgetItem* item2 = new QTableWidgetItem("");
			item2->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item2);
			QTableWidgetItem* item3 = new QTableWidgetItem(QString::number(dfr.imgPosX, 'f', 2));
			item3->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item3);
			QTableWidgetItem* item4 = new QTableWidgetItem(QString::number(dfr.imgPosY, 'f', 2));
			item4->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item4);
			QTableWidgetItem* item5 = new QTableWidgetItem(QString::number(dfr.width, 'f', 2));
			item5->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item5);
			QTableWidgetItem* item6 = new QTableWidgetItem(QString::number(dfr.length, 'f', 2));
			item6->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item6);
			QTableWidgetItem* item7 = new QTableWidgetItem(QString::number(dfr.size, 'f', 2));
			item7->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item7);
			QTableWidgetItem* item8 = new QTableWidgetItem(QString::number(dfr.grayMean, 'f', 2));
			item8->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item8);
			QTableWidgetItem* item9 = new QTableWidgetItem(QString::number(dfr.linearity, 'f', 2));
			item9->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item9);
			QTableWidgetItem* item10 = new QTableWidgetItem(dfr.panelId);
			item10->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item10);
			QTableWidgetItem* item11 = new QTableWidgetItem(QString::number(dfr.meangraydiff2));
			item11->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item11);
			QTableWidgetItem* item12 = new QTableWidgetItem(QString::number(dfr.area2smallestRectRatio));
			item12->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item12);
			QTableWidgetItem* item13 = new QTableWidgetItem(QString::number(dfr.roundness));
			item13->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item13);
			QTableWidgetItem* item14 = new QTableWidgetItem(QString::number(dfr.rect2Phi));
			item14->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item14);
			QTableWidgetItem* item15 = new QTableWidgetItem(QString::number(dfr.grayDeviation));
			item15->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item15);
			QTableWidgetItem* item16 = new QTableWidgetItem(QString::number(dfr.grayNeiRegionRingMean));
			item16->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item16);
			QTableWidgetItem* item17 = new QTableWidgetItem(QString::number(dfr.thicknessDev));
			item17->setTextAlignment(Qt::AlignCenter);
			ui.tableWidget->setItem(i, c++, item17);

			//for (int col = 0; col < ui.tableWidget->columnCount(); col++)
			//{
			//	//ui.tableWidget->item(i, col)->setBackground(dfr.levelColor);
			//}
		}
		int a = ui.tableWidget->rowCount();
	}
	else if (m_type == TABLE_TYPE::ScreenOutTable)
	{

	}
}

void QWndDefectTable::initTable(TABLE_TYPE type)
{
	ui.tableWidget->clear();
	QStringList headers;
	if (type == TABLE_TYPE::ScreenOutTable)
	{
		headers << "ID" << "type" << "level" << "x(mm)" << "y(mm)" << "width(mm)" << "length(mm)" << "size(mm²)" << "gray" << "linearity" << "panelId" << "Warning等级" << "是否屏蔽" << "是否共通";
	}
	else if (type == TABLE_TYPE::OrgDefectTable)
	{
		headers << "ID" << "type" << "level" << "x(mm)" << "y(mm)" << "width(mm)" << "length(mm)" << "size(mm²)" << "gray" << "linearity" << "panelId" << "区域与背景灰度均值灰度标准差" << "面积与最小外接矩形面积比例"
			<< "圆度1" << "最小外接矩形角度" << "灰度方差" << "邻域背景平均灰度" << "主方向宽度标准差";
	}
	ui.tableWidget->setColumnCount(headers.size());
	ui.tableWidget->setHorizontalHeaderLabels(headers);
	ui.tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tableWidget->setAlternatingRowColors(true); // 隔行变色
	ui.tableWidget->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tableWidget->verticalHeader()->hide();

	//ui.tableWidget->setColumnWidth(0, 200);
	//ui.tableWidget->setColumnWidth(1, 120);
	//ui.tableWidget->setColumnWidth(2, 120);
	//ui.tableWidget->setColumnWidth(3, 120);
	//ui.tableWidget->setColumnWidth(4, 120);
	//ui.tableWidget->setColumnWidth(5, 120);
	for (int i = 0; i < headers.size(); i++)
	{
		ui.tableWidget->setColumnWidth(i, 130);
	}
}

void QWndDefectTable::on_currentRowSelected(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tableWidget->rowCount())
	{
		if (currentRow != previousRow)
		{
			emit DefectSelected(currentRow);
		}
	}
	else
	{

	}
}
