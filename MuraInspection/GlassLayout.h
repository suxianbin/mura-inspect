#pragma once
#include "PanelLayout.h"
#include "Base.h"
#include <QString>
#include <QVector>

enum class eFlowDir
{
	null,
	Left2Right,
	Right2Left,
	Down2Top,
	Top2Down,
};

enum class eUnit
{
	MM,
	INCH,
	PIXEL,

};

//坐标模式
enum class eCoorMode
{
	null,
	TopLeft,//左上
	TopRight,
	BotRight,
	BotLeft,
	Center,
};

//坐标方向
enum class eCoorDir
{
	null,
	XY_LeftUp,//左上
	XY_RightUp,
	XY_RightDown,
	XY_LeftDown,
};

class GlassLayout : public Base
{
public:
	GlassLayout(QString key);
	virtual ~GlassLayout();
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;

	QString LayoutName;
	eFlowDir FlowDir = eFlowDir::null;//玻璃流向
	eCoorMode GlassCoorMode = eCoorMode::null; //坐标系
	eCoorDir GlassCoorDir = eCoorDir::null;
	eUnit GlassUnit = eUnit::MM;
	QString CreateTime = ""; //最后更新时间
	QString Remark; //备注信息
	double GlassX = 0;
	double GlassY = 0;
	double GlassWidth = 0; //玻璃宽高
	double GlassHeight = 0;


	QString ChamferLabel; 

	double Station1PixelEquivalent = 0.1; //工站1像素当量
	double Station2PixelEquivalent = 0.1; //工站2像素当量
	//double Station3PixelEquivalent = 0.1; //工站3像素当量
	//double Station4PixelEquivalent = 0.1; //工站4像素当量

	QVector<PanelLayout> PanelLists;

	//一些辅助信息
	double TopLeft_X;
	double TopLeft_Y;
	double TopRight_X;
	double TopRight_Y;
	double Space_X;
	double Space_Y;
	int Row = 1;
	int Col = 1;
	QString ModelName;
	QString LayoutModelImagePath; //制作layout的图片路径
};

