#include "MuraInspection.h"
#include <QtWidgets/QApplication>
#include <QSharedMemory>
#include <QMessageBox>
#include <QIcon>
#include <QScreen>

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);

	// 禁止重复启动
	QSharedMemory singleton(a.applicationName());
	if (!singleton.create(1))
	{
		QMessageBox::warning(NULL, "提示", "软件已启动!!!!", QMessageBox::Ok);
		return 0;
	}
	a.setApplicationName("MuraInspection");
	//a.setWindowIcon(QIcon(":/Icons/icon.png"));

	MuraInspection w;

	// 获取第二个屏幕的坐标
	QList<QScreen*> desktop = QApplication::screens();
	if (desktop.size() > 1)
	{
		QRect screenGeometry = desktop[1]->geometry(); // 第二个屏幕的索引是1
		w.move(screenGeometry.x() + 50, screenGeometry.y() + 50); // 移动到第二个屏幕上
	}
	w.showMaximized();
	return a.exec();
}
