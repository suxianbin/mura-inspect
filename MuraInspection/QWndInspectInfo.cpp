#include "QWndInspectInfo.h"
#include <QVBoxLayout>
#include "Glass.h"

QWndInspectInfo::QWndInspectInfo(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	//MySystemParam sysParam = QWndSystemParam::Instance()->SystemParam;
	m_pDefectTable1 = new QWndDefectTable(this);
	m_pDefectTable2 = new QWndDefectTable(this);
	m_pDefectGraph = new QWndDefectGraph(this);
	m_pPlatformLog = new QWidget();
	m_pPlatformLog->setContentsMargins(0, 0, 0, 0);
	m_pSpme = new QWndSpmeResult();
	m_pSpme->setContentsMargins(0, 0, 0, 0);
	m_pSpmeChart = new QWndSpmeChart();
	m_pSpmeChart->setContentsMargins(0, 0, 0, 0);
	m_pSecs = new QWidget();
	m_pSecs->setContentsMargins(0, 0, 0, 0);
	m_pOrgDefect1 = new QWndDefectTable(this, TABLE_TYPE::OrgDefectTable);
	m_pOrgDefect1->setContentsMargins(0, 0, 0, 0);
	m_pOrgDefect2 = new QWndDefectTable(this, TABLE_TYPE::OrgDefectTable);
	m_pOrgDefect2->setContentsMargins(0, 0, 0, 0);
	//主界面布局
	QVBoxLayout* m_pMainDispFrameLayout = new QVBoxLayout();
	m_pMainDispFrameLayout->setMargin(0);
	m_pMainDispFrameLayout->addWidget(m_pDefectTable1);
	m_pMainDispFrameLayout->addWidget(m_pDefectTable2);
	m_pMainDispFrameLayout->addWidget(m_pPlatformLog);
	m_pMainDispFrameLayout->addWidget(m_pSpme);
	m_pMainDispFrameLayout->addWidget(m_pSpmeChart);
	m_pMainDispFrameLayout->addWidget(m_pSecs);
	m_pMainDispFrameLayout->addWidget(m_pOrgDefect1);
	m_pMainDispFrameLayout->addWidget(m_pOrgDefect2);
	m_pMainDispFrameLayout->addWidget(m_pDefectGraph);
	ui.frame_2->setLayout(m_pMainDispFrameLayout);
	hiddenAllWidget();
	m_pDefectTable1->setHidden(false);
	setButtonStyleSheet(ui.pb_Station1Defect);

	//ui.pb_Station1Defect->setVisible(sysParam.Station1Display);
	//ui.pb_Station2Defect->setVisible(sysParam.Station2Display);
	//ui.pb_PlatformLog->setVisible(sysParam.PlatformDisplay);
	//ui.pb_Spme->setVisible(sysParam.SPMEDisplay);
	//ui.pb_SpmeChart->setVisible(sysParam.SPMEChartDisplay);
	//ui.pb_Secs->setVisible(sysParam.SECSDisplay);
	//ui.pb_OrgDefect1->setVisible(sysParam.OrgDefect1Display);
	//ui.pb_OrgDefect2->setVisible(sysParam.OrgDefect2Display);

	connect(ui.pb_Station1Defect, SIGNAL(clicked()), SLOT(on_pb_Station1DefectClicked()));
	connect(ui.pb_Station2Defect, SIGNAL(clicked()), SLOT(on_pb_Station2DefectClicked()));
	connect(ui.pb_PlatformLog, SIGNAL(clicked()), SLOT(on_pb_PlatformLogClicked()));
	connect(ui.pb_Spme, SIGNAL(clicked()), SLOT(on_pb_SpmeClicked()));
	connect(ui.pb_SpmeChart, SIGNAL(clicked()), SLOT(on_pb_SpmeChartClicked()));
	connect(ui.pb_Secs, SIGNAL(clicked()), SLOT(on_pb_SecsClicked()));
	connect(ui.pb_OrgDefect1, SIGNAL(clicked()), SLOT(on_pb_OrgDefect1Clicked()));
	connect(ui.pb_OrgDefect2, SIGNAL(clicked()), SLOT(on_pb_OrgDefect2Clicked()));
	connect(ui.pb_DefectGraph, SIGNAL(clicked()), SLOT(on_pb_DefectGraphClicked()));
}

QWndInspectInfo::~QWndInspectInfo()
{}

void QWndInspectInfo::updateOrgFlawTable(int station)
{
	//DefectRlt datarlt;
	//QVector<DefectRlt>showDefects;
	//for (int index = 0; index < runTimeData->classDefects.size(); index++)
	//{
	//	datarlt.panelId = runTimeData->classDefects.at(index).panelId;
	//	datarlt.levelColor = Qt::darkYellow;
	//	datarlt.defect = runTimeData->classDefects.at(index);
	//	showDefects.append(datarlt);
	//}
	//if (station == 1)
	//{
	//	m_pOrgDefect1->updateTable(showDefects, runTimeData->pixelEquivalent);
	//}
	//else if (station == 2)
	//{
	//	m_pOrgDefect2->updateTable(showDefects, runTimeData->pixelEquivalent);
	//}
}

void QWndInspectInfo::updateDefectTable(int station)
{
	QVector<Defect>defects;
	if (station == 1)
	{
		defects = Glass::Instance()->Station1OrgDefect;
		m_pDefectTable1->updateTable(defects);
		m_pOrgDefect1->updateTable(defects);
	}
	else if(station == 2)
	{
		defects = Glass::Instance()->Station2OrgDefect;
		m_pDefectTable2->updateTable(defects);
	}


}

QWndDefectTable* QWndInspectInfo::getDefectTable(int idx)
{
	return m_pDefectTable1;
}

void QWndInspectInfo::hiddenAllWidget()
{
	m_pDefectTable1->setHidden(true);
	m_pDefectTable2->setHidden(true);
	m_pPlatformLog->setHidden(true);
	m_pSpme->setHidden(true);
	m_pSpmeChart->setHidden(true);
	m_pSecs->setHidden(true);
	m_pOrgDefect1->setHidden(true);
	m_pOrgDefect2->setHidden(true);
	m_pDefectGraph->setHidden(true);
}

void QWndInspectInfo::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);color:rgb(20, 20, 20);border-radius:5px;border:0px;}\
					QPushButton:pressed {background-color:rgba(13,55,147,0.85);}\
					QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_Station1Defect->setStyleSheet(style);
	ui.pb_Station2Defect->setStyleSheet(style);
	ui.pb_PlatformLog->setStyleSheet(style);
	ui.pb_Spme->setStyleSheet(style);
	ui.pb_SpmeChart->setStyleSheet(style);
	ui.pb_Secs->setStyleSheet(style);
	ui.pb_OrgDefect1->setStyleSheet(style);
	ui.pb_OrgDefect2->setStyleSheet(style);	
	ui.pb_DefectGraph->setStyleSheet(style);
}

void QWndInspectInfo::setButtonStyleSheet(QPushButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(87, 153, 239);border-radius:5px;border:0px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}

void QWndInspectInfo::on_pb_Station1DefectClicked()
{
	hiddenAllWidget();
	m_pDefectTable1->setHidden(false);
	setButtonStyleSheet(ui.pb_Station1Defect);
}

void QWndInspectInfo::on_pb_Station2DefectClicked()
{
	hiddenAllWidget();
	m_pDefectTable2->setHidden(false);
	setButtonStyleSheet(ui.pb_Station2Defect);
}

void QWndInspectInfo::on_pb_PlatformLogClicked()
{
	hiddenAllWidget();
	m_pPlatformLog->setHidden(false);
	setButtonStyleSheet(ui.pb_PlatformLog);
}

void QWndInspectInfo::on_pb_SpmeClicked()
{
	hiddenAllWidget();
	m_pSpme->setHidden(false);
	setButtonStyleSheet(ui.pb_Spme);
}

void QWndInspectInfo::on_pb_SpmeChartClicked()
{
	hiddenAllWidget();
	m_pSpmeChart->setHidden(false);
	setButtonStyleSheet(ui.pb_SpmeChart);
}

void QWndInspectInfo::on_pb_SecsClicked()
{
	hiddenAllWidget();
	m_pSecs->setHidden(false);
	setButtonStyleSheet(ui.pb_Secs);
}

void QWndInspectInfo::on_pb_OrgDefect1Clicked()
{
	hiddenAllWidget();
	m_pOrgDefect1->setHidden(false);
	setButtonStyleSheet(ui.pb_OrgDefect1);
}

void QWndInspectInfo::on_pb_OrgDefect2Clicked()
{
	hiddenAllWidget();
	m_pOrgDefect2->setHidden(false);
	setButtonStyleSheet(ui.pb_OrgDefect2);
}

void QWndInspectInfo::on_pb_DefectGraphClicked()
{
	hiddenAllWidget();
	m_pDefectGraph->setHidden(false);
	setButtonStyleSheet(ui.pb_DefectGraph);
}
