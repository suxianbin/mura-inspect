#pragma once

#include <QStyledItemDelegate>

enum eColumnType
{
	NormalEdit,
	ComboBox,
	CheckBox,
	Button,
	Color,
};

class QTableViewDelegate  : public QStyledItemDelegate
{
	Q_OBJECT

public:
	QTableViewDelegate(QObject *parent=nullptr);
	~QTableViewDelegate();

	void setEditor(eColumnType eType, bool bEdit=true);
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;

	void setStringList(const QStringList &strList);
	void setTextAlignment(Qt::AlignmentFlag = Qt::AlignLeft);

	QWidget * createEditor(QWidget *parent, const QStyleOptionViewItem & option, const QModelIndex & index) const override;
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex & index) const override;

private:
	bool     m_bEditEna = false;
	eColumnType m_eType = eColumnType::NormalEdit;
	QStringList m_strList;
	Qt::AlignmentFlag m_eAlign = Qt::AlignLeft;
};

class QDelegateCheckBox : public QStyledItemDelegate
{
	Q_OBJECT

public:
	QDelegateCheckBox(QObject *parent = nullptr);
	~QDelegateCheckBox();

	//QWidget * createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;

private:
	bool     m_bEditEna = false;
	eColumnType m_eType = eColumnType::NormalEdit;
	QStringList m_strList;
	Qt::AlignmentFlag m_eAlign = Qt::AlignLeft;
};
