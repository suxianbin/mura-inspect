#pragma once

#include <QWidget>
#include "ui_QWndCheckSum.h"

class QWndCheckSum : public QWidget
{
	Q_OBJECT

public:
	QWndCheckSum(QWidget *parent = nullptr);
	~QWndCheckSum();
	void updateCheckSum();
private:
	Ui::QWndCheckSumClass ui;
	void InitTable();
	void ReadParam();

	QStringList DefectTypeList;
	QStringList DefectLevels;
	//QList<QPair<QString, QList<QPair<QString, int>>>>m_LevelStatistical
	//LevelStatistical m_LevelStatistical;					//缺陷分类标签数据
};
