#include "ModelPainter.h"

ModelPainter::ModelPainter(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

ModelPainter::~ModelPainter()
{
}

void ModelPainter::drawModel(const PanelModel& _model)
{
	m_model = _model;
	m_drawModel = true;
	calculate();
	update();
}

void ModelPainter::clearModel()
{
	m_drawModel = false;
	update();	
}

void ModelPainter::paintEvent(QPaintEvent* event)
{
	QPainter p(this);
	//绘制背景颜色
	QBrush b;
	b.setColor(Qt::darkGray);
	b.setStyle(Qt::SolidPattern);
	p.setBackground(b);
	p.fillRect(rect(), b);
	
	if(m_drawModel) {
		QPen pen;
		pen.setColor(Qt::green);
		pen.setWidth(1);
		pen.setStyle(Qt::SolidLine);
		p.setPen(pen);

		//p.drawRect(m_x, m_y, m_w, m_h);

		p.setRenderHint(QPainter::Antialiasing); // 启用抗锯齿


		QRect rect(m_x, m_y, m_w, m_h); // 矩形区域
		//p.drawRoundedRect(rect, m_radius, m_radius);	
		p.drawRoundedRect(rect, 0, 0);



		QRect rectContract(m_xContract, m_yContract, m_wContract, m_hContract); // 内缩矩形区域
		p.drawRoundedRect(rectContract, m_radius, m_radius);		

		//绘制标注

		p.drawLine(m_x, m_y - 5, m_x, m_y - 30);

		p.drawLine(m_x+m_w, m_y - 10, m_x + m_w, m_y - 30);

		QTextOption to1;
		to1.setAlignment(Qt::AlignCenter);
		p.drawText(QRect(m_x, m_y - 30, m_w, 25), QString::number(m_model.Width, 'f', 2) + "mm", to1);

		p.drawLine(m_x - 5, m_y, m_x - 30, m_y);

		p.drawLine(m_x - 5, m_y+m_h, m_x - 30, m_y + m_h);
		//插入换行
		QString s = QString::number(m_model.Height, 'f', 2) + "mm";
		for(int i =0;i<s.size();i+=2) {
			s.insert(i + 1, '\n');
		}
		p.drawText(QRect(m_x - 30, m_y , 25, m_h), s, to1);

		//////////////////////////////////////////////////////////////////////////
		//  [2024/7/24 10:33 zaiyuan.li]
		p.save(); // 保存当前绘图状态

		pen.setColor("#0000FF");
		p.setPen(pen);

		// 设置填充颜色为红色
		QBrush brush("#FFA500", Qt::FDiagPattern);
		p.setBrush(brush);
		// 示例：转换坐标系，可以根据需要调整 dx 和 dy
		p.translate(m_x, m_y); // 将坐标系原点向右下角移动


		//for (auto itemIgnore : m_model.modelIgnoreList)
		//{
		//	if (itemIgnore.type == MaskType::Polygon)
		//	{
		//		QVector<QPointF> points = itemIgnore.polygon.vertices;
		//		QVector<double> cornerRadius = itemIgnore.polygon.vecRadius;

		//		QPainterPath path;
		//		int pointCount = points.size();
		//		for (int i = 0; i < pointCount; ++i) {
		//			QPointF current = points[i] * m_scale;
		//			QPointF next = points[(i + 1) % pointCount] * m_scale;
		//			QPointF prev = points[(i - 1 + pointCount) % pointCount] * m_scale;

		//			// 向量
		//			QPointF v1 = current - prev;
		//			QPointF v2 = next - current;

		//			// 归一化向量
		//			qreal len1 = qSqrt(v1.x() * v1.x() + v1.y() * v1.y());
		//			qreal len2 = qSqrt(v2.x() * v2.x() + v2.y() * v2.y());
		//			QPointF unitV1 = v1 / len1;
		//			QPointF unitV2 = v2 / len2;

		//			// 计算内切点
		//			QPointF p1 = current - unitV1 * cornerRadius[i] * m_scale;
		//			QPointF p2 = current + unitV2 * cornerRadius[i] * m_scale;

		//			if (i == 0) {
		//				path.moveTo(p1);
		//			}
		//			else {
		//				path.lineTo(p1);
		//			}

		//			// 二次贝赛斯曲线 [2024/7/26 14:48 zaiyuan.li]
		//			// 计算控制点
		//			path.quadTo(current, p2);
		//			 
		//			//三次贝赛斯曲线
		//			// 计算第二个控制点，可以根据需要调整这里的计算方法
		//			//QPointF controlPoint2 = current + unitV2 * cornerRadius[i] * m_scale;
		//			// 使用 cubicTo 绘制三次贝塞尔曲线
		//			//path.cubicTo(current, controlPoint2, p2);
		//			
		//		}
		//		path.closeSubpath();
		//		// 绘制路径
		//		p.drawPath(path);
		//	}
		//	else
		//	{

		//		QRect recttest(itemIgnore.x * m_scale, itemIgnore.y * m_scale, itemIgnore.width * m_scale, itemIgnore.height * m_scale); // 矩形区域
		//		p.drawRoundedRect(recttest, itemIgnore.radius * m_scale, itemIgnore.radius * m_scale);
		//	}
		//}
		//p.restore(); // 恢复绘图状态
		//pen.setColor(Qt::green);
		//p.setPen(pen);
		////////////////////////////////////////////////////////////////////////////	
	}
	
}

void ModelPainter::calculate()
{
	QSize s = size();
	qreal hScale = 0;	//水平比例
	hScale = m_model.Width / s.width();
	qreal vScale = 0;	//垂直比例
	vScale = m_model.Height / s.height();
	if (vScale > hScale) {
		m_h = s.height() * 0.8;
		m_scale = m_h / m_model.Height;
		m_w = m_scale * m_model.Width;
		m_radius = m_scale * m_model.Radius;
	}
	else {
		m_w = s.width() * 0.8;
		m_scale = m_w / m_model.Width;
		m_h = m_scale * m_model.Height;
		m_radius = m_scale * m_model.Radius;;

	}
	m_x = s.width() / 2 - m_w / 2;
	m_y = s.height() / 2 - m_h / 2;


	m_xContract = m_x + m_scale * m_model.LeftDst;
	m_yContract = m_y + m_scale * m_model.TopDst;
	m_wContract = m_w - m_scale * (m_model.LeftDst + m_model.RightDst);
	m_hContract = m_h - m_scale * (m_model.TopDst + m_model.BotDst);
}

void ModelPainter::resizeEvent(QResizeEvent* event)
{
	if(m_drawModel) {
		calculate();
	}
}
