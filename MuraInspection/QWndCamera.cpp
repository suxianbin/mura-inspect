#include "QWndCamera.h"
#include "CameraParam.h"
#include "QWndMessageBox.h"
#include "CameraParamMgr.h"
#include <QTableWidgetItem>
#pragma execution_character_set("utf-8")

QWndCamera::QWndCamera(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	setStyleSheet();
	refreshCameraParamListTable();

	connect(ui.pb_ModifyBatch, &QPushButton::clicked, this, &QWndCamera::on_pb_ModifyBatchClicked);
	connect(ui.pb_CameraPrmNew, &QPushButton::clicked, this, &QWndCamera::on_pb_CameraPrmNewClicked);
	connect(ui.pb_CameraPrmEdit, &QPushButton::clicked, this, &QWndCamera::on_pb_CameraPrmEditClicked);
	connect(ui.pb_CameraPrmDelete, &QPushButton::clicked, this, &QWndCamera::on_pb_CameraPrmDeleteClicked);
	connect(ui.pb_CameraPrmSearch, &QPushButton::clicked, this, &QWndCamera::on_pb_CameraPrmSearchClicked);
	connect(ui.pb_CameraPrmSave, &QPushButton::clicked, this, &QWndCamera::on_pb_CameraPrmSaveClicked);

	connect(ui.tw_CameraParamList, &QTableWidget::currentCellChanged, this, &QWndCamera::on_ParamTableSeletedChanged);
}

QWndCamera::~QWndCamera()
{}

void QWndCamera::loadConfig()
{
}

void QWndCamera::refreshCameraParamListTable()
{
	QStringList headers;
	headers << "参数名称" << "更新时间" << "备注信息";
	ui.tw_CameraParamList->setColumnCount(headers.size());
	ui.tw_CameraParamList->setHorizontalHeaderLabels(headers);
	ui.tw_CameraParamList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_CameraParamList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_CameraParamList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_CameraParamList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_CameraParamList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_CameraParamList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_CameraParamList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_CameraParamList->verticalHeader()->hide();
	ui.tw_CameraParamList->setRowCount(CameraParamMgr::Instance()->Size());

	int i = 0;
	for (auto var : CameraParamMgr::Instance()->FindAll())
	{
		CameraParam* camera = dynamic_cast<CameraParam*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(camera->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_CameraParamList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(camera->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_CameraParamList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(camera->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_CameraParamList->setItem(i, 2, item2);
		i++;
	}
}

void QWndCamera::setStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(13,55,147);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_ModifyBatch->setStyleSheet(style);
	ui.pb_CameraPrmNew->setStyleSheet(style);
	ui.pb_CameraPrmEdit->setStyleSheet(style);
	ui.pb_CameraPrmDelete->setStyleSheet(style);
	ui.pb_CameraPrmSearch->setStyleSheet(style);
	ui.pb_CameraPrmSave->setStyleSheet(style);
}

void QWndCamera::refreshParam(CameraParam* param)
{
	ui.le_Remark->setText(param->Remark);
	ui.le_ParamName->setText(param->Key);
	ui.le_ExposureTime->setText(QString::number(param->ExposureTime));
	ui.cmb_Baudrate->setCurrentIndex(param->BaudRateIndex);
	ui.cmb_UserSelect->setCurrentIndex(param->SelectedUserFFTIndex);
}

void QWndCamera::on_pb_ModifyBatchClicked()
{

}

void QWndCamera::on_pb_CameraPrmNewClicked()
{
}

void QWndCamera::on_pb_CameraPrmEditClicked()
{
}

void QWndCamera::on_pb_CameraPrmDeleteClicked()
{
}

void QWndCamera::on_pb_CameraPrmSearchClicked()
{
}

void QWndCamera::on_pb_CameraPrmSaveClicked()
{
	if (ui.le_ParamName->text().isEmpty())
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未选中有效配方!");
		int ret = messageBox->exec();
		return;
	}

	CameraParam* cameraPrm = dynamic_cast<CameraParam*>(CameraParamMgr::Instance()->FindBy(ui.le_ParamName->text()));
	if (!cameraPrm)
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未找到配方!" + ui.le_ParamName->text());
		int ret = messageBox->exec();
		return;
	}
	cameraPrm->CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm::ss");
	cameraPrm->Remark = ui.le_Remark->text();
	cameraPrm->ExposureTime = ui.le_ExposureTime->text().toInt();
	cameraPrm->SelectedUserFFTIndex = ui.cmb_UserSelect->currentIndex();
	cameraPrm->BaudRateIndex = ui.cmb_Baudrate->currentIndex();
	CameraParamMgr::Instance()->Save("CameraMgr.json");
}

void QWndCamera::on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tw_CameraParamList->rowCount())
	{
		if (currentRow != previousRow)
		{
			QTableWidgetItem* item = ui.tw_CameraParamList->item(currentRow, 0);
			QString Name = item->text();
			CameraParam* cameraParam = dynamic_cast<CameraParam*>(CameraParamMgr::Instance()->FindBy(Name));
			refreshParam(cameraParam);
		}
	}
	else
	{

	}
}
