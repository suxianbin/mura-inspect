#include "GlassLayout.h"
#include <QJsonArray>

GlassLayout::GlassLayout(QString key) : Base(key)
{
	LayoutName = "default layout name";
	FlowDir = eFlowDir::null;
	CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
	GlassX = 0;
	GlassY = 0;
	GlassWidth = 0;
	GlassHeight = 0;
	GlassCoorMode = eCoorMode::null;
	ChamferLabel = "";
	Remark = "no remark";
	Station1PixelEquivalent = 0.1;
	Station2PixelEquivalent = 0.1;
}


GlassLayout::~GlassLayout()
{
}

void GlassLayout::write(QJsonObject& json) const
{
	Base::write(json);
	json["LayoutName"] = LayoutName;
	json["FlowDir"] = (int)FlowDir;
	json["CreateTime"] = CreateTime;
	json["GlassX"] = GlassX;
	json["GlassY"] = GlassY;
	json["GlassWidth"] = GlassWidth;
	json["GlassHeight"] = GlassHeight;
	json["GlassCoorMode"] = (int)GlassCoorMode;
	json["GlassUnit"] = (int)GlassUnit;
	json["GlassCoorDir"] = (int)GlassCoorDir;
	json["ChamferLabel"] = ChamferLabel;
	json["Remark"] = Remark;
	json["Station1PixelEquivalent"] = Station1PixelEquivalent;
	json["Station2PixelEquivalent"] = Station2PixelEquivalent;
	json["TopLeft_X"] = TopLeft_X;
	json["TopLeft_Y"] = TopLeft_Y;
	json["TopRight_X"] = TopRight_X;
	json["TopRight_Y"] = TopRight_Y;
	json["Space_X"] = Space_X;
	json["Space_Y"] = Space_Y;
	json["Row"] = Row;
	json["Col"] = Col;
	json["ModelName"] = ModelName;
	json["LayoutModelImagePath"] = LayoutModelImagePath;

	QJsonArray array;
	for (int i = 0; i < PanelLists.size(); i++)
	{
		QJsonObject panleObj;
		PanelLists.at(i).write(panleObj);
		array.append(panleObj);
	}
	json["PanelList"] = array;

}

void GlassLayout::read(const QJsonObject& json)
{
	Base::read(json);
	LayoutName = json["LayoutName"].toString();
	FlowDir  = (eFlowDir)json["FlowDir"].toInt();
	GlassUnit = (eUnit)json["GlassUnit"].toInt();
	GlassCoorDir = (eCoorDir)json["GlassCoorDir"].toInt();
	CreateTime = json["CreateTime"].toString();
	GlassX = json["GlassX"].toDouble();
	GlassY = json["GlassY"].toDouble();
	GlassWidth = json["GlassWidth"].toDouble();
	GlassHeight = json["GlassHeight"].toDouble();
	GlassCoorMode = (eCoorMode)json["GlassCoorMode"].toInt();
	ChamferLabel = json["ChamferLabel"].toString();
	Remark = json["Remark"].toString();
	Station1PixelEquivalent = json["Station1PixelEquivalent"].toDouble();
	Station2PixelEquivalent = json["Station2PixelEquivalent"].toDouble();
	TopLeft_X = json["TopLeft_X"].toDouble();
	TopLeft_Y = json["TopLeft_Y"].toDouble();
	TopRight_X = json["TopRight_X"].toDouble();
	TopRight_Y = json["TopRight_Y"].toDouble();
	Space_X = json["Space_X"].toDouble();
	Space_Y = json["Space_Y"].toDouble();
	Row = json["Row"].toInt();
	Col = json["Col"].toInt();
	ModelName = json["ModelName"].toString();
	LayoutModelImagePath = json["LayoutModelImagePath"].toString();
	if (json.contains("PanelList") && json["PanelList"].isArray())
	{
		QJsonArray maksArr = json["PanelList"].toArray();
		for (int i = 0; i < maksArr.size(); i++)
		{
			QJsonObject obj = maksArr[i].toObject();
			PanelLayout layout;
			layout.read(obj);
			PanelLists.append(layout);
		}
	}
}
