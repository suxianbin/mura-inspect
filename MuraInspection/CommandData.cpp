#include "CommandData.h"

CommandData* CommandData::instance = Q_NULLPTR;

CommandData::CommandData()
{
	m_plcCommandData.resize(100);
	m_plcCommandData.fill(0, 100);
	m_pcCommandData.resize(100);
	m_pcCommandData.fill(0, 100);
	m_pcRecipeListData.resize(64);
	m_pcRecipeListData.fill(0, 64);
}

CommandData* CommandData::getInstance()
{
	if (instance == Q_NULLPTR)
	{
		instance = new CommandData();
	}
	return instance;
}

CommandData::~CommandData()
{
}

QVector<quint16>* CommandData::getPLCommand() 
{
	return &getInstance()->m_plcCommandData;
}

QVector<quint16>* CommandData::getPCommand()
{
	return &getInstance()->m_pcCommandData;
}

QVector<quint16>* CommandData::getPCRecipeListData()
{
	return &getInstance()->m_pcRecipeListData;
}

bool CommandData::getPLCCommandLoadDone()
{
	return getInstance()->getPLCCommandFlag(0, 0);
}

bool CommandData::getPLCCommandScanRequestReply()
{
	return getInstance()->getPLCCommandFlag(0, 1);
}

bool CommandData::getPLCCommandScanDoneRequest()
{
	return getInstance()->getPLCCommandFlag(0, 2);
}

bool CommandData::getPLCCommandUnLoadRequestReply() 
{
	return getInstance()->getPLCCommandFlag(0, 3);
}

bool CommandData::getPLCCommandAutoChangeRequest()
{
	return false;
}

bool CommandData::getPLCCommandRecipeID()
{
	return false;
}

int CommandData::getPLCCameraTrigger()
{
	return 0;
}

int CommandData::getPLCStatus()
{
	return CommandData::getPLCommand()->at(12);
}

bool CommandData::getPLCCommandGlassReady()
{
	return getInstance()->getPLCCommandFlag(17, 0);
}

bool CommandData::getPLCCommandSkipGlassReply()
{
	return getInstance()->getPLCCommandFlag(17, 1);
}

bool CommandData::getPLCCommandStartReferenceReply()
{
	return getInstance()->getPLCCommandFlag(17, 2);
}

bool CommandData::getPLCCommandStartMeasureReply()
{
	return getInstance()->getPLCCommandFlag(17, 3);
}

void CommandData::setPCCommandLoadDoneReply(bool f)
{
	getInstance()->setPCCommandFlag(f, 0, 0);
}

void CommandData::setPCCommandScanRequest(bool f)
{
	getInstance()->setPCCommandFlag(f, 0, 1);
}

void CommandData::setPCCommandScanDoneReply(bool f)
{
	getInstance()->setPCCommandFlag(f, 0, 2);
}

void CommandData::setPCCommandUnloadRequest(bool f)
{
	getInstance()->setPCCommandFlag(f, 0, 3);
}

void CommandData::setCommandHeartBeat(bool f)
{
	getInstance()->setPCCommandFlag(f, 1, 0);
}

void CommandData::setPCCoaterStopUpStream(bool f)
{
	getInstance()->setPCCommandFlag(f, 3, 0);
}

void CommandData::setPCCoaterStopDownStream(bool f)
{
	getInstance()->setPCCommandFlag(f, 4, 0);
}

void CommandData::setPCMuraStop(bool f)
{
	getInstance()->setPCCommandFlag(f, 2, 0);
}

void CommandData::setPCWarningStopStream(bool f)
{
	getInstance()->setPCCommandFlag(f, 5, 0);
}

void CommandData::setPCCommandAlarmDown(bool f)
{
	getInstance()->setPCCommandFlag(f, 12, 0);
}

void CommandData::setPLCCommandAutoChangeRequest()
{
}

void CommandData::setPCCommandGlassReadyReply(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 0);
}

void CommandData::setPCCommandSkipGlass(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 1);
}

void CommandData::setPCCommandSPMEReady(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 2);
}

void CommandData::setPCCommandStartReference(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 3);
}

void CommandData::setPCCommandStartMeasure(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 4);
}

void CommandData::setPCCommandMeasurePointReply(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 5);
}

void CommandData::setPCCommandThisPointMeasureFinished(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 6);
}

void CommandData::setPCCommandUnloadGlass(bool f)
{
	getInstance()->setPCCommandFlag(f, 17, 7);
}

void CommandData::setPCCommandMeasurePointIndex(int index)
{
	getInstance()->setPCCommand(18, index);
}

void CommandData::setPCCommandMeasurePointCount(int count)
{
	getInstance()->setPCCommand(19, count);
}

void CommandData::setPCCommandHeartbeat(int beat)
{
	getInstance()->setPCCommand(1, beat);
}

void CommandData::setPCStatusStream(bool f, int Dn, int bit)
{
	getInstance()->setPCRecipeListFlag(f, Dn, bit);
}

bool CommandData::getPLCCommandReachMeasurePoint()
{
	return getInstance()->getPLCCommandFlag(17, 4);
}

bool CommandData::getPLCCommandThisPointMeasureFinishedReply()
{
	return getInstance()->getPLCCommandFlag(17, 5);
}

bool CommandData::getPLCCommandUnloadGlassReply()
{
	return getInstance()->getPLCCommandFlag(17, 6);
}

void CommandData::setPCCommandFlag(bool f, int Dn, int bit)
{
	qint16 n = m_pcCommandData[Dn];
	m_pcCommandData[Dn] = f ? n | (1 << bit) : n & ~(1 << bit);
}

void CommandData::setPCCommand(int Dn,int val)
{
	m_pcCommandData[Dn] = val;
}

bool CommandData::getPLCCommandFlag(int Dn, int bit) 
{
	return (getInstance()->m_plcCommandData[Dn] >> bit & 1) ? true : false;
}

void CommandData::setPCRecipeListFlag(bool f, int Dn, int bit)
{
	qint16 n = m_pcRecipeListData[Dn];
	f ? n = n | (1 << bit) : n = n & ~(1 << bit);
	m_pcRecipeListData[Dn] = n;
}

void CommandData::setPCCommandStation2LoadDoneReply(bool f)
{
	getInstance()->setPCCommandFlag(f, 40, 0);
}

void CommandData::setPCCommandStation2ScanRequest(bool f)
{
	getInstance()->setPCCommandFlag(f, 40, 1);
}

void CommandData::setPCCommandStation2ScanDoneReply(bool f)
{
	getInstance()->setPCCommandFlag(f, 40, 2);
}

void CommandData::setPCCommandStation2UnloadRequest(bool f)
{
	getInstance()->setPCCommandFlag(f, 40, 3);
}

bool CommandData::getPLCCommandStation2LoadDone()
{
	return getInstance()->getPLCCommandFlag(40, 0);
}

bool CommandData::getPLCCommandStation2ScanRequestReply()
{
	return getInstance()->getPLCCommandFlag(40, 1);
}

bool CommandData::getPLCCommandStation2ScanDoneRequest()
{
	return getInstance()->getPLCCommandFlag(40, 2);
}

bool CommandData::getPLCCommandStation2UnLoadRequestReply()
{
	return getInstance()->getPLCCommandFlag(40, 3);
}
