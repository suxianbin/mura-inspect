#include "QWndSpmeChart.h"

QWndSpmeChart::QWndSpmeChart(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	m_pCustomPlot = new QCustomPlot(this);
	m_pCustomPlot->resize(1200, 340);
	InitPlot();
}

QWndSpmeChart::~QWndSpmeChart()
{}

void QWndSpmeChart::InitPlot()
{
	QCPAxis* xAxis = m_pCustomPlot->xAxis;//x轴
	QCPAxis* yAxis = m_pCustomPlot->yAxis;//y轴
	QPen pen;
	pen.setColor(Qt::black);
	QFont f;
	f.setPointSize(18);
	pen.setWidth(2);
	xAxis->setBasePen(pen);
	xAxis->setTickPen(pen);
	xAxis->setTickLength(10);
	xAxis->setTickLabelFont(f);
	xAxis->setSubTickLength(6);
	xAxis->setSubTickPen(pen);
	xAxis->setLabelColor(Qt::black);
	xAxis->setTickLabelColor(Qt::black);
	xAxis->setLabelFont(f);
	yAxis->setBasePen(pen);
	yAxis->setTickPen(pen);
	yAxis->setTickLength(10);
	yAxis->setSubTickLength(6);
	yAxis->setSubTickPen(pen);
	yAxis->setLabelColor(Qt::black);
	yAxis->setTickLabelColor(Qt::black);
	yAxis->setLabelFont(f);
	yAxis->setLabel("Meassure Result");
	m_pCustomPlot->legend->setVisible(true);
	QFont legendFont = font();
	legendFont.setPointSize(10);
	m_pCustomPlot->legend->setFont(legendFont);
	m_pCustomPlot->legend->setSelectedFont(legendFont);
	m_pCustomPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items
	m_pCustomPlot->setBackground(QColor(221, 221, 221));
	// 设置图例
	m_pCustomPlot->legend->setVisible(true);
	m_pCustomPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop | Qt::AlignRight);

	QVector<double> ticks;
	QVector<QString> labels;
	for (int i = 0; i < 100; i++)
	{
		ticks.push_back(i);
	}
	QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
	textTicker->addTicks(ticks, labels);
	xAxis->setTicker(textTicker);        // 设置为文字轴
	xAxis->setTickLabelRotation(60);     // 轴刻度文字旋转60度
	xAxis->setSubTicks(true);           // 不显示子刻度
	xAxis->setTickLength(0, 4);          // 轴内外刻度的长度分别是0,4,也就是轴内的刻度线不显示
	xAxis->setRange(0, 108);             // 设置x轴范围
	yAxis->setRange(0, 30);

	AddPlotGraph(QColor(147, 68, 255), u8"透过率");
	AddPlotGraph(QColor(33, 103, 251), u8"膜  厚");
	//AddPlotGraph(QColor(53, 217, 255), u8"通光孔");
	//AddPlotGraph(QColor(111, 192, 55), u8"侧面检");
	//AddPlotGraph(QColor(252, 233, 45), u8"背面检");
	//AddPlotGraph(QColor(254, 138, 1), u8"下料机械手");
	//AddPlotGraph(QColor(255, 12, 2), u8"转 盘");

	m_pCustomPlot->replot();
}

void QWndSpmeChart::AddPlotGraph(QColor color, QString text)
{
	QCPGraph* graph = m_pCustomPlot->addGraph();
	QPen p = QPen(QColor(color));
	p.setWidth(2);
	graph->setPen(p);
	graph->setName(text);
	graph->setLineStyle(QCPGraph::lsLine);
	graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, QColor(color), 6));
	QVector<double> keys1;
	QVector<double> keys2;
	QVector<double> values1;
	QVector<double> values2;
	int numValues = 100; // 要生成的值的数量

	int A = 15, B = 20;
	for (int i = 0; i < numValues; ++i) {
		int n = QRandomGenerator::global()->generateDouble() * (B - A) + A;
		keys1.push_back(i);
		keys2.push_back(i);
		values1.push_back(n);
	}
	// 设置数据并显示图表
	graph->setData(keys1, values1);
}