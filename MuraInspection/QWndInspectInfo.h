#pragma once

#include <QWidget>
#include "ui_QWndInspectInfo.h"
#include "QWndDefectTable.h"
#include "QWndSpmeResult.h"
#include "QWndSpmeChart.h"
#include "QWndDefectGraph.h"

class QWndInspectInfo : public QWidget
{
	Q_OBJECT

public:
	QWndInspectInfo(QWidget *parent = nullptr);
	~QWndInspectInfo();
	void updateOrgFlawTable(int station);
	void updateDefectTable(int station);
	QWndDefectTable* getDefectTable(int idx);
private:
	Ui::QWndInspectInfoClass ui;

	QWndDefectTable* m_pDefectTable1 = nullptr;
	QWndDefectTable* m_pDefectTable2 = nullptr;
	QWidget* m_pPlatformLog = nullptr;
	QWndSpmeResult* m_pSpme = nullptr;
	QWndSpmeChart* m_pSpmeChart = nullptr;
	QWidget* m_pSecs = nullptr;
	QWndDefectTable* m_pOrgDefect1 = nullptr;
	QWndDefectTable* m_pOrgDefect2 = nullptr;
	QWndDefectGraph* m_pDefectGraph = nullptr;

	void hiddenAllWidget();
	void setAllButtonDefaultStyleSheet();
	void setButtonStyleSheet(QPushButton* button);
private slots:
	void on_pb_Station1DefectClicked();
	void on_pb_Station2DefectClicked();
	void on_pb_PlatformLogClicked();
	void on_pb_SpmeClicked();
	void on_pb_SpmeChartClicked();
	void on_pb_SecsClicked();
	void on_pb_OrgDefect1Clicked();
	void on_pb_OrgDefect2Clicked();
	void on_pb_DefectGraphClicked();
};
