#include "PanelLayout.h"
#include <QJsonArray>

void PanelLayout::write(QJsonObject& json) const
{
	json["serial"] = serial;
	json["panelName"] = panelName;
	json["panelModel"] = panelModel;
	json["x"] = x;
	json["y"] = y;
	json["width"] = width;
	json["height"] = height;
	json["contract"] = contract;
	json["radius"] = radius;
	json["topDst"] = topDst;
	json["botDst"] = botDst;
	json["leftDst"] = leftDst;
	json["rightDst"] = rightDst;

	QJsonArray array;
	for (int i = 0; i < panelMaskList.size(); i++)
	{
		QJsonObject maskObj;
		panelMaskList.at(i).write(maskObj);
		array.append(maskObj);
	}
	json["MaskList"] = array;
}

void PanelLayout::read(const QJsonObject& json)
{
	serial = json["serial"].toInt();
	panelName = json["panelName"].toString();
	panelModel = json["panelModel"].toString();
	x = json["x"].toDouble();
	y = json["y"].toDouble();
	width = json["width"].toDouble();
	height = json["height"].toDouble();
	contract = json["contract"].toDouble();
	radius = json["radius"].toDouble();
	topDst = json["topDst"].toDouble();
	botDst = json["botDst"].toDouble();
	leftDst = json["leftDst"].toDouble();
	rightDst = json["rightDst"].toDouble();

	if (json.contains("MaskList") && json["MaskList"].isArray())
	{

		QJsonArray maksArr = json["MaskList"].toArray();
		for (int i = 0; i < maksArr.size(); i++)
		{
			QJsonObject obj = maksArr[i].toObject();
			PanelMask mask;
			mask.read(obj);
			panelMaskList.append(mask);
		}
	}
}
