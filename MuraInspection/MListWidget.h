#pragma once
#include <QObject>
#include <QScrollBar>
#include <QListWidget>
#include <QWheelEvent>

class MListWidget : public QListWidget
{
	Q_OBJECT

public:
	MListWidget(QWidget  *parent);
	~MListWidget();

signals:
	void SignalSliderUp();
	void SignalSliderDown();

private slots:
	void wheelEvent(QWheelEvent *event);

private:
	QScrollBar* m_vscrollBar;
};
