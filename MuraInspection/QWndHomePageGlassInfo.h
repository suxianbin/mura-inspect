#pragma once

#include <QWidget>
#include "ui_QWndHomePageGlassInfo.h"
#include "QWndCheckInfo.h"
#include "QWndCheckSum.h"
#include "QWndDefectDeputyImage.h"
#include "QWndSelectDefect.h"
#include <QVBoxLayout>

class QWndHomePageGlassInfo : public QWidget
{
	Q_OBJECT
signals:
	void sig_DefectSelectChanged(QString, bool);
public:
	QWndHomePageGlassInfo(QWidget *parent = nullptr);
	~QWndHomePageGlassInfo();
	void updateCheckInfo();
private:
	Ui::QWndHomePageGlassInfoClass ui;

	QVBoxLayout* m_pMainDispFrameLayout = nullptr;
	//QWndCheckInfo* m_pCheckInfo;
	QWndCheckSum* m_pCheckSum;
	QWndDefectDeputyImage* m_pDeputyImg;
	QWndSelectDefect* m_pSelectDefect;
	
	void hiddenAllWidget();
	void setAllButtonDefaultStyleSheet();
	void setButtonStyleSheet(QPushButton* button);
private slots:
	void on_pb_DeputyDefectImgClicked();
	void on_pb_CheckSumClicked();
	void on_pb_SelectDefectClicked();

};
