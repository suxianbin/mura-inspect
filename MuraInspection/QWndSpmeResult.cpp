﻿#include "QWndSpmeResult.h"
#include <QDateTime>

QWndSpmeResult::QWndSpmeResult(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	InitTable();

	//EventCenter::registerReceiver(this, {
	//	SPMETestFinishedEvent::type()
	//	});
}

QWndSpmeResult::~QWndSpmeResult()
{}

bool QWndSpmeResult::event(QEvent * ev)
{
	//auto eventType = ev->type();
	//if (eventType == SPMETestFinishedEvent::type())
	//{
	//	SPMETestFinishedEvent* newProgramEvent = static_cast<SPMETestFinishedEvent*>(ev);
	//	if (newProgramEvent->TypeIndex() == 4)
	//	{
	//		ui.tableWidget->clear();
	//	}
	//	else
	//	{
	//		int index = newProgramEvent->PointIndex();
	//		if (index < 1 || index>20)
	//		{
	//			return QWidget::event(ev);
	//		}
	//		QTableWidgetItem* item0 = new QTableWidgetItem(newProgramEvent->GetGlassID());
	//		item0->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 0, item0);
	//		QTableWidgetItem* item1 = new QTableWidgetItem(QString::number(newProgramEvent->xPos()));
	//		item1->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 1, item1);
	//		QTableWidgetItem* item2 = new QTableWidgetItem(QString::number(newProgramEvent->yPos()));
	//		item2->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 2, item2);
	//		QTableWidgetItem* item3 = new QTableWidgetItem(QString::number(newProgramEvent->TypeIndex()));
	//		item3->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 3, item3);
	//		QTableWidgetItem* item4 = new QTableWidgetItem(QString::number(newProgramEvent->ThicknessValue()));
	//		item4->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 4, item4);
	//		QTableWidgetItem* item5 = new QTableWidgetItem(QString::number(newProgramEvent->xValue()));
	//		item5->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 5, item5);
	//		QTableWidgetItem* item6 = new QTableWidgetItem(QString::number(newProgramEvent->yValue()));
	//		item6->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 6, item6);
	//		QTableWidgetItem* item7 = new QTableWidgetItem(QString::number(newProgramEvent->YValue()));
	//		item7->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 7, item7);
	//		QTableWidgetItem* item8 = new QTableWidgetItem(QString::number(newProgramEvent->OdValue()));
	//		item8->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 8, item8);
	//		QTableWidgetItem* item9 = new QTableWidgetItem(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz"));
	//		item9->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(index - 1, 9, item9);
	//	}
	//	return true;
	//}
	return QWidget::event(ev);
}

void QWndSpmeResult::InitTable()
{
	ui.tableWidget->clear();
	QStringList headers;
	headers << "GlassID" << "X" << "Y" << "type" << "Thickness" << "Color_x" << "Color_y" << "Color_Y" << "OD" << "Time";
	ui.tableWidget->setColumnCount(headers.size());
	ui.tableWidget->setHorizontalHeaderLabels(headers);
	ui.tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tableWidget->setAlternatingRowColors(true); // 隔行变色
	ui.tableWidget->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tableWidget->setRowCount(20);
	ui.tableWidget->setColumnWidth(0, 180);
	for (int i = 1; i < headers.size(); i++)
	{
		ui.tableWidget->setColumnWidth(i, 90);
	}
	// 设置列宽自适应
	//for (int col = 0; col < ui.tableWidget->columnCount(); ++col) {
	//	ui.tableWidget->horizontalHeader()->setSectionResizeMode(col, QHeaderView::ResizeToContents);
	//}

}
