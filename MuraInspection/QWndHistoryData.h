#pragma once

#include <QWidget>
#include "ui_QWndHistoryData.h"
#include "QWndGlassResult.h"

class QWndHistoryData : public QWidget
{
	Q_OBJECT

public:
	QWndHistoryData(QWidget *parent = nullptr);
	~QWndHistoryData();
	
private:
	Ui::QWndHistoryDataClass ui;
	QWndGlassResult* m_pGlassResult;


};
