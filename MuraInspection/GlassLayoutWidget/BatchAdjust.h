#pragma once
#pragma execution_character_set("UTF-8")

#include <QDoubleSpinBox>
#include <QWidget>
#include <QDialog>
#include "ui_BatchAdjust.h"
#include "PanelLayout.h"

class BatchAdjust : public QWidget
{
	Q_OBJECT
signals:
	void sig_changed();
	void sig_save();
	void sig_cancel();
public:
	BatchAdjust(QWidget *parent = Q_NULLPTR);
	~BatchAdjust();
	void showPanelLayout(const QVector<PanelLayout>& layoutList,const QList<int>& indexList,const qreal& _pixEquivalent);

	QVector<PanelLayout> getChangedPanelLayout();
	QVector<PanelLayout> getOrgPanelLayout();

private:
	Ui::BatchAdjust ui;
	QList<int> m_indexList;
	QVector<PanelLayout> m_orgList;
	QVector<PanelLayout> m_changedList;

	qreal m_pixEquivalent = 1;
private slots:
	void slot_changed(double value);
};
