#pragma once

#include <QStyledItemDelegate>
#include <QItemDelegate>
#include <QLineEdit>

class DoubleDelegate : public QStyledItemDelegate
{
	Q_OBJECT
signals:
	void sig_valueChanged() const;
public:
	explicit DoubleDelegate(QWidget* parent = Q_NULLPTR);
	~DoubleDelegate() override;
	
	//第一步：创建一个Widget作为编辑器
	QWidget* createEditor(QWidget* parent,
	                      const QStyleOptionViewItem& option,
	                      const QModelIndex& index) const override;

	//第二步：编辑器的显示位置
	void updateEditorGeometry(QWidget* editor,
	                          const QStyleOptionViewItem& option,
	                          const QModelIndex& index) const override;

	//第三步：初始化显示数据
	void setEditorData(QWidget* editor, const QModelIndex& index) const override;
	
	// 第四步：用户完成编辑、编辑器被关闭时，提供数据到model
	void setModelData(QWidget* editor,
	                  QAbstractItemModel* model,
	                  const QModelIndex& index) const override;


private:
	qreal m_bottom = -99999;
	qreal m_top    = 999999;

};
