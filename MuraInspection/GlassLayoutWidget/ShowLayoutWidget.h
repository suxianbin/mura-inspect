#pragma once
#ifndef _SHOWLAYOUTWIDGET_FILE_H_
#define _SHOWLAYOUTWIDGET_FILE_H_
#pragma execution_character_set("UTF-8")

#include <QFileDialog>
#include <QWidget>
#include <QCursor>
#include <QStackedLayout>
#include <QToolTip>
#include <QMenu>
#include <QAction>
#include <QGraphicsItem>
#include <QMouseEvent>
#include "ROIDisplay.h"
#include "ui_ShowLayoutWidget.h"
#include "GlassLayout.h"
#include "Defect.h"
#include "FlowDirectionItem.h"
#include "DrawRectItem.h"
#include "PanelItem.h"
#include "ROIRectItem.h"
#include "FlawItem.h"
#include "GlassItem.h"
//#include "ROIAbstractBaseItem.h"
//#include "LineMeasureItem.h"
//#include "AreaMeasureItem.h"
//#include "ImageEnhanceItem.h"
//#include "ImageEnhanceWidget.h"
#include "IgnoreItem.h"

class GrayLevelMeasureWidget;

class ShowLayoutWidget : public QWidget
{
	Q_OBJECT
signals:
	void sig_LayoutChecked(bool);
	void sig_PanelChecked(bool);
	void sig_DefectChecked(bool);
public:
	explicit ShowLayoutWidget(QWidget *parent = Q_NULLPTR);
	~ShowLayoutWidget() override;
	
	void loadImage(const cv::Mat& mat);
	void loadImage(const QImage& qmat);
	void loadImageMap(const QPixmap& qpixmap);
	/********************************
	 * 功能 根据名称显示相关布局
	 * 输入 layoutName
	 ********************************/
	void showLayoutByName(const QString& layoutName, bool bshowlayout = true, bool bhideIgnore = true);
	void showLayout(GlassLayout* layout, bool bhideIgnore = true);

	/********************************
	 * 功能 清除布局
	 ********************************/
	void clearLayout();

	/********************************
	 * 功能 调整合适的窗口大小
	 ********************************/
	void suitableView();

	ROIDisplay* getRoiDisplay();

	/********************************
	 * 功能 显示缺陷
	 * 输入 flaws
	 * 输入 type 0 点  1 最小矩形 2 轮廓 3不显示
	 ********************************/
	void showFlaws(const QVector<Defect>& flaws);
	void showFlaws(const QVector<Defect>& flaws, QPointF orginPoint, int type = 0);

	/********************************
	 * 功能 选择缺陷
	 * 输入 index
	 * 返回
	 ********************************/
	bool selectFlawByIndex(int index);

	/*
	* 缩放到合适的大小到相应的缺陷flaw //缺陷显示大小缩放
	*/
	void suitableToFlaw(int index);

	/********************************
	 * 功能 清除缺陷
	 ********************************/
	void clearFlaws();

	/**
	 * \brief 设置是否可以选择Panel
	 * \param f
	 */
	void setCanSelectPanel(bool f = false, bool onlyOne = true);

	/**
	 * \brief 获取选中的panelData
	 * \param f 是否有选中
	 * \return
	 */
	PanelLayout getSelectPanelData(bool* f = Q_NULLPTR);


	/**
	 * \brief 设置标尺是否可见
	 * \param visable
	 */
	void setRuleVisable(bool visable);

	/**
	 * \brief 设置鼠标是否可用,视图是否可移动
	 * \param enable
	 */
	void setMouseEnable(bool enable);

	/**
	 * \brief 选中panel 根据panelIds
	 * \param _ids
	 */
	void selectedPanelByIds(const QStringList& _ids);

	QList<PanelLayout*> getPanelItems();


	void setPanelChecked(bool);
	void setDefectChecked(bool);
	void setLayoutChecked(bool);
	void setCheckBoxVisible(bool);
private:
	Ui::ShowLayoutWidget ui;

	cv::Mat m_showMat;
	QVector<QGraphicsItem*> m_layoutitem;
	QVector<PanelItem*> m_panelItem;
	QList<FlawItem*> m_flawItemList;
	//QVector<IgnoreItem*> m_ignoreItem;
	FlowDirectionItem* m_flowDir = nullptr;
	bool m_isShowLayout = true;
	bool m_showId = true;
	bool m_isCanSelect = false;
	bool m_isSelectOnlyOne = true;
	QString m_showLayoutName;
	QMenu m_menu;
	QAction m_lineMeasure;			//线状检测
	QAction m_areaMeasure;			//面状检测
	QAction m_imageEnhanceAction;	//图像增强
	QAction m_imageBrightnessAction;	//图像亮度调节
	QAction m_imageEnhancePartAction;//局部图像增强
	QAction m_imageAction;			//显示原图
	QAction m_cancleMeasureAction;	//取消测量
	QAction m_cancleEnhanceAction;	//取消增强

	//ImageEnhanceWidget* m_imageEnhance = Q_NULLPTR;	//图像增强
	GrayLevelMeasureWidget*m_grayLevelWidget;
	ROIAbstractBaseItem* m_measureItem = Q_NULLPTR;	//测量item
	//ImageEnhanceItem* m_ImageEnhanceItem = Q_NULLPTR;
private:
	void resizeEvent(QResizeEvent* event) override;

	void mouseMoveEvent(QMouseEvent* event) override;

	void mousePressEvent(QMouseEvent *event) override;

	void keyPressEvent(QKeyEvent* event) override;
	void showToolTip();
	void contextMenuEvent(QContextMenuEvent* event) override;
	//bool eventFilter(QObject* watched, QEvent* event) override;
private slots:
	/********************************
	 * 功能 idCheck stateChanged
	 * 输入 _state
	 ********************************/
	void slot_idStateChanged(bool _state);

	void slot_layoutStateChanged(bool _checked);

	void slot_defecftStateChanged(bool _checked);

	void on_roiComlete(QRectF rect);

	void slot_panelSelected();

	void slot_flawSelected();

	void slot_lineMeasureAction();
	void slot_areaMeasureAction();
	void slot_imageEnhanceAction();
	void slot_imageEnhancePartAction();
	void slot_imageBrightnessAction();
	void slot_cancleMeasure();
	void slot_cancleEnhance();
	void slot_colseEnhanceWidget();//释放图像增强弹框

	void loadEnhanceIamg(const cv::Mat& mat);
	void loadEnhanceIamgMap(const QPixmap& qpixmap);
	void slot_showIamg();

	void slot_sendRect(cv::Rect roi);//接收坐标信息

	void slot_showCoord(QString str);//显示坐标信息

	/********************************
	 * 功能 PixMap转Mat
	 * 输入 Pixmap
	 * 返回
	 ********************************/
	static cv::Mat qPixmap2Mat(const QPixmap& _pixmap);
};
#endif