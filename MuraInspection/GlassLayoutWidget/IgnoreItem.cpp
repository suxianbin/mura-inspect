#include "IgnoreItem.h"
#include <QGraphicsSceneMouseEvent>

IgnoreItem::IgnoreItem(QGraphicsItem *parent)
	:ROIAbstractBaseItem()
{
	setFlag(ItemIsFocusable, true);
	setFlag(ItemIsMovable, false);
}

IgnoreItem::~IgnoreItem()
{
}

//void IgnoreItem::setIgnoreData(const QPointF& originP, const IgnoreLayout & pignore, const qreal & _piex)
//{
//	QPointF p(originP.x() + pignore.topLeft_x / _piex, originP.y() + pignore.topLeft_y / _piex);
//
//	addPoint(p);
//	p.setX(p.rx() + pignore.Mask_Width / _piex);
//	p.setY(p.ry() + pignore.Mask_Length / _piex);
//	addPoint(p);
//}

void IgnoreItem::setShowId(bool f)
{
	m_idFlag = f;
}

void IgnoreItem::setCanSelect(bool f)
{
	if (f != m_isCanSelect)
	{
		m_isCanSelect = f;
		if (m_isCanSelect)
		{
			QPen p = pen();
		}
		update();
	}
	if (!f)
	{
		m_isSelect = false;
	}
}

void IgnoreItem::setSelect(bool f)
{
	if (f)
	{
		QPen p = pen();
		p.setWidth(4);
		p.setColor("#CA5100");
		setPen(p);
	}
	else
	{
		QPen p = pen();
		p.setWidth(2);
		p.setColor(Qt::green);
		setPen(p);
	}
	update();
	m_isSelect = f;
}

bool IgnoreItem::getSelected()
{
	return m_isSelect;
}
//
//PanelLayout PanelItem::getPanelData()
//{
//	return m_data;
//}

void IgnoreItem::addPoint(const QPointF & _point)
{
	if (m_addPointIndex == 0) {
		setPos(_point);
		m_addPointIndex++;
	}
	else if (m_addPointIndex == 1) {
		QPointF p = mapFromScene(_point);
		if (p.x() >= 0 && p.y() >= 0) {
			//getItemByIndex(1)->setPos(p);
			m_data.height = p.y();
			m_data.width = p.x();
			m_addPointIndex++;
		}
	}
}

void IgnoreItem::paintToImage(QPainter* painter)
{
	QPen p;
	p.setColor(Qt::green);
	p.setWidth(20);
	p.setStyle(Qt::SolidLine);
	painter->setPen(p);

	QFont f("����", m_data.height / 1.5);

	// QFontMetrics metrics = painter.fontMetrics();
	painter->setFont(f);
	//painter->drawText(QRect(m_data.x, m_data.y, m_data.width, m_data.length), Qt::AlignCenter, m_data.id);
	painter->drawRect(m_data.x, m_data.y, m_data.width, m_data.height);
}

// QList<QAction*> PanelItem::getActions()
// {
// 	QList<QAction*> res;
// 	res << m_delAction;
// 	return res;
// }

void IgnoreItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF point = event->scenePos();
	if (index == 0)
	{
		m_data.x = point.x();
		m_data.y = point.y();
		setPos(point);
	}
	else {
		QPointF p = mapFromScene(point);
		if (p.x() >= 0 && p.y() >= 0)
		{
			getItemByIndex(1)->setPos(p);
			m_data.height = p.y();
			m_data.width = p.x();
		}
	}
}

QPainterPath IgnoreItem::getMyShap() const
{
	QPainterPath path;
	if (m_addPointIndex > 0 && m_state != NomaleState)
	{
		path.addRect(m_pGlass->GlassX, m_pGlass->GlassY, m_data.width, m_data.height);
	}
	return path;
}

void IgnoreItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	m_pen.setColor(Qt::yellow);
	m_pen.setWidth(2);
	m_pen.setStyle(Qt::SolidLine);

	m_brush.setColor(Qt::yellow);
	m_brush.setStyle(Qt::BDiagPattern);
	painter->setPen(m_pen);
	painter->setBrush(m_brush);

	painter->drawRect(0, 0, m_data.width, m_data.height);
	painter->drawRect(m_data.contract, m_data.contract, m_data.width - m_data.contract * 2, m_data.height - m_data.contract * 2);

	//ROIAbstractBaseItem::paint(painter, option, widget);

	//DrawRectItem::paint(painter, option, widget);
}

void IgnoreItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
	if (event->button() == Qt::LeftButton)
	{
		if (m_isCanSelect)
		{
			emit sig_isClicked();
		}
	}
	//DrawRectItem::mouseReleaseEvent(event);
}

void IgnoreItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
	//DrawRectItem::mouseMoveEvent(event);
}

void IgnoreItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	/*if (getState() == NomaleState && event->button() == Qt::LeftButton&&m_selectAbel) {
		setState(SelectState);
	}
	ROIAbstractBaseItem::mousePressEvent(event);*/
	//DrawRectItem::mousePressEvent(event);
}