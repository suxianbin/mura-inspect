#include "PanelItem.h"
#include <qmath.h>
#include <QGraphicsSceneMouseEvent>

PanelItem::PanelItem(QGraphicsItem *parent)
	:ROIAbstractBaseItem()
{
	setFlag(ItemIsFocusable, true);
	setFlag(ItemIsMovable, true);
	//setColor(Qt::blue);

	m_delAction = new QAction(this);
	m_delAction->setText("删除此项");

	/*connect(m_delAction, &QAction::triggered, this, [=](bool f)
	{
		emit sig_deletedActionClicked();
	});*/

	m_pen.setColor(Qt::green);
	m_pen.setWidth(1);
	m_pen.setStyle(Qt::SolidLine);

	m_brush.setColor(Qt::yellow);
	m_brush.setStyle(Qt::BDiagPattern);

	setPen(m_pen);
	setBrush(m_brush);

	setFlag(ItemIsFocusable, true);
}

PanelItem::~PanelItem()
{
}



void PanelItem::setPanelData(const PanelLayout& _panel)
{
	m_data = _panel;
	if(m_addPointIndex==0)
	{
		addPoint(QPointF(m_data.x / m_data.equivalent, m_data.y / m_data.equivalent));
		addPoint(QPointF(m_data.x / m_data.equivalent + m_data.width / m_data.equivalent, m_data.y / m_data.equivalent + m_data.height / m_data.equivalent));
	}
	else
	{
		setPos(QPointF(m_data.x / m_data.equivalent, m_data.y / m_data.equivalent));
		QPointF p(m_data.x / m_data.equivalent + m_data.width / m_data.equivalent, m_data.y / m_data.equivalent + m_data.height / m_data.equivalent);
		if (p.x() >= 0 && p.y() >= 0)
		{
			getItemByIndex(1)->setPos(p);
		}
	}
	setState(NomaleState);
	//setRect(_panel.x, _panel.y, _panel.width, _panel.length);
}

void PanelItem::setShowId(bool f)
{
	m_idFlag = f;
}

void PanelItem::setCanSelect(bool f)
{
	if (f != m_isCanSelect)
	{
		m_isCanSelect = f;
		if (m_isCanSelect)
		{
			QPen p = pen();
		}
		update();
	}
	if(!f)
	{
		m_isSelect = false;
	}
}

void PanelItem::setSelect(bool f)
{
	if(f)
	{
		QPen p = pen();
		p.setWidth(4);
		p.setColor("#CA5100");
		setPen(p);
	}
	else
	{
		QPen p = pen();
		p.setWidth(2);
		p.setColor(Qt::green);
		setPen(p);
	}
	update();
	m_isSelect = f;
}

bool PanelItem::getSelected()
{
	return m_isSelect;
}

PanelLayout* PanelItem::getPanelData()
{
	return &m_data;
}

void PanelItem::addPoint(const QPointF & _point)
{
	if (m_addPointIndex == 0)
	{
		HandleControlItem* item0 = new HandleControlItem(QPointF(0, 0), this, 0, eControlItemType::Base);
		addControlItem(item0);
		setPos(_point);
		HandleControlItem* item1 = new HandleControlItem(QPointF(0, 0), this, 1);
		addControlItem(item1);
		m_addPointIndex++;
	}
	else if (m_addPointIndex == 1)
	{
		QPointF p = mapFromScene(_point);
		if (p.x() >= 0 && p.y() >= 0) 
		{
			getItemByIndex(1)->setPos(p);
			//m_data.height = p.y();
			//m_data.width = p.x();
			setState(SelectState);
			m_addPointIndex++;
		}
	}
}

void PanelItem::paintToImage(QPainter* painter)
{
	QPen p;
	p.setColor(Qt::green);
	p.setWidth(20);
	p.setStyle(Qt::SolidLine);
	painter->setPen(p);

	QFont f("宋体", m_data.height / 1.5);

	// QFontMetrics metrics = painter.fontMetrics();
	painter->setFont(f);
	//painter->drawText(QRect(m_data.x, m_data.y, m_data.width, m_data.length), Qt::AlignCenter, m_data.id);
	painter->drawRect(m_data.x, m_data.y, m_data.width, m_data.height);

}

void PanelItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{

	QPointF point = event->scenePos();
	if (index == 0) {
		m_data.x = point.x();
		m_data.y = point.y();
		setPos(point);
	}
	else {
		QPointF p = mapFromScene(point);
		if (p.x() >= 0 && p.y() >= 0) {
			getItemByIndex(1)->setPos(p);
			m_data.height = p.y();
			m_data.width = p.x();
		}
	}
}

QPainterPath PanelItem::getMyShap() const
{
	QPainterPath path;
	if (m_addPointIndex > 0) {
		path.addRect(m_GlassX, m_GlassY, m_data.width, m_data.height);
	}
	return path;
}

//void PanelItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
//{
//	if (m_addPointIndex < 1) {
//		return;
//	}
//
//	painter->setBrush(m_brush);
//	painter->setPen(pen());
//
//
//	if (m_addPointIndex == 1) {
//		QPointF point = mapFromScene(m_mouserPos);
//		if (point.x() >= 0 && point.y() >= 0) {
//			//第三点缓存位置
//			getItemByIndex(1)->setPos(point);
//			m_width = point.x();
//			m_high = point.y();
//		}
//		else {
//			return;
//		}
//	}
//	painter->drawRect(0, 0, m_width, m_high);
//	ROIAbstractBaseItem::paint(painter, option, widget);
//}

void PanelItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if (m_idFlag)
	{
		QFont f("宋体", m_data.height / (2.0 * 10.0));
		// QFontMetrics metrics = painter.fontMetrics();
		painter->setFont(f);
		painter->setPen(pen());
		//绘制id到左上角
		QRect r(0, 0, m_data.width / 10, m_data.height / 10);
		painter->drawText(r, Qt::AlignCenter, QString::number(m_data.serial));
	}
	//painter->drawRect(0, 0, m_data.width, m_data.height);

	//  [2024/7/10 14:46 zaiyuan.li]
	//////////////////////////////////////////////////////////////////////////
	painter->setRenderHint(QPainter::Antialiasing); // 启用抗锯齿
	QRect rect1(0, 0, m_data.width, m_data.height); // 矩形区域
	//int radius = 20; // 圆角半径
	
	//painter->drawRoundedRect(rect1, m_data.radius, m_data.radius);
	//painter->drawRoundedRect(rect1, 0, 0);

	//////////////////////////////////////////////////////////////////////////

	QRect rect2(m_data.leftDst / m_data.equivalent, m_data.topDst / m_data.equivalent,
		m_data.width/m_data.equivalent - m_data.leftDst / m_data.equivalent - m_data.rightDst / m_data.equivalent,
		m_data.height/ m_data.equivalent - m_data.topDst / m_data.equivalent - m_data.botDst / m_data.equivalent); // 矩形区域	
	//painter->drawRect(rect2);
	painter->drawRoundedRect(rect2, m_data.radius / m_data.equivalent, m_data.radius / m_data.equivalent);

	
	///*
	//////////////////////////////////////////////////////////////////////////
	//  [2024/7/26 16:36 zaiyuan.li]
	painter->save(); // 保存当前绘图状态
	QPen pen;
	pen.setWidth(1);
	pen.setColor("#0000FF");
	painter->setPen(pen);

	// 设置填充颜色为红色
	QBrush brush("#FFA500", Qt::FDiagPattern);
	painter->setBrush(brush);

	// 示例：转换坐标系，可以根据需要调整 dx 和 dy
	//painter->translate(m_x, m_y); // 将坐标系原点向右下角移动 10 像素

	//for (auto itemIgnore : m_data.panelMaskList)
	//{
	//	if (itemIgnore.type == eMaskType::Polygon)
	//	{
	//		QVector<QPointF> points = itemIgnore.polygon.vertices;
	//		QVector<double> cornerRadius = itemIgnore.polygon.vecRadius;

	//		QPainterPath path;
	//		int pointCount = points.size();
	//		for (int i = 0; i < pointCount; ++i)
	//		{
	//			QPointF current = points[i] / scale;
	//			QPointF next = points[(i + 1) % pointCount] / scale;
	//			QPointF prev = points[(i - 1 + pointCount) % pointCount] / scale;

	//			// 向量
	//			QPointF v1 = current - prev;
	//			QPointF v2 = next - current;

	//			// 归一化向量
	//			qreal len1 = qSqrt(v1.x() * v1.x() + v1.y() * v1.y());
	//			qreal len2 = qSqrt(v2.x() * v2.x() + v2.y() * v2.y());
	//			QPointF unitV1 = v1 / len1;
	//			QPointF unitV2 = v2 / len2;

	//			// 计算内切点
	//			QPointF p1 = current - unitV1 * cornerRadius[i] / scale;
	//			QPointF p2 = current + unitV2 * cornerRadius[i] / scale;

	//			if (i == 0) {
	//				path.moveTo(p1);
	//			}
	//			else {
	//				path.lineTo(p1);
	//			}

	//			// 二次贝赛斯曲线 [2024/7/26 14:48 zaiyuan.li]
	//			// 计算控制点
	//			path.quadTo(current, p2);

	//		}
	//		path.closeSubpath();
	//		// 绘制路径
	//		painter->drawPath(path);
	//	}
	//	else
	//	{

	//		QRect recttest(itemIgnore.x / scale, itemIgnore.y / scale, itemIgnore.width / scale, itemIgnore.height / scale); // 矩形区域
	//		painter->drawRoundedRect(recttest, itemIgnore.radius / scale, itemIgnore.radius / scale);
	//	}
	//}
	//painter->restore(); // 恢复绘图状态
	//pen.setColor(Qt::green);
	//painter->setPen(pen);
	//////////////////////////////////////////////////////////////////////////
	//*/
	//  [2024/7/10 16:33 zaiyuan.li]
	////////////////////////////////////////////////////////////////////////// 
	//// 创建路径并添加两个矩形
	//QPainterPath path;
	//path.addRoundedRect(rect1, radius, radius);
	//path.addRect(rect2);
	//// 设置填充颜色
	//painter->setBrush(QBrush(Qt::blue,Qt::BDiagPattern));
	//painter->setPen(Qt::NoPen); // 不需要边框
	//// 填充路径
	//painter->drawPath(path);
	//////////////////////////////////////////////////////////////////////////

	//ROIAbstractBaseItem::paint(painter, option, widget);

	//DrawRectItem::paint(painter, option, widget);
}


void PanelItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
	if(event->button()==Qt::LeftButton)
	{
		if(m_isCanSelect)
		{
			emit sig_isClicked();
		}
	}
	//DrawRectItem::mouseReleaseEvent(event);
}

void PanelItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
	//DrawRectItem::mouseMoveEvent(event);
}

void PanelItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	/*if (getState() == NomaleState && event->button() == Qt::LeftButton&&m_selectAbel) {
		setState(SelectState);
	}
	ROIAbstractBaseItem::mousePressEvent(event);*/
	//DrawRectItem::mousePressEvent(event);
}
