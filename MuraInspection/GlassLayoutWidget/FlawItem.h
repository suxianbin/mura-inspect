#pragma once
#include <QObject>
#include <QBrush>
#include "KeepPenWidthGraphicsItem.h"
#include "Defect.h"

enum class eShowType
{
	Point = 0,					//点
	Rect,						//最小矩形
	Outline,					//轮廓
	hide						//不显示
};

class FlawItem :public QObject, public QGraphicsItem
{
	Q_OBJECT
signals:
	void sig_selected();
public:
	FlawItem(const Defect defect, QGraphicsItem *parent = Q_NULLPTR);
	~FlawItem() override;

	Defect getData();
	/**
	 * \brief
	 * \param _type 0 点  1最小矩形 2轮廓
	 */
	void setSelect(bool f);
	void setCanSelect(bool f);
	QRectF boundingRect() const override;
	QPainterPath shape() const override;

	Defect m_defect;
private:
	bool m_selectState = false;
	bool m_canSelect = false;
	QPolygonF m_rectPolygon;		//最小矩形
	QList<QPolygonF> m_outLinePolygons;		//最小矩形
	eShowType m_showType = eShowType::Point;
	QPen m_pen;
	qreal m_radius = 2;
	qreal m_matrix = 1;
protected:
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
};
