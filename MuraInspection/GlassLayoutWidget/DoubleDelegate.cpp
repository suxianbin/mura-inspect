#include "DoubleDelegate.h"

DoubleDelegate::DoubleDelegate(QWidget *parent)
	: QStyledItemDelegate(parent)
{
}

DoubleDelegate::~DoubleDelegate()
{
}


QWidget* DoubleDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option
	, const QModelIndex& index) const
{
	QLineEdit* le = new QLineEdit(parent);
	le->setContextMenuPolicy(Qt::NoContextMenu);
	le->setValidator(new QDoubleValidator(m_bottom, m_top,4, le));
	return (QWidget*)le;
	
}

void DoubleDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	QString strDisplay = index.data(Qt::DisplayRole).toString();
	QLineEdit *edit = (QLineEdit *)editor;
	edit->setText(strDisplay);
}



void DoubleDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
	QLineEdit *edit = (QLineEdit *)editor;
	QString strVale = edit->text();
	QString s = model->data(index, Qt::DisplayRole).toString();
	if(s!= strVale) {
		model->setData(index, strVale, Qt::DisplayRole);
		emit sig_valueChanged();
	}
	
	
}

void DoubleDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option
                                        , const QModelIndex& index) const
{
	editor->setGeometry(option.rect);
}
