#pragma once

#include <QObject>
#include <QAbstractGraphicsShapeItem>
#include <QTimer>
#include <QPen>
#include <QBrush>
#include <QPainter>
#include "GlassLayout.h"

class FlowDirectionItem : public QObject, public QAbstractGraphicsShapeItem
{
	Q_OBJECT

public:
	FlowDirectionItem(QGraphicsItem* parent = Q_NULLPTR);
	~FlowDirectionItem() override;

	/************************************
	 * 功能: 根据glass数据绘制玻璃流向动画   
	 * 参数: const GlassLayout & _data
	 * 返回: void
	*************************************/
	void setData_Direction(GlassLayout* _data);

	void setWh(qreal w, qreal h, int direction);

	QRectF boundingRect() const override;
	/********************************
		 * 功能 绘制
		 * 输入 painter
		 * 输入 option
		 * 输入 widget
		 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

private slots:
	void toggleDirection();
private:
	QTimer m_timer;
	QRect m_rect;
	int m_num = 10;
	QVector<QPolygonF> m_polygons1;
	QVector<QPolygonF> m_polygons2;
	bool m_f = true;
	int m_direction;
	QBrush m_brush;
};
