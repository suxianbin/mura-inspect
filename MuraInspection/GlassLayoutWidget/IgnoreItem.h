#pragma once

#include <QGraphicsRectItem>
#include <QObject>
#include "ROIAbstractBaseItem.h"
#include "GlassLayout.h"

/********************************
 * 功能 绘制屏蔽区域
 ********************************/

class IgnoreItem : public ROIAbstractBaseItem
{
	Q_OBJECT
signals:
	void sig_isClicked();
public:
	IgnoreItem(QGraphicsItem *parent = Q_NULLPTR);

	~IgnoreItem() override;
	/********************************
	 * 功能 设置PanelData
	 * 输入 _panel panel数据
	 * 输入 _piex 像素当量
	 ********************************/
	//void setIgnoreData(const QPointF& originP, const Ign& pignore, const qreal& _piex);

	/********************************
	 * 功能 设置是否显示id
	 * 输入 f
	 ********************************/
	void setShowId(bool f);

	/**
	 * \brief 是否可以被选择
	 * \param f
	 */
	void setCanSelect(bool f = false);

	/**
	 * \brief 设置选中状态
	 * \param f
	 */
	void setSelect(bool f = false);

	bool getSelected();

	//PanelLayout getPanelData();

	/********************************
		 * 功能 添加节点
		 * 输入 _point
		 ********************************/
	void addPoint(const QPointF& _point) override;

	//QList<QAction*> getActions() override;
	void paintToImage(QPainter* painter);
public slots:

	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;

protected:

	/********************************
	 * 功能 获取主要绘制是碰撞检测路径
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;

	/********************************
	 * 功能 重写绘制事件
	 * 输入 painter
	 * 输入 option
	 * 输入 widget
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
private:
	bool m_idFlag = true;
	PanelLayout m_data;
	bool m_isCanSelect = false;
	bool m_isSelect = false;
	GlassLayout *m_pGlass;

	int m_addPointIndex = 0;
	QBrush m_brush;
	QAction* m_delAction = Q_NULLPTR;

	//void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;

	void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
	/**
	 * \brief
	 * \param event
	 */
	void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
};
