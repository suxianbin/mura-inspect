#include "BatchAdjust.h"

BatchAdjust::BatchAdjust(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	connect(ui.db_height,SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));
	connect(ui.db_width, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));
	connect(ui.db_left, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));
	connect(ui.db_up, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));

	connect(ui.db_upside, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));
	connect(ui.db_leftside, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));
	connect(ui.db_rightside, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));
	connect(ui.db_downside, SIGNAL(valueChanged(double)), this, SLOT(slot_changed(double)));

	connect(ui.pb_save, &QPushButton::clicked, this, &BatchAdjust::sig_save);
	connect(ui.pb_cancel, &QPushButton::clicked, this, &BatchAdjust::sig_cancel);
	setWindowModality(Qt::WindowModal);
	setWindowTitle("批量调整");
}

BatchAdjust::~BatchAdjust()
{
}

void BatchAdjust::showPanelLayout(const QVector<PanelLayout>& layoutList,const QList<int>& indexList, const qreal& _pixEquivalent)
{
	m_indexList = indexList;
	m_orgList = layoutList;
	m_changedList = layoutList;
	m_pixEquivalent = _pixEquivalent;

	ui.db_height->setValue(0);
	ui.db_width->setValue(0);
	ui.db_left->setValue(0);
	ui.db_up->setValue(0);

	ui.db_upside->setValue(0);
	ui.db_leftside->setValue(0);
	ui.db_rightside->setValue(0);
	ui.db_downside->setValue(0);
	
	show();
}

QVector<PanelLayout> BatchAdjust::getChangedPanelLayout()
{
	return m_changedList;
}

QVector<PanelLayout> BatchAdjust::getOrgPanelLayout()
{
	return m_orgList;
}


void BatchAdjust::slot_changed(double value)
{
	int up_mm = round(ui.db_up->value());
	int left_mm = round(ui.db_left->value());
	int h_mm = round(ui.db_height->value());
	int w_mm = round(ui.db_width->value());

	int upside_mm = round(ui.db_upside->value());
	int leftside_mm = round(ui.db_leftside->value());
	int rightside_mm = round(ui.db_rightside->value());
	int downside_mm = round(ui.db_downside->value());


	int up = round(ui.db_up->value() / m_pixEquivalent);
	int left = round(ui.db_left->value() / m_pixEquivalent);
	int h = round(ui.db_height->value() / m_pixEquivalent);
	int w = round(ui.db_width->value() / m_pixEquivalent);

	int upside = round(ui.db_upside->value() / m_pixEquivalent);
	int leftside = round(ui.db_leftside->value() / m_pixEquivalent);
	int rightside = round(ui.db_rightside->value() / m_pixEquivalent);
	int downside = round(ui.db_downside->value() / m_pixEquivalent);

	for (int i = 0; i < m_indexList.size(); i++)
	{
		//整体调整
		m_changedList[m_indexList[i]].x = m_orgList[m_indexList[i]].x + left_mm;
		m_changedList[m_indexList[i]].y = m_orgList[m_indexList[i]].y + up_mm ;
		m_changedList[m_indexList[i]].height = m_orgList[m_indexList[i]].height + h_mm;
		m_changedList[m_indexList[i]].width = m_orgList[m_indexList[i]].width + w_mm;

		m_changedList[m_indexList[i]].x = m_orgList[m_indexList[i]].x + left;
		m_changedList[m_indexList[i]].y = m_orgList[m_indexList[i]].y + up;
		m_changedList[m_indexList[i]].height = m_orgList[m_indexList[i]].height + h;
		m_changedList[m_indexList[i]].width = m_orgList[m_indexList[i]].width + w;

		//边框调整
		m_changedList[m_indexList[i]].x = m_changedList[m_indexList[i]].x + leftside_mm;
		m_changedList[m_indexList[i]].y = m_changedList[m_indexList[i]].y +upside_mm;
		m_changedList[m_indexList[i]].height = m_changedList[m_indexList[i]].height + upside_mm + downside_mm;
		m_changedList[m_indexList[i]].width = m_changedList[m_indexList[i]].width + leftside_mm +rightside_mm;

		m_changedList[m_indexList[i]].x = m_changedList[m_indexList[i]].x +leftside;
		m_changedList[m_indexList[i]].y = m_changedList[m_indexList[i]].y + upside;
		m_changedList[m_indexList[i]].height = m_changedList[m_indexList[i]].height + upside +downside;
		m_changedList[m_indexList[i]].width = m_changedList[m_indexList[i]].width + leftside + rightside;
	}
	emit sig_changed();
}
