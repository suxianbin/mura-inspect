#include "ShowLayoutWidget.h"
#include <QResizeEvent>
#include "GlassLayoutMgr.h"

//#include "GrayLevelMeasureItem.h"
//#include "GrayLevelMeasureWidget.h"

ShowLayoutWidget::ShowLayoutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_flowDir = new FlowDirectionItem();
	connect(ui.chk_PanelID, &QCheckBox::clicked, this, &ShowLayoutWidget::slot_idStateChanged);
	connect(ui.chk_Layout, &QCheckBox::clicked, this, &ShowLayoutWidget::slot_layoutStateChanged);
	connect(ui.chk_Defect, &QCheckBox::clicked, this, &ShowLayoutWidget::slot_defecftStateChanged);
	setRuleVisable(false);
	setMouseTracking(true);


	m_lineMeasure.setText("线测量");
	m_areaMeasure.setText("灰阶测量");
	m_imageEnhanceAction.setText("图增强");
	m_imageBrightnessAction.setText("亮度调节");
	m_imageEnhancePartAction.setText("添加增强ROI");
	m_cancleEnhanceAction.setText("取消增强ROI");
	m_imageAction.setText("显示原图");
	m_cancleMeasureAction.setText("取消测量");
	connect(&m_lineMeasure, &QAction::triggered, this, &ShowLayoutWidget::slot_lineMeasureAction);
	connect(&m_areaMeasure, &QAction::triggered, this, &ShowLayoutWidget::slot_areaMeasureAction);
	connect(&m_imageEnhanceAction, &QAction::triggered, this, &ShowLayoutWidget::slot_imageEnhanceAction);
	connect(&m_imageBrightnessAction, &QAction::triggered, this, &ShowLayoutWidget::slot_imageBrightnessAction);
	connect(&m_imageEnhancePartAction, &QAction::triggered, this, &ShowLayoutWidget::slot_imageEnhancePartAction);
	connect(&m_cancleEnhanceAction, &QAction::triggered, this, &ShowLayoutWidget::slot_cancleEnhance);
	connect(&m_cancleMeasureAction, &QAction::triggered, this, &ShowLayoutWidget::slot_cancleMeasure);
	connect(&m_imageAction, &QAction::triggered, this, &ShowLayoutWidget::slot_showIamg);

	//获取鼠标当前点
	connect(ui.wid_roi, &ROIDisplay::sig_mouserMovedPoint, [=](const QPoint& point){
			//showToolTip();
		});
	QString style = "QCheckBox::indicator:unchecked {\
		image: url(:/Icons/checkbox_unchecked1.png);}\
		QCheckBox{color:yellow;}\
		QCheckBox::indicator:checked {\
		image: url(:/Icons/checkbox_checked1.png);\
		}";
	ui.chk_PanelID->setStyleSheet(style);
	ui.chk_Layout->setStyleSheet(style);
	ui.chk_Defect->setStyleSheet(style);
}

ShowLayoutWidget::~ShowLayoutWidget()
{
	if (m_flowDir != nullptr) {
		delete m_flowDir;
		m_flowDir = nullptr;
	}
}

void ShowLayoutWidget::loadImage(const cv::Mat& mat)
{
	m_showMat = mat;
	//显示新图，更新直方分布图
	//if (m_imageEnhance != Q_NULLPTR /*&& m_imageEnhance->isShowEnhance == true*/)
	//{
	//	emit m_imageEnhance->sig_garyChartImage(&m_showMat);
	//	ui.wid_roi->updataImage(m_imageEnhance->slot_enhanceImages(&m_showMat));
	//}
	//else
	//{
		ui.wid_roi->updataImage(mat);
		ui.wid_roi->scaleToSuitableRect();
	//}
}

void ShowLayoutWidget::loadImage(const QImage& qmat)
{
	ui.wid_roi->updataImageMap(QPixmap::fromImage(qmat));
	ui.wid_roi->scaleToSuitableRect();
}

void ShowLayoutWidget::loadImageMap(const QPixmap& qpixmap)
{
	m_showMat = qPixmap2Mat(qpixmap);
	//显示新图，更新直方分布图
	//if (m_imageEnhance != Q_NULLPTR)
	//{
	//	emit m_imageEnhance->sig_garyChartImage(&m_showMat);
	//}
	//ui.wid_roi->updataImageMap(qpixmap);
}

void ShowLayoutWidget::showLayoutByName(const QString& layoutName, bool bshowlayout, bool bhideIgnore)
{
	showLayout(dynamic_cast<GlassLayout*>(GlassLayoutMgr::Instance()->FindBy(layoutName)), true);
}


void ShowLayoutWidget::showLayout(GlassLayout* layout, bool bhideIgnore)
{
	clearLayout();
	//玻璃的外边框
	MyGlassItem* glassItem = new MyGlassItem();
	glassItem->setData(layout);
	glassItem->setVisible(m_isShowLayout);

	ui.wid_roi->addItem(glassItem);
	ui.wid_roi->setRuleParam(layout->Station1PixelEquivalent, "mm");
	m_layoutitem << glassItem;

	for (int i = 0; i < layout->PanelLists.size(); i++)
	{
		const PanelLayout& pl = layout->PanelLists[i];
		PanelItem* item = new PanelItem();
		connect(item, &PanelItem::sig_isClicked, this, &ShowLayoutWidget::slot_panelSelected);
		item->setPanelData(pl);
		if (m_isCanSelect)
		{
			item->setCanSelect(m_isCanSelect);
		}
		item->setVisible(m_isShowLayout);
		item->setShowId(m_showId);
		ui.wid_roi->addItem(item);
		m_layoutitem << item;
		m_panelItem << item;
	}
	//显示屏蔽区域
	//if (!bhideIgnore)
	//{
	//	QPointF originP(layout.x, layout.y);
	//	for (int i = 0; i < layout.ignorelayoutList.size(); i++) {
	//		const IgnoreLayout& pl = layout.ignorelayoutList[i];
	//		IgnoreItem* item = new IgnoreItem;
	//		item->setIgnoreData(originP, pl, layout.pixelEquivalent);
	//		item->setVisible(m_isShowLayout);
	//		item->setState(NomaleState);
	//		ui.wid_roi->addItem(item);
	//		m_layoutitem << item;
	//	}
	//}
	
	FlowDirectionItem* flowdirectionitem = new FlowDirectionItem();
	flowdirectionitem->setData_Direction(layout);
	flowdirectionitem->setVisible(m_isShowLayout);
	ui.wid_roi->addItem(flowdirectionitem);
	m_layoutitem << flowdirectionitem;
	ui.wid_roi->scaleToSuitableRect();
}

//void ShowLayoutWidget::showLayout(const GlassLayoutRecipe& data, bool bhideIgnore/* = true*/)
//{
//	showLayout(data.layout, bhideIgnore);
//}

void ShowLayoutWidget::clearLayout()
{
	for (int i = m_layoutitem.size() - 1; i >= 0; i--) 
	{
		QGraphicsItem* item = m_layoutitem[i];
		ui.wid_roi->removeItem(item);
		if (item != Q_NULLPTR)
		{
			delete item;
			item = Q_NULLPTR;
		};
	}
	m_layoutitem.clear();
	m_panelItem.clear();
	update();
}

void ShowLayoutWidget::suitableView()
{
	ui.wid_roi->scaleToSuitableRect();
}

ROIDisplay* ShowLayoutWidget::getRoiDisplay()
{
	return ui.wid_roi;
}

void ShowLayoutWidget::showFlaws(const QVector<Defect>&flaws)
{
	for (int i = 0; i < flaws.size(); i++)
	{
		FlawItem* item = new FlawItem(flaws[i]);
		connect(item, &FlawItem::sig_selected, this, &ShowLayoutWidget::slot_flawSelected);
		item->setVisible(flaws[i].isVisible);
		item->setCanSelect(true);
		ui.wid_roi->addItem(item);
		m_flawItemList.append(item);
	}
}

void ShowLayoutWidget::showFlaws(const QVector<Defect>& flaws, QPointF orginPoint, int type)
{
	for (int i = 0; i < flaws.size(); i++) 
	{
		FlawItem* item = new FlawItem(flaws[i]);
		connect(item, &FlawItem::sig_selected, this, &ShowLayoutWidget::slot_flawSelected);
		item->setCanSelect(true);
		ui.wid_roi->addItem(item);
		m_flawItemList.append(item);
	}
}

bool ShowLayoutWidget::selectFlawByIndex(int index)
{
	for (int i = 0; i < m_flawItemList.size(); i++) {
		m_flawItemList[i]->setSelect(false);
	}
	if (index >= 0 && m_flawItemList.size() > 0 && m_flawItemList.size() > index)
	{
		m_flawItemList[index]->setSelect(true);
		ui.wid_roi->updateScene();
		return true;
	}
	else
	{
		ui.wid_roi->updateScene();
		return false;
	}
}

void ShowLayoutWidget::suitableToFlaw(int index)
{
	if (index >= 0 && m_flawItemList.size() > index)
	{
		Defect rlt = m_flawItemList[index]->getData();
		QPolygon po;
		for (int i = 0; i < rlt.pts.size(); i++)
		{
			po.append(QPoint(rlt.pts[i].x, rlt.pts[i].y));
		}
		QRect r = po.boundingRect();
		int w = r.width();//显示区域的宽
		int h = r.height();
		int x = r.x() - w * 2;//x 左上角坐标
		int y = r.y() - h * 2;
		w = w * 4;
		h = h * 4;
		r.setX(x);
		r.setY(y);
		r.setWidth(w);
		r.setHeight(h);
		ui.wid_roi->scaleToRect(r);
	}
}

void ShowLayoutWidget::clearFlaws()
{
	for (int i = 0; i < m_flawItemList.size(); i++)
	{
		ui.wid_roi->removeItem(m_flawItemList[i]);
		delete m_flawItemList[i];
		m_flawItemList[i] = nullptr;
	}
	m_flawItemList.clear();
}

void ShowLayoutWidget::setCanSelectPanel(bool f, bool isOnlyOne)
{
	//m_isCanSelect = f;
	//m_isSelectOnlyOne = isOnlyOne;
	//for (int i = 0; i < m_panelItem.size(); i++)
	//{
	//	m_panelItem[i]->setCanSelect(m_isCanSelect);
	//}
}

PanelLayout ShowLayoutWidget::getSelectPanelData(bool* f)
{
	PanelLayout res;
	if (m_showLayoutName != "")
	{
		PanelItem* selectedItem = Q_NULLPTR;
		for (int i = 0; i < m_panelItem.size(); i++)
		{
			//if (m_panelItem[i]->getSelected())
			//{
			//	selectedItem = m_panelItem[i];
			//	break;
			//}
		}
		if (selectedItem != Q_NULLPTR)
		{
		}
	}
	if (f != Q_NULLPTR)
	{
		*f = false;
	}
	return res;
}

void ShowLayoutWidget::setRuleVisable(bool visable)
{
	ui.wid_roi->setRuleVisable(visable);
}

void ShowLayoutWidget::setMouseEnable(bool enable)
{
	ui.wid_roi->setMouseEnable(enable);
	ui.label->setVisible(enable);
}

void ShowLayoutWidget::selectedPanelByIds(const QStringList& _ids)
{
	//for (int i = 0; i < m_panelItem.size(); i++)
	//{
	//	m_panelItem[i]->setSelect(false);
	//}
	//for (int i = 0; i < m_panelItem.size(); i++)
	//{
	//	if (_ids.contains(m_panelItem[i]->getPanelData().id))
	//	{
	//		m_panelItem[i]->setSelect(true);
	//	}
	//}
}

QList<PanelLayout*> ShowLayoutWidget::getPanelItems()
{
	return QList<PanelLayout*>();
}


void ShowLayoutWidget::setPanelChecked(bool checked)
{
	ui.chk_PanelID->setChecked(checked);
}

void ShowLayoutWidget::setDefectChecked(bool checked)
{
	ui.chk_Defect->setChecked(checked);
}

void ShowLayoutWidget::setLayoutChecked(bool checked)
{
	ui.chk_Layout->setChecked(checked);
}

void ShowLayoutWidget::setCheckBoxVisible(bool visi)
{
	ui.chk_Layout->setVisible(visi);
	ui.chk_PanelID->setVisible(visi);
	ui.chk_Defect->setVisible(visi);
}

//QList<PanelItem*> ShowLayoutWidget::getPanelItems()
//{
//	return m_panelItem.toList();
//}

void ShowLayoutWidget::slot_idStateChanged(bool _state)
{
	emit sig_PanelChecked(_state);	
	//m_showId = _state == Qt::Checked ? true : false;
	//for (int i = 0; i < m_panelItem.size(); i++) {
	//	m_panelItem[i]->setShowId(m_showId);
	//	m_panelItem[i]->update();
	//}
}

void ShowLayoutWidget::on_roiComlete(QRectF rect)
{
	int w = rect.width();
	int h = rect.height();
	int top = rect.top();
	int left = rect.left();

	//if (m_grayLevelWidget != Q_NULLPTR)
	//{
	//	m_grayLevelWidget->deleteLater();
	//	m_grayLevelWidget = Q_NULLPTR;
	//}
	//m_grayLevelWidget = new GrayLevelMeasureWidget(&m_showMat, rect);
	//if (m_grayLevelWidget->isVisible()) {
	//	m_grayLevelWidget->close();
	//}
	//m_grayLevelWidget->slot_GrayChartImage(&m_showMat);

	//m_grayLevelWidget->setWindowFlags(Qt::WindowCloseButtonHint);
	//m_grayLevelWidget->setWindowFlag(Qt::WindowStaysOnTopHint);

	//m_grayLevelWidget->show();
}

void ShowLayoutWidget::slot_layoutStateChanged(bool _checked)
{
	m_isShowLayout = _checked;
	emit sig_LayoutChecked(_checked);
	for (int i = 0; i < m_layoutitem.size(); i++)
	{
		m_layoutitem[i]->setVisible(m_isShowLayout);
		m_layoutitem[i]->update();
	}
	m_flowDir->setVisible(m_isShowLayout);
	m_flowDir->update();
}

void ShowLayoutWidget::slot_defecftStateChanged(bool _checked)
{
	emit sig_DefectChecked(_checked);
	for (auto var : m_flawItemList)
	{
		var->setVisible(_checked);
	}	
}

void ShowLayoutWidget::slot_panelSelected()
{
	PanelItem* item = (PanelItem*)sender();
	//for (int i = 0; i < m_panelItem.size(); i++)
	//{
	//	if (m_panelItem[i] != item)
	//	{
	//		if (m_isSelectOnlyOne)
	//		{
	//			m_panelItem[i]->setSelect(false);
	//		}
	//	}
	//	else
	//	{
	//		m_panelItem[i]->setSelect(true);
	//	}
	//}
}

void ShowLayoutWidget::slot_flawSelected()
{
	FlawItem* item = (FlawItem*)sender();
	for (int i = 0; i < m_flawItemList.size(); i++)
	{
		if (item == m_flawItemList.at(i))
		{
			//emit sig_selectDefectsIndex(i);
			update();
		}
		else
		{
			m_flawItemList[i]->setSelect(false);
		}
	}
}

void ShowLayoutWidget::resizeEvent(QResizeEvent* event)
{
	QSize s1 = event->size();
	QSize s2 = ui.widget->size();
	QPoint p(s1.width() - s2.width() - 20, 50);
	if (p.x() < 0)
	{
		p.setX(0);
	}
	ui.widget->move(p);
}

void ShowLayoutWidget::mouseMoveEvent(QMouseEvent* event)
{
	//ctrl显示灰度信息
	if (QApplication::queryKeyboardModifiers() == Qt::ControlModifier)
	{
		showToolTip();
	}
	QWidget::mouseMoveEvent(event);
}

void ShowLayoutWidget::mousePressEvent(QMouseEvent * event)
{
	showToolTip();//鼠标点击显示当前的像素信息
	QWidget::mousePressEvent(event);
}

void ShowLayoutWidget::keyPressEvent(QKeyEvent* event)
{
	if (event->modifiers() & Qt::ControlModifier)
	{
		showToolTip();
	}
	QWidget::keyPressEvent(event);
}

void ShowLayoutWidget::showToolTip()
{
	QGraphicsView* view = ui.wid_roi->getGraphicsView();
	QPoint globalPos = QCursor::pos();
	QPoint viewPos = view->mapFromGlobal(globalPos);
	QPointF scenePos = view->mapToScene(viewPos);

	cv::Scalar meangray; 
	cv::Scalar stddev;
	QString str;
	int x = round(scenePos.x());
	int y = round(scenePos.y());

	//需要将原图坐标系转换为glass坐标系 ljx2022520
	//if (m_showLayoutName != "")
	//{
	//	int mapX = m_showLayout.layout.toGlassPositionX(x);
	//	int mapY = m_showLayout.layout.toGlassPositionY(y);

	//	str += QString("Glass坐标(%1,%2)").arg(QString::number(mapX*m_showLayout.layout.pixelEquivalent, 'f', 2) + "mm").arg(QString::number(mapY*m_showLayout.layout.pixelEquivalent, 'f', 2) + "mm");
	//}
	//else
	//{
	//	str += QString("图像坐标(%1,%2)[pixel] (%3,%4)[mm] ").arg(QString::number(x, 10))
	//		.arg(QString::number(y, 10)).arg(QString::number(x*m_showLayout.layout.pixelEquivalent, 'f', 2))
	//		.arg(QString::number(y*m_showLayout.layout.pixelEquivalent, 'f', 2));
	//}

	//str += "\n";

	if (x >= 0 && x < m_showMat.cols && y >= 0 && y < m_showMat.rows)
	{
		const cv::Vec3b& data = m_showMat.at<cv::Vec3b>(y, x);
		int b = data[0];
		int g = data[1];
		int r = data[2];
		str += QString("RGB:%1,%2,%3").arg(r).arg(g).arg(b);
		int gray = m_showMat.at<uchar>(y, x);
		str += QString(" 灰度:%1").arg(gray);
	}
	slot_showCoord(str);
	//冒泡框
	QToolTip::showText(globalPos, str);
}

void ShowLayoutWidget::contextMenuEvent(QContextMenuEvent* event)
{
	m_menu.clear();
	if (m_measureItem == Q_NULLPTR)
	{
		m_menu.addAction(&m_lineMeasure);
		m_menu.addAction(&m_areaMeasure);
		m_menu.addAction(&m_imageEnhanceAction);
		m_menu.addAction(&m_imageBrightnessAction);
		m_menu.addAction(&m_imageAction);
	}
	else
	{
		m_menu.addAction(&m_cancleMeasureAction);
		m_menu.addAction(&m_imageAction);
	}
	m_menu.exec(event->globalPos());
	QWidget::contextMenuEvent(event);
}

void ShowLayoutWidget::slot_lineMeasureAction()
{
	if (m_measureItem != Q_NULLPTR)
	{
		ui.wid_roi->removeItem(m_measureItem);
		m_measureItem->deleteLater();
		m_measureItem = Q_NULLPTR;
	}
	//m_measureItem = new LineMeasureItem(&m_showLayout);
	ui.wid_roi->addROIItem(m_measureItem);
}

void ShowLayoutWidget::slot_areaMeasureAction()
{
	if (m_measureItem != Q_NULLPTR)
	{
		ui.wid_roi->removeItem(m_measureItem);
		m_measureItem->deleteLater();
		m_measureItem = Q_NULLPTR;
	}
	//m_measureItem = new GrayLevelMeasureItem(&m_showLayout);
	//m_measureItem = new AreaMeasureItem(&m_showLayout);
	ui.wid_roi->addROIItem(m_measureItem);
	QObject::connect(m_measureItem, SIGNAL(sig_roiComplete(QRectF)), this, SLOT(on_roiComlete(QRectF)));
}

void ShowLayoutWidget::slot_imageEnhanceAction()
{
	//if (m_imageEnhance != Q_NULLPTR)
	//{
	//	m_imageEnhance->deleteLater();
	//	m_imageEnhance = Q_NULLPTR;
	//}
	//m_imageEnhance = new ImageEnhanceWidget(&m_showMat);//图像增强显示框
	//connect(m_imageEnhance, &ImageEnhanceWidget::sig_garyChartImage, m_imageEnhance, &ImageEnhanceWidget::slot_GrayChartImage);
	//emit m_imageEnhance->sig_garyChartImage(&m_showMat);

	//if (m_imageEnhance != Q_NULLPTR)
	//{
	//	m_imageEnhance->deleteLater();
	//	m_imageEnhance = Q_NULLPTR;
	//}
	//m_imageEnhance = new ImageEnhanceWidget(&m_showMat);//图像增强显示框
	//if (m_imageEnhance->isVisible()) {
	//	m_imageEnhance->close();
	//}
	//m_imageEnhance->slot_GrayChartImage(&m_showMat);

	//m_imageEnhance->setWindowFlags(Qt::WindowCloseButtonHint);
	//m_imageEnhance->setWindowFlag(Qt::WindowStaysOnTopHint);

	//m_imageEnhance->show();
	//connect(m_imageEnhance, &ImageEnhanceWidget::sig_EnhanceImage, this, &ShowLayoutWidget::loadEnhanceIamg);
	//connect(m_imageEnhance, &ImageEnhanceWidget::sig_colseEnhanceWidget, this, &ShowLayoutWidget::slot_colseEnhanceWidget);
	//m_imageEnhance->slot_enhanceImage(&m_showMat);
	//m_imageEnhance->isShowEnhance = true;
}

void ShowLayoutWidget::slot_imageEnhancePartAction()
{
	//if (m_imageEnhance != Q_NULLPTR)
	//{
	//	m_imageEnhance->deleteLater();
	//	m_imageEnhance = Q_NULLPTR;
	//}
	//m_imageEnhance = new ImageEnhanceWidget(&m_showMat);
	//connect(m_imageEnhance, &ImageEnhanceWidget::sig_garyChartImage, m_imageEnhance, &ImageEnhanceWidget::slot_GrayChartImage);
	//emit m_imageEnhance->sig_garyChartImage(&m_showMat);
	//connect(m_imageEnhance, &ImageEnhanceWidget::sig_EnhanceImage, this, &ShowLayoutWidget::loadEnhanceIamg);

	//if (m_ImageEnhanceItem != Q_NULLPTR)
	//{
	//	ui.wid_roi->removeItem(m_ImageEnhanceItem);
	//	m_ImageEnhanceItem->deleteLater();
	//	m_ImageEnhanceItem = Q_NULLPTR;
	//}
	//m_ImageEnhanceItem = new ImageEnhanceItem(&m_showMat);
	//ui.wid_roi->addROIItem(m_ImageEnhanceItem);

	//connect(m_ImageEnhanceItem, &ImageEnhanceItem::sig_sendEnhanceRoi, m_imageEnhance, &ImageEnhanceWidget::slot_enhanceRoi);
}

void ShowLayoutWidget::slot_imageBrightnessAction()
{
}

void ShowLayoutWidget::slot_cancleEnhance()
{
	//if (m_ImageEnhanceItem != Q_NULLPTR)
	//{
	//	ui.wid_roi->removeItem(m_ImageEnhanceItem);
	//	m_ImageEnhanceItem->deleteLater();
	//	m_ImageEnhanceItem = Q_NULLPTR;
	//}
	slot_showIamg();
}

void ShowLayoutWidget::slot_colseEnhanceWidget()
{
	//if (m_imageEnhance->isVisible())
	//{
	//	m_imageEnhance->close();
	//}
	////m_imageEnhance->isShowEnhance = false;
	//if (m_imageEnhance != Q_NULLPTR)
	//{
	//	m_imageEnhance->deleteLater();
	//	m_imageEnhance = Q_NULLPTR;
	//}
}

void ShowLayoutWidget::slot_cancleMeasure()
{
	if (m_measureItem != Q_NULLPTR)
	{
		ui.wid_roi->removeItem(m_measureItem);
		m_measureItem->deleteLater();
		m_measureItem = Q_NULLPTR;
	}
}

void ShowLayoutWidget::loadEnhanceIamg(const cv::Mat & mat)
{
	ui.wid_roi->updataImage(mat);
}

void ShowLayoutWidget::loadEnhanceIamgMap(const QPixmap& qpixmap)
{
	ui.wid_roi->updataImageMap(qpixmap);
}

void ShowLayoutWidget::slot_showIamg()
{
	//m_imageEnhance->isShowEnhance = false;
	if (m_showMat.data)
	{
		ui.wid_roi->updataImage(m_showMat);
	}
}

void ShowLayoutWidget::slot_sendRect(cv::Rect roi)
{
}

void ShowLayoutWidget::slot_showCoord(QString str)
{
	ui.label->setText(str);
	QFont ft;
	ft.setPixelSize(14);
	ft.setFamily(u8"微软雅黑");
	ui.label->setFont(ft);
	ui.label->setStyleSheet("color:yellow;background-color:rgba(128,128,128,0)");
}

// bool ShowLayoutWidget::eventFilter(QObject* watched, QEvent* event)
// {
// 	if(watched==this&&event->type()== QEvent::MouseMove&&QApplication::queryKeyboardModifiers() == Qt::ControlModifier)
// 	{
// 		showToolTip();
// 	}
// 	return IShowLayoutWidget::eventFilter(watched, event);
// }


cv::Mat ShowLayoutWidget::qPixmap2Mat(const QPixmap& _pixmap)
{
	cv::Mat mat;
	QImage image = _pixmap.toImage();
	switch (image.format())
	{
	case QImage::Format_ARGB32:
	case QImage::Format_RGB32:
	case QImage::Format_ARGB32_Premultiplied:
		mat = cv::Mat(image.height(), image.width(), CV_8UC4, (void*)image.constBits(), image.bytesPerLine());
		break;
	case QImage::Format_RGB888:
		mat = cv::Mat(image.height(), image.width(), CV_8UC3, (void*)image.constBits(), image.bytesPerLine());
		break;
	case QImage::Format_Indexed8:
	case QImage::Format_Grayscale8:
		mat = cv::Mat(image.height(), image.width(), CV_8UC1, (void*)image.bits(), image.bytesPerLine());
		break;
	}
	cv::cvtColor(mat, mat, cv::COLOR_RGBA2RGB);
	return mat;
}