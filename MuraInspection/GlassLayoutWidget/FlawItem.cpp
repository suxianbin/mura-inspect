#include "FlawItem.h"

FlawItem::FlawItem(const Defect  defect, QGraphicsItem *parent)
	: QObject(), QGraphicsItem(parent), m_defect(defect)
{
	this->setAcceptedMouseButtons(Qt::LeftButton);
	setFlag(QGraphicsItem::ItemIsSelectable);
	m_pen.setWidth(1);
	if (defect.type == 0)
	{
		m_pen.setColor(Qt::red);
	}
	setZValue(999);
	for (int i = 0; i < m_defect.pts.size(); i++) {
		QPointF p;
		p.setX(m_defect.pts[i].x);
		p.setY(m_defect.pts[i].y);
		m_rectPolygon.append(p);
	}
	for (int i1 = 0; i1 < m_defect.contours.size(); i1++)
	{
		QPolygon poly;
		for (int i2 = 0; i2 < m_defect.contours[i1].size(); i2++)
		{
			poly.append(QPoint(m_defect.contours[i1][i2].x, m_defect.contours[i1][i2].y));
		}
		m_outLinePolygons << poly;
	}
	if (m_outLinePolygons.size() < 1)
	{
		m_outLinePolygons << m_rectPolygon;
	}
	setPos(0, 0);
	setSelect(false);
	show();
}

FlawItem::~FlawItem()
{
}

Defect FlawItem::getData()
{
	return m_defect;
}

void FlawItem::setSelect(bool f)
{
	m_selectState = f;
	if (m_selectState)
	{
		m_radius = 10;
		m_pen.setWidth(4);
		m_pen.setColor(QColor(0, 255, 255));
	}
	else
	{
		m_radius = 6;
		m_pen.setWidth(2);
		//m_pen.setColor(m_defect.levelColor);
	}
	update();
}

void FlawItem::setCanSelect(bool f)
{
	m_canSelect = f;
}

QRectF FlawItem::boundingRect() const
{
	return shape().boundingRect();
}

QPainterPath FlawItem::shape() const
{
	QPainterPath path;
	QRectF rectangle(m_defect.imgPosX - m_defect.width, m_defect.imgPosY - m_defect.length, 2* m_defect.width, 2* m_defect.length);
	path.addRect(rectangle);
	return path;
}

void FlawItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	QPen p = m_pen;
	p.setColor(Qt::yellow);
	if (painter->transform().m11() > 0)
	{
		if (m_matrix > 0.00001 || m_matrix < -0.00001) 
		{
			m_matrix = painter->transform().m11();
			qreal pwidth = p.widthF() / m_matrix;
			p.setWidthF(pwidth);
			painter->setPen(p);
		}
		else
		{
			m_matrix = 1;
		}
	}
	qreal radius;
	QBrush brush;
	int wmm = 250;
	int lmm = 100;
	wmm = (wmm < m_defect.width || m_defect.width < 200) ? wmm : m_defect.width;
	lmm = (lmm < m_defect.length || m_defect.length < 100) ? lmm : m_defect.length;
	switch (m_defect.DefectType)
	{
	case eDefectType::BrightDot:
		brush.setColor(m_pen.color());
		brush.setStyle(Qt::SolidPattern);
		painter->setBrush(brush);
		radius = m_radius / m_matrix;
		painter->drawEllipse(m_defect.imgPosX, m_defect.imgPosY, radius, radius);
	case eDefectType::DarkDot:
		if (m_selectState)
		{
			p.setStyle(Qt::DashDotDotLine);
		}
		painter->drawPolygon(m_rectPolygon);
		break;
	case eDefectType::BrightHLine:
	case eDefectType::DarkHLine:
	case eDefectType::BrightVLine:
	case eDefectType::DarkVLine:
		if (m_selectState)
		{
			p.setStyle(Qt::DashDotDotLine);
		}
		for (int i = 0; i < m_outLinePolygons.size(); i++)
		{
			painter->drawPolygon(m_outLinePolygons[i]);
		}
	default:
		p.setStyle(Qt::DashDotDotLine);
		painter->setPen(p);
		radius = m_radius / m_matrix;
		painter->drawEllipse(m_defect.imgPosX, m_defect.imgPosY, radius, radius);
		break;
	}
}

void FlawItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
	emit sig_selected();
	setSelect(true);
	QGraphicsItem::mouseReleaseEvent(event);
}