#pragma once
#pragma execution_character_set("UTF-8")

#include <QHeaderView>
#include <QTableWidget>
#include <QMenu>
#include <QAction>
#include "PanelItem.h"
#include "DoubleDelegate.h"
#include "UniqueDelegate.h"
#include "BatchAdjust.h"
#include "PanelLayout.h"

class GlassLayout;
class PanelLayout;

class PanelTable : public QTableWidget
{
	Q_OBJECT
signals:
	void sig_panelChanged();
public:
	PanelTable(QWidget *parent  = Q_NULLPTR);
	~PanelTable() override;

	/********************************
	 * 功能 显示相应的列表
	 * 输入 _panelList panel列表
	 * 输入 _pixEquivalent  像素当量
	 ********************************/
	void showPanelList(GlassLayout* _panelList,const QList<PanelItem*>);


	/********************************
	 * 功能 获取panel 列表
	 * 输入 _pixEquivalent  像素当量
	 * 返回 
	 ********************************/
	QVector<PanelLayout> getLayouts();

	/********************************
	* 功能 获取panel 列表
	* 输入 _pixEquivalent  像素当量
	* 返回
	********************************/
	QVector<PanelLayout> getLayouts(GlassLayout* _panelList,qreal _pixEquivalent,QPointF dataPointf);

	void setPixelEquivalent(double _pixEquivalent);
	void clearLayout();
private:
	bool m_itemChanged = true;
	double m_pixelEquivalent = 0.1;
	QMenu* m_menu = Q_NULLPTR;
	QAction* m_action = Q_NULLPTR;
	QAction* m_itemSelectAction = Q_NULLPTR;
	BatchAdjust* m_adjust = Q_NULLPTR;
	QList<PanelItem*> m_panelItem;
	GlassLayout *m_data = Q_NULLPTR;
private:

	/**
	 * \brief 右键菜单事件
	 * \param event 
	 */
	void contextMenuEvent(QContextMenuEvent* event) override;

private slots:
	void slot_action();
	void slot_batchChanged();//批量调整
	void slot_batchSave();
	void slot_batchCancel();
	void slot_itemSelectAction();
};
