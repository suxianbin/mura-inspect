#pragma once
#include <QGraphicsRectItem>
#include <QObject>
#include<qmath.h>
#include "ROIAbstractBaseItem.h"
#include "GlassLayout.h"
#include "PanelLayout.h"

/********************************
 * 功能 绘制panel  外框加字体
 ********************************/

class PanelItem :public ROIAbstractBaseItem
{

	Q_OBJECT
signals:
	void sig_isClicked();
public:
	PanelItem(QGraphicsItem *parent = Q_NULLPTR);

	~PanelItem() override;
	/********************************
	 * 功能 设置PanelData
	 * 输入 _panel panel数据
	 ********************************/
	void setPanelData(const PanelLayout& _panel);

	/********************************
	 * 功能 设置是否显示id
	 * 输入 f 
	 ********************************/
	void setShowId(bool f);

	/**
	 * \brief 是否可以被选择
	 * \param f 
	 */
	void setCanSelect(bool f = false);

	/**
	 * \brief 设置选中状态
	 * \param f 
	 */
	void setSelect(bool f = false);

	bool getSelected();

	PanelLayout* getPanelData();

	/***********************************************************************************************************/

	/********************************
	* 功能 添加节点
	* 输入 _point
	********************************/
	void addPoint(const QPointF& _point) override;

	//QList<QAction*> getActions() override;
	void paintToImage(QPainter* painter);
public slots:

	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;

protected:

	/********************************
	 * 功能 获取主要绘制是碰撞检测路径
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;

	

	/********************************
	 * 功能 重写绘制事件
	 * 输入 painter
	 * 输入 option
	 * 输入 widget
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
private:
	bool m_idFlag = true;
	PanelLayout m_data;
	bool m_isCanSelect = false;
	bool m_isSelect = false;
	double m_GlassX = 0;;
	double m_GlassY = 0;
	double m_PixelEquivalent = 0.1;
	
	int m_addPointIndex = 0;
	QBrush m_brush;
	QAction* m_delAction = Q_NULLPTR;

	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;

	void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
	/**
	 * \brief 
	 * \param event 
	 */
	void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
};
