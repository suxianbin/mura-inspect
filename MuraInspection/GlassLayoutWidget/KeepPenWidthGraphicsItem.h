#pragma once
#ifndef _KEEPPENWIDTHGRAPHICSITEM_FILE_H_
#define _KEEPPENWIDTHGRAPHICSITEM_FILE_H_
#include <QGraphicsItem>
#include <QObject>
#include <QPainter>


/********************************
 * 功能 保证绘制的视觉线宽
 * 输入 BaseType 
 ********************************/
template <typename BaseType = QAbstractGraphicsShapeItem>
class KeepPenWidthGraphicsItem : public BaseType
{
public:
	explicit KeepPenWidthGraphicsItem(QGraphicsItem* parent = Q_NULLPTR) :BaseType(parent)
	{
	};

	virtual ~KeepPenWidthGraphicsItem()
	{
	};
	
	void setPen(const QPen& pen)
	{
		m_pen = pen;
		BaseType::setPen(pen);
	}

	QPen pen() const
	{
		return m_pen;
	}
protected:
	
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)  override
	{
		QPen p = m_pen;
		if (painter->transform().m11() > 0) 
		{
			qreal m11 = painter->transform().m11();
			if (m11 > 0.00001 || m11 < -0.00001)
			{
				qreal pwidth = p.widthF() / m11;
				p.setWidthF(pwidth);
				painter->setPen(p);
			}
			//底层绘制是在绘制的时候会重新使用Pen()方式来获取pen因此需要使用此方式修改底层绘制的Pen
			BaseType::setPen(p);
		}
		BaseType::paint(painter, option, widget);
	};
private:
	QPen m_pen;
};
#endif