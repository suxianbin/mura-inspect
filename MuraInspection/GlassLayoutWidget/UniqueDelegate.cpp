#include "UniqueDelegate.h"

UniqueDelegate::UniqueDelegate(QWidget* parent)
	: QStyledItemDelegate(parent)
{
}

UniqueDelegate::~UniqueDelegate()
{
}


QWidget* UniqueDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option
	, const QModelIndex& index) const
{
	QLineEdit* le = new QLineEdit(parent);
	le->setContextMenuPolicy(Qt::NoContextMenu);
	return (QWidget*)le;

}

void UniqueDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	QString strDisplay = index.data(Qt::DisplayRole).toString();
	QLineEdit *edit = (QLineEdit *)editor;
	edit->setText(strDisplay);
}



void UniqueDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
	QLineEdit *edit = (QLineEdit *)editor;
	QString strVale = edit->text();
	QString s = model->data(index, Qt::DisplayRole).toString();

	if(strVale.isEmpty()) {
		QMessageBox::warning(Q_NULLPTR, "����", "��ֹΪ�գ�");
		return;
	}
	if (s != strVale) {
		int c = index.column();
		for (int r = 0; r < model->rowCount(); r++) {
			QModelIndex mi = model->index(r, c);
			QString str = model->data(mi, Qt::DisplayRole).toString();
			/*if (str == strVale) {
				QMessageBox::warning(Q_NULLPTR, "����", "��ֹ�ظ���");
				return;
			}*/
		}
		model->setData(index, strVale, Qt::DisplayRole);
		emit sig_valueChanged();
	}


}

void UniqueDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option
	, const QModelIndex& index) const
{
	editor->setGeometry(option.rect);
}