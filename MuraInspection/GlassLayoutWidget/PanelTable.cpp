#include "PanelTable.h"
#include "GlassLayout.h"
#include <QEvent>
#include <QContextMenuEvent>

PanelTable::PanelTable(QWidget *parent)
	: QTableWidget(parent)
{
	QStringList headerList;
	headerList <<"序号" << "x(mm)" << "y(mm)" << "width(mm)" << "length(mm)" << "内缩(mm)" << "Id" << "model类型"<<"圆角半径";
	setColumnCount(headerList.size());
	setHorizontalHeaderLabels(headerList);
	horizontalHeader()->setStretchLastSection(true);

	for (int i = 0; i <= headerList.size(); i++)
	{
		setColumnWidth(i, 115);      			
	}

	setAlternatingRowColors(true); // 隔行变色
	setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色

	setEditTriggers(DoubleClicked);
	DoubleDelegate* dg = new DoubleDelegate(this);
	connect(dg, &DoubleDelegate::sig_valueChanged, this, &PanelTable::sig_panelChanged);
	
	setItemDelegateForColumn(0, dg);
	setItemDelegateForColumn(1, dg);
	setItemDelegateForColumn(2, dg);
	setItemDelegateForColumn(3, dg);
	setItemDelegateForColumn(4, dg);
	setItemDelegateForColumn(5, dg);

	//不为空不重复校验
	UniqueDelegate* ud = new UniqueDelegate(this);
	setItemDelegateForColumn(6, ud);
	setItemDelegateForColumn(7, ud);

	QAbstractItemModel* m = model();
	connect(m, &QAbstractListModel::dataChanged, [=](
		const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
	{
		if (m_itemChanged) {
			int c = topLeft.column();
			if (c == 5 && roles.contains(Qt::DisplayRole)) {
				emit sig_panelChanged();
			}
			// if((c==0||c==1)&& roles.contains(Qt::DisplayRole))
			// {
			// 	emit sig_panelChanged();
			// }
		}
	});
	m_menu = new QMenu(this);
	m_action = new QAction(this);
	//m_itemSelectAction = new  QAction(this);
	m_action->setText("批量调整");
	//m_itemSelectAction->setText("选中可拖动");
	m_menu->addAction(m_action);
	//m_menu->addAction(m_itemSelectAction);
	connect(m_action, &QAction::triggered, this, &PanelTable::slot_action);
	//connect(m_itemSelectAction, &QAction::triggered, this, &PanelTable::slot_itemSelectAction);

	m_adjust = new BatchAdjust;
	connect(m_adjust, &BatchAdjust::sig_save, this, &PanelTable::slot_batchSave);
	connect(m_adjust, &BatchAdjust::sig_cancel, this, &PanelTable::slot_batchCancel);
	connect(m_adjust, &BatchAdjust::sig_changed, this, &PanelTable::slot_batchChanged);

	setSelectionBehavior(SelectRows);
}

PanelTable::~PanelTable()
{
}

void PanelTable::showPanelList(GlassLayout* _layout, QList<PanelItem*> items)
{
	m_panelItem = items;
	m_data = _layout;
	m_itemChanged = false;
	clearContents();
	setRowCount(m_data->PanelLists.size());

	for (int r = 0; r < m_data->PanelLists.size(); r++) 
	{
		int c = 0;
		const PanelLayout& pl = m_data->PanelLists[r];	

		QTableWidgetItem* item = new QTableWidgetItem;
		item->setText(QString::number(pl.serial));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		item = new QTableWidgetItem;
		item->setText(QString::number(pl.x, 'f', 5));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		item = new QTableWidgetItem;
		item->setText(QString::number(pl.y, 'f', 5));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		item = new QTableWidgetItem;
		item->setText(QString::number(pl.width, 'f', 5));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		item = new QTableWidgetItem;
		item->setText(QString::number(pl.height, 'f', 5));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		item = new QTableWidgetItem;
		item->setText(QString::number(pl.contract, 'f', 5));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);	

		item = new QTableWidgetItem;
		item->setText(pl.panelName);
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		item = new QTableWidgetItem;
		item->setText(pl.panelModel);
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);

		//  [2024/7/12 14:44 zaiyuan.li]
		item = new QTableWidgetItem;
		item->setText(QString::number(pl.radius, 'f', 5));
		item->setTextAlignment(Qt::AlignCenter);
		setItem(r, c++, item);
		/*
		//item->setText(QString::number((pl.x-m_data.x)*m_data.pixelEquivalent,'f',2));
		////item->setFlags(Qt::ItemIsEditable);//可编辑
		//setItem(r, c++, item);

		//item = new QTableWidgetItem;
		//item->setText(QString::number((pl.y - m_data.y)*m_data.pixelEquivalent, 'f', 2));
		////item->setFlags(Qt::ItemIsEditable);//可编辑
		//setItem(r, c++, item);

		//item = new QTableWidgetItem;
		//item->setText(QString::number(pl.width*m_data.pixelEquivalent, 'f', 2));
		////item->setFlags(Qt::ItemIsEditable);//可编辑
		//setItem(r, c++, item);

		//item = new QTableWidgetItem;
		//item->setText(QString::number(pl.length*m_data.pixelEquivalent, 'f', 2));
		////item->setFlags(Qt::ItemIsEditable);//可编辑
		//setItem(r, c++, item);

		//item = new QTableWidgetItem;
		//item->setText(QString::number(pl.contract*m_data.pixelEquivalent, 'f', 2));
		////item->setFlags(Qt::ItemIsEditable);//可编辑
		//setItem(r, c++, item);

		//item = new QTableWidgetItem;
		//item->setText(pl.id);
		////item->setFlags(Qt::ItemIsEditable);//可编辑

		setItem(r, c++, item);*/
	}
	m_itemChanged = true;
}

QVector<PanelLayout> PanelTable::getLayouts()
{
	QVector<PanelLayout> res;
	for (int r = 0; r < rowCount(); r++)
	{
		int c = 1;
		PanelLayout pl;
		pl.serial = r;
		pl.x = item(r, c++)->text().toDouble();
		//pl.x += m_data->GlassX;
		pl.y = item(r, c++)->text().toDouble();
		//pl.y += m_data->GlassY;
		//因为图像逆时针旋转90度  因此实际图像沿流道方向为长
		pl.width = item(r, c++)->text().toDouble();
		pl.height = item(r, c++)->text().toDouble();
		pl.contract = item(r, c++)->text().toDouble();
		pl.panelName = item(r, c++)->text();
		pl.panelModel = item(r, c++)->text();
		pl.radius = item(r, c++)->text().toInt();
		pl.equivalent = m_pixelEquivalent;
		res << pl;
	}
	return res;
}

QVector<PanelLayout> PanelTable::getLayouts(GlassLayout* _panelList, qreal _pixEquivalent, QPointF dataPointf)
{
	//QVector<PanelLayout> res;
	//if (_pixEquivalent < 0.0000001) {
	//	return res;
	//}
	//int a = rowCount();
	//for (int r = 0; r < rowCount(); r++) {
	//	int c = 0;
	//	PanelLayout pl;

	//	pl.num = r;
	//	//把panel坐标转换到glass坐标系内的相对坐标
	//	pl.x_mm = item(r, c++)->text().toDouble();
	//	pl.x = panelPositionToGlassPosition(pl.x_mm, _panelList, 1) / _pixEquivalent;

	//	pl.y_mm = item(r, c++)->text().toDouble();
	//	pl.y = panelPositionToGlassPosition(pl.y_mm, _panelList, 0) / _pixEquivalent;

	//	//因为图像逆时针旋转90度  因此实际图像沿流道方向为长
	//	pl.width_mm = item(r, c++)->text().toDouble();
	//	pl.width = pl.width_mm / _pixEquivalent;

	//	pl.length_mm = item(r, c++)->text().toDouble();
	//	pl.length = pl.length_mm / _pixEquivalent;

	//	pl.contract_mm = item(r, c++)->text().toDouble();
	//	pl.contract = pl.contract_mm / _pixEquivalent;

	//	pl.id = item(r, c++)->text();
	//	pl.panelModel = item(r, c++)->text();

	//	//  [2024/7/12 14:54 zaiyuan.li]
	//	pl.radius = item(r, c++)->text().toDouble();

	//	pl.topDst = _panelList.layoutList.at(c).topDst;
	//	pl.bottomDst = _panelList.layoutList.at(c).bottomDst;
	//	pl.leftDst = _panelList.layoutList.at(c).leftDst;
	//	pl.rightDst = _panelList.layoutList.at(c).rightDst;
	//	if (_panelList.layoutList.size() >= c)
	//	{
	//		pl.modelIgnoreList = _panelList.layoutList.at(c).modelIgnoreList;
	//	}
	//	res << pl;
	//}
	//return res;
}


void PanelTable::setPixelEquivalent(double _pixEquivalent)
{
	m_pixelEquivalent = _pixEquivalent;
}

void PanelTable::clearLayout()
{
	m_itemChanged = false;
	clearContents();
	setRowCount(0);
	m_itemChanged = true;
}

void PanelTable::contextMenuEvent(QContextMenuEvent* event)
{
	QList<QTableWidgetItem*> items = selectedItems();
	if (items.size() > 0)
	{
		m_menu->exec(event->globalPos());
	}
	QTableWidget::contextMenuEvent(event);
}

void PanelTable::slot_action()
{
	QList<QTableWidgetItem*> items = selectedItems();
	QSet<int> indexList;
	for (int i = 0; i < items.size(); i++)
	{
		indexList << items[i]->row();
	}
	m_adjust->showPanelLayout(m_data->PanelLists, indexList.values(), m_data->Station1PixelEquivalent);
}

void PanelTable::slot_batchChanged()
{
	m_data->PanelLists = m_adjust->getChangedPanelLayout();
	showPanelList(m_data, m_panelItem);
	emit sig_panelChanged();
}

void PanelTable::slot_batchSave()
{
	m_data->PanelLists = m_adjust->getChangedPanelLayout();
	showPanelList(m_data, m_panelItem);
	emit sig_panelChanged();
	m_adjust->close();
}

void PanelTable::slot_batchCancel()
{
	m_data->PanelLists = m_adjust->getChangedPanelLayout();
	showPanelList(m_data, m_panelItem);
	emit sig_panelChanged();
	m_adjust->close();
}

void PanelTable::slot_itemSelectAction()
{
	QList<QTableWidgetItem*> items = selectedItems();

	for (int i = 0; i < items.size(); i++)
	{
		m_panelItem[items[i]->row()]->setState(SelectState);
	}
}