#include "FlowDirectionItem.h"


FlowDirectionItem::FlowDirectionItem(QGraphicsItem *parent)
	: QObject(),QAbstractGraphicsShapeItem(parent)
{
	m_brush.setColor(Qt::yellow);
	m_brush.setStyle(Qt::SolidPattern);

	m_timer.setInterval(1000);
	connect(&m_timer, &QTimer::timeout, this, &FlowDirectionItem::toggleDirection);
	m_timer.start();
}

FlowDirectionItem::~FlowDirectionItem()
{
}

void FlowDirectionItem::setData_Direction(GlassLayout* _data)
{
	//m_direction = _data.glassFlowDirection;
	//qreal w = _data.width * 0.4;;
	//switch (m_direction)
	//{
	//case eLeftToRight_LDM:
	//	setWh(w, w * 0.1,eLeftToRight_LDM);  // 传入左到右的流向方向
	//	setPos(_data.x + _data.width * 0.2, _data.y+ _data.length);
	//	break;	
	//case eRightToLeft_LDM:
	//	setWh(w, w * 0.1, eRightToLeft_LDM);
	//	setPos(_data.x + _data.width * 0.2, _data.y + _data.length);
	//	break;	
	//case eTopToBottom_LDM:
	//	setWh(w, w * 0.1, eTopToBottom_LDM);
	//	setPos(_data.x + _data.width, _data.y + w * 0.8);
	//	break;
	//case eBottomToTop_LDM:
	//	setWh(w, w * 0.1, eBottomToTop_LDM);
	//	setPos(_data.x + _data.width, _data.y + w * 0.8);
	//	break;
	//default:
	//	break;
	//}


}
void FlowDirectionItem::setWh(qreal w, qreal h, int direction)
{
	m_polygons1.clear();
	m_polygons2.clear();
	m_direction = direction;

	QVector<qreal> xList;
	qreal q = 0;
	qreal a = w / m_num;
	for (int i = 0; i <= m_num; i++) {
		xList << q;
		q += a;
	}
	qreal h2 = h / 2;

	for (int i = 0; i < xList.size() - 2; i++) {
		QPolygonF r;

		// 从左到右的箭头
		if (m_direction == 0) {
			r.append(QPointF(xList[i], h / 3));
			r.append(QPointF(xList[i] + a / 2, h / 3));
			r.append(QPointF(xList[i] + a, h / 2));
			r.append(QPointF(xList[i] + a / 2, 2 * h / 3));
			r.append(QPointF(xList[i], 2 * h / 3));
			r.append(QPointF(xList[i] + a / 2, h / 2));
		}
		// 从右到左的箭头
		else if (m_direction == 1) {
			r.append(QPointF(xList[i] + a, h / 3));
			r.append(QPointF(xList[i] + a / 2, h / 3));
			r.append(QPointF(xList[i], h / 2));
			r.append(QPointF(xList[i] + a / 2, 2 * h / 3));
			r.append(QPointF(xList[i] + a, 2 * h / 3));
			r.append(QPointF(xList[i] + a / 2, h / 2));
		}
		// 从上到下的箭头
		else if (m_direction == 2) {

			r.append(QPointF(h / 3, xList[i]));
			r.append(QPointF(h / 3, xList[i] + a / 2));
			r.append(QPointF(h / 2, xList[i] + a));
			r.append(QPointF(2 * h / 3, xList[i] + a / 2));
			r.append(QPointF(2 * h / 3, xList[i]));
			r.append(QPointF(h / 2, xList[i] + a / 2));
		}
		// 从下到上的箭头
		else if (m_direction == 3) {
			r.append(QPointF(h / 3, xList[i] + a));
			r.append(QPointF(h / 3, xList[i] +a / 2));
			r.append(QPointF(h / 2, xList[i]));
			r.append(QPointF(2 * h / 3, xList[i] + a / 2));
			r.append(QPointF(2 * h / 3, xList[i] +  a));
			r.append(QPointF(h / 2, xList[i] + a / 2));
		}

		if (i % 2 == 0) {
			m_polygons1 << r;
		}
		else {
			m_polygons2 << r;
		}
	}
	//计算 m_polygons1 和 m_polygons2 中所有箭头的边界，然后将 m_rect 设置为可以包含所有箭头的最小矩形
	qreal left = 0, right = w + a, top = 0, bottom = h;
	for (const QPolygonF& polygon : m_polygons1) {
		QRectF rect = polygon.boundingRect();
		left = qMin(left, rect.left());
		right = qMax(right, rect.right());
		top = qMin(top, rect.top());
		bottom = qMax(bottom, rect.bottom());
	}
	for (const QPolygonF& polygon : m_polygons2) {
		QRectF rect = polygon.boundingRect();
		left = qMin(left, rect.left());
		right = qMax(right, rect.right());
		top = qMin(top, rect.top());
		bottom = qMax(bottom, rect.bottom());
	}
	m_rect = QRect(left, top, right - left, bottom - top);
}

QRectF FlowDirectionItem::boundingRect() const
{
	return m_rect;
}

void FlowDirectionItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	//painter->setBrush(m_brush);

	//if (m_direction == eLeftToRight_LDM || m_direction == eRightToLeft_LDM) {
	//	if (m_direction == eLeftToRight_LDM) {
	//		for (int i = 0; i < m_polygons1.size(); i++) {
	//			painter->drawPolygon(m_polygons1[i]);
	//		}
	//	}
	//	else {
	//		for (int i = 0; i < m_polygons2.size(); i++) {
	//			painter->drawPolygon(m_polygons2[i]);
	//		}
	//	}
	//}
	//else {
	//	if (m_direction == eTopToBottom_LDM) {
	//		for (int i = 0; i < m_polygons1.size(); i++) {
	//			painter->drawPolygon(m_polygons1[i]);
	//		}
	//	}
	//	else {
	//		for (int i = 0; i < m_polygons2.size(); i++) {
	//			painter->drawPolygon(m_polygons2[i]);
	//		}
	//	}
	//}
}

void FlowDirectionItem::toggleDirection()
{
	//if (m_direction == eLeftToRight_LDM) {
	//	m_direction = eRightToLeft_LDM;
	//}
	//else if (m_direction == eRightToLeft_LDM) {
	//	m_direction = eLeftToRight_LDM;
	//}
	//else if (m_direction == eTopToBottom_LDM) {
	//	m_direction = eBottomToTop_LDM;
	//}
	//else if (m_direction == eBottomToTop_LDM) {
	//	m_direction = eTopToBottom_LDM;
	//}
	//update();
}

