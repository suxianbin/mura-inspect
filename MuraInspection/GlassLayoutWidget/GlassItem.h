#pragma once

#include <QObject>
#include <QGraphicsRectItem>
#include "GlassLayout.h"
#include "KeepPenWidthGraphicsItem.h"

class GlassItem : public QGraphicsRectItem
{
	
public:
	GlassItem(QGraphicsItem *parent = Q_NULLPTR);
	~GlassItem() override;

	/************************************
	 * 功能: 设置刻度尺、倒角数据  
	 * 参数: const GlassLayout & _data
	 * 返回: void
	*************************************/
	void setData(GlassLayout* _data);

	/********************************
	 * 功能 重写绘制事件
	 * 输入 painter
	 * 输入 option
	 * 输入 widget
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	QPainterPath shape() const override;

	QRectF boundingRect() const override;

	void paintToImage(QPainter* painter);

protected:
	GlassLayout *m_data;
	QVector<QLineF> m_scaleLabel; //标尺刻度
	QVector<QPair<QString, QRectF>> m_scaleLabelTextH;	//	水平标尺刻度
	QVector<QPair<QString, QRectF>> m_scaleLabelTextV;	//	垂直标尺刻度
	QPainterPath m_trianglePath;

private:

	/************************************
	 * 功能: 旧刻度尺-废弃
	 * 返回: void
	*************************************/
	void calculateScaleLabel();

	/************************************
	 * 功能: 绘制glass中心点在玻璃中心坐标系   
	 * 参数: GOM gom 玻璃坐标模式
	 * 参数: GCM gcm 坐标系方向
	 * 返回: void
	*************************************/
	void calculateScaleLabelFromCenter(eCoorMode mode, eCoorDir dir);

	/************************************
	 * 功能: 绘制glass中心点在玻璃角点坐标系   
	 * 参数: GOM gom 玻璃坐标模式
	 * 参数: GCM gcm 坐标系方向模式
	 * 返回: void
	*************************************/
	void calculateScaleLabelFromVertex(eCoorMode mode, eCoorDir dir);

	/************************************
	 * 功能: 玻璃倒角位置   
	 * 参数: Position position
	 * 返回: void
	*************************************/
	//void calculateTriangle(Position position = eTopRight_Corner);

	/************************************
	 * 功能: 横向刻度尺文字标签  
	 * 参数: QPainter * painter
	 * 返回: void
	*************************************/
	void drawHText(QPainter* painter);
	/************************************
	 * 功能: 纵向刻度尺文字标签   
	 * 参数: QPainter * painter
	 * 返回: void
	*************************************/
	void drawVText(QPainter* painter);

};

class MyGlassItem : public KeepPenWidthGraphicsItem<GlassItem>
{
public:
	MyGlassItem(QGraphicsItem *parent = Q_NULLPTR) {
		QPen p;
		p.setColor(Qt::yellow);
		p.setWidth(1);
		setPen(p);
	};
	~MyGlassItem() override {};

};