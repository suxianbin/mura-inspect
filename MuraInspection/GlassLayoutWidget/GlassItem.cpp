#include "GlassItem.h"
//#include "ROIAbstractBaseItem.h"

GlassItem::GlassItem(QGraphicsItem *parent)
	: QGraphicsRectItem(parent)
{

	
}

GlassItem::~GlassItem()
{
}


void GlassItem::setData(GlassLayout* _data)
{
	m_data = _data;
	setPos(m_data->GlassX, m_data->GlassY);
	//calculateTriangle(_data.chamferlabel);

	switch (_data->GlassCoorMode)
	{
	case eCoorMode::TopLeft:
	case eCoorMode::TopRight:
	case eCoorMode::BotRight:
	case eCoorMode::BotLeft:
		calculateScaleLabelFromVertex(_data->GlassCoorMode, _data->GlassCoorDir);
		break;
	case eCoorMode::Center:
		calculateScaleLabelFromCenter(_data->GlassCoorMode, _data->GlassCoorDir);
		break;
	}
}

void GlassItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	QPen p = pen();

	painter->drawRect(0, 0, m_data->GlassWidth / m_data->Station1PixelEquivalent, m_data->GlassHeight / m_data->Station1PixelEquivalent);
	//==================================绘制右上角的缺口================================================
	QBrush b;
	b.setColor(p.color());
	b.setStyle(Qt::SolidPattern);
	painter->fillPath(m_trianglePath,b);


	//=================================绘制两个标尺===============================================
	p.setColor(Qt::black);
	painter->setPen(p);
	painter->drawLines(m_scaleLabel);

	QFont f;
	f.setPointSize(150);
	painter->setFont(f);
	//绘制文字
	drawHText(painter);
	drawVText(painter);


	//QGraphicsRectItem::paint(painter, option, widget);
}

QPainterPath GlassItem::shape() const
{
	QPainterPath path;
	//将范围外扩 用于自动调整视图范围使用
	QRectF r;
	r.setX(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 8);
	r.setY(-m_data->GlassHeight / m_data->Station1PixelEquivalent / 8);
	r.setWidth(m_data->GlassWidth / m_data->Station1PixelEquivalent / 8 + m_data->GlassWidth / m_data->Station1PixelEquivalent + m_data->GlassWidth / m_data->Station1PixelEquivalent / 35);
	r.setHeight(m_data->GlassHeight / m_data->Station1PixelEquivalent / 8 + m_data->GlassHeight / m_data->Station1PixelEquivalent + m_data->GlassHeight / m_data->Station1PixelEquivalent / 35);
	path.addRect(r);
	return path;
}

QRectF GlassItem::boundingRect() const
{
	return shape().boundingRect();
}

void GlassItem::calculateScaleLabel()
{
	//计算x刻度尺
	QLineF line;
	//横坐标标尺
	line.setLine(0, - m_data->GlassHeight / m_data->Station1PixelEquivalent / 60, m_data->GlassWidth / m_data->Station1PixelEquivalent, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60);
	m_scaleLabel << line;
	
	int i = 0;
	int halfW = m_data->GlassWidth / m_data->Station1PixelEquivalent / 2;
	for (int x = halfW; x <= m_data->GlassWidth / m_data->Station1PixelEquivalent; x += 100)
	{
		if (i % 5 == 0)
		{
			//每5个一个长的标尺
			line.setLine(x , -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60, x , (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3);
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number((halfW-x)*m_data->Station1PixelEquivalent);
			QRectF r(x-100, (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3.5, (m_data->GlassHeight / m_data->Station1PixelEquivalent / 20), 200);
			text.second = r;
			m_scaleLabelTextH << text;
		}
		else
		{
			line.setLine(x , -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60, x , -m_data->GlassHeight / m_data->Station1PixelEquivalent / 30);
			m_scaleLabel << line;
		}
		i++;
	}
	i = 0;
	for (int x = halfW; x >=0; x -= 100)
	{
		if (i % 5 == 0)
		{
			line.setLine(x , -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60, x , (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3);
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number(( halfW-x)*m_data->Station1PixelEquivalent);
			QRectF r(x-100, (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3.5, (m_data->GlassHeight / m_data->Station1PixelEquivalent / 20),  200);
			text.second = r;
			m_scaleLabelTextH << text;
		}
		else
		{
			line.setLine(x, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60, x , -m_data->GlassHeight / m_data->Station1PixelEquivalent / 30);
			m_scaleLabel << line;
		}
		i++;
	}

	//纵坐标标尺
	line.setLine(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60, 0, -m_data->GlassWidth / m_data->Station1PixelEquivalent / 60, m_data->GlassHeight / m_data->Station1PixelEquivalent);
	m_scaleLabel << line;	
	i = 0;
	int halfH = m_data->GlassHeight / m_data->Station1PixelEquivalent / 2;
	for (int y = halfH; y <= m_data->GlassHeight / m_data->Station1PixelEquivalent; y += 100)
	{
		if (i % 5 == 0)
		{
			line.setLine(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60,y, (-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3, y );
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number((halfH - y)*m_data->Station1PixelEquivalent);
			QRectF r((-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3.5 - (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20),y -100, (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20), 200 );
			text.second = r;
			m_scaleLabelTextV << text;

		}
		else
		{
			line.setLine(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60, y , (-m_data->GlassWidth / m_data->Station1PixelEquivalent / 30), y );
			m_scaleLabel << line;
		}
		i++;
	}
	i = 0;
	for (int y = halfH; y >= 0; y -= 100)
	{
		if (i % 5 == 0)
		{
			line.setLine(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60, y , (-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3, y );
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number( (halfH-y)*m_data->Station1PixelEquivalent);
			QRectF r((-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3.5-(m_data->GlassWidth / m_data->Station1PixelEquivalent / 20), y-100, (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20), 200);
			text.second = r;
			m_scaleLabelTextV << text;
		}
		else
		{
			line.setLine(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60, y , (-m_data->GlassWidth / m_data->Station1PixelEquivalent / 30), y );
			m_scaleLabel << line;
		}
		i++;
	}
}

void GlassItem::calculateScaleLabelFromVertex(eCoorMode mode, eCoorDir dir)
{
	//首先清除旧的刻度尺
	m_scaleLabel.clear();
	m_scaleLabelTextH.clear();
	m_scaleLabelTextV.clear();

	//定义刻度尺的方向和原点位置偏移量
	int xDirection = 1;
	int yDirection = 1;
	qreal xOffset = 0;
	qreal yOffset = 0;

	//根据 m_gom 调整原点位置
	switch (mode) {
	case eCoorMode::TopLeft:
		xOffset = 0; yOffset = m_data->GlassHeight / m_data->Station1PixelEquivalent;
		xDirection = 1;  yDirection = -1;
		break;
	case eCoorMode::TopRight:
		xDirection = 1;  yDirection = 1;
		break;
	case eCoorMode::BotRight:  
		xOffset = m_data->GlassWidth / m_data->Station1PixelEquivalent;
		xDirection = -1;  yDirection = 1;
		break;
	case eCoorMode::BotLeft:
		xOffset = m_data->GlassWidth / m_data->Station1PixelEquivalent; yOffset = m_data->GlassHeight / m_data->Station1PixelEquivalent;
		xDirection = -1;  yDirection = -1;
		break;
	case eCoorMode::Center:
	 break;
	}

	int i = 0;
	for (int x = 0; x <= m_data->GlassWidth / m_data->Station1PixelEquivalent; x += 100)
	{
		qreal actualX = xDirection * x + xOffset;
		if (i % 5 == 0) 
		{
			//每5个一个长的标尺
			QLineF line(actualX, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60 * yDirection + yOffset,
				actualX, (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3 * yDirection + yOffset);
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number(x * m_data->Station1PixelEquivalent);

			QRectF r(actualX - 100, (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3.5 * yDirection + yOffset,
				(m_data->GlassHeight / m_data->Station1PixelEquivalent / 20), 200);
			text.second = r;
			m_scaleLabelTextH << text;
		}
		else {
			QLineF line(actualX, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60 * yDirection + yOffset,
				actualX, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 30 * yDirection + yOffset);
			m_scaleLabel << line;
		}
		i++;
	}

	//同样的方式计算y轴刻度尺
	i = 0;

	for (int y = 0; y <= m_data->GlassHeight / m_data->Station1PixelEquivalent; y += 100)
	{
		qreal actualY = yDirection * y + yOffset;
		if (i % 5 == 0) {
			QLineF line(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60 * xDirection + xOffset, actualY,
				(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3 * xDirection + xOffset, actualY);
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number(y * m_data->Station1PixelEquivalent);
			QRectF r((-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3.5 * xDirection + xOffset - (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20),
				actualY - 100, (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20), 200);
			text.second = r;
			m_scaleLabelTextV << text;
		}
		else {
			QLineF line(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60 * xDirection + xOffset, actualY,
				-m_data->GlassWidth / m_data->Station1PixelEquivalent / 30 * xDirection + xOffset, actualY);
			m_scaleLabel << line;
		}
		i++;
	}
}

void GlassItem::calculateScaleLabelFromCenter(eCoorMode mode, eCoorDir dir)
{
	//首先清除旧的刻度尺
	m_scaleLabel.clear();
	m_scaleLabelTextH.clear();
	m_scaleLabelTextV.clear();

	//定义刻度尺的方向和原点位置偏移量
	int xDirection = 1;
	int yDirection = 1;
	qreal xOffset = 0;
	qreal yOffset = 0;

	//根据 m_gom 调整原点位置 

	//根据 m_gcm 调整刻度尺的方向	
	switch (dir)
	{
	case eCoorDir::XY_LeftUp:
		xDirection = -1; yDirection = 1;
		break;
	case eCoorDir::XY_RightUp:
		xDirection = 1;  yDirection = 1;
		break;
	case eCoorDir::XY_RightDown:
		xDirection = 1;  yDirection = -1;
		break;
	case eCoorDir::XY_LeftDown:
		xDirection = -1;  yDirection = -1;
		break;
	}

	//根据上面的方向和偏移量来计算刻度尺
	int i = 0;
	int halfW  =m_data->GlassWidth / m_data->Station1PixelEquivalent  / 2;
	for (int x = 0; x <= m_data->GlassWidth / m_data->Station1PixelEquivalent; x += 100)
	{
		if (i % 5 == 0)
		{
			//每5个一个长的标尺
			qreal actualX =  x + xOffset;
			QLineF line(actualX, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60 + yOffset,
				actualX, (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3  + yOffset);
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number((halfW - x) * m_data->Station1PixelEquivalent* xDirection);

			QRectF r(actualX - 100, (-m_data->GlassHeight / m_data->Station1PixelEquivalent / 60) * 3.5 + yOffset,
				(m_data->GlassHeight / m_data->Station1PixelEquivalent / 20), 200);
			text.second = r;
			m_scaleLabelTextH << text;
		}
		else
		{
			qreal actualX =  x + xOffset;
			QLineF line(actualX, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 60  + yOffset,
				actualX, -m_data->GlassHeight / m_data->Station1PixelEquivalent / 30 + yOffset);
			m_scaleLabel << line;
		}
		i++;
	}

	//同样的方式计算y轴刻度尺
	i = 0;
	int halfH = m_data->GlassHeight / m_data->Station1PixelEquivalent / 2;
	for (int y = 0; y <= m_data->GlassHeight / m_data->Station1PixelEquivalent; y += 100)
	{
		if (i % 5 == 0)
		{
			qreal actualY =  y + yOffset;
			QLineF line(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60  + xOffset, actualY,
				(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3  + xOffset, actualY);
			m_scaleLabel << line;

			//绘制的文字
			QPair<QString, QRectF> text;
			text.first = QString::number((halfH - y) * m_data->Station1PixelEquivalent * yDirection);

			QRectF r((-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60) * 3.5 + xOffset - (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20),
				actualY - 100, (m_data->GlassWidth / m_data->Station1PixelEquivalent / 20), 200);
			text.second = r;
			m_scaleLabelTextV << text;
		}
		else
		{
			qreal actualY =  y + yOffset;
			QLineF line(-m_data->GlassWidth / m_data->Station1PixelEquivalent / 60  + xOffset, actualY,
				-m_data->GlassWidth / m_data->Station1PixelEquivalent / 30  + xOffset, actualY);
			m_scaleLabel << line;
		}
		i++;
	}
}
//
//void GlassItem::calculateTriangle(Position position)
//{
//	QPolygonF pf;
//
//	// 根据传入的位置参数确定三角形的顶点坐标
//	switch (position) {
//	case eTopLeft_Corner:
//		pf.append(QPointF(0, 0));
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent / 40, 0));
//		pf.append(QPointF(0, m_data->GlassWidth / m_data->Station1PixelEquivalent / 40));
//		break;
//	case eTopRight_Corner:
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent - m_data->GlassWidth / m_data->Station1PixelEquivalent / 40, 0));
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent, 0));
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent, m_data->GlassWidth / m_data->Station1PixelEquivalent / 40));
//		break;
//	case eBottomLeft_Corner:
//		pf.append(QPointF(0, m_data->GlassHeight / m_data->Station1PixelEquivalent - m_data->GlassWidth / m_data->Station1PixelEquivalent / 40));
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent / 40, m_data->GlassHeight / m_data->Station1PixelEquivalent));
//		pf.append(QPointF(0, m_data->GlassHeight / m_data->Station1PixelEquivalent));
//		break;
//	case eBottomRight_Corner:
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent - m_data->GlassWidth / m_data->Station1PixelEquivalent / 40, m_data->GlassHeight / m_data->Station1PixelEquivalent));
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent, m_data->GlassHeight / m_data->Station1PixelEquivalent - m_data->GlassWidth / m_data->Station1PixelEquivalent / 40));
//		pf.append(QPointF(m_data->GlassWidth / m_data->Station1PixelEquivalent, m_data->GlassHeight / m_data->Station1PixelEquivalent));
//		break;
//	default:
//		break;
//	}
//	m_trianglePath.clear();
//	m_trianglePath.addPolygon(pf);
//}

void GlassItem::drawHText(QPainter* painter)
{
	for (int i = 0; i < m_scaleLabelTextH.size(); i++)
	{
		// 第1步：变换旋转中心到所绘制文字中心
		painter->translate(m_scaleLabelTextH[i].second.topLeft());
		// 第2步： 旋转一定角度
		painter->rotate(-90);
		//此时坐标原点位于文字左下角
		QRectF r(0, 0, m_scaleLabelTextH[i].second.width(), m_scaleLabelTextH[i].second.height());

		// 根据文字的位置动态调整偏移量
		qreal offset = 400;  // 初始偏移量

		int kk = m_scaleLabelTextH[i].second.top();

		if (m_scaleLabelTextH[i].second.top() > 0) {
			offset = -offset;
		}
		else {
			offset = 0;
		}
		// 微调文字的绘制位置，使其稍微偏离刻度线
		r.adjust(offset, 0, offset, 0);

		painter->drawText(r, Qt::AlignLeft, m_scaleLabelTextH[i].first);
		//恢复原来的坐标系
		painter->rotate(90);
		painter->translate(-m_scaleLabelTextH[i].second.topLeft());
	}
}

void GlassItem::drawVText(QPainter* painter)
{
	for (int i = 0; i < m_scaleLabelTextV.size(); i++)
	{
		// 获取刻度尺文字的矩形区域
		QRectF textRect = m_scaleLabelTextV[i].second;

		// 根据文字的位置动态调整偏移量
		qreal offset = -400;  // 初始偏移量
		if (textRect.left() > 0) {
			offset = -offset;// 如果文字在矩形右侧，将偏移量取负值
		} 
		else{
			offset = 0;
		}
		// 微调文字的绘制位置，使其稍微偏离刻度线
		textRect.adjust(offset, 0, offset, 0);

		// 绘制文字
		painter->drawText(textRect, Qt::AlignRight, m_scaleLabelTextV[i].first);
	}
}

void GlassItem::paintToImage(QPainter* painter)
{
	int x = boundingRect().topLeft().x();
	int y = boundingRect().topLeft().y();
	painter->translate(-x,-y);
	QPen p;
	p.setColor(Qt::yellow);
	p.setWidth(20);
	painter->setPen(p);
	if (painter->transform().m11() > 0) 
	{
		qreal m11 = painter->transform().m11();
		if (m11 > 0.00001 || m11 < -0.00001)
		{
			qreal pwidth = p.widthF() / m11;
			p.setWidthF(pwidth);
			painter->setPen(p);
		}
	}

	painter->drawRect(0, 0, m_data->GlassWidth / m_data->Station1PixelEquivalent, m_data->GlassHeight / m_data->Station1PixelEquivalent);
	//==================================绘制右上角的缺口================================================
	QBrush b;
	b.setColor(p.color());
	b.setStyle(Qt::SolidPattern);
	painter->fillPath(m_trianglePath, b);


	//=================================绘制两个标尺===============================================
	p.setColor(Qt::black);
	painter->setPen(p);
	painter->drawLines(m_scaleLabel);

	QFont f;
	f.setPointSize(150);
	painter->setFont(f);
	//绘制文字
	drawHText(painter);
	drawVText(painter);
	painter->resetTransform();
}


