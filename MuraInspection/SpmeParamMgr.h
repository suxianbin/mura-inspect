#pragma once
#include "SpmeParam.h"
#include "EntityMgr.h"

class SpmeParamMgr : public EntityMgr
{
	//�����ṹ
	SpmeParamMgr();
	~SpmeParamMgr();
public:
	static SpmeParamMgr* Instance()
	{
		static SpmeParamMgr instance;
		return &instance;
	}
	bool Load(std::string name) override;
	bool Save(std::string name) override;
};

