#pragma once

#include <QWidget>
#include "ui_QWndAlgorithmDebug.h"
#include "GvVision.h"
#include "Glass.h"

class QWndAlgorithmDebug : public QWidget
{
	Q_OBJECT
signals:
	void sig_acqCompleted();
public:
	QWndAlgorithmDebug(QWidget *parent = nullptr);
	~QWndAlgorithmDebug();
	void InitTable();
	void loadConfig();
	void refreshAlgoParamListTable();
	void InitPT3();

	QList<QString> GetRecipeFileList();
private:
	Ui::QWndAlgorithmDebugClass ui;
	void setStyleSheet();
private slots:
	void on_pb_AlgoPrmNewClicked();
	void on_pb_AlgoPrmEditClicked();
	void on_pb_AlgoPrmDeleteClicked();
	void on_pb_AlgoPrmSearchClicked();
	void on_pb_ModifyBatchClicked();
	void on_pb_AlgoPrmSaveClicked();
	void on_pb_ExecSingleClicked();
	void on_pb_ExecContinueClicked();
	void on_pb_StopExecClicked();
	void on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

	void slot_GvVision_CheckResult(s_CheckResult rltdata);
	void slot_GvVision_sigProductId(int id);
	void slot_GvVision_sigExeCuteLog(QString log);
private:
	bool ImageData2Mat(const QImage& ImageData, cv::Mat& MatImage);
	/************************************
	 * 功能: 查找对应缺陷属性特征数据
	 * 参数: const std::vector<std::pair<QString
	 * 参数: double>> & vec
	 * 参数: const QString & searchString
	 * 返回: double
	*************************************/
	double findDoubleBySubstring(const std::vector<std::pair<QString, double>>& vec, const QString& searchString);

	/************************************
	 * 功能: 轮廓外接矩形求中心点坐标
	 * 参数: QVector<QPointF> vecQPointF
	 * 返回: QT_NAMESPACE::QPointF
	*************************************/
	QPointF getCentroid(std::vector<cv::Point2f> vecQPointF);

};
