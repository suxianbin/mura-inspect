#include "QWndLight.h"
#include "LightParamMgr.h"
#include "HardwareConfig.h"
#include "QWndMessageBox.h"
#include "LightDrv.h"
#include <QTableWidgetItem>
#pragma execution_character_set("utf-8")

QWndLight::QWndLight(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	loadConfig();
	refreshLightParamListTable();
	m_pLightDrv = new LightDrv(HardwareConfig::Instance()->LightVendor);

	setStyleSheet();
	connect(ui.pb_ModifyBatch, &QPushButton::clicked, this, &QWndLight::on_pb_ModifyBatchClicked);
	connect(ui.pb_LightPrmNew, &QPushButton::clicked, this, &QWndLight::on_pb_LightPrmNewClicked);
	connect(ui.pb_LightPrmEdit, &QPushButton::clicked, this, &QWndLight::on_pb_LightPrmEditClicked);
	connect(ui.pb_LightPrmDelete, &QPushButton::clicked, this, &QWndLight::on_pb_LightPrmDeleteClicked);
	connect(ui.pb_LightPrmSearch, &QPushButton::clicked, this, &QWndLight::on_pb_LightPrmSearchClicked);
	connect(ui.pb_LightPrmSave, &QPushButton::clicked, this, &QWndLight::on_pb_LightPrmSaveClicked);
	
	connect(ui.tw_LightParamList, &QTableWidget::currentCellChanged, this, &QWndLight::on_ParamTableSeletedChanged);

	connect(ui.sp_Light1_Ch1, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light1_Ch1ValueChanged(int)));
	connect(ui.sld_Light1_Ch1, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light1_Ch1ValueChanged(int)));
	connect(ui.sp_Light1_Ch2, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light1_Ch2ValueChanged(int)));
	connect(ui.sld_Light1_Ch2, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light1_Ch2ValueChanged(int)));
	connect(ui.sp_Light1_Ch3, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light1_Ch3ValueChanged(int)));
	connect(ui.sld_Light1_Ch3, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light1_Ch3ValueChanged(int)));
	connect(ui.sp_Light1_Ch4, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light1_Ch4ValueChanged(int)));
	connect(ui.sld_Light1_Ch4, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light1_Ch4ValueChanged(int)));
	connect(ui.sp_Light2_Ch1, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light2_Ch1ValueChanged(int)));
	connect(ui.sld_Light2_Ch1, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light2_Ch1ValueChanged(int)));
	connect(ui.sp_Light2_Ch2, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light2_Ch2ValueChanged(int)));
	connect(ui.sld_Light2_Ch2, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light2_Ch2ValueChanged(int)));
	connect(ui.sp_Light2_Ch3, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light2_Ch3ValueChanged(int)));
	connect(ui.sld_Light2_Ch3, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light2_Ch3ValueChanged(int)));
	connect(ui.sp_Light2_Ch4, SIGNAL(valueChanged(int)), this, SLOT(on_sp_Light2_Ch4ValueChanged(int)));
	connect(ui.sld_Light2_Ch4, SIGNAL(valueChanged(int)), this, SLOT(on_sld_Light2_Ch4ValueChanged(int)));

	connect(ui.chk_Light1_Ch1, &QCheckBox::clicked, this, &QWndLight::on_chk_Light1_Ch1Clicked);
	connect(ui.chk_Light1_Ch2, &QCheckBox::clicked, this, &QWndLight::on_chk_Light1_Ch2Clicked);
	connect(ui.chk_Light1_Ch3, &QCheckBox::clicked, this, &QWndLight::on_chk_Light1_Ch3Clicked);
	connect(ui.chk_Light1_Ch4, &QCheckBox::clicked, this, &QWndLight::on_chk_Light1_Ch4Clicked);
	connect(ui.chk_Light2_Ch1, &QCheckBox::clicked, this, &QWndLight::on_chk_Light2_Ch1Clicked);
	connect(ui.chk_Light2_Ch2, &QCheckBox::clicked, this, &QWndLight::on_chk_Light2_Ch2Clicked);
	connect(ui.chk_Light2_Ch3, &QCheckBox::clicked, this, &QWndLight::on_chk_Light2_Ch3Clicked);
	connect(ui.chk_Light2_Ch4, &QCheckBox::clicked, this, &QWndLight::on_chk_Light2_Ch4Clicked);
}

QWndLight::~QWndLight()
{}

void QWndLight::loadConfig()
{
	QString light1 = HardwareConfig::Instance()->lightSN1;
	QString light2 = HardwareConfig::Instance()->lightSN2;
	bool light1Enable = HardwareConfig::Instance()->lightEnable1;
	bool light2Enable = HardwareConfig::Instance()->lightEnable2;

	ui.le_Light1SN->setEnabled(light1Enable);
	ui.le_Light1SN->setText(light1);
	ui.le_Light2SN->setEnabled(light2Enable);
	ui.le_Light2SN->setText(light2);

}

void QWndLight::refreshLightParamListTable()
{
	QStringList headers;
	headers << "参数名称" << "更新时间" << "备注信息";
	ui.tw_LightParamList->setColumnCount(headers.size());
	ui.tw_LightParamList->setHorizontalHeaderLabels(headers);
	ui.tw_LightParamList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_LightParamList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_LightParamList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_LightParamList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_LightParamList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_LightParamList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_LightParamList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_LightParamList->verticalHeader()->hide();
	ui.tw_LightParamList->setRowCount(LightParamMgr::Instance()->Size());

	int i = 0;
	for (auto var : LightParamMgr::Instance()->FindAll())
	{
		LightParam* light = dynamic_cast<LightParam*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(light->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_LightParamList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(light->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_LightParamList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(light->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_LightParamList->setItem(i, 2, item2);
		i++;
	}
}

void QWndLight::setStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(13,55,147);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_ModifyBatch->setStyleSheet(style);
	ui.pb_LightPrmNew->setStyleSheet(style);
	ui.pb_LightPrmEdit->setStyleSheet(style);
	ui.pb_LightPrmDelete->setStyleSheet(style);
	ui.pb_LightPrmSearch->setStyleSheet(style);
	ui.pb_LightPrmSave->setStyleSheet(style);
}

void QWndLight::refreshParam(LightParam* lightParam)
{
	ui.le_ParamName->setText(lightParam->Key);
	ui.le_Remark->setText(lightParam->Remark);
	ui.chk_Light1_Ch1->setChecked(lightParam->Chanel1Enable);
	ui.chk_Light1_Ch2->setChecked(lightParam->Chanel2Enable);
	ui.chk_Light1_Ch3->setChecked(lightParam->Chanel3Enable);
	ui.chk_Light1_Ch4->setChecked(lightParam->Chanel4Enable);
	ui.chk_Light2_Ch1->setChecked(lightParam->Chanel5Enable);
	ui.chk_Light2_Ch2->setChecked(lightParam->Chanel6Enable);
	ui.chk_Light2_Ch3->setChecked(lightParam->Chanel7Enable);
	ui.chk_Light2_Ch4->setChecked(lightParam->Chanel8Enable);

	ui.sld_Light1_Ch1->setValue(lightParam->Chanel1Intensity);
	ui.sld_Light1_Ch2->setValue(lightParam->Chanel2Intensity);
	ui.sld_Light1_Ch3->setValue(lightParam->Chanel3Intensity);
	ui.sld_Light1_Ch4->setValue(lightParam->Chanel4Intensity);
	ui.sld_Light2_Ch1->setValue(lightParam->Chanel5Intensity);
	ui.sld_Light2_Ch2->setValue(lightParam->Chanel6Intensity);
	ui.sld_Light2_Ch3->setValue(lightParam->Chanel7Intensity);
	ui.sld_Light2_Ch4->setValue(lightParam->Chanel8Intensity);

	ui.sp_Light1_Ch1->setValue(lightParam->Chanel1Intensity);
	ui.sp_Light1_Ch2->setValue(lightParam->Chanel2Intensity);
	ui.sp_Light1_Ch3->setValue(lightParam->Chanel3Intensity);
	ui.sp_Light1_Ch4->setValue(lightParam->Chanel4Intensity);
	ui.sp_Light2_Ch1->setValue(lightParam->Chanel5Intensity);
	ui.sp_Light2_Ch2->setValue(lightParam->Chanel6Intensity);
	ui.sp_Light2_Ch3->setValue(lightParam->Chanel7Intensity);
	ui.sp_Light2_Ch4->setValue(lightParam->Chanel8Intensity);

	ui.sld_Light1_Ch1->setEnabled(lightParam->Chanel1Enable);
	ui.sp_Light1_Ch1->setEnabled(lightParam->Chanel1Enable);
	ui.sld_Light1_Ch2->setEnabled(lightParam->Chanel2Enable);
	ui.sp_Light1_Ch2->setEnabled(lightParam->Chanel2Enable);
	ui.sld_Light1_Ch3->setEnabled(lightParam->Chanel3Enable);
	ui.sp_Light1_Ch3->setEnabled(lightParam->Chanel3Enable);
	ui.sld_Light1_Ch4->setEnabled(lightParam->Chanel4Enable);
	ui.sp_Light1_Ch4->setEnabled(lightParam->Chanel4Enable);
	ui.sld_Light2_Ch1->setEnabled(lightParam->Chanel5Enable);
	ui.sp_Light2_Ch1->setEnabled(lightParam->Chanel5Enable);
	ui.sld_Light2_Ch2->setEnabled(lightParam->Chanel6Enable);
	ui.sp_Light2_Ch2->setEnabled(lightParam->Chanel6Enable);
	ui.sld_Light2_Ch3->setEnabled(lightParam->Chanel7Enable);
	ui.sp_Light2_Ch3->setEnabled(lightParam->Chanel7Enable);
	ui.sld_Light2_Ch4->setEnabled(lightParam->Chanel8Enable);
	ui.sp_Light2_Ch4->setEnabled(lightParam->Chanel8Enable);
}

void QWndLight::on_pb_ModifyBatchClicked()
{
}

void QWndLight::on_pb_LightPrmNewClicked()
{
}

void QWndLight::on_pb_LightPrmEditClicked()
{
}

void QWndLight::on_pb_LightPrmDeleteClicked()
{
}

void QWndLight::on_pb_LightPrmSearchClicked()
{
}

void QWndLight::on_pb_LightPrmSaveClicked()
{
	if (ui.le_ParamName->text().isEmpty())
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未选中有效配方!");
		int ret = messageBox->exec();
		return;
	}

	LightParam* lightPrm = dynamic_cast<LightParam*>(LightParamMgr::Instance()->FindBy(ui.le_ParamName->text()));
	if (!lightPrm)
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未找到配方!" + ui.le_ParamName->text());
		int ret = messageBox->exec();
		return;
	}
	lightPrm->CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm::ss");
	lightPrm->Remark = ui.le_Remark->text();
	lightPrm->Chanel1Enable = ui.chk_Light1_Ch1->isChecked();
	lightPrm->Chanel2Enable = ui.chk_Light1_Ch2->isChecked();
	lightPrm->Chanel3Enable = ui.chk_Light1_Ch3->isChecked();
	lightPrm->Chanel4Enable = ui.chk_Light1_Ch4->isChecked();
	lightPrm->Chanel5Enable = ui.chk_Light2_Ch1->isChecked();
	lightPrm->Chanel6Enable = ui.chk_Light2_Ch2->isChecked();
	lightPrm->Chanel7Enable = ui.chk_Light2_Ch3->isChecked();
	lightPrm->Chanel8Enable = ui.chk_Light2_Ch4->isChecked();
	lightPrm->Chanel1Intensity = ui.sp_Light1_Ch1->value();
	lightPrm->Chanel2Intensity = ui.sp_Light1_Ch2->value();
	lightPrm->Chanel3Intensity = ui.sp_Light1_Ch3->value();
	lightPrm->Chanel4Intensity = ui.sp_Light1_Ch4->value();
	lightPrm->Chanel5Intensity = ui.sp_Light2_Ch1->value();
	lightPrm->Chanel6Intensity = ui.sp_Light2_Ch2->value();
	lightPrm->Chanel7Intensity = ui.sp_Light2_Ch3->value();
	lightPrm->Chanel8Intensity = ui.sp_Light2_Ch4->value();
	LightParamMgr::Instance()->Save("LightMgr.json");
}

void QWndLight::on_sld_Light1_Ch1ValueChanged(int value)
{
	ui.sp_Light1_Ch1->setValue(value);
	m_pLightDrv->SetIntensity(1, value);
}

void QWndLight::on_sp_Light1_Ch1ValueChanged(int value)
{
	ui.sld_Light1_Ch1->setValue(value);
}

void QWndLight::on_sld_Light1_Ch2ValueChanged(int value)
{
	ui.sp_Light1_Ch2->setValue(value);
	m_pLightDrv->SetIntensity(2, value);
}

void QWndLight::on_sp_Light1_Ch2ValueChanged(int value)
{
	ui.sld_Light1_Ch2->setValue(value);
}

void QWndLight::on_sld_Light1_Ch3ValueChanged(int value)
{
	ui.sp_Light1_Ch3->setValue(value);
	m_pLightDrv->SetIntensity(3, value);
}

void QWndLight::on_sp_Light1_Ch3ValueChanged(int value)
{
	ui.sld_Light1_Ch3->setValue(value);
}

void QWndLight::on_sld_Light1_Ch4ValueChanged(int value)
{
	ui.sp_Light1_Ch4->setValue(value);
	m_pLightDrv->SetIntensity(4, value);
}

void QWndLight::on_sp_Light1_Ch4ValueChanged(int value)
{
	ui.sld_Light1_Ch4->setValue(value);
}

void QWndLight::on_sld_Light2_Ch1ValueChanged(int value)
{
	ui.sp_Light2_Ch1->setValue(value);
	m_pLightDrv->SetIntensity(5, value);
}

void QWndLight::on_sp_Light2_Ch1ValueChanged(int value)
{
	ui.sld_Light2_Ch1->setValue(value);
}

void QWndLight::on_sld_Light2_Ch2ValueChanged(int value)
{
	ui.sp_Light2_Ch2->setValue(value);
	m_pLightDrv->SetIntensity(6, value);
}

void QWndLight::on_sp_Light2_Ch2ValueChanged(int value)
{
	ui.sld_Light2_Ch2->setValue(value);
}

void QWndLight::on_sld_Light2_Ch3ValueChanged(int value)
{
	ui.sp_Light2_Ch3->setValue(value);
	m_pLightDrv->SetIntensity(7, value);
}

void QWndLight::on_sp_Light2_Ch3ValueChanged(int value)
{
	ui.sld_Light2_Ch3->setValue(value);
}

void QWndLight::on_sld_Light2_Ch4ValueChanged(int value)
{
	ui.sp_Light2_Ch4->setValue(value);
	m_pLightDrv->SetIntensity(8, value);
}

void QWndLight::on_sp_Light2_Ch4ValueChanged(int value)
{
	ui.sld_Light2_Ch4->setValue(value);
}

void QWndLight::on_chk_Light1_Ch1Clicked(bool checked)
{
	ui.sp_Light1_Ch1->setEnabled(checked);
	ui.sld_Light1_Ch1->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(1);
	}
}

void QWndLight::on_chk_Light1_Ch2Clicked(bool checked)
{
	ui.sp_Light1_Ch2->setEnabled(checked);
	ui.sld_Light1_Ch2->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(2);
	}
}

void QWndLight::on_chk_Light1_Ch3Clicked(bool checked)
{
	ui.sp_Light1_Ch3->setEnabled(checked);
	ui.sld_Light1_Ch3->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(3);
	}
}

void QWndLight::on_chk_Light1_Ch4Clicked(bool checked)
{
	ui.sp_Light1_Ch4->setEnabled(checked);
	ui.sld_Light1_Ch4->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(4);
	}
}

void QWndLight::on_chk_Light2_Ch1Clicked(bool checked)
{
	ui.sp_Light2_Ch1->setEnabled(checked);
	ui.sld_Light2_Ch1->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(5);
	}
}

void QWndLight::on_chk_Light2_Ch2Clicked(bool checked)
{
	ui.sp_Light2_Ch2->setEnabled(checked);
	ui.sld_Light2_Ch2->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(6);
	}
}

void QWndLight::on_chk_Light2_Ch3Clicked(bool checked)
{
	ui.sp_Light2_Ch3->setEnabled(checked);
	ui.sld_Light2_Ch3->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(7);
	}
}

void QWndLight::on_chk_Light2_Ch4Clicked(bool checked)
{
	ui.sp_Light2_Ch4->setEnabled(checked);
	ui.sld_Light2_Ch4->setEnabled(checked);
	if (!checked)
	{
		m_pLightDrv->TurnOff(8);
	}
}

void QWndLight::on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tw_LightParamList->rowCount())
	{
		if (currentRow != previousRow)
		{
			QTableWidgetItem* item = ui.tw_LightParamList->item(currentRow, 0);
			QString Name = item->text();
			LightParam* lightParam = dynamic_cast<LightParam*>(LightParamMgr::Instance()->FindBy(Name));
			refreshParam(lightParam);
		}
	}
	else
	{

	}
}
