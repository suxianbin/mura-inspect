#pragma once

#include <QWidget>
#include "ui_QWndLight.h"

class LightParam;
class LightDrv;

class QWndLight : public QWidget
{
	Q_OBJECT

public:
	QWndLight(QWidget *parent = nullptr);
	~QWndLight();
	void loadConfig();
	void refreshLightParamListTable();
	void setStyleSheet();

	void refreshParam(LightParam* param);
private:
	Ui::QWndLightClass ui;
	LightDrv* m_pLightDrv;
private slots:
	void on_pb_ModifyBatchClicked();
	void on_pb_LightPrmNewClicked();
	void on_pb_LightPrmEditClicked();
	void on_pb_LightPrmDeleteClicked();
	void on_pb_LightPrmSearchClicked();
	void on_pb_LightPrmSaveClicked();

	void on_sld_Light1_Ch1ValueChanged(int value);
	void on_sp_Light1_Ch1ValueChanged(int value);
	void on_sld_Light1_Ch2ValueChanged(int value);
	void on_sp_Light1_Ch2ValueChanged(int value);
	void on_sld_Light1_Ch3ValueChanged(int value);
	void on_sp_Light1_Ch3ValueChanged(int value);
	void on_sld_Light1_Ch4ValueChanged(int value);
	void on_sp_Light1_Ch4ValueChanged(int value);
	void on_sld_Light2_Ch1ValueChanged(int value);
	void on_sp_Light2_Ch1ValueChanged(int value);
	void on_sld_Light2_Ch2ValueChanged(int value);
	void on_sp_Light2_Ch2ValueChanged(int value);
	void on_sld_Light2_Ch3ValueChanged(int value);
	void on_sp_Light2_Ch3ValueChanged(int value);
	void on_sld_Light2_Ch4ValueChanged(int value);
	void on_sp_Light2_Ch4ValueChanged(int value);

	void on_chk_Light1_Ch1Clicked(bool checked);
	void on_chk_Light1_Ch2Clicked(bool checked);
	void on_chk_Light1_Ch3Clicked(bool checked);
	void on_chk_Light1_Ch4Clicked(bool checked);
	void on_chk_Light2_Ch1Clicked(bool checked);
	void on_chk_Light2_Ch2Clicked(bool checked);
	void on_chk_Light2_Ch3Clicked(bool checked);
	void on_chk_Light2_Ch4Clicked(bool checked);

	void on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
};
