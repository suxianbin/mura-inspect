#include "PanelModel.h"
#include <QDateTime>

PanelModel::PanelModel(QString key) : Base(key)
{
	PanelModelName = "DefaultModel";
	CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
	TopDst = 0;
	BotDst = 0;
	LeftDst = 0;
	RightDst = 0;
	Radius = 0;
	Width = 100;
	Height = 100;
}

PanelModel::PanelModel() : Base("")
{
	
}

PanelModel::~PanelModel()
{
}

void PanelModel::write(QJsonObject& json) const
{
	Base::write(json);
	json["PanelModelName"] = PanelModelName;
	json["Remark"] = Remark;
	json["CreateTime"] = CreateTime;
	json["TopDst"] = TopDst;
	json["BotDst"] = BotDst;
	json["LeftDst"] = LeftDst;
	json["RightDst"] = RightDst;
	json["Radius"] = Radius;
	json["Width"] = Width;
	json["Height"] = Height;
}

void PanelModel::read(const QJsonObject& json)
{
	Base::read(json);
	PanelModelName = json["PanelModelName"].toString();
	Remark = json["Remark"].toString();
	CreateTime = json["CreateTime"].toString();
	TopDst = json["TopDst"].toDouble();
	BotDst = json["BotDst"].toDouble();
	LeftDst = json["LeftDst"].toDouble();
	RightDst = json["RightDst"].toDouble() ;
	Radius = json["Radius"].toDouble();
	Width = json["Width"].toDouble();
	Height = json["Height"].toDouble();
}
