#include "RecipeMgr.h"
#include <QJsonArray>
#include "RecipeInfo.h"

RecipeMgr::RecipeMgr()
{
	for (int i = 0; i < 10000; i++)
	{
		BindingMap.insert(std::make_pair(i, ""));
	}	
}

RecipeMgr::~RecipeMgr()
{
}

bool RecipeMgr::Load(std::string name)
{
	QString path = QString::fromStdString(m_path);
	QFile file(path + QString::fromStdString(name));
	if (file.open(QFile::ReadOnly))
	{
		QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
		if (!doc.isNull())
		{
			QJsonObject json = doc.object();
			QJsonArray dataArrays = json["RecipeInfo"].toArray();
			QJsonArray dataArrays2 = json["BindingInfo"].toArray();
			CurrentId = json["CurrentID"].toString();
			CurrentName = json["CurrentName"].toString();
			if (dataArrays.size() == 0)
			{
				return false;
			}
			else
			{
				for (auto dataArr : dataArrays)
				{
					RecipeInfo* prm = new RecipeInfo("");
					QJsonObject obj = dataArr.toObject();
					prm->read(obj);
					RecipeMgr::Instance()->Add(prm);
				}
				int i = 0;
				for (auto dataArr : dataArrays2)
				{
					BindingMap[i++] = dataArr.toString();
				}
				file.close();
				return true;
			}
		}
	}
	file.close();
	return false;
}

bool RecipeMgr::Save(std::string name)
{
	QJsonArray dataArray;
	QJsonArray dataArray2;
	for (std::map<QString, Base*>::iterator itr = m_map.begin(); itr != m_map.end(); ++itr)
	{
		QJsonObject obj;
		itr->second->write(obj);
		dataArray.append(obj);
	}
	for (int i = 0; i < 10000; i++)
	{
		dataArray2.append(BindingMap[i]);
	}
	QJsonObject json;
	json["RecipeInfo"] = dataArray;
	json["BindingInfo"] = dataArray2;
	json["CurrentID"] = CurrentId;
	json["CurrentName"] = CurrentName;
	QJsonDocument doc(json);
	QString path = QString::fromStdString(m_path);
	QDir dir(path);
	if (!dir.exists())
	{
		dir.mkpath(path);
	}
	QFile file(path + QString::fromStdString(name));
	file.open(QFile::WriteOnly);
	file.write(doc.toJson());
	file.close();
	return true;
}

