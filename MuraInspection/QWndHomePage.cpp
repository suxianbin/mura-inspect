#include "QWndHomePage.h"
#include <QHBoxLayout>
#include "DevLog.h"
#include "Glass.h"

QWndHomePage::QWndHomePage(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	SetIcons();
	InitWindow();
	timer2000.setInterval(3000);
	timer2000.start();
	connect(&timer2000, SIGNAL(timeout()), this, SLOT(on_timertimeout()));
	connect(ui.pb_StartInspect, SIGNAL(clicked(bool)), this, SLOT(on_pb_StartInspectClicked(bool)));
	connect(ui.pb_PauseInspect, SIGNAL(clicked()), this, SLOT(on_pb_PauseInspectClicked()));
	connect(ui.pb_StopInspect, SIGNAL(clicked()), this, SLOT(on_pb_StopInspectClicked(bool)));
	connect(ui.pb_NgMark, SIGNAL(clicked()), this, SLOT(on_pb_NgMarkClicked()));
	connect(ui.pb_NgMarkOpen, SIGNAL(clicked()), this, SLOT(on_pb_NgMarkOpenClicked()));
	connect(ui.pb_ManualStop, SIGNAL(clicked()), this, SLOT(on_pb_ManualStopClicked()));
	connect(ui.pb_CoaterStop, SIGNAL(clicked()), this, SLOT(on_pb_CoaterStopClicked()));
	connect(ui.pb_AutoRecipe, SIGNAL(clicked()), this, SLOT(on_pb_AutoRecipeClicked()));
	connect(ui.pb_Review, SIGNAL(clicked()), this, SLOT(on_pb_ReviewClicked()));
	connect(ui.pb_SingleStep, SIGNAL(clicked()), this, SLOT(on_pb_SingleStepClicked()));
	connect(m_pGlassInfo, SIGNAL(sig_DefectSelectChanged(QString, bool)), this, SLOT(on_DefectTypeSelectChanged(QString, bool)));
}

QWndHomePage::~QWndHomePage()
{}

void QWndHomePage::SetIcons()
{
	QString style = "background-color:rgb(245, 245, 249);color:rgb(20, 20, 20);border-radius:5px;border:0px;padding-top: 10px;padding-bottom: 5px;}\
					QToolButton:pressed {background-color:rgba(13,55,147,0.85);}\
					QToolButton:hover {background-color:rgb(107, 173, 239);}\
					QToolButton:checked {background-color:rgb(87, 153, 239);}";
	ui.pb_StartInspect->setStyleSheet(style);
	ui.pb_PauseInspect->setStyleSheet(style);
	ui.pb_StopInspect->setStyleSheet(style);
	ui.pb_NgMark->setStyleSheet(style);
	ui.pb_NgMarkOpen->setStyleSheet(style);
	ui.pb_ManualStop->setStyleSheet(style);
	ui.pb_CoaterStop->setStyleSheet(style);
	ui.pb_AutoRecipe->setStyleSheet(style);
	ui.pb_Review->setStyleSheet(style);
	ui.pb_SingleStep->setStyleSheet(style);

	ui.pb_StartInspect->setCheckable(true);
	ui.pb_PauseInspect->setCheckable(true);
	ui.pb_StopInspect->setCheckable(true);
	ui.pb_Review->setCheckable(true);

	ui.pb_StartInspect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_StartInspect->setIcon(QIcon(":/Icons/ic_homepage_side_start.png"));
	ui.pb_PauseInspect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_PauseInspect->setIcon(QIcon(":/Icons/ic_homepage_side_pause.png"));
	ui.pb_StopInspect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_StopInspect->setIcon(QIcon(":/Icons/ic_homepage_side_stop.png"));
	ui.pb_NgMark->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_NgMark->setIcon(QIcon(":/Icons/ic_homepage_side_ngmark.png"));
	ui.pb_NgMarkOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_NgMarkOpen->setIcon(QIcon(":/Icons/ic_homepage_side_openng.png"));
	ui.pb_ManualStop->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_ManualStop->setIcon(QIcon(":/Icons/ic_homepage_side_manual.png"));
	ui.pb_CoaterStop->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_CoaterStop->setIcon(QIcon(":/Icons/ic_homepage_side_coater.png"));
	ui.pb_AutoRecipe->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_AutoRecipe->setIcon(QIcon(":/Icons/ic_homepage_side_auto.png"));
	ui.pb_Review->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_Review->setIcon(QIcon(":/Icons/ic_homepage_side_one.png"));
	ui.pb_SingleStep->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_SingleStep->setIcon(QIcon(":/Icons/ic_homepage_side_one.png"));
}

void QWndHomePage::InitWindow()
{
	m_pInspInfo = new QWndInspectInfo();
	QHBoxLayout* hlay = new QHBoxLayout();
	hlay->setMargin(0);
	hlay->addWidget(m_pInspInfo);
	ui.frame_defect_info->setLayout(hlay);


	m_pImageShow = new QWndImageMap();
	QHBoxLayout* hlay1 = new QHBoxLayout();
	hlay1->setMargin(0);
	hlay1->addWidget(m_pImageShow);
	ui.frame_imagemap->setLayout(hlay1);

	m_pProcessInfo = new QWndProcessInfo();
	QHBoxLayout* hlay2 = new QHBoxLayout();
	hlay2->setMargin(0);
	hlay2->addWidget(m_pProcessInfo);
	ui.frame_processInfo->setLayout(hlay2);

	m_pGlassInfo = new QWndHomePageGlassInfo();
	QHBoxLayout* hlay3 = new QHBoxLayout();
	hlay3->setMargin(0);
	hlay3->addWidget(m_pGlassInfo);
	ui.frame_glass_info->setLayout(hlay3);

	m_pShowMap = new ShowLayoutWidget(this);
	QHBoxLayout* hlay4 = new QHBoxLayout();
	hlay4->setMargin(0);
	hlay4->addWidget(m_pShowMap);
	ui.frame_map->setLayout(hlay4);
	m_pShowMap->setMouseEnable(false);
	m_pShowMap->suitableView();


	connect(m_pInspInfo->getDefectTable(1), &QWndDefectTable::DefectSelected, this, &QWndHomePage::on_selectedDefectStation1);
	connect(m_pInspInfo->getDefectTable(2), &QWndDefectTable::DefectSelected, this, &QWndHomePage::on_selectedDefectStation2);
}

void QWndHomePage::RefreshWidget()
{
	m_pImageShow->updateImageMap(1);
	m_pInspInfo->updateDefectTable(1);

	m_pShowMap->clearFlaws();

	//m_pInspInfo->updateOrgFlawTable(station,m_runTimeData);
}

//void QWndHomePage::CreateImageShow(ILayoutPlug* plug)
//{
//	m_pImageShow->setStationWidget(1, plug->createLayoutWidget(this));
//	m_pImageShow->setStationWidget(2, plug->createLayoutWidget(this));
//}

//void QWndHomePage::setRunTimeData(RunTimeData* _data)
//{
//	m_runTimeData = _data;
//}

//void QWndHomePage::updateResult(int station)
//{
//	//HANDLE handle = QThread::currentThreadId();
//	//glog << "QWndHomePage 线程ID:" << (int)handle << gendl;
//
//	m_pImageShow->updateImageMap(station, m_runTimeData);
//	colectScreenOutFlaws(m_runTimeData->rlt);
//
//	m_pShowMap->clearFlaws();
//	m_pShowMap->showFlaws(m_showDefects);
//
//	m_pGlassInfo->updateCheckInfo(m_pRecipePlug, m_runTimeData);
//
//	m_pProcessInfo->updateRuntimeData(m_runTimeData);
//	m_pProcessInfo->updateStatus(2, 5);
//
//	m_pInspInfo->updateDefectTable(station,m_runTimeData);
//	m_pInspInfo->updateOrgFlawTable(station,m_runTimeData);
//}
//
//void QWndHomePage::updateRecipeInfo(const RecipeNodeParam& recipeParam)
//{
//	m_pProcessInfo->updateRecipe(m_pRecipePlug, recipeParam);
//	m_pShowMap->showLayoutByName(recipeParam.name, true, false);
//	m_pShowMap->suitableView();
//}
//
//void QWndHomePage::setOperationPlug(IAlgorithmPlug* _operationPlug)
//{
//	m_pOperationPlug = _operationPlug;
//	if (m_pOperationPlug != Q_NULLPTR)
//	{
//		QObject::connect(m_pOperationPlug, &IAlgorithmPlug::sig_acqOperation_CurrentRecipe, this, &QWndHomePage::on_acqOperation_CurrentRecipe);
//		QObject::connect(m_pOperationPlug, &IAlgorithmPlug::sig_acqOperation_StopStep, this, &QWndHomePage::on_acqOperation_StopStep);
//		QObject::connect(m_pOperationPlug, &IAlgorithmPlug::sig_acqOperationExeCuteLog, this, &QWndHomePage::on_acqOperationExeCuteLog);
//	}
//}

//void QWndHomePage::setRecipePlug(IRecipePlug* _recipePlug)
//{
//	m_pRecipePlug = _recipePlug;
//}

//void QWndHomePage::setDeviceControlPlug(IDeviceControlPlug* _devicePlug)
//{
//	m_pDevicePlug = _devicePlug;
//}

//void QWndHomePage::setFilmAnalyzerPlug(IFilmAnalyzerPlug* _filmPlug)
//{
//}

//void QWndHomePage::colectOrgFlaws(const GlassRlt& _data)
//{
//
//}

//void QWndHomePage::colectScreenOutFlaws(const GlassRlt& _data)
//{
//	m_showDefects.clear();
//	for (int i = 0; i < _data.panelRlts.size(); i++)
//	{
//		m_showDefects << _data.panelRlts[i].defects;
//	};
//}

void QWndHomePage::showEvent(QShowEvent* event)
{
	m_pShowMap->showLayoutByName("B12_2");
	QWidget::showEvent(event);
}

void QWndHomePage::on_SelectedDefectChangded(int index)
{
}

void QWndHomePage::on_pb_StartInspectClicked(bool checked)
{
	ui.pb_StopInspect->setChecked(!checked);
	if (checked)
	{
		//m_pOperationPlug->setStartLoopStep(true);
		//emit m_pOperationPlug->sig_acqOperationMode(1);
	}

}

void QWndHomePage::on_pb_PauseInspectClicked()
{
	//emit m_pOperationPlug->sig_acqOperationMode(0);
}

void QWndHomePage::on_pb_StopInspectClicked(bool checked)
{
	ui.pb_StartInspect->setChecked(!checked);
	if (checked) {
		//m_pOperationPlug->setStartLoopStep(false);
		//emit m_pOperationPlug->sig_acqOperationMode(2);// nMode nMode=0单次执行,  nMode=1 连续执行 nMode=2 停止执行
	}
}

void QWndHomePage::on_pb_NgMarkClicked()
{
	emit sigPanelJudgeEdit();
}

void QWndHomePage::on_pb_NgMarkOpenClicked()
{

}

void QWndHomePage::on_pb_ManualStopClicked()
{
}

void QWndHomePage::on_pb_CoaterStopClicked()
{
}

void QWndHomePage::on_pb_AutoRecipeClicked()
{
}

void QWndHomePage::on_pb_ReviewClicked()
{
	//m_runTimeData->rlt.startdateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz");
	//glog << u8"开始算法时间:" << m_runTimeData->rlt.startdateTime << gendl;
	//emit m_pOperationPlug->sig_acqOperationMode(0);// nMode nMode=0单次执行,  nMode=1 连续执行 nMode=2 停止执行
}

void QWndHomePage::on_pb_SingleStepClicked()
{
	//m_pOperationPlug->setStartLoopStep(false);
	//emit m_pOperationPlug->sig_acqOperationMode(0);// nMode nMode=0单次执行,  nMode=1 连续执行 nMode=2 停止执行
}

void QWndHomePage::on_selectedDefectStation1(int index)
{
	m_pImageShow->selectedDefect(1,index);
}

void QWndHomePage::on_selectedDefectStation2(int index)
{
	m_pImageShow->selectedDefect(2, index);
}

void QWndHomePage::on_DefectTypeSelectChanged(QString defectType, bool enable)
{
	QString ty = defectType;
	bool ena = enable;
	//GlassRlt& _data = m_runTimeData->rlt;
	//for (int i = 0; i < _data.panelRlts.size(); i++)
	//{
	//	for (int j = 0;j < _data.panelRlts[i].defects.size();j++)
	//	{
	//		if (_data.panelRlts[i].defects[j].defect.type == defectType)
	//		{
	//			_data.panelRlts[i].defects[j].isVisible = enable;
	//		}
	//	}
	//};
	//updateResult(1);
}

void QWndHomePage::on_acqOperation_CurrentRecipe()
{


}

void QWndHomePage::on_acqOperation_StopStep()
{
}

void QWndHomePage::on_acqOperationExeCuteLog(QString strlog)
{
}

void QWndHomePage::on_timertimeout()
{
	//m_pDevicePlug->SendMsgToVision3(100);
	//timer2000.stop();
}
