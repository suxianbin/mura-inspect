#include "HardwareConfig.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include "DevLog.h"

HardwareConfig::HardwareConfig()
{
    plcIP = "127.0.0.1";
    plcPort = 9600;

    plcCommonAddress = 18000;  //
    pcCommonAddress = 19000;   //电脑发送给PLC起始寄存器地址
    //光源控制器1配置参数
    lightIP1 = "";
    lightPort1 = 9000;
    lightSN1 = "";
    //光源控制器2配置参数
    lightIP2 = "";
    lightPort2 = 9000;
    lightSN2 = "";

    //相机1配置参数
    Camere1_COM = "";
    //相机2配置参数
    Camere2_COM = "";
    //相机3配置参数
    Camere3_COM = "";
    //相机4配置参数
    Camere4_COM = "";
    //相机5配置参数
    Camere5_COM = "";

    //分光计配置参数
    OMAddr = "";
    OMPort = 5000;
    CTSAddr = "";
    CTSPort = 5001;

    Station1Enable = true;
    Station2Enable = true;
    SPMEEnable = false;
}

HardwareConfig::~HardwareConfig()
{
}

bool HardwareConfig::writeToFile(const QString& path)
{
    QJsonObject json;
    json["plcIP"] = plcIP;
    json["plcPort"] = plcPort;
    json["plcCommonAddress"] = plcCommonAddress;
    json["pcCommonAddress"] = pcCommonAddress;
    json["LightVendor"] = (int)LightVendor;
    json["lightIP1"] = lightIP1;
    json["lightPort1"] = lightPort1;
    json["lightSN1"] = lightSN1;
    json["lightEnable1"] = lightEnable1;
    json["lightIP2"] = lightIP2;
    json["lightPort2"] = lightPort2;
    json["lightSN2"] = lightSN2;
    json["lightEnable2"] = lightEnable2;
    json["Camere1_COM"] = Camere1_COM;
    json["Camere2_COM"] = Camere2_COM;
    json["Camere3_COM"] = Camere3_COM;
    json["Camere4_COM"] = Camere4_COM;
    json["Camere5_COM"] = Camere5_COM;
    json["OMAddr"] = OMAddr;
    json["OMPort"] = OMPort;
    json["CTSAddr"] = CTSAddr;
    json["CTSPort"] = CTSPort;
    json["Station1Enable"] = Station1Enable;
    json["Station2Enable"] = Station2Enable;
    json["SPMEEnable"] = SPMEEnable;
    QJsonDocument doc(json);
    QDir dir(path);
    if (!dir.exists())
    {
        dir.mkpath(path);
    }
    QFile file(path + QString::fromStdString("HardwareConfig.json"));
    file.open(QFile::WriteOnly);
    file.write(doc.toJson());
    file.close();
    return true;
}

bool HardwareConfig::loadFromFile(const QString& path)
{
    QFile file(path + "HardwareConfig.json");
    if (file.open(QFile::ReadOnly))
    {
        QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
        if (!doc.isNull())
        {
            QJsonObject json = doc.object();
            plcIP = json["plcIP"].toString();
            plcPort = json["plcPort"].toInt();
            plcCommonAddress = json["plcCommonAddress"].toInt();
            pcCommonAddress = json["pcCommonAddress"].toInt();
            LightVendor = (eLightVendor)json["LightVendor"].toInt();
            lightIP1 = json["lightIP1"].toString();
            lightPort1 = json["lightPort1"].toInt();
            lightSN1 = json["lightSN1"].toString();
            lightEnable1 = json["lightEnable1"].toBool();
            lightIP2 = json["lightIP2"].toString();
            lightPort2 = json["lightPort2"].toInt();
            lightSN2 = json["lightSN2"].toString();
            lightEnable2 = json["lightEnable2"].toBool();
            Camere1_COM = json["Camere1_COM"].toString();
            Camere2_COM = json["Camere2_COM"].toString();
            Camere3_COM = json["Camere3_COM"].toString();
            Camere4_COM = json["Camere4_COM"].toString();
            Camere5_COM = json["Camere5_COM"].toString();
            OMAddr = json["OMAddr"].toString();
            OMPort = json["OMPort"].toInt();
            CTSAddr = json["CTSAddr"].toString();
            CTSPort = json["CTSPort"].toInt();
            Station1Enable = json["Station1Enable"].toBool();
            Station2Enable = json["Station2Enable"].toBool();
            SPMEEnable = json["SPMEEnable"].toBool();
            return true;
        }
        else
        {
            glog << "Json文件异常:" << path + "HardwareConfig.json" << gendl;
        }
    }
    return false;
}
