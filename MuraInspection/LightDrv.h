#pragma once
#include "LightBase.h"

class LightDrv
{
public:
	LightDrv(eLightVendor vendor);
	~LightDrv();
	void SetIntensity(int ch, int intensity);
	void TurnOff(int ch);
private:
	LightBase* lightDrv;
};

