#include "SpmeParam.h"
#include <QJsonArray>

SpmeParam::SpmeParam(QString key) : Base(key)
{

}

SpmeParam::~SpmeParam()
{
}

void SpmeParam::write(QJsonObject& json) const
{
	Base::write(json);
	json["SpmeType"] = (int)SpmeType;
	json["Remark"] = Remark;
	json["CreateTime"] = CreateTime;
	QJsonArray arrayMajor;
	QJsonArray arrayMinor;
	for (int i = 0; i < MajorPoints.size(); i++)
	{

		QJsonObject majorPts;
		MajorPoints.at(i).write(majorPts);
		arrayMajor.append(majorPts);
	}
	json["MajorPoints"] = arrayMajor;

	for (int i = 0; i < MinorPoints.size(); i++)
	{

		QJsonObject minorPts;
		MajorPoints.at(i).write(minorPts);
		arrayMinor.append(minorPts);
	}
	json["MinorPoints"] = arrayMinor;

}

void SpmeParam::read(const QJsonObject& json)
{
	Base::read(json);
	SpmeType = (eSpmeType)json["SpmeType"].toInt();
	Remark = json["Remark"].toString();
	CreateTime = json["CreateTime"].toString();

	if (json.contains("MajorPoints") && json["MajorPoints"].isArray())
	{
		QJsonArray arrayMajor = json["MajorPoints"].toArray();
		for (int i = 0; i < arrayMajor.size(); i++)
		{
			QJsonObject obj = arrayMajor[i].toObject();
			SpmeMeasPoint pts;
			pts.read(obj);
			MajorPoints.append(pts);
		}
	}

	if (json.contains("MinorPoints") && json["MinorPoints"].isArray())
	{

		QJsonArray arrayMinor = json["MinorPoints"].toArray();
		for (int i = 0; i < arrayMinor.size(); i++)
		{
			QJsonObject obj = arrayMinor[i].toObject();
			SpmeMeasPoint pts;
			pts.read(obj);
			MinorPoints.append(pts);
		}
	}
}
