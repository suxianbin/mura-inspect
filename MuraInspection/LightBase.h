#pragma once
#include "HardwareConfig.h"
#include <QObject>

class LightBase : public QObject
{
	Q_OBJECT
public:
	LightBase();
	virtual ~LightBase();
	virtual void SetIntensity(int ch, int intensity) = 0;
	virtual void TurnOff(int ch) = 0;
};

