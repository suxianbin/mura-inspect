﻿#pragma once
#include "qftp.h"
#include <map>
#include <QObject>
#include <QHash>
#include <QVector>
//#include <QNetworkConfigurationManager>
#include <QEventLoop>

QT_BEGIN_NAMESPACE
class QDialogButtonBox;
class QFile;
class QFtp;
class QLabel;
class QLineEdit;
class QTreeWidget;
class QTreeWidgetItem;
class QProgressDialog;
class QPushButton;
class QUrlInfo;
class QNetworkSession;
QT_END_NAMESPACE

enum class eFtpResult
{
	CONENCT_FAIL,   //连接失败
	CONNECT_SUCCESS,//连接成功
	UPLOAD_FAIL,    //上传失败
	UPLOAD_SUCCESS, //上传成功
	DOWNLOAD_FAIL,  //下载失败
	DOWNLOAD_SUCCESS,//下载成功
	DIRNOTEXIST,    //路径不存在
	FILENOTEXIST,   //文件不存在
	UPLOADFILENOTEXIST,//待上传文件不存在

	LISTFINISHED,
};

class FtpClient : public QObject
{
	Q_OBJECT
public:
	FtpClient();
	~FtpClient();
	/// <summary>
	/// 连接到FTP服务器
	/// </summary>
	/// <param name="ip">服务器IP</param>
	/// <param name="user">服务器开放的用户名</param>
	/// <param name="pwd">用户密码</param>
	bool connectServer(QString ip, QString user, QString pwd);

	/// <summary>
	/// 与服务器断开连接
	/// </summary>
	void disconect();

	/// <summary>
	/// 从当前返回上级目录。相当于./
	/// </summary>
	void cdToParent();

	/// <summary>
	/// 从当前目录去到下级目录,path可以是多级目录,最前面有/,从根目录下找
	/// 最前面没有/，从当前目录下找
	/// </summary>
	/// <param name="Path"></param>
	bool cd(QString path);

	/// <summary>
	/// 上传文件到服务器
	/// </summary>
	/// <param name="file">文件在本地的全路径与文件名(D:/test.txt)</param>
	/// <param name="path"></param>
	bool uploadFile(QString filePath, QString remotePath, QString remotefileName);

	/// <summary>
	/// 从服务器下载指定文件
	/// </summary>
	/// <param name="remoteFullPath"></param>
	/// <param name="localPath">文件存放的路径(D:/usr01),不包括文件名</param>
	bool downloadFile(QString remoteFullPath, QString localPath);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="filename"></param>
	void deleteFile(QString filename);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="dir"></param>
	void deleteDir(QString dir);

	/// <summary>
	/// 
	/// </summary>
	void deleteMulDir(); //删除多级目录

	/// <summary>
	/// 在服务器根目录上建立指定目录
	/// 最前面没有/,从当前目录下创建（cfftp/on）,最前面有/,从根目录下创建
	/// </summary>
	/// <param name="">可以是单级目录（cfftp），也可以是多级目录(/cfftp/on/data)</param>
	bool mkDir(QString);

	/// <summary>
	/// 单级路径是否存在
	/// </summary>
	/// <param name="dir"></param>
	/// <returns></returns>
	//bool existDir(QString dir);

	/// <summary>
	/// 多级路径是否存在
	/// </summary>
	/// <param name="mulDir"></param>
	/// <returns></returns>
	bool existMulDir(QString mulDir);

	void updateDataTransferProgress(qint64 readBytes, qint64 totalBytes);
	std::map<QString, bool> listFile();
	QFtp::State State();
private:
	void ftpCommandFinished(int commandId, bool error);
	void addToList(const QUrlInfo& urlInfo);
	//void doFinished();
	void ftpStateChanged(int);
	bool connectServer();
	std::map<QString, bool> fileList;
	QHash<QString, bool> isDirectory;
	QString currentPath;
	QString tmpCdPath;
	QString lastCommand;
	QFtp* ftp;
	QFile* file;
	QNetworkSession* networkSession;
	//QNetworkConfigurationManager manager;

	QString m_ip;
	QString m_userName;
	QString m_pwd;
	bool ExecSuccess = false;
signals:
	void FtpClientSig(eFtpResult);
	void doneSignal();
};