#pragma once
#include "MCExport.h"
#include <QByteArray>
#include <QVector>
#include <QString>
#include <QPointF>
#include "JobData.h"

class InteractiveUtil : public MC_EXPORT
{
	Q_OBJECT
public:
	InteractiveUtil(QObject* parent = Q_NULLPTR);
	~InteractiveUtil() override;

	int PLC_Address = 0;
	int PC_Address = 0;
	bool readPlcCommandData();
	bool sendPcCommandData();

	bool readPLCJobData(JobData& data);
	bool sendPCJobData(JobData& data);
	bool readPLCCurrentPos(double& x, double& y);
	int readPLCLocalRecipeId(bool& f);
	bool sendPCLocalRecipeId(int& nLocalRecipe);
	bool sendPCRecipeIdState(int& nState);
	bool sendRecipeList(QVector<QString>& vecRecipeList);
	bool sendSpmeMeasurePoints(QVector<QPointF>& vecMeasurePoint);
	int readPCAlarmStatus(bool& f);
	bool sendPCAlarmStatus(int& nSate);
	bool sendPCPRUNDelay(int& nSate);
private:
	static bool VecToArray(const QVector<quint16>& srcData, QByteArray& disData);
	static bool ArrayToVec(const QByteArray& srcData, QVector<quint16>& disData);
	static bool ArrayToJobData(const QByteArray& srcData, JobData& disData);
	static bool JobDataToArray(const JobData& srcData, QByteArray& disData);
	static bool RecipeListDataToArray(const QVector<QString>& VecData, QByteArray& disData);
	static bool MeasurePointListToArray(const QVector<QPointF>& VecData, QByteArray& disData);
	static bool Bytes2UShort(const QByteArray& srcData, int position, quint16& res);
	static bool Bytes2Int(const QByteArray& srcData, int position, int& res);
	static QByteArray UShort2Bytes(quint16 i);
	static QByteArray UShort2Bytes(quint16 i, int nsize);
	static QByteArray QString2Bytes(const QString& str, int len);
	static bool IntToByte(const int number, QByteArray& disData);
};

