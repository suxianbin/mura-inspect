#pragma once
#include <QLineEdit>
#include <QComboBox>
#include <QSplitter>
#include <QPushButton>
#include <QTextBrowser>
#include <QGroupBox>
#include <QTableWidget>
#include <QLabel>
#include "SpmeParam.h"
#include <QTcpSocket>

class SPMEControl : public QObject
{
	Q_OBJECT
public:
	SPMEControl(QObject* parent = nullptr);
	~SPMEControl();
	void InitSpme();
	void IsConnected();
	void SetMeasType(eSpmeType type);
	void SetMeasCondition(int measID);
	void SetAnalyzeCondition(int condition);
	void MeasReference();
	void MeasSample();
	void AnalyzeSample();
	void SetIsMeasReference(bool needMeas);

	//void 
private slots:
	void on_RecipeChanged(const QString& recipeName); //接收到配方更改信号
	void on_RecieveTestResult(QString msg);  //接收到spme发送过来的测量结果
private:
	QTcpSocket* m_pClient; //通信客户端
	int m_commandIndex = 0;
	bool m_isRecieveReply = false;
	eSpmeType m_spmeType = eSpmeType::null;
	double m_OD = 0;
	double m_thick = 0;
	double m_color_x = 0;
	double m_color_y = 0;
	double m_color_Y = 0;

	
	bool ProcessRecievedMsg(QString msg); //处理从spme接收到的数据
};
