#pragma once
#include <QPainter>
#include <QWidget>
#include "ui_ModelPainter.h"
#include "PanelModel.h"
#include "QPainterPath.h"
#include <QtMath>

class ModelPainter : public QWidget
{
	Q_OBJECT

public:
	ModelPainter(QWidget *parent = Q_NULLPTR);
	~ModelPainter() override;

	/********************************
	 * 功能 绘制model
	 * 输入 _model 
	 ********************************/
	void drawModel(const PanelModel& _model);
	
	/********************************
	 * 功能 清除绘制的model
	 ********************************/
	void clearModel();
private:
	PanelModel m_model;
	double m_scale = 1.0;
	bool m_drawModel = false;
	double m_x = 0;
	double m_y = 0;
	double m_w = 0;
	double m_h = 0;

	//  [2024/7/12 17:26 zaiyuan.li]
	double m_radius = 0;			//圆角半径
	//内缩坐標
	double m_xContract = 0;
	double m_yContract = 0;
	double m_wContract = 0;
	double m_hContract = 0;


	Ui::ModelPainter ui;
private:
	/********************************
	 * 功能 绘制事件
	 * 输入 event 
	 ********************************/
	void paintEvent(QPaintEvent* event) override;

	/********************************
	 * 功能 计算相关信息
	 ********************************/
	void calculate();

	

	/********************************
	 * 功能 大小改变事件
	 * 输入 event 
	 ********************************/
	void resizeEvent(QResizeEvent* event) override;
};
