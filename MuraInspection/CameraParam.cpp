#include "CameraParam.h"

CameraParam::CameraParam(QString key) : Base(key)
{
}

CameraParam::~CameraParam()
{
}

void CameraParam::write(QJsonObject& json) const
{
	Base::write(json);
	json["CreateTime"] = CreateTime;
	json["Remark"] = Remark;
	json["ExposureTime"] = ExposureTime;
	json["SelectedUserFFTIndex"] = SelectedUserFFTIndex;
	json["BaudRateIndex"] = BaudRateIndex;

}

void CameraParam::read(const QJsonObject& json)
{
	Base::read(json);
	CreateTime = json["CreateTime"].toString();
	Remark = json["Remark"].toString();
	ExposureTime = json["ExposureTime"].toInt();
	SelectedUserFFTIndex = json["SelectedUserFFTIndex"].toInt();
	BaudRateIndex = json["BaudRateIndex"].toInt();
}
