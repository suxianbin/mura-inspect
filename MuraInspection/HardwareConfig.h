#pragma once
#include <QString>

enum class eLightVendor
{
	null,
	OPT,
	RUISHI
};

class HardwareConfig
{
private:
	HardwareConfig();
	~HardwareConfig();
public:
	static HardwareConfig* Instance()
	{
		static HardwareConfig config;
		return &config;
	}

	QString plcIP;
	int plcPort;

	int plcCommonAddress;  //
	int pcCommonAddress;   //电脑发送给PLC起始寄存器地址

	eLightVendor LightVendor = eLightVendor::null;
	//光源控制器1配置参数
	QString lightIP1;
	int lightPort1;
	QString lightSN1;
	bool lightEnable1;

	//光源控制器2配置参数
	QString lightIP2;
	int lightPort2;
	QString lightSN2;
	bool lightEnable2;

	//相机1配置参数
	QString Camere1_COM;
	//相机2配置参数
	QString Camere2_COM;
	//相机3配置参数
	QString Camere3_COM;
	//相机4配置参数
	QString Camere4_COM;
	//相机5配置参数
	QString Camere5_COM;

	//分光计配置参数
	QString OMAddr;
	int OMPort;
	QString CTSAddr;
	int CTSPort;

	bool Station1Enable;
	bool Station2Enable;
	bool SPMEEnable;

	bool writeToFile(const QString& path);
	bool loadFromFile(const QString& path);
};

