#pragma once
#include <QString>
#include "SpmeParam.h"

enum class eUnitType
{
	null,
	AI,
	NM,
	UM,
};

class SpmeResult
{
public:
	QString GlassID;
	eSpmeType SpmeType = eSpmeType::null;
	QString MeasDateTime;
	eUnitType UnitType = eUnitType::null;
	QString RecipeType = "";

	double ValueColor_x = 0;
	double ValueColor_y = 0;
	double ValueColor_Y = 0;

	double ValueOD = 0;
	double ValueThickness = 0;

	double OOCMin = 0.0;
	double OOCMax = 0.0;
	double OOSMin = 0.0;
	double OOSMax = 0.0;

};

