/*****************************************
 * 作者: bhj
 * 日期: 2022-11-23
 * 功能：初始化屏幕
 * ***************************************/

#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <QSplashScreen>
#include <QPropertyAnimation>
#include <QMouseEvent>

class SplashScreen : public QSplashScreen
{
    Q_OBJECT
public:
    explicit SplashScreen(const QPixmap &pixmap);
    ~SplashScreen();

    static SplashScreen *getInstance();

public:
	void setStagePercent(const int &percent, const QString &message);
	void setStart(QWidget *widget, const QString &title, const QString &logoFile);
	void setFinish();

protected:
    void paintEvent (QPaintEvent *event);
	void mousePressEvent(QMouseEvent *);

private:
    double m_percent;
    QWidget *m_mainWidget;
    QPixmap m_pixLogo;
    QString m_textLogo;
    QString m_message;
    static SplashScreen *m_pInst;
    QPropertyAnimation *m_propertyAnimation;

    const int ANIMATION_DURATION = 100;
};

#endif // SPLASHSCREEN_H
