#include "McLoop.h"
#include "CommandData.h"
#include <QApplication>
#include <QFile>
#include "HardwareConfig.h"
#include "DevLog.h"

McLoop::McLoop(QObject* parent)
{
	Init();
}

McLoop::~McLoop()
{
}

void McLoop::Init()
{
	if (m_util != Q_NULLPTR)
	{
		m_util->deleteLater();
		m_util = Q_NULLPTR;
	}
	if (m_hbTimer != Q_NULLPTR)
	{
		m_hbTimer->deleteLater();
		m_hbTimer = Q_NULLPTR;
	}

	m_util = new InteractiveUtil();
	m_util->moveToThread(this);

	m_hbTimer = new QTimer;
	m_hbTimer->setInterval(1000);
	m_hbTimer->setSingleShot(false);
	m_hbTimer->start();
	connect(m_hbTimer, &QTimer::timeout, [=]()
		{
			CommandData::setCommandHeartBeat(m_hbf);
			m_hbf = !m_hbf;
		});
}

void McLoop::ResetStation1()
{

}

void McLoop::ResetStation2()
{
	if (HardwareConfig::Instance()->Station2Enable)
	{
		
	}
	else
	{

	}
}

bool McLoop::IsConnecting()
{
	return m_util->state() == QAbstractSocket::ConnectedState;
}

void McLoop::SendSpmeMeasurePoints(QVector<QPointF> points)
{
}

void McLoop::SendJobData()
{
}

void McLoop::run()
{
	while (1)
	{
		if (IsConnecting())
		{
			m_util->readPlcCommandData();

			m_util->sendPcCommandData();

			//m_util->readPLCCurrentPos(PLCCurrentPosX, PLCCurrentPosY);
			SwitchRecipe();

			if (HardwareConfig::Instance()->Station1Enable)
			{
				Station1ScanLoop();
			}
			if (HardwareConfig::Instance()->Station2Enable)
			{
				Station2ScanLoop();
			}
			if (HardwareConfig::Instance()->SPMEEnable)
			{
				SPMELoop();
			}

			if (ReadJobData)
			{
				 m_util->readPLCJobData(m_jobData);
			}
			if (SyncJobData)
			{
				m_util->sendPCJobData(m_jobData);
			}
		}
		QApplication::processEvents();
		msleep(50);
	}

}

void McLoop::generalPolling()
{

}

void McLoop::SwitchRecipe()
{
	int isAutoChange = (int)CommandData::getPLCommand()->at(10);//读取PLC是否自动切换RecipeId状态 1:自动 0：手动
	if (1 == isAutoChange)
	{
		//保持plc下发的recipeId 与 当前recipeId 一直
		bool pcF = false;
		int plcRecipeId = m_util->readPLCLocalRecipeId(pcF);//获取PLC RecipeId
		if (pcF)
		{
			emit sig_revicedRecipeId(isAutoChange, plcRecipeId);//（自动还是手动切换recipe,0 手动	1 开启）
		}
	}
}

void McLoop::Station1ScanLoop()
{
	switch (Station1Step)
	{
	case eScanStep::WaitGlassLoadDone:
		Station1RunStepStr = "等待玻璃装载完成";
		if (CommandData::getPLCCommandLoadDone())
		{
			ScaneStartTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss zzz");
			m_util->readPLCJobData(m_jobData);
			Station1Step = eScanStep::GlassLoadDoneReply;
		}
		break;
	case eScanStep::GlassLoadDoneReply:
		Station1RunStepStr = "等待玻璃装载完成回应";
		CommandData::setPCCommandLoadDoneReply(true);
		Station1Step = eScanStep::ScanRequst;
		break;
	case eScanStep::ScanRequst:
		Station1RunStepStr = "PC请求扫描";
		CommandData::setPCCommandLoadDoneReply(false);
		CommandData::setPCCommandScanRequest(true);
		Station1Step = eScanStep::WaitScanRequstReply;
		break;
	case eScanStep::WaitScanRequstReply:
		Station1RunStepStr = "等扫描请求回应";
		if (CommandData::getPLCCommandScanRequestReply())
		{
			CommandData::setPCCommandScanRequest(false);
			Station1Step = eScanStep::WaitScanDone;
		}
		break;
	case eScanStep::WaitScanDone:
		Station1RunStepStr = "正在扫描";
		if (CommandData::getPLCCommandScanDoneRequest())
		{

			Station1Step = eScanStep::ScanDoneReply;
		}
		break;
	case eScanStep::ScanDoneReply:
		Station1RunStepStr = "扫描完成";
		CommandData::setPCCommandScanDoneReply(true);
		Station1Step = eScanStep::GlassChecking;
		break;
	case eScanStep::GlassChecking:
		Station1RunStepStr = "正在检测";
		if (!CommandData::getPLCCommandScanDoneRequest())
		{
			CommandData::setPCCommandScanDoneReply(false);
			m_jobData.JobJudgeCode = "G";
			m_jobData.JobGradeCode = "G";
			m_util->sendPCJobData(m_jobData);
			Station1Step = eScanStep::UnLoadRequst;
		}
		break;
	case eScanStep::UnLoadRequst:
		Station1RunStepStr = "正在下玻璃到SPME";
		CommandData::setPCCommandUnloadRequest(true);
		Station1Step = eScanStep::WaitUnloadFinished;
		break;
	case eScanStep::WaitUnloadFinished:
		Station1RunStepStr = "下玻璃到SPME完成";
		if (CommandData::getPLCCommandUnLoadRequestReply())
		{
			CommandData::setPCCommandUnloadRequest(false);
			Station1Step = eScanStep::WaitGlassLoadDone;
		}
		break;
	default:
		break;
	}
}

void McLoop::Station2ScanLoop()
{
	switch (Station2Step)
	{
	case eScanStep::WaitGlassLoadDone:
		if (CommandData::getPLCCommandStation2LoadDone())
		{
			bool f = false;
			Station2Step = eScanStep::GlassLoadDoneReply;
		}
		break;
	case eScanStep::GlassLoadDoneReply:
		CommandData::setPCCommandStation2LoadDoneReply(true);
		Station2Step = eScanStep::ScanRequst;
		break;
	case eScanStep::ScanRequst:
		CommandData::setPCCommandStation2LoadDoneReply(false);
		CommandData::setPCCommandStation2ScanRequest(true);
		Station2Step = eScanStep::WaitScanRequstReply;
		break;
	case eScanStep::WaitScanRequstReply:
		if (CommandData::getPLCCommandStation2ScanRequestReply())
		{
			CommandData::setPCCommandStation2ScanRequest(false);
			Station2Step = eScanStep::WaitScanDone;
		}
		break;
	case eScanStep::WaitScanDone:
		if (CommandData::getPLCCommandStation2ScanDoneRequest())
		{

			Station2Step = eScanStep::ScanDoneReply;
		}
		break;
	case eScanStep::ScanDoneReply:
		CommandData::setPCCommandStation2ScanDoneReply(true);
		Station2Step = eScanStep::GlassChecking;
		break;
	case eScanStep::GlassChecking:
		if (!CommandData::getPLCCommandStation2ScanDoneRequest())
		{
			CommandData::setPCCommandStation2ScanDoneReply(false);
			m_util->sendPCJobData(m_jobData);
			Station2Step = eScanStep::UnLoadRequst;
		}
		break;
	case eScanStep::UnLoadRequst:
		CommandData::setPCCommandStation2UnloadRequest(true);
		Station2Step = eScanStep::WaitUnloadFinished;
		break;
	case eScanStep::WaitUnloadFinished:
		if (CommandData::getPLCCommandStation2UnLoadRequestReply())
		{
			CommandData::setPCCommandStation2UnloadRequest(false);
			Station2Step = eScanStep::WaitGlassLoadDone;
		}
		break;
	default:
		break;
	}
}

void McLoop::SPMELoop()
{
	switch (SpmeStep)
	{
	case eSpmeStep::WaitGlassReady:
		SpmeRunStepStr = "SPME等待玻璃就绪";
		WaitGlassReadyAct();
		break;
	case eSpmeStep::GlassReadyReply:
		SpmeRunStepStr = "回复玻璃就绪信号";
		GlassReadyReplyAct();
		break;
	case eSpmeStep::SendStartMeasure:
		SpmeRunStepStr = "发送开始测量信号";
		SendStartMeasureAct();
		break;
	case eSpmeStep::WaitStartMeasureReply:
		SpmeRunStepStr = "等待开始测量回应";
		WaitStartMeasureReplyAct();
		break;
	case eSpmeStep::SendStartReference:
		SpmeRunStepStr = "开始测量基准";
		SendStartReferenceAct();
		break;
	case eSpmeStep::WaitStartReferenceReply:
		SpmeRunStepStr = "等待测量基准回应";
		WaitStartReferenceReplyAct();
		break;
	case eSpmeStep::SendSkipGlass:
		SpmeRunStepStr = "跳过测量";
		SendSkipGlassAct();
		break;
	case eSpmeStep::WaitSkipGlassReply:
		SpmeRunStepStr = "跳过测量回应";
		WaitSkipGlassReplyAct();
		break;
	case eSpmeStep::WaitReachPoint:
		SpmeRunStepStr = "等待到达测量点";
		WaitReachPointAct();
		break;
	case eSpmeStep::SendSPMEMeasure:
		SpmeRunStepStr = "SPME发送测量指令";
		SendSPMEMeasureAct();
		break;
	case eSpmeStep::WaitSPMEMeasFinished:
		SpmeRunStepStr = "等待SPME测量完成";
		WaitSPMEMeasFinishedAct();
		break;
	case eSpmeStep::SendSPMEAnalyze:
		SpmeRunStepStr = "SPME正在分析";
		SendSPMEAnalyzeAct();
		break;
	case eSpmeStep::WaitSPMEAnalyzeFinished:
		SpmeRunStepStr = "SPME正在分析";
		WaitSPMEAnalyzeFinishedAct();
		break;
	case eSpmeStep::SendSPMEMeasureFinished:
		SpmeRunStepStr = "SPEME测量完成";
		SendSPMEMeasureFinishedAct();
		break;
	case eSpmeStep::WaitThisPointMeasureFinishedReply:
		SpmeRunStepStr = "等待点检测完成";
		WaitThisPointMeasureFinishedReplyAct();
		break;
	case eSpmeStep::SendUnloadGlass:
		SpmeRunStepStr = "卸载玻璃";
		SendUnloadGlassAct();
		break;
	case eSpmeStep::WaitUnloadGlassReply:
		SpmeRunStepStr = "卸载玻璃完成";
		WaitUnloadGlassReplyAct();
		break;
	default:
		break;
	}
}

void McLoop::WaitGlassReadyAct()
{
	if (CommandData::getPLCCommandGlassReady())
	{
		glog << "接收到玻璃就绪信号" << gendl;
		//CommandData::setPCCommandMeasurePointCount(MeasurePointCount);
		//CommandData::setPCCommandGlassReadyReply(true);
		//CommandData::setPCCommandMeasureType((int)m_spmeType);
		SpmeStep = eSpmeStep::GlassReadyReply;
	}
}

void McLoop::GlassReadyReplyAct()
{
	glog << "回应玻璃就绪信号" << gendl;
	//MeasurePointIndex = 1;
	//CommandData::setPCCommandMeasurePointIndex(MeasurePointIndex);
	//if (IsMeasureReference)
	//{
	//	SpmeStep = eSpmeStep::SendStartReference;
	//}
	//else
	//{
	//	SpmeStep = eSpmeStep::SendStartMeasure;
	//}
}

void McLoop::SendStartMeasureAct()
{
	//CommandData::setPCCommandThisPointMeasureFinished(false);
	//CommandData::setPCCommandGlassReadyReply(false);
	//CommandData::setPCCommandStartMeasure(true);
	//MeasurePointIndex = 1;
	//CommandData::setPCCommandMeasurePointIndex(MeasurePointIndex);
	//CommandData::setPCCommandMeasureType((int)m_spmeType);
	//CommandData::setPCCommandRunMode(1);
	//glog << "发送开始测量信号" << gendl;
	SpmeStep = eSpmeStep::WaitStartMeasureReply;
}

void McLoop::WaitStartMeasureReplyAct()
{
	if (CommandData::getPLCCommandStartMeasureReply())
	{
		glog << "接收到开始测量回应" << gendl;
		CommandData::setPCCommandStartMeasure(false);
		SpmeStep = eSpmeStep::WaitReachPoint;
	}
}

void McLoop::SendStartReferenceAct()
{
	glog << "开始测量基准" << gendl;
	CommandData::setPCCommandStartReference(true);
	CommandData::setPCCommandGlassReadyReply(false);
	SpmeStep = eSpmeStep::WaitStartReferenceReply;
}

void McLoop::WaitStartReferenceReplyAct()
{
	if (CommandData::getPLCCommandStartReferenceReply())
	{
		glog << "收到测量基准回复" << gendl;
		CommandData::setPCCommandStartReference(false);
		SpmeStep = eSpmeStep::WaitReachPoint;
	}
}

void McLoop::SendSkipGlassAct()
{
}

void McLoop::WaitSkipGlassReplyAct()
{
}

void McLoop::WaitReachPointAct()
{
	if (CommandData::getPLCCommandReachMeasurePoint())
	{
		glog << "接收到到达点信号" << gendl;
		CommandData::setPCCommandMeasurePointReply(true);
		SpmeStep = eSpmeStep::SendSPMEMeasure;
	}
}

void McLoop::SendSPMEMeasureAct()
{
	//IsRecieveSPME = false;
	//if (IsMeasureReference)
	//{
	//	//发送测量指令
	//	IsRecieveSPME = false;
	//	measReference();
	//	glog << "开始测量SPME基准" << gendl;
	//}
	//else
	//{
	//	//发送测量指令
	//	IsRecieveSPME = false;
	//	QString str = QString("%1;MEASURE-SAMPLE;1").arg(commandIndex++);
	//	int ret = spmeOD->write(str.toLocal8Bit());
	//	spmeOD->flush();
	//	glog << "开始测量SPME,发送str:" << str << ",recieve:" << ret << gendl;
	//}
	SpmeStep = eSpmeStep::WaitSPMEMeasFinished;
}

void McLoop::WaitSPMEMeasFinishedAct()
{
	CommandData::setPCCommandMeasurePointReply(false);
	//if (IsRecieveSPME)
	//{
	//	if (IsMeasureReference)//基准测量完成
	//	{
	//		glog << "接收到基准测量完成" << gendl;
	//		SpmeStep = eSpmeStep::SendSPMEMeasureFinished;
	//	}
	//	else
	//	{
	//		SpmeStep = eSpmeStep::SendSPMEAnalyze;
	//	}
	//}
	//else if (RecieveTimeout > 1000)
	//{
	//	RecieveTimeout = 0;
	//	SpmeStep = eSpmeStep::SendSPMEMeasure;
	//}
	//RecieveTimeout++;
}

void McLoop::SendSPMEAnalyzeAct()
{
	//发送测量指令
	//IsRecieveAnalyze = false;
	//QString str = QString("%1;ANALYZE-SAMPLE;0").arg(commandIndex++);
	//int ret = spmeOD->write(str.toLocal8Bit());
	//spmeOD->flush();
	//glog << "发送分析指令SPME,发送str:" << str << ",recieve:" << ret << gendl;
	SpmeStep = eSpmeStep::WaitSPMEAnalyzeFinished;
}

void McLoop::WaitSPMEAnalyzeFinishedAct()
{
	//if (IsRecieveAnalyze)
	//{
	//	SPMETestFinishedEvent* ev = new SPMETestFinishedEvent(MeasurePointIndex, PCjobData.JobID, m_spmeType, Thickness, color_x, color_y, color_Y, OD, PLCCurrentPosX - (GlassPLCOffSetX + 462.5), PLCCurrentPosY - (GlassPLCOffSetY + 750));
	//	try
	//	{
	//		CommandData::getInstance()->setPCCommandColor_x(color_x * 1000);
	//		CommandData::getInstance()->setPCCommandColor_y(color_y * 1000);
	//		CommandData::getInstance()->setPCCommandColor_Y(color_Y * 1000);

	//		EventCenter::postEvent(ev);
	//	}
	//	catch (const std::exception&)
	//	{

	//	}
	//	if (m_spmeType == enVIS)
	//	{
	//		ThickResults.push_back(Thickness);

	//		if (m_runTimeData->rlt.spmeGlassRlt.THICK_CONTENTS.size() > 50)
	//		{
	//			m_runTimeData->rlt.spmeGlassRlt.THICK_CONTENTS.pop_front();
	//		}
	//		m_runTimeData->rlt.spmeGlassRlt.THICK_CONTENTS.push_back(Thickness);
	//	}
	//	else if (m_spmeType == enColor)
	//	{
	//		ColorResults.push_back(SpmeColor(color_x, color_y, color_Y));
	//	}
	//	else if (m_spmeType == enOD)
	//	{
	//		ODResults.push_back(OD);
	//		m_runTimeData->rlt.spmeGlassRlt.OD_CONTENTS.push_back(OD);
	//	}
	//	m_runTimeData->rlt.spmeGlassRlt.GLASSID_CONTENTS.push_back(SpmeGlassID);


	//	SpmeStep = eSpmeStep::SendSPMEMeasureFinished;
	//	glog << "SPME测量分析完成" << gendl;
	//}
}

void McLoop::SendSPMEMeasureFinishedAct()
{
	//CommandData::setPCCommandThisPointMeasureFinished(true);
	//glog << "发送这个点测量完成信号,index++" << gendl;
	//MeasurePointIndex++;
	//CommandData::setPCCommandMeasurePointIndex(MeasurePointIndex);
	//if (IsMeasureReference)
	//{
	//	//if (MeasurePointIndex > 3)
	//	//{
	//	glog << "测量基准完成,发送这个点测量完成信号" << gendl;
	//	MeasurePointIndex = 1;
	//	IsMeasureReference = false;
	//	SpmeStep = eSpmeStep::SendStartMeasure;
	//	//}
	//	//else
	//	//{
	//	//	SpmeStep = eSpmeStep::WaitThisPointMeasureFinishedReplay;
	//	//}
	//}
	//else
	//{
	//	if (MeasurePointIndex > MeasurePointCount)
	//	{
	//		SpmeStep = eSpmeStep::SendUnloadGlass;
	//	}
	//	else
	//	{
	//		SpmeStep = eSpmeStep::WaitThisPointMeasureFinishedReplay;
	//		glog << "等待检测点完成回应" << gendl;
	//	}
	//}
}

void McLoop::WaitThisPointMeasureFinishedReplyAct()
{
	if (CommandData::getPLCCommandThisPointMeasureFinishedReply())
	{
		glog << "接收到这个点测量完成回应，把此点完成置0" << "下一步，等待点到位" << gendl;
		CommandData::setPCCommandThisPointMeasureFinished(false);
		SpmeStep = eSpmeStep::WaitReachPoint;
	}
}

void McLoop::SendUnloadGlassAct()
{
	//static int id = 0;
	//glog << "SPME测量分析完成,测量点数" << gendl;
	//if (SpmeGlassID.isEmpty())
	//{
	//	SpmeGlassID = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
	//}
	//if (m_spmeType == enVIS)
	//{
	//	glog << "Write ThickFile!!!" << gendl;
	//	QFile f(QString("D:\\ThickFile\\%1.csv").arg(SpmeGlassID));
	//	f.open(QIODevice::ReadWrite | QIODevice::Text);
	//	WriteThickFile(f);
	//	f.close();
	//}
	//else if (m_spmeType == enColor)
	//{
	//	QFile f(QString("D:\\ColorFile\\%1.csv").arg(SpmeGlassID));
	//	f.open(QIODevice::ReadWrite | QIODevice::Text);
	//	WriteColorFile(f);
	//	f.close();
	//}
	//else if (m_spmeType == enOD)
	//{

	//	QFile f(QString("D:\\ODFile\\%1.csv").arg(SpmeGlassID));
	//	f.open(QIODevice::ReadWrite | QIODevice::Text);
	//	WriteODFile(f);
	//	f.close();
	//}

	glog << "发送下玻璃请求" << gendl;
	CommandData::setPCCommandThisPointMeasureFinished(false);
	CommandData::setPCCommandUnloadGlass(true);
	SpmeStep = eSpmeStep::WaitUnloadGlassReply;
}

void McLoop::WaitUnloadGlassReplyAct()
{
	if (CommandData::getPLCCommandUnloadGlassReply())
	{
		glog << "接收到下玻璃回应" << gendl;
		CommandData::setPCCommandUnloadGlass(false);
		SpmeStep = eSpmeStep::WaitGlassReady;
	}
}

JobData* McLoop::GetJobData()
{
	return &m_jobData;
}

