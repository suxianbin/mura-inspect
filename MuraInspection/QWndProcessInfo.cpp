#include "QWndProcessInfo.h"
#include <QDateTime>

QWndProcessInfo::QWndProcessInfo(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

QWndProcessInfo::~QWndProcessInfo()
{}

void QWndProcessInfo::updateRecipe()
{
	//ui.lb_RecipeID->setText("  " + QString::number(recipePlug->getCurrentRecipeId()));
	//ui.lb_RecipeName->setText("  " + QString(recipeParam.name));
	//ui.lb_RecipeType->setText("  CF" );
	//ui.lb_RecipeDate->setText("  " + QString(recipeParam.updateTime));
	//ui.lb_RecipeSwitchTime->setText("  " + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
}

void QWndProcessInfo::updateRuntimeData()
{
	//ui.lb_ProduceId->setText("  3");
	//ui.lb_GlassId->setText("  3");
	//ui.lb_StartTime->setText("  " + runtimeData->rlt.startdateTime);
	//ui.lb_EndTime->setText("  " + runtimeData->rlt.enddateTime);
	//ui.lb_GlassJudge->setText("  " + runtimeData->rlt.WarninglevelLabel);
	//ui.lb_ScanNum->setText("  1");



	ui.lb_SPMEJudge->setText("  " + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));

}

void QWndProcessInfo::updateStatus(int pcStatus, int plcStatus)
{
	switch (pcStatus)
	{
	case 0:
		ui.lb_PCStatus->setText("  " + QString("Scaning"));
		break;
	case 1:
		ui.lb_PCStatus->setText("  " + QString("Caculate"));
		break;
	case 2:
		ui.lb_PCStatus->setText("  " + QString("Testing Spme"));
		break;
	case 3:
		ui.lb_PCStatus->setText("  " + QString("Loading Glass"));
		break;
	case 4:
		break;
	case 5:
		break;
	case 6:
		break;
	case 7:
		break;
	default:
		break;
	}

	switch (plcStatus)
	{
	case 0:
		ui.lb_PLCStatus->setText("  " + QString());
		break;
	case 1:
		ui.lb_PLCStatus->setText("  " + QString("PM"));
		break;
	case 2:
		ui.lb_PLCStatus->setText("  " + QString("Down"));
		break;
	case 3:
		ui.lb_PLCStatus->setText("  " + QString("Pause"));
		break;
	case 4:
		ui.lb_PLCStatus->setText("  " + QString("Idle"));
		break;
	case 5:
		ui.lb_PLCStatus->setText("  " + QString("Run"));
		break;
	case 6:
		ui.lb_PLCStatus->setText("  " + QString("JobChange"));
		break;
	case 7:
		ui.lb_PLCStatus->setText("  " + QString("ETC"));
		break;
	default:
		break;
	}
}
