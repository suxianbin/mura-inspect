#include "LightParam.h"

LightParam::LightParam(QString key) : Base(key)
{
}

LightParam::~LightParam()
{
}

void LightParam::write(QJsonObject& json) const
{
	Base::write(json);
	json["Chanel1Enable"] = Chanel1Enable;
	json["Chanel1Intensity"] = Chanel1Intensity;
	json["Chanel2Enable"] = Chanel2Enable;
	json["Chanel2Intensity"] = Chanel2Intensity;
	json["Chanel3Enable"] = Chanel3Enable;
	json["Chanel3Intensity"] = Chanel3Intensity;
	json["Chanel4Enable"] = Chanel4Enable;
	json["Chanel4Intensity"] = Chanel4Intensity;
	json["Chanel5Enable"] = Chanel5Enable;
	json["Chanel5Intensity"] = Chanel5Intensity;
	json["Chanel6Enable"] = Chanel6Enable;
	json["Chanel6Intensity"] = Chanel6Intensity;
	json["Chanel7Enable"] = Chanel7Enable;
	json["Chanel7Intensity"] = Chanel7Intensity;
	json["Chanel8Enable"] = Chanel8Enable;
	json["Chanel8Intensity"] = Chanel8Intensity;
}

void LightParam::read(const QJsonObject& json)
{
	Base::read(json);
	Chanel1Enable = json["Chanel1Enable"].toBool();
	Chanel1Intensity = json["Chanel1Intensity"].toInt();
	Chanel2Enable = json["Chanel2Enable"].toBool();
	Chanel2Intensity = json["Chanel2Intensity"].toInt();
	Chanel3Enable = json["Chanel3Enable"].toBool();
	Chanel3Intensity = json["Chanel3Intensity"].toInt();
	Chanel4Enable = json["Chanel4Enable"].toBool();
	Chanel4Intensity = json["Chanel4Intensity"].toInt();
	Chanel5Enable = json["Chanel5Enable"].toBool();
	Chanel5Intensity = json["Chanel5Intensity"].toInt();
	Chanel6Enable = json["Chanel6Enable"].toBool();
	Chanel6Intensity = json["Chanel6Intensity"].toInt();
	Chanel7Enable = json["Chanel7Enable"].toBool();
	Chanel7Intensity = json["Chanel7Intensity"].toInt();
	Chanel8Enable = json["Chanel8Enable"].toBool();
	Chanel8Intensity = json["Chanel8Intensity"].toInt();
	
}
