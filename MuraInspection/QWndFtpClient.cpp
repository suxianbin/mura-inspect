#pragma execution_character_set("utf-8")
#include "QWndFtpClient.h"
#include <QDebug>
#include <QThread>
#include <QDir>
#include <QFile>

QWndFtpClient::QWndFtpClient(QWidget* parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	client = new FtpClient();
	connect(client, &FtpClient::FtpClientSig, this, &QWndFtpClient::onClientSig);
	//connect(client, &FtpClient::doneSignal, this, &QWndFtpClient::on_doneSignal);
	int ConnectCount = 0;
	while (true)
	{
		if (ConnectCount > 3)
		{
			ui.label->setText("连接失败");
			break;
		}
		if (client->connectServer("127.0.0.1", "userftp", "123456"))
		{
			ui.label->setText("连接成功");
			break;
		}
		else
		{
			ConnectCount++;
			ui.label->setText("重新连接");
		}
	}
	//QThread::msleep(1000);

	//client->cd("/");
	ui.leRoute->setText("/on/data/cf//nfec/33/05/00/37/");
	ui.leMulRoute->setText("D:\\testFtp.txt");
	connect(ui.btnConnect, &QPushButton::clicked, this, &QWndFtpClient::on_connectClicked);
	connect(ui.btnDisconnect, &QPushButton::clicked, this, &QWndFtpClient::on_disconnectClicked);
	connect(ui.btnUpload, &QPushButton::clicked, this, &QWndFtpClient::on_UploadClicked);
	connect(ui.btnDownload, &QPushButton::clicked, this, &QWndFtpClient::on_donwloadClicked);
	connect(ui.btnCD, &QPushButton::clicked, this, &QWndFtpClient::on_CDClicked);
	connect(ui.btnCDParent, &QPushButton::clicked, this, &QWndFtpClient::on_CDParentClicked);
	connect(ui.btnList, &QPushButton::clicked, this, &QWndFtpClient::on_ListClicked);
	connect(ui.btnMKDIR, &QPushButton::clicked, this, &QWndFtpClient::on_MKDirClicked);
	connect(ui.btnCreateDir, &QPushButton::clicked, this, &QWndFtpClient::on_CreateDirClicked);
	connect(ui.btnExistPath, &QPushButton::clicked, this, &QWndFtpClient::on_ExistPathClicked);
}

QWndFtpClient::~QWndFtpClient()
{}

void QWndFtpClient::on_UploadClicked()
{
	////network 上传测试
	//ftpNetwork = new FtpNetWork();

	//// Qt线程测试
	//UploadThread*up = new UploadThread();
	//up->start();

	ui.label->setText("正在上传");
	QString pathfile = ui.leMulRoute->text();
	int pos = pathfile.lastIndexOf('\\');
	QString filename = pathfile.mid(pos + 1, pathfile.size() - pos - 1);
	////client->disconect();
	std::thread* th = new std::thread([=]()
		{
			//ftpNetwork->uploadFile("D:\\VisionInspectMura.zip", "ftp://127.0.0.1/on/test.zip", "21", "cfftp", "cfftp");
			bool rtn = client->uploadFile(pathfile, ui.leRoute->text(), filename);
			client->disconect();
			qDebug() << "上传结果:" << rtn;
		});
	th->detach();


	//if (rtn)
	//{
	//	ui.label->setText("上传成功");
	//}
	//else
	//{
	//	ui.label->setText("上传失败");
	//}
}

void QWndFtpClient::on_donwloadClicked()
{
}

void QWndFtpClient::on_doneSignal(bool error)
{
	this->ui.label_2->setText("Done Signal:" + QString::number(error));
}

void QWndFtpClient::on_connectClicked()
{
	client->connectServer("127.0.0.1","cfftp","cfftp");
}

void QWndFtpClient::on_disconnectClicked()
{
	client->disconect();
}

void QWndFtpClient::on_CDClicked()
{
	client->cd(ui.leRoute->text());
}

void QWndFtpClient::on_CDParentClicked()
{
	client->cdToParent();
}

void QWndFtpClient::on_ListClicked()
{
	ui.label_2->setText("clear Info");
	ui.treeWidget->clear();
	std::map<QString, bool>a = client->listFile();

	for(auto var : a)
	{
		QTreeWidgetItem* item = new QTreeWidgetItem;
		item->setText(0, var.first);
		ui.treeWidget->addTopLevelItem(item);
	}
}

void QWndFtpClient::on_MKDirClicked()
{
	//client->mkDir(ui.leRoute->text());
}

void QWndFtpClient::on_deleteFileCliced()
{
}

void QWndFtpClient::on_deleteDirClicked()
{
}

void QWndFtpClient::on_CreateDirClicked()
{
	for (int i = 0; i < 1000; i++)
	{
		QDir dir("D:/usr01/cfftp/on/" + QString::number(i));
		if (!dir.exists())
		{
			dir.mkdir("D:/usr01/cfftp/on/" + QString::number(i));
		}
	}
}

void QWndFtpClient::on_ExistPathClicked()
{
	//client->existDir(ui.leRoute->text()) ? ui.label->setText("exist path") : ui.label->setText("not exist path");
}

void QWndFtpClient::onClientSig(eFtpResult val)
{
	//switch (val)
	//{
	//case eFtpResult::CONENCT_FAIL:
	//	ui.label->setText("连接失败");
	//	break;
	//case eFtpResult::CONNECT_SUCCESS:
	//	ui.label->setText("连接成功");
	//	break;
	//case eFtpResult::UPLOAD_FAIL:
	//	ui.label->setText("上传失败");
	//	break;
	//case eFtpResult::UPLOAD_SUCCESS:
	//	ui.label->setText("上传成功");
	//	break;
	//case eFtpResult::DOWNLOAD_FAIL:
	//	ui.label->setText("下载失败");
	//	break;
	//case eFtpResult::DOWNLOAD_SUCCESS:
	//	ui.label->setText("下载成功");
	//	break;
	//case eFtpResult::UPLOADFILENOTEXIST:
	//	ui.label->setText("待上传文件不存在");
	//	break;
	//default:
	//	break;
	//}



}
