#pragma once
#include <QString>

#ifdef MURA_BOE_B12
class JobData
{
public:
	QString PRODID;
	QString OperID;
	QString LotID;
	QString PPID01_1;
	quint16 PPID01_2;
	QString PPID02_1;
	quint16 PPID02_2;
	QString PPID03_1;
	quint16 PPID03_2;
	QString PPID04_1;
	quint16 PPID04_2;
	QString PPID05_1;
	quint16 PPID05_2;
	QString PPID06_1;
	quint16 PPID06_2;
	QString PPID07_1;
	quint16 PPID07_2;
	QString PPID08_1;
	quint16 PPID08_2;
	QString PPID09_1;
	quint16 PPID09_2;
	QString PPID10_1;
	quint16 PPID10_2;
	QString PPID11_1;
	quint16 PPID11_2;
	QString PPID12_1;
	quint16 PPID12_2;
	QString PPID13_1;
	quint16 PPID13_2;
	QString PPID14_1;
	quint16 PPID14_2;
	QString PPID15_1;
	quint16 PPID15_2;
	QString PPID16_1;
	quint16 PPID16_2;
	QString PPID17_1;
	quint16 PPID17_2;
	QString PPID18_1;
	quint16 PPID18_2;
	QString PPID19_1;
	quint16 PPID19_2;
	QString PPID20_1;
	quint16 PPID20_2;
	QString PPID21_1;
	quint16 PPID21_2;
	QString PPID22_1;
	quint16 PPID22_2;
	QString PPID23_1;
	quint16 PPID23_2;
	QString PPID24_1;
	quint16 PPID24_2;
	QString PPID25_1;
	quint16 PPID25_2;
	QString PPID26_1;
	quint16 PPID26_2;
	quint16 JobType;
	QString JobID;
	quint16 LotSequenceNumber;
	quint16 SlotSequenceNumber;
	quint16 PropertyCode;
	QString JobJudgeCode;
	QString JobGradeCode;
	quint16 SubstrateType;
	quint16 ProcessingFlag;
	quint16 InspectionFlag;
	quint16 SkipFlag;
	quint16 JobSize;
	quint16 GlassThickness;
	quint16 JobAngle;
	quint16 JobFlip;
	quint16 CuttingGlassType;
	quint16 InspectionJudgeData;
	quint16 PairLotSequenceNumber;
	quint16 PairSlotSequenceNumber;
	quint16 PortNumber;
	quint16 InputMode;
	quint16 EVRouteRecipe;
	quint16 ENRoutePathInfo;
	quint16 JobChangeflag;
	QString OptionValue;
	QString Reserved;

	QString Step_Id;

	static void Array2JobData(const QByteArray& arrData,JobData& jobData);
	static void JobData2Array(const JobData& jobData, QByteArray& arrData);
};
#endif // MURA_BOE_B12

#ifdef MURA_BOE_B7
class JobData
{

};
#endif // MURA_BOE_B12


