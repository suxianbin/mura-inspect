#pragma once
#include <QString>
#include <QXmlStreamWriter>
#include <QJsonObject>
#include <QObject>
#include <QFile>

class Base
{
public:
	Base(QString key);
	Base();
	virtual ~Base();
	virtual void write(QJsonObject& json)const;
	virtual void read(const QJsonObject& json);
	//virtual void writeXml(QXmlStreamWriter& xml)const;
	//virtual void readXml(const QXmlStreamReader& xml);
	virtual void writeFile(QFile& file)const;
	virtual void readFile(const QFile& file);
	QString Key;
};
