﻿#pragma once
#include <vector>
#include <string>
#include <map>
#include <typeinfo>
#include <QDir>
#include <QJsonDocument>
#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include "Base.h"

class EntityMgr
{
public:
	EntityMgr();
	bool Add(Base* entity);
	bool Remove(QString key);
	Base* FindBy(QString key)const;
	std::vector<Base*>FindAll() const;
	void Clear();
	int Size() const;
	/// <summary>
	/// 保存成一个单一的文件
	/// </summary>
	/// <param name="name"> 文件名 </param>
	/// <returns>成功-返回true 失败-返回false</returns>
	virtual bool Save(std::string name);
	virtual bool Load(std::string name);
	/// <summary>
	/// 保存到json中，以便最后跟其他的mgr汇总成一个文件
	/// </summary>
	/// <param name="json"></param>
	/// <returns></returns>
	virtual bool SaveToFile(QJsonObject& json);
	virtual bool LoadFromFile(QJsonObject& json);
protected:
	std::string m_path;
	std::map<QString, Base*>m_map;

};

