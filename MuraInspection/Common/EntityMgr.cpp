#include "EntityMgr.h"
#include "DevLog.h"
#include <QJsonArray>
#include <QDir>

using namespace std;

EntityMgr::EntityMgr()
{
		QString path = QDir::currentPath();
		string s = typeid(*this).name();
		m_path = path.toStdString() + "\\RecipeLibrary\\";
}
bool EntityMgr::Add(Base* entity)
{
	pair<map<QString, Base*>::iterator, bool>insert_pair;
	insert_pair = m_map.insert(make_pair(entity->Key, entity));
	if (insert_pair.second)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool EntityMgr::Remove(QString key)
{
	map<QString, Base*>::iterator it = m_map.find(key);
	if (it != m_map.end())
	{
		delete it->second;
		it->second = nullptr;
		m_map.erase(m_map.find(key));
	}
	return true;
}
Base* EntityMgr::FindBy(QString key)const
{
	map<QString, Base*>::const_iterator it = m_map.find(key);
	if (it == m_map.end())
	{
		return nullptr;
	}
	return it->second;
}
vector<Base*>EntityMgr::FindAll() const
{
	vector<Base*> entities;
	for (map<QString, Base*>::const_iterator it = m_map.begin(); it != m_map.end(); it++)
	{
		entities.push_back(it->second);
	}
	return entities;
}
void EntityMgr::Clear()
{
	map<QString, Base*>::iterator it = m_map.begin();
	while (it != m_map.end())
	{
		delete it->second;
		it->second = nullptr;
		m_map.erase(it++);
	}
}
int EntityMgr::Size() const
{
	return (int)m_map.size();
}
bool EntityMgr::Save(string name)
{
	return false;
}

bool EntityMgr::Load(string name)
{
	return false;
}

bool EntityMgr::SaveToFile(QJsonObject& json)
{
	return false;
}

bool EntityMgr::LoadFromFile(QJsonObject& json)
{
	return false;
}