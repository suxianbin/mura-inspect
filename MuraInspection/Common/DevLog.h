﻿#pragma once

#include <QtCore/qglobal.h>
#include <QDateTime>
#include <QMutex>

#ifndef BUILD_STATIC
# if defined(DEVLOG_LIB)
#  define DEVLOG_EXPORT Q_DECL_EXPORT
# else
#  define DEVLOG_EXPORT Q_DECL_IMPORT
# endif
#else
# define DEVLOG_EXPORT
#endif


/****************************************************************************
** Copyright (C) 2020.
**  
** 用法：包含此头文件，把DevLog.lib添加到项目中，
** 把DevLog.dll放到对应应用程序目录下
**
** 在程序中直接调用 glog << "需要写到本地的日志内容" << gendl;
**
** 仅在QT项目中测试过
****************************************************************************/

//前向声明
class DevLog;
struct EndLine;

extern DEVLOG_EXPORT  DevLog glog;
extern DEVLOG_EXPORT  EndLine gendl;
//extern DEVLOG_EXPORT  QMutex g_datetimeMutex;

DEVLOG_EXPORT QDateTime currentDateTimeThreadSafe();

class DEVLOG_EXPORT DevLog
{
public:
	DevLog();
	void clear();
	void print(const QString& msg);
	void print(const QString& tag, const QString& msg);
	void dprint(const QString& msg);
	void dprint(const QString& tag, const QString& msg);
	void cprint(const QString& msg);
	void cprint(const QString& tag, const QString& msg);

	DevLog& operator << (const QString& msg);
	DevLog& operator << (const char* msg);
	DevLog& operator << (bool value);
	DevLog& operator << (qint32 value);
	DevLog& operator << (quint32 value);
	DevLog& operator << (qint64 value);
	DevLog& operator << (quint64 value);
	DevLog& operator << (qreal value);
	DevLog& operator << (const EndLine& ends);

private:
	void buildLogStr(const QString& tag, const QString& msg, QString& logStr);
};

struct DEVLOG_EXPORT EndLine
{
};

