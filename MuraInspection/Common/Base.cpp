#include "Base.h"

Base::Base(QString key)
{
	Key = key;
}

Base::Base()
{

}

Base::~Base()
{
}

void Base::write(QJsonObject& json) const
{
	json["Key"] = Key;
}

void Base::read(const QJsonObject& json)
{
	Key = json["Key"].toString();
}

//void Base2::writeXml(QXmlStreamWriter& xml) const
//{
//	xml.writeAttribute("Key", QString("%1").arg(Key));
//}
//
//void Base2::readXml(const QXmlStreamReader& xml)
//{
//	Key = xml.attributes().value("Key").toString();
//}

void Base::writeFile(QFile& json) const
{
	
}

void Base::readFile(const QFile& json)
{
	
}
