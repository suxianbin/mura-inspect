#pragma once

#include <QDialog>
#include "ui_QWndMessageBox.h"
#include <QMouseEvent>

class QWndMessageBox : public QDialog
{
	Q_OBJECT

public:
	QWndMessageBox(QString msg, QColor textColor = QColor(223, 228, 255), QWidget *parent = nullptr);
	~QWndMessageBox();
protected:
	QPoint move_point;                                    //移动的距离
	bool mouse_press = false;                             //鼠标按下
	void mousePressEvent(QMouseEvent* qevent);            //鼠标按下事件
	void mouseReleaseEvent(QMouseEvent* qevent);          //鼠标释放事件
	void mouseMoveEvent(QMouseEvent* qevent);             //鼠标移动事件
public slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
	void on_btnClose_clicked();


private:
	Ui::QWndMessageBoxClass ui;
};
