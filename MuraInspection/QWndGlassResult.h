#pragma once

#include <QWidget>
#include "ui_QWndGlassResult.h"

class QWndGlassResult : public QWidget
{
	Q_OBJECT

public:
	QWndGlassResult(QWidget *parent = nullptr);
	~QWndGlassResult();

private:
	Ui::QWndGlassResultClass ui;
};
