#pragma once

#include <QWidget>
#include "ui_QWndGlassCrack.h"

class QWndGlassCrack : public QWidget
{
	Q_OBJECT

public:
	QWndGlassCrack(QWidget *parent = nullptr);
	~QWndGlassCrack();

private:
	Ui::QWndGlassCrackClass ui;
};
