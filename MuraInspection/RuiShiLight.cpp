#include "RuiShiLight.h"
#include "HardwareConfig.h"

RuiShiLight::RuiShiLight()
{
	m_pClient1 = new QTcpSocket();
	m_pClient1->connectToHost(HardwareConfig::Instance()->lightIP1, HardwareConfig::Instance()->lightPort1);
	connect(m_pClient1, SIGNAL(readyRead()), this, SLOT(on_Client1ReadyRead()));


	m_pClient2 = new QTcpSocket();
	m_pClient2->connectToHost(HardwareConfig::Instance()->lightIP2, HardwareConfig::Instance()->lightPort2);


}

RuiShiLight::~RuiShiLight()
{
	m_pClient1->disconnect();
	m_pClient2->disconnect();
}

void RuiShiLight::SetIntensity(int ch, int intensity)
{
	if (ch <= 4)
	{
		QString msg = QString("%1%2").arg(ch).arg(intensity);
		m_pClient1->write(msg.toLocal8Bit());
		m_pClient1->flush();
	}
	else if (ch > 4 && ch <= 8)
	{
		QString msg = QString("%1%2").arg(ch - 4).arg(intensity);
		m_pClient2->write(msg.toLocal8Bit());
		m_pClient2->flush();
	}

	int chgg = ch;
	int intent = intensity;
}

void RuiShiLight::TurnOff(int ch)
{
	int chgg = ch;
}

void RuiShiLight::on_Client1ReadyRead()
{
	int a = 9 + 9;
}


