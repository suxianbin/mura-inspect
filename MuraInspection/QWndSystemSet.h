#pragma once

#include <QWidget>
#include "ui_QWndSystemSet.h"
#include "QWndFtpClient.h"

class QWndSystemSet : public QWidget
{
	Q_OBJECT

public:
	QWndSystemSet(QWidget *parent = nullptr);
	~QWndSystemSet();

private:
	Ui::QWndSystemSetClass ui;

	QWndFtpClient* m_pFTPPage;
	QWidget* m_pSoftCofigPage;
	//������
	QVBoxLayout* m_pMainDispFrameLayout = nullptr;

	void hiddenAllWidget();
	void setAllButtonDefaultStyleSheet();
	void setButtonStyleSheet(QPushButton* button);
private slots:
	void on_pb_FTPClicked();

};
