#pragma execution_character_set("utf-8")
#include "QWndResultPanelJudgeEdit.h"
#include <QListWidget>
#include <QLineEdit>

QWndResultPanelJudgeEdit::QWndResultPanelJudgeEdit(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(Qt::WindowCloseButtonHint);
	this->setWindowTitle("Panel结果编辑");
	this->setWindowIcon(QIcon(":/Icons/icon.png"));
	InitTable();
}

QWndResultPanelJudgeEdit::~QWndResultPanelJudgeEdit()
{}

void QWndResultPanelJudgeEdit::setRunTimeData()
{
	//if (runTimeData.rlt.panelRlts.size() <= 0)
	//{
	//	return;
	//}
	//ItemMap.clear();
	//for (int i = 0; i < ui.tableWidget->rowCount(); i++)
	//{
	//	ui.tableWidget->removeRow(0);
	//}
	//ui.tableWidget->setRowCount(runTimeData.rlt.panelRlts.size());
	//QFont f;
	//f.setPointSize(14);
	//f.setFamily(u8"微软雅黑");

	//ui.tableWidget->setItem(0, 2, new QTableWidgetItem("PM"));
	//QStringList itemsPanelJudge;
	//itemsPanelJudge << tr("RP") << tr("PD") << tr("NG");
	//ItemMap.clear();

	//for (int i = 0; i < runTimeData.rlt.panelRlts.size(); i++)
	//{
	//	QTableWidgetItem* item0 = new QTableWidgetItem(QString::number(i + 1));
	//	item0->setTextAlignment(Qt::AlignCenter);
	//	item0->setFont(f);
	//	item0->setFlags(item0->flags() & ~Qt::ItemIsEditable);
	//	ui.tableWidget->setItem(i, 0, item0);

	//	QTableWidgetItem* item1 = new QTableWidgetItem(runTimeData.rlt.panelRlts[i].panelId);
	//	item1->setTextAlignment(Qt::AlignCenter);
	//	item1->setFont(f);
	//	item1->setFlags(item1->flags() & ~Qt::ItemIsEditable);
	//	ui.tableWidget->setItem(i, 1, item1);

	//	QTableWidgetItem* item2 = new QTableWidgetItem(runTimeData.rlt.panelRlts[i].levelLabel);
	//	item2->setTextAlignment(Qt::AlignCenter);
	//	item2->setFont(f);
	//	item2->setFlags(item2->flags() & ~Qt::ItemIsEditable);
	//	ui.tableWidget->setItem(i, 2, item2);

	//	ui.tableWidget->setRowHeight(i, 35);
	//	TableWidgetAddComboBox(ui.tableWidget, i, 3, itemsPanelJudge, runTimeData.rlt.panelRlts[i].level - 1);
	//	
	//}
}

void QWndResultPanelJudgeEdit::InitTable()
{
	ui.tableWidget->clear();
	QStringList headers;
	headers << u8"序 号" << "PanelID" << u8"自动判级" << u8"手动判级";
	ui.tableWidget->setColumnCount(headers.size());
	ui.tableWidget->setHorizontalHeaderLabels(headers);
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tableWidget->horizontalHeader()->setStyleSheet("QHeaderView::section {background-color:rgb(218, 226, 240);color: rgb(58,59,59);}");
	ui.tableWidget->horizontalHeader()->setAutoFillBackground(false);
	QFont f;
	f.setPointSize(14);
	for (int i = 0; i < headers.size(); i++)
	{
		QTableWidgetItem* item = new QTableWidgetItem(headers[i]);
		//item->setForeground(QColor(44, 44, 44));
		item->setBackground(QColor(58, 59, 59));
		item->setFont(f);
		ui.tableWidget->setHorizontalHeaderItem(i, item);
	}
	ui.tableWidget->setAlternatingRowColors(true);
	ui.tableWidget->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	//ui.tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tableWidget->verticalHeader()->setVisible(false);
	ui.tableWidget->setRowCount(20);
	ui.tableWidget->setColumnWidth(0, 60);

	for (int i = 1; i < headers.size(); i++)
	{
		ui.tableWidget->setColumnWidth(i, 90);
	}
	ui.tableWidget->setColumnWidth(1, 250);
	QStringList itemsPanelJudge;
	itemsPanelJudge << tr("PD") << tr("RP") << tr("NG");
	ItemMap.clear();
	for (int i = 0; i < 20; i++)
	{
		ui.tableWidget->setRowHeight(i, 35);
		TableWidgetAddComboBox(ui.tableWidget, i, 3, itemsPanelJudge, 0);
	}
	//connect(ui.tableWidget, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(on_TableWidgetItemChanged(QTableWidgetItem*)));
}

void QWndResultPanelJudgeEdit::ComboBoxSetItems(QComboBox* comboBox, QStringList* items)
{
	QListWidget* listWidget = new QListWidget(this);
	QLineEdit* lineEdit = new QLineEdit;

	for (int i = 0; i < items->count(); i++)
	{
		QListWidgetItem* item = new QListWidgetItem(items->at(i));
		item->setTextAlignment(Qt::AlignCenter);
		listWidget->addItem(item);
	}

	//ComboBox使用listWidget的内容
	comboBox->setModel(listWidget->model());
	comboBox->setView(listWidget);
	QFont f;
	f.setPointSize(14);
	f.setFamily(u8"微软雅黑");
	lineEdit->setFont(f);
	lineEdit->setReadOnly(true);
	lineEdit->setAlignment(Qt::AlignCenter);
	comboBox->setLineEdit(lineEdit);

	comboBox->setStyleSheet("QComboBox {"
		"    border: 1px solid #8f8f8f;"
		"    padding: 1px;"
		"    background-color: #ffffff;"
		"}"
		"QComboBox QAbstractItemView {"
		"    outline: none;"
		"}"
		"QComboBox QAbstractItemView::item{"
		"height:35px;"
		"}"
		"QComboBox::drop-down {"
		"    subcontrol-origin: padding;"
		"    subcontrol-position: top right;"
		"    width: 20px;"
		"    border-left-width: 1px;"
		"    border-left-color: darkgray;"
		"    border-left-style: solid;"
		"    background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #f0f0f0, stop:1 #dddddd);"
		"}"
		"QComboBox::down-arrow{"
		"image: url(:/Icons/add_bottom.png);"
		"width: 15px;"
		"height:15px;"
		"border-radius:3px;}"
	);

}

void QWndResultPanelJudgeEdit::TableWidgetAddComboBox(QTableWidget* tableWidget, int x, int y, QStringList items, int comboBoxIdx)
{
	QComboBox* combox = new QComboBox();
	QFont f;
	f.setPointSize(14);
	f.setFamily(u8"微软雅黑");
	combox->setFont(f);
	combox->setStyleSheet("background-color:rgb(255,255,255)");
	ComboBoxSetItems(combox, &items);
	combox->setCurrentIndex(comboBoxIdx);
	tableWidget->setCellWidget(x, y, (QWidget*)combox);
	ItemMap.insert(x, combox);
	connect(combox, SIGNAL(currentIndexChanged(int)), this, SLOT(on_ComboboxCurrentIndexChanged(int)));
}

void QWndResultPanelJudgeEdit::on_TableWidgetItemChanged(QTableWidgetItem* item)
{
	int itemRow = item->row();
	QString text = item->text();
	ui.tableWidget->update();
}

void QWndResultPanelJudgeEdit::on_ComboboxCurrentIndexChanged(int index)
{
	QComboBox* sendCom = dynamic_cast<QComboBox*>(sender());
	int senderIndex = -1;
	for (int i=0;i<ItemMap.size();i++)
	{
		if (ItemMap[i] == sendCom)
		{
			senderIndex = i;
			break;
		}
	}
	int currentRow = senderIndex;

	QTableWidgetItem* item = ui.tableWidget->item(currentRow, 2);
	if (item && item->text()!= sendCom->currentText())
	{
		item->setBackground(QBrush(Qt::yellow));
	}
	else if(item)
	{
		item->setBackground(QBrush(Qt::white));
	}
}
