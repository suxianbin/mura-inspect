#pragma once
#include <QWidget>
#include "ui_QWndDefectGraph.h"
#include <QStandardItemModel>
#include <QPoint>
#include <QMouseEvent>
#include "QCustomPlot.h"

class QWndDefectGraph : public QWidget
{
	Q_OBJECT

public:
	QWndDefectGraph(QWidget *parent = nullptr);
	~QWndDefectGraph();

private:
	Ui::QWndDefectGraphClass ui;

	QCustomPlot* m_pCustomPlot;
	QCPPlottableLegendItem* m_pSelectedLegendItem;
	QCPTextElement* m_pPlotTitle;
	void InitPlot();
	void SetStyleSheet();
	void AddPlotGraph(QColor color, QString text);
private slots:
	void contextMenuRequest(QPoint pos);
	void graphClicked(QCPAbstractPlottable* plottable, int dataIndex);
	void hideSelectedGraph();
	void on_LegendClicked(QCPLegend* legend, QCPAbstractLegendItem* item, QMouseEvent*);

	void on_btnTypeClicked();
	void on_btnStackClicked();
	void on_btnJudgementClicked();
	void on_btnOptionClicked();
	void on_btnSizeClicked();
};
