#include "QWndPLC.h"
#include "McLoop.h"

QWndPLC::QWndPLC(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

    timer50.setInterval(50);
    timer50.start();
    connect(&timer50, SIGNAL(timeout()), this, SLOT(on_timertimeout()));

    connect(ui.chk_refreshJobData, SIGNAL(clicked()), this, SLOT(on_chk_refreshJobDataClicked()));
    connect(ui.pb_ResetStation1, SIGNAL(clicked()), this, SLOT(on_pb_ResetStation1Clicked()));
    connect(ui.pb_ResetStation2, SIGNAL(clicked()), this, SLOT(on_pb_ResetStation2Clicked()));  
    connect(ui.pb_TransData, SIGNAL(clicked()), this, SLOT(on_pb_TransDataClicked()));
}

QWndPLC::~QWndPLC()
{}

void QWndPLC::refreshJobData()
{
    ui.chkLoadDone->setChecked(CommandData::getPLCCommandLoadDone());
    ui.chkScanRequstReply->setChecked(CommandData::getPLCCommandScanRequestReply());
    ui.chkScanDoneRequest->setChecked(CommandData::getPLCCommandScanDoneRequest());
    ui.chkUnloadRequestReply->setChecked(CommandData::getPLCCommandUnLoadRequestReply());
    ui.chkAutoChangeRequest->setChecked(CommandData::getPLCCommandAutoChangeRequest());
    ui.chkRecipeID->setChecked(CommandData::getPLCCommandRecipeID());
    ui.chkCameraTrigger->setChecked(CommandData::getPLCCameraTrigger());

    ui.lePRODID->setText(McLoop::Instance()->GetJobData()->PRODID);
    ui.leOperID->setText(McLoop::Instance()->GetJobData()->OperID);
    ui.leLotID->setText(McLoop::Instance()->GetJobData()->LotID);
    ui.lePPID01_1->setText(McLoop::Instance()->GetJobData()->PPID01_1);
    ui.leJobType->setText(QString::number(McLoop::Instance()->GetJobData()->JobType));
    ui.leJobID->setText(McLoop::Instance()->GetJobData()->JobID);
    ui.leLotSequenceNumber->setText(QString::number(McLoop::Instance()->GetJobData()->LotSequenceNumber));
    ui.leSlotSequenceNumber->setText(QString::number(McLoop::Instance()->GetJobData()->SlotSequenceNumber));
    ui.lePropertyCode->setText(QString::number(McLoop::Instance()->GetJobData()->PropertyCode));
    ui.leJobJudgeCode->setText(McLoop::Instance()->GetJobData()->JobJudgeCode);
    ui.leJobGradeCode->setText(McLoop::Instance()->GetJobData()->JobGradeCode);
    ui.leSubstrateType->setText(QString::number(McLoop::Instance()->GetJobData()->SubstrateType));
    ui.leProcessingFlag->setText(QString::number(McLoop::Instance()->GetJobData()->ProcessingFlag));
    ui.leInspectionFlag->setText(QString::number(McLoop::Instance()->GetJobData()->InspectionFlag));
    ui.leSkipFlag->setText(QString::number(McLoop::Instance()->GetJobData()->SkipFlag));
    ui.leJobSize->setText(QString::number(McLoop::Instance()->GetJobData()->JobSize));
    ui.leGlassThickness->setText(QString::number(McLoop::Instance()->GetJobData()->GlassThickness));
    ui.leJobAngle->setText(QString::number(McLoop::Instance()->GetJobData()->JobAngle));
    ui.leJobFlip->setText(QString::number(McLoop::Instance()->GetJobData()->JobFlip));
    ui.leCuttingGlassType->setText(QString::number(McLoop::Instance()->GetJobData()->CuttingGlassType));
    ui.leInspectionJudgeData->setText(QString::number(McLoop::Instance()->GetJobData()->InspectionJudgeData));
    ui.lePairLotSequenceNumber->setText(QString::number(McLoop::Instance()->GetJobData()->PairLotSequenceNumber));
    ui.lePairSlotSequenceNumber->setText(QString::number(McLoop::Instance()->GetJobData()->PairSlotSequenceNumber));
    ui.lePortNumber->setText(QString::number(McLoop::Instance()->GetJobData()->PortNumber));
    ui.leInputMode->setText(QString::number(McLoop::Instance()->GetJobData()->InputMode));
    ui.leEVRouteRecipe->setText(QString::number(McLoop::Instance()->GetJobData()->EVRouteRecipe));
    ui.leENRoutePathInfo->setText(QString::number(McLoop::Instance()->GetJobData()->ENRoutePathInfo));
    ui.leJobChangeflag->setText(QString::number(McLoop::Instance()->GetJobData()->JobChangeflag));
    ui.leOptionValue->setText(McLoop::Instance()->GetJobData()->OptionValue);
   
}

void QWndPLC::on_timertimeout()
{
    if (ui.chk_refreshJobData->isChecked())
    {
        refreshJobData();
    }

}

void QWndPLC::on_chk_refreshJobDataClicked()
{
    McLoop::Instance()->ReadJobData = ui.chk_refreshJobData->isChecked();
}

void QWndPLC::on_pb_ResetStation1Clicked()
{
    McLoop::Instance()->ResetStation1();
}

void QWndPLC::on_pb_ResetStation2Clicked()
{
    McLoop::Instance()->ResetStation2();
}

void QWndPLC::on_pb_TransDataClicked()
{
}
