#pragma once

#include <QWidget>
#include "ui_QWndGlassJudge.h"

class QWndGlassJudge : public QWidget
{
	Q_OBJECT

public:
	QWndGlassJudge(QWidget *parent = nullptr);
	~QWndGlassJudge();

private:
	Ui::QWndGlassJudgeClass ui;
};
