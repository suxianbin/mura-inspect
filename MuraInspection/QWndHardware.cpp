#include "QWndHardware.h"

QWndHardware::QWndHardware(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	//初始化界面布局
	m_pMainDispFrameLayout = new QVBoxLayout(0);
	m_pMainDispFrameLayout->setMargin(0);
	m_pSpmePage = new QWndSPME();
	ui.frame->setLayout(m_pMainDispFrameLayout);
	m_pMainDispFrameLayout->addWidget(m_pSpmePage);
	m_pCameraPage = new QWndCamera();
	m_pMainDispFrameLayout->addWidget(m_pCameraPage);
	m_pPLCPage = new QWndPLC();
	m_pMainDispFrameLayout->addWidget(m_pPLCPage);
	m_pLightPage = new QWndLight();
	m_pMainDispFrameLayout->addWidget(m_pLightPage);

	setAllButtonDefaultStyleSheet();
	hiddenAllWidget();
	setButtonStyleSheet(ui.pb_SPME);
	m_pSpmePage->setHidden(false);

	connect(ui.pb_PLC, SIGNAL(clicked()), this, SLOT(on_pb_PLCClicked()));
	connect(ui.pb_Camera, SIGNAL(clicked()), this, SLOT(on_pb_CameraClicked()));
	connect(ui.pb_Light, SIGNAL(clicked()), this, SLOT(on_pb_LightClicked()));
	connect(ui.pb_SPME, SIGNAL(clicked()), this, SLOT(on_pb_SPMEClicked()));
}

QWndHardware::~QWndHardware()
{}

void QWndHardware::hiddenAllWidget()
{
	m_pPLCPage->setHidden(true);
	m_pSpmePage->setHidden(true);
	m_pCameraPage->setHidden(true);
	m_pLightPage->setHidden(true);

}

void QWndHardware::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(10,45,121);color:rgb(221, 221, 221);border-radius:3px;border:0px;}\
					QPushButton:pressed{ background-color:rgb(13,55,147); }\
					QPushButton:hover{ background-color:rgb(87, 153, 239);}";

	ui.pb_Camera->setStyleSheet(style);
	ui.pb_Light->setStyleSheet(style);
	ui.pb_PLC->setStyleSheet(style);
	ui.pb_SPME->setStyleSheet(style);
}

void QWndHardware::setButtonStyleSheet(QPushButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(87, 153, 239);border-radius:3px;border:0px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}

void QWndHardware::on_pb_PLCClicked()
{
	setButtonStyleSheet(ui.pb_PLC);
	hiddenAllWidget();
	m_pPLCPage->setHidden(false);
}

void QWndHardware::on_pb_CameraClicked()
{
	setButtonStyleSheet(ui.pb_Camera);
	hiddenAllWidget();
	m_pCameraPage->setHidden(false);
}

void QWndHardware::on_pb_LightClicked()
{
	setButtonStyleSheet(ui.pb_Light);
	hiddenAllWidget();
	m_pLightPage->setHidden(false);
}

void QWndHardware::on_pb_SPMEClicked()
{
	setButtonStyleSheet(ui.pb_SPME);
	hiddenAllWidget();
	m_pSpmePage->setHidden(false);
}
