#pragma once

#include <QObject>
#include <QVector>

class CommandData
{
public:
	static CommandData* getInstance();
	static QVector<quint16>* getPLCommand();
	static QVector<quint16>* getPCommand();
	static QVector<quint16>* getPCRecipeListData();
	static bool getPLCCommandLoadDone();
	static bool getPLCCommandScanRequestReply();
	static bool getPLCCommandScanDoneRequest();
	static bool getPLCCommandUnLoadRequestReply();
	static bool getPLCCommandAutoChangeRequest();
	static bool getPLCCommandRecipeID();
	static int  getPLCCameraTrigger();
	static int  getPLCStatus();

	static void setPCCommandLoadDoneReply(bool f);
	static void setPCCommandScanRequest(bool f);
	static void setPCCommandScanDoneReply(bool f);
	static void setPCCommandUnloadRequest(bool);
	static void setCommandHeartBeat(bool f);
	static void setPCMuraStop(bool f);
	static void setPCCoaterStopUpStream(bool f);
	static void setPCCoaterStopDownStream(bool f);
	static void setPCWarningStopStream(bool f);
	static void setPCStatusStream(bool f, int Dn, int bit);
	static void setPCCommandAlarmDown(bool f);
	static void	setPLCCommandAutoChangeRequest();

	static bool getPLCCommandGlassReady();
	static bool getPLCCommandSkipGlassReply();
	static bool getPLCCommandStartReferenceReply();
	static bool getPLCCommandStartMeasureReply();
	static bool getPLCCommandReachMeasurePoint();
	static bool getPLCCommandThisPointMeasureFinishedReply();
	static bool getPLCCommandUnloadGlassReply();

	static void setPCCommandGlassReadyReply(bool f);
	static void setPCCommandSkipGlass(bool f);
	static void setPCCommandSPMEReady(bool f);
	static void setPCCommandStartReference(bool f);
	static void setPCCommandStartMeasure(bool f);
	static void setPCCommandMeasurePointReply(bool f);
	static void setPCCommandThisPointMeasureFinished(bool f);
	static void setPCCommandUnloadGlass(bool f);
	static void setPCCommandMeasurePointIndex(int index);
	static void setPCCommandMeasurePointCount(int count);

	static void setPCCommandHeartbeat(int beat);
	//static bool getPLCCommandStartSpmeMeasure();
	//static void setPCCommandStartSpmeMeasure(bool f);
	//static bool getPLCCommandReachMeasurePoint();
	//static void setPCCommandReachMeasurePoint(bool f);
	//static bool getPLCCommandMeasurePointEnd();
	//static void setPCCommandMeasurePointEnd(bool f);

	static void setPCCommandStation2LoadDoneReply(bool f);
	static void setPCCommandStation2ScanRequest(bool f);
	static void setPCCommandStation2ScanDoneReply(bool f);
	static void setPCCommandStation2UnloadRequest(bool);

	static bool getPLCCommandStation2LoadDone();
	static bool getPLCCommandStation2ScanRequestReply();
	static bool getPLCCommandStation2ScanDoneRequest();
	static bool getPLCCommandStation2UnLoadRequestReply();

private:
	static CommandData* instance;

	QVector<quint16> m_plcCommandData;					//读取的命令数据
	QVector<quint16> m_pcCommandData;					//发送的命令数据

	QVector<quint16> m_pcRecipeListData;				//发送的ReciprList数据

private:
	CommandData();
	~CommandData();
	CommandData(const CommandData &rhs) = delete;            //拷贝构造函数
	CommandData &operator=(const CommandData &rhs) = delete; //拷贝赋值运算符

	CommandData(const CommandData &&rhs) = delete;            //移动构造函数
	CommandData &operator=(const CommandData &&rhs) = delete; //移动赋值运算符
	void setPCCommandFlag(bool f, int Dn, int bit);
	void setPCCommand(int Dn, int val);
	bool getPLCCommandFlag( int Dn, int bit);
	void setPCRecipeListFlag(bool f, int Dn, int bit);
};
