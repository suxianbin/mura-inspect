#pragma once
#include "LightBase.h"
#include <QTcpSocket>
#include <QObject>

//锐视光源控制类
class RuiShiLight : public LightBase
{
	Q_OBJECT
public:
	RuiShiLight();
	~RuiShiLight();
	void SetIntensity(int ch, int intensity) override;
	void TurnOff(int ch) override;
private:
	QTcpSocket* m_pClient1;
	QTcpSocket* m_pClient2;
private slots:
	void on_Client1ReadyRead();
};

