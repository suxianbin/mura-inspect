#pragma once

#include <QMenu>
#include <QMap>
#include <QAction>
#include <QString>

class MyMenu : public QMenu
{
	Q_OBJECT

public:
	MyMenu(QWidget *parent);
	~MyMenu();

	void addCustomMenu(const QString& qsName, const QString& icon, const QString& zhName);
	void addMyAction(QAction& action, const QString zhName);
	QAction* getAction(const QString& qsName);
private:
	QMap<QString, QAction*> m_menuActionMap;
};
