#pragma once

#include <QWidget>
#include "ui_QWndSelectDefect.h"

class QStandardItemModel;

class QWndSelectDefect : public QWidget
{
	Q_OBJECT
signals:
	void sig_DefectSelectChanged(QString, bool);
public:
	QWndSelectDefect(QWidget *parent = nullptr);
	~QWndSelectDefect();
	void updateCheckSum();
private:
	Ui::QWndSelectDefectClass ui;
	void InitTable();
	void ReadParam();

	QStandardItemModel* m_pItemModel = nullptr;
	QStringList DefectTypeList;
	QStringList DefectLevels;
private slots:
	void on_tableSelectDefect_clicked(QModelIndex);
};
