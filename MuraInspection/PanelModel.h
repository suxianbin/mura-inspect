#pragma once
#include "Base.h"

class PanelModel : public Base
{
public:
	PanelModel(QString key);
	PanelModel();
	~PanelModel();
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;
	QString CreateTime = ""; //����ʱ��
	QString Remark = "";
	QString PanelModelName;
	double TopDst;
	double BotDst;
	double LeftDst;
	double RightDst;
	double Radius;
	double Width;
	double Height;
};

