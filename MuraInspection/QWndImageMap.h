#pragma once
#include <QWidget>
#include "ui_QWndImageMap.h"
#include <QVBoxLayout>
#include "ShowLayoutWidget.h"

class QWndImageMap : public QWidget
{
	Q_OBJECT

public:
	QWndImageMap(QWidget *parent = nullptr);
	~QWndImageMap();

	void updateImageMap(int station);
	void selectedDefect(int station, int index);
private:
	Ui::QWndImageMapClass ui;

	//���沼��
	QVBoxLayout* m_pMainDispFrameLayout = nullptr;
	ShowLayoutWidget* m_pStation1 = nullptr;
	ShowLayoutWidget* m_pStation2 = nullptr;

	void hiddenAllWidget();
	void setAllButtonDefaultStyleSheet();
	void setButtonStyleSheet(QPushButton* button);
private slots:
	void on_pb_Station1Clicked();
	void on_pb_Station2Clicked();
	void on_Station1sig_LayoutChecked(bool);
	void on_Station1sig_PanelChecked(bool);
	void on_Station1sig_DefectChecked(bool);
	void on_Station2sig_LayoutChecked(bool);
	void on_Station2sig_PanelChecked(bool);
	void on_Station2sig_DefectChecked(bool);

};
