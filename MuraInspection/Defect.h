#pragma once
#include <QString>
#include <vector>
#include <opencv2/opencv.hpp>

enum class eDefectType //缺陷类型
{
	null,
	BrightHLine,
	BrightVLine,
	DarkHLine,
	DarkVLine,
	BrightDot,
	DarkDot,
};

enum class eDefectLevel //缺陷等级
{
	null,
	Level_S, //小缺陷
	Level_M, //中等缺陷
	Level_L, //大缺陷
	Level_O, //超大缺陷
};


struct Defect
{
	int defectId = 0;						//缺陷id 方便与平台结果对应
	cv::Mat defectImg;						//缺陷小图
	cv::Mat defectImgPath;					//缺陷小图路径
	QString panelId = "";					//所属panel
	eDefectType DefectType = eDefectType::null;
	eDefectLevel DefectLevel = eDefectLevel::null;
	QString type = "BrightHLine";			//缺陷类型
	QString panelModel = "";				//panelModel
	double imgPosX = 0.0;					//像素坐标X 
	double imgPosY = 0.0;					//像素坐标Y 
	double phyPosX = 0.0;					//物理坐标X 
	double phyPosY = 0.0;					//物理坐标Y 
	double width = 0.0;						//缺陷宽
	double length = 0.0;					//缺陷长
	double size = 0.0;						//缺陷面积
	double linearity = 0.0;					//区域线性度
	double meangraydiff2;					//缺陷区域与背景灰度均值的灰度标准差
	double area2smallestRectRatio;			//缺陷面积与最小外接矩形面积比例
	double roundness;						//圆度1(<1)
	double rect2Phi;						//最小外接矩形角度(长边方向角度，(-pi/2,pi/2))
	double grayDeviation;					//灰度方差
	double grayNeiRegionRingMean;			//缺陷邻域背景平均灰度
	double thicknessDev;					//区域主方向宽度标准差

	int defectrank = 0;						//缺陷等级  越大越严重
	double grayMean = 0.0;					//平均灰度
	std::vector<cv::Point2f> pts;		    //缺陷外接的五个点，顺序左上、右上、右下、左下、左上，可直接画图,20180808 新增
	std::vector<std::vector<cv::Point2f>> contours; //每一个缺陷可能包括多个轮廓区域，存储一个缺陷信息的所有轮廓集合

	bool isIgnoreAlarm = false;			    //是否屏蔽报警
	bool isCommonDefect = false;		    //是否是共通缺陷
	int  warningLevel = -1;				    //缺陷报警等级标识 0;无 1：warning pass 2:warning stop 3：commonDefect
	bool isVisible = true;                  //是否显示

};
