#include "QWndHomePageGlassInfo.h"
//#include "QWndSystemParam.h"

QWndHomePageGlassInfo::QWndHomePageGlassInfo(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	connect(ui.pb_DeputyDefectImg, SIGNAL(clicked()), this, SLOT(on_pb_DeputyDefectImgClicked()));
	connect(ui.pb_CheckSum, SIGNAL(clicked()), this, SLOT(on_pb_CheckSumClicked()));
	connect(ui.pb_SelectDefect, SIGNAL(clicked()), this, SLOT(on_pb_SelectDefectClicked()));

	m_pDeputyImg = new QWndDefectDeputyImage();
	m_pCheckSum = new QWndCheckSum();
	m_pSelectDefect = new QWndSelectDefect();
	connect(m_pSelectDefect, SIGNAL(sig_DefectSelectChanged(QString, bool)), this, SIGNAL(sig_DefectSelectChanged(QString, bool)));

	m_pMainDispFrameLayout = new QVBoxLayout(0);
	m_pMainDispFrameLayout->setMargin(0);
	ui.frame->setLayout(m_pMainDispFrameLayout);
	m_pMainDispFrameLayout->addWidget(m_pDeputyImg);
	m_pMainDispFrameLayout->addWidget(m_pCheckSum);	
	m_pMainDispFrameLayout->addWidget(m_pSelectDefect);
	hiddenAllWidget();
	//if (QWndSystemParam::Instance()->SystemParam.CheckSumFirst)
	//{
	//	m_pCheckSum->setHidden(false);
	//	setButtonStyleSheet(ui.pb_CheckSum);
	//}
	//else if (QWndSystemParam::Instance()->SystemParam.DeputyImgFirst)
	//{
	//	m_pDeputyImg->setHidden(false);
	//	setButtonStyleSheet(ui.pb_DeputyDefectImg);
	//}
	//else //Ĭ��ֵ
	//{
	//	m_pCheckSum->setHidden(false);
	//	setButtonStyleSheet(ui.pb_CheckSum);
	//}
}

QWndHomePageGlassInfo::~QWndHomePageGlassInfo()
{}

void QWndHomePageGlassInfo::updateCheckInfo()
{
	//m_pCheckSum->updateCheckSum(recipePlug,runTimeData->rlt);
	//m_pDeputyImg->updateDeputyImage();
}

void QWndHomePageGlassInfo::hiddenAllWidget()
{
	m_pDeputyImg->setHidden(true);
	m_pCheckSum->setHidden(true);
	m_pSelectDefect->setHidden(true);
}

void QWndHomePageGlassInfo::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);color:rgb(20, 20, 20);border-radius:5px;border:0px;}\
					QPushButton:pressed {background-color:rgba(13,55,147,0.85);}\
					QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_DeputyDefectImg->setStyleSheet(style);
	ui.pb_CheckSum->setStyleSheet(style);
	ui.pb_SelectDefect->setStyleSheet(style);
}

void QWndHomePageGlassInfo::setButtonStyleSheet(QPushButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(87, 153, 239);border-radius:5px;border:0px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}

void QWndHomePageGlassInfo::on_pb_DeputyDefectImgClicked()
{
	setButtonStyleSheet(ui.pb_DeputyDefectImg);
	hiddenAllWidget();
	m_pDeputyImg->setHidden(false);
}

void QWndHomePageGlassInfo::on_pb_CheckSumClicked()
{
	setButtonStyleSheet(ui.pb_CheckSum);
	hiddenAllWidget();
	m_pCheckSum->setHidden(false);
}

void QWndHomePageGlassInfo::on_pb_SelectDefectClicked()
{
	setButtonStyleSheet(ui.pb_SelectDefect);
	hiddenAllWidget();
	m_pSelectDefect->setHidden(false);
}
