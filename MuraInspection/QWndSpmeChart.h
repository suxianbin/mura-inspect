#pragma once

#include <QWidget>
#include "ui_QWndSpmeChart.h"
#include "QCustomPlot.h"

class QWndSpmeChart : public QWidget
{
	Q_OBJECT

public:
	QWndSpmeChart(QWidget *parent = nullptr);
	~QWndSpmeChart();

private:
	Ui::QWndSpmeChartClass ui;
	void InitPlot();
	void AddPlotGraph(QColor color, QString text);
	QCustomPlot* m_pCustomPlot;
};
