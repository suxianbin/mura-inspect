#pragma once
#include "Base.h"

class CameraParam : public Base
{
public:
	CameraParam(QString key);
	~CameraParam() override;
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;

	QString CreateTime;
	QString Remark;
	int ExposureTime = 1000;
	int SelectedUserFFTIndex;
	int BaudRateIndex = 1;
};

