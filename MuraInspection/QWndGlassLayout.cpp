#include "QWndGlassLayout.h"
#include "PanelModelMgr.h"
#include "ShowLayoutWidget.h"
#include <QMessageBox>
#pragma execution_character_set("utf-8")

QWndGlassLayout::QWndGlassLayout(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_pShowLayout = new ShowLayoutWidget(this);
	//m_pShowLayout->setCheckBoxVisible(false);
	m_pMainDispFrameLayout = new QVBoxLayout();
	m_pMainDispFrameLayout->setMargin(0);
	ui.frame->setLayout(m_pMainDispFrameLayout);
	m_pMainDispFrameLayout->addWidget(m_pShowLayout);
	refreshLayoutListTable();
	refreshModelListTable();
	refreshModelComboBox();
	setStyleSheet();
	connect(ui.tw_layoutList, &QTableWidget::currentCellChanged, this, &QWndGlassLayout::on_LayoutTableSelectedChanged);
	connect(ui.tw_ModelList, &QTableWidget::currentCellChanged, this, &QWndGlassLayout::on_ModelTableSeletedChanged);

	connect(ui.pb_NewModel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_NewModelClicked);
	connect(ui.pb_EditModel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_EditModelClicked);
	connect(ui.pb_SaveModel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_SaveModelClicked);
	connect(ui.pb_DeleteModel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_DeleteModelClicked);
	connect(ui.pb_GlassLayoutSave, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_GlassLayoutSaveClicked);
	connect(ui.pb_GlassLayoutNew, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_GlassLayoutNewClicked);
	connect(ui.pb_GlassLayoutEdit, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_GlassLayoutEditClicked);
	connect(ui.pb_GlassLayoutDelete, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_GlassLayoutDeleteClicked);
	connect(ui.pb_GlassLayoutSearch, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_GlassLayoutSearchClicked);
	connect(ui.pb_DrawPanel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_DrawPanelClicked);
	connect(ui.pb_ImportCSV, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_ImportCSVClicked);
	connect(ui.pb_ExportCSV, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_ExportCSVClicked);
	connect(ui.pb_LoadImg, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_LoadImgClicked);
	connect(ui.pb_PanelSure, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_PanelSureClicked);
	connect(ui.pb_PanelCancel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_PanelCancelClicked);
	connect(ui.pb_DelPanel, &QPushButton::clicked, this, &QWndGlassLayout::on_pb_DelPanelClicked);
}

QWndGlassLayout::~QWndGlassLayout()
{}

void QWndGlassLayout::refreshLayoutListTable()
{
	QStringList headers;
	headers << "布局名称" << "更新时间" << "备注信息";
	ui.tw_layoutList->setColumnCount(headers.size());
	ui.tw_layoutList->setHorizontalHeaderLabels(headers);
	ui.tw_layoutList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_layoutList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_layoutList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_layoutList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_layoutList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_layoutList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_layoutList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_layoutList->verticalHeader()->hide();
	ui.tw_layoutList->setRowCount(GlassLayoutMgr::Instance()->Size());

	int i = 0;
	for (auto var :  GlassLayoutMgr::Instance()->FindAll())
	{
		GlassLayout* layout = dynamic_cast<GlassLayout*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(layout->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_layoutList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(layout->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_layoutList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(layout->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_layoutList->setItem(i, 2, item2);
		i++;
	}
}

void QWndGlassLayout::refreshModelListTable()
{
	QStringList headers;
	headers << "Model名称" << "更新时间" << "备注信息";
	ui.tw_ModelList->setColumnCount(headers.size());
	ui.tw_ModelList->setHorizontalHeaderLabels(headers);
	ui.tw_ModelList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_ModelList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_ModelList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_ModelList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_ModelList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_ModelList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_ModelList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_ModelList->verticalHeader()->hide();
	ui.tw_ModelList->setRowCount(PanelModelMgr::Instance()->Size());

	int i = 0;
	for (auto var : PanelModelMgr::Instance()->FindAll())
	{
		PanelModel* model = dynamic_cast<PanelModel*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(model->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_ModelList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(model->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_ModelList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(model->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_ModelList->setItem(i, 2, item2);
		i++;
	}
}

void QWndGlassLayout::setStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(13,55,147);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_NewModel->setStyleSheet(style);
	ui.pb_EditModel->setStyleSheet(style);
	ui.pb_SaveModel->setStyleSheet(style);
	ui.pb_DeleteModel->setStyleSheet(style);
	ui.pb_GlassLayoutSave->setStyleSheet(style);
	ui.pb_GlassLayoutEdit->setStyleSheet(style);
	ui.pb_GlassLayoutDelete->setStyleSheet(style);
	ui.pb_GlassLayoutSearch->setStyleSheet(style);
	ui.pb_DrawPanel->setStyleSheet(style);
	ui.pb_ImportCSV->setStyleSheet(style);
	ui.pb_ExportCSV->setStyleSheet(style);
	ui.pb_LoadImg->setStyleSheet(style);
	ui.pb_PanelSure->setStyleSheet(style);
	ui.pb_PanelCancel->setStyleSheet(style);
	ui.pb_DelPanel->setStyleSheet(style);
}

void QWndGlassLayout::refreshParam(GlassLayout* param)
{
	ui.le_GlassLayout_X->setText(QString::number(param->GlassX));
	ui.le_GlassLayout_Y->setText(QString::number(param->GlassY));
	ui.le_GlassLayout_Width->setText(QString::number(param->GlassWidth));
	ui.le_GlassLayout_Height->setText(QString::number(param->GlassHeight));
	ui.le_GlassLayout_Remark->setText(param->Remark);
	ui.le_GlassLayout_Name->setText(param->Key);
	ui.le_GlassLayout_X->setText(QString::number(param->GlassX));
	ui.le_GlassLayout_LDM->setCurrentIndex((int)param->FlowDir);
	ui.le_GlassLayout_GCM->setCurrentIndex((int)param->GlassCoorDir);
	ui.le_GlassLayout_Org->setCurrentIndex((int)param->GlassCoorMode);
	ui.cmb_GlassLayout_Unit->setCurrentIndex((int)param->GlassUnit);
	ui.le_layout_PixelStation1->setText(QString::number(param->Station1PixelEquivalent, 'f', 3));
	ui.le_layout_PixelStation2->setText(QString::number(param->Station2PixelEquivalent, 'f', 3));
	ui.le_GlassLayout_ImgPath->setText(param->LayoutModelImagePath);

	int i = 0;
	for (auto var : PanelModelMgr::Instance()->FindAll())
	{
		if (var->Key == param->ModelName)
		{
			break;
		}
		i++;
	}
	ui.cb_modelList->setCurrentIndex(i);
	ui.le_addPanel_row->setText(QString::number(param->Row));
	ui.le_addPanel_column->setText(QString::number(param->Col));
	ui.le_addPanel_x->setText(QString::number(param->TopLeft_X, 'f', 3));
	ui.le_addPanel_y->setText(QString::number(param->TopLeft_Y, 'f', 3));
	ui.le_addPanel_xInterval->setText(QString::number(param->Space_X, 'f', 3));
	ui.le_addPanel_yInterval->setText(QString::number(param->Space_Y, 'f', 3));
	//ui.le_addPanel_contract->setText(QString::number(param->contract, 'f', 3));
}

void QWndGlassLayout::refreshLayoutWidget(GlassLayout* param)
{
	m_pShowLayout->showLayout(param);
}

void QWndGlassLayout::refreshPanelTable(GlassLayout* param)
{
	QList<PanelItem*>items;
	ui.tw_panel->showPanelList(param,items);
}

void QWndGlassLayout::refreshModelComboBox()
{
	ui.cb_modelList->clear();
	for (int i = 0; i < PanelModelMgr::Instance()->Size(); i++) 
	{
		const QString& str = PanelModelMgr::Instance()->FindAll()[i]->Key;
		ui.cb_modelList->addItem(str);
	}
}

QVector<PanelLayout> QWndGlassLayout::getPanelLayouts()
{
	QVector<PanelLayout> res;
	QString modelName = ui.cb_modelList->currentText();
	if (modelName != "") {
		PanelModel* pm = dynamic_cast<PanelModel*>(PanelModelMgr::Instance()->FindBy(modelName));
		if (!pm) return res;
		bool f = false;
		qreal x = ui.le_addPanel_x->text().toDouble(&f);
		if (!f)
		{
			return QVector<PanelLayout>();
		}
		float x_mm = x;


		
		//基于图片坐标的x
		x = panelPositionToGlassPosition(x_mm, *m_layoutParam, true) / m_layoutParam->Station1PixelEquivalent;
		//x = m_currentGlr.layout.toGlassPositionX(m_currentGlr.layout.x) + m_currentGlr.layout.x + (x / m_currentGlr.layout.pixelEquivalent*xDirection);

		qreal y = ui.le_addPanel_y->text().toDouble(&f);
		if (!f) {
			return QVector<PanelLayout>();
		}
		float y_mm = y;
		//基于图片坐标的y
		y = panelPositionToGlassPosition(y_mm, *m_layoutParam, false) / m_layoutParam->Station1PixelEquivalent;
		//y = m_currentGlr.layout.toGlassPositionY(m_currentGlr.layout.y) + m_currentGlr.layout.y +( y / m_currentGlr.layout.pixelEquivalent * yDirection);

		qreal xI = ui.le_addPanel_xInterval->text().toDouble(&f);
		if (!f) {
			return QVector<PanelLayout>();
		}
		float xI_mm = xI;
		xI = xI / m_layoutParam->Station1PixelEquivalent;

		qreal yI = ui.le_addPanel_yInterval->text().toDouble(&f);
		if (!f) {
			return QVector<PanelLayout>();
		}
		float yI_mm = yI;
		yI = yI / m_layoutParam->Station1PixelEquivalent;

		qreal contract = ui.le_addPanel_contract->text().toDouble(&f);
		if (!f) {
			return QVector<PanelLayout>();
		}
		float contract_mm = contract;
		contract = contract / m_layoutParam->Station1PixelEquivalent;

		int r = ui.le_addPanel_row->text().toDouble(&f);
		if (!f) {
			return QVector<PanelLayout>();
		}
		int c = ui.le_addPanel_column->text().toDouble(&f);
		if (!f) {
			return QVector<PanelLayout>();
		}
		qreal w = pm->Width / m_layoutParam->Station1PixelEquivalent;
		qreal l = pm->Height / m_layoutParam->Station1PixelEquivalent;
		for (int ir = 0; ir < r; ir++)
		{
			for (int ic = 0; ic < c; ic++)
			{
				PanelLayout pl;
				//像素值
				pl.width = w;
				pl.height = l;
				pl.x = x + ic * (xI + w);
				pl.y = y + ir * (yI + l);
				pl.contract = contract;
				//图像值mm
				//pl.width_mm = pm.width;
				//pl.length_mm = pm.length;
				//pl.x_mm = x_mm + ic * (xI_mm + pm.width);
				//pl.y_mm = y_mm + ir * (yI_mm + pm.length);
				//pl.contract_mm = contract_mm;
				//pl.panelModel = pm.name;

				//[2024/7/18 11:46 zaiyuan.li]	
				pl.radius = pm->Radius;

				pl.topDst = pm->TopDst;
				pl.botDst = pm->BotDst;
				pl.leftDst = pm->LeftDst;
				pl.rightDst = pm->RightDst;
				//pl.panelMaskList = pm->panelMaskList
				res << pl;
			}
		}
	}
	return res;
}

double QWndGlassLayout::panelPositionToGlassPosition(float xORy, const GlassLayout& glassLayout, bool isX)
{
	int xDirection = 1;
	int yDirection = 1;
	qreal xOffset = 0;
	qreal yOffset = 0;
	//switch (_panelList.glassCoordinateMode) {
	//case eBottomLeft_GOM:
	//	yOffset = _panelList.length_mm;
	//	xDirection = 1;  yDirection = -1;
	//	break;
	//case eTopLeft_GOM:
	//	xDirection = 1;  yDirection = 1;
	//	break;
	//case eTopRight_GOM:    xOffset = _panelList.width_mm;
	//	xDirection = -1;  yDirection = 1;
	//	break;
	//case eBottomRight_GOM: xOffset = _panelList.width_mm; yOffset = _panelList.length_mm;
	//	xDirection = -1;  yDirection = -1;
	//	break;
	//case eCenter_GOM:
	//	xOffset = _panelList.width_mm / 2;
	//	yOffset = _panelList.length_mm / 2;
	//	switch (_panelList.glassCoordinatePosition) {
	//	case eRightUp_GCM:
	//		xDirection = 1; yDirection = -1;
	//		break;
	//	case eLeftUp_GCM:
	//		xDirection = -1;  yDirection = -1;
	//		break;
	//	case eLeftDown_GCM:
	//		xDirection = -1;  yDirection = 1;
	//		break;
	//	case eRightDown_GCM:
	//		xDirection = 1;  yDirection = 1;
	//		break;
	//	}
	//	break;
	//}
	//float res;
	//if (isX)
	//{
	//	res = (_panelList.x_mm + xOffset) + (xORy * xDirection);
	//}
	//else
	//{
	//	res = (_panelList.y_mm + yOffset) + (xORy * yDirection);
	//}
	return 0;
}

void QWndGlassLayout::on_LayoutTableSelectedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tw_layoutList->rowCount())
	{
		if (currentRow != previousRow)
		{
			QTableWidgetItem* item = ui.tw_layoutList->item(currentRow, 0);
			QString layoutName = item->text();
			GlassLayout* layoutParam = dynamic_cast<GlassLayout*>(GlassLayoutMgr::Instance()->FindBy(layoutName));
			m_layoutParam = layoutParam;
			refreshParam(m_layoutParam);
			refreshLayoutWidget(m_layoutParam);
			refreshPanelTable(m_layoutParam);
		}
	}
	else
	{

	}
}

void QWndGlassLayout::on_ModelTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tw_ModelList->rowCount())
	{
		if (currentRow != previousRow)
		{
			QTableWidgetItem*item = ui.tw_ModelList->item(currentRow, 0);
			QString modelName = item->text();
			PanelModel* panelModel =dynamic_cast<PanelModel*>(PanelModelMgr::Instance()->FindBy(modelName));
			ui.le_ModelName->setText(panelModel->Key);
			ui.le_ModelLength->setText(QString::number(panelModel->Height,'f',2));
			ui.le_ModelWidth->setText(QString::number(panelModel->Width, 'f', 2));
			ui.le_ModelRadius->setText(QString::number(panelModel->Radius, 'f', 2));
			ui.sp_ModelTopDst->setValue(panelModel->TopDst);
			ui.sp_ModelBotDst->setValue(panelModel->BotDst);
			ui.sp_ModelLeftDst->setValue(panelModel->LeftDst);
			ui.sp_ModelRightDst->setValue(panelModel->RightDst);
		}
	}
	else
	{

	}
}

void QWndGlassLayout::on_pb_NewModelClicked()
{
}

void QWndGlassLayout::on_pb_EditModelClicked()
{
}

void QWndGlassLayout::on_pb_SaveModelClicked()
{
	PanelModel* panelModel = dynamic_cast<PanelModel*>(PanelModelMgr::Instance()->FindBy(ui.le_ModelName->text()));
	panelModel->CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm::ss");
	panelModel->Remark = "";
	panelModel->PanelModelName = ui.le_ModelName->text();
	panelModel->TopDst = ui.sp_ModelTopDst->value();
	panelModel->BotDst = ui.sp_ModelBotDst->value();
	panelModel->LeftDst = ui.sp_ModelLeftDst->value();
	panelModel->RightDst = ui.sp_ModelRightDst->value();
	panelModel->Radius = ui.le_ModelRadius->text().toDouble();
	panelModel->Width = ui.le_ModelWidth->text().toDouble();
	panelModel->Height = ui.le_ModelLength->text().toDouble();
	PanelModelMgr::Instance()->Save("PanelModelMgr.json");
	refreshModelComboBox();
}

void QWndGlassLayout::on_pb_DeleteModelClicked()
{
	refreshModelComboBox();
}

void QWndGlassLayout::on_pb_GlassLayoutNewClicked()
{
}

void QWndGlassLayout::on_pb_GlassLayoutSaveClicked()
{
	GlassLayout* glassLayout = dynamic_cast<GlassLayout*>(GlassLayoutMgr::Instance()->FindBy(ui.le_GlassLayout_Name->text()));
	glassLayout->CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm::ss");
	glassLayout->FlowDir = (eFlowDir)(ui.le_GlassLayout_LDM->currentIndex());
	glassLayout->GlassCoorDir = (eCoorDir)(ui.le_GlassLayout_GCM->currentIndex());
	glassLayout->GlassCoorMode = (eCoorMode)(ui.le_GlassLayout_Org->currentIndex());
	glassLayout->GlassUnit = (eUnit)(ui.cmb_GlassLayout_Unit->currentIndex());
	glassLayout->Remark = ui.le_GlassLayout_Remark->text();
	glassLayout->LayoutName = ui.le_GlassLayout_Name->text();
	glassLayout->GlassX = ui.le_GlassLayout_X->text().toDouble();
	glassLayout->GlassY = ui.le_GlassLayout_X->text().toDouble();
	glassLayout->GlassWidth = ui.le_GlassLayout_Width->text().toDouble();
	glassLayout->GlassHeight = ui.le_GlassLayout_Height->text().toDouble();
	glassLayout->Station1PixelEquivalent = ui.le_layout_PixelStation1->text().toDouble();
	glassLayout->Station2PixelEquivalent = ui.le_layout_PixelStation2->text().toDouble();
	glassLayout->ModelName = ui.cb_modelList->currentText();
	glassLayout->Row = ui.le_addPanel_row->text().toDouble();
	glassLayout->Col = ui.le_addPanel_column->text().toDouble();
	glassLayout->TopLeft_X = ui.le_addPanel_x->text().toDouble();
	glassLayout->TopLeft_Y = ui.le_addPanel_y->text().toDouble();
	glassLayout->Space_X = ui.le_addPanel_xInterval->text().toDouble();
	glassLayout->Space_Y = ui.le_addPanel_yInterval->text().toDouble();
	glassLayout->LayoutModelImagePath = ui.le_GlassLayout_ImgPath->text();
	ui.tw_panel->setPixelEquivalent(glassLayout->Station1PixelEquivalent);
	glassLayout->PanelLists = ui.tw_panel->getLayouts();
	GlassLayoutMgr::Instance()->Save("GlassLayoutMgr.json");
}

void QWndGlassLayout::on_pb_GlassLayoutEditClicked()
{
}

void QWndGlassLayout::on_pb_GlassLayoutDeleteClicked()
{
}

void QWndGlassLayout::on_pb_GlassLayoutSearchClicked()
{
}

void QWndGlassLayout::on_pb_GlassLayoutBatchModifyClicked()
{
}

void QWndGlassLayout::on_pb_DrawPanelClicked()
{
}

void QWndGlassLayout::on_pb_ImportCSVClicked()
{
}

void QWndGlassLayout::on_pb_ExportCSVClicked()
{
}

void QWndGlassLayout::on_pb_LoadImgClicked()
{
	QString fPath = QFileDialog::getOpenFileName(Q_NULLPTR, "请选择需要加载的图片", "D:\\", "Image Files(*.jpg *.png *.bmp)");
	if (!fPath.isEmpty())
	{
		cv::Mat mat = cv::imread(fPath.toStdString());
		ui.le_GlassLayout_ImgPath->setText(fPath);
		//cv::Mat mat = cv::imread(fPath.toStdString(), cv::IMREAD_GRAYSCALE);
		if (mat.empty())
		{
			QMessageBox::warning(this, "加载失败", "无法加载选择的图片。");
			return;
		}
		m_pShowLayout->loadImage(mat);
	}
}

void QWndGlassLayout::on_pb_PanelSureClicked()
{
	if (ui.cb_modelList->currentText() == "")
	{
		QMessageBox::warning(Q_NULLPTR, "错误", "Model类型不能为空！");
	}



}

void QWndGlassLayout::on_pb_PanelCancelClicked()
{
}

void QWndGlassLayout::on_pb_DelPanelClicked()
{
}
