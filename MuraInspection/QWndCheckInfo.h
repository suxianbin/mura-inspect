#pragma once

#include <QWidget>
#include "ui_QWndCheckInfo.h"

class QWndCheckInfo : public QWidget
{
	Q_OBJECT

public:
	QWndCheckInfo(QWidget *parent = nullptr);
	~QWndCheckInfo();
	void updateCheckInfo();
private:
	Ui::QWndCheckInfoClass ui;
};
