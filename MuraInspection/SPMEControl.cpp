#pragma execution_character_set("utf-8")
#include "SPMEControl.h"
#include <QGridLayout>
#include <QLabel>
#include <QHeaderView>
#include <QMessageBox>
#include "RecipeInfo.h"
#include "RecipeMgr.h"
#include "DevLog.h"

SPMEControl::SPMEControl(QObject* parent)
{
	m_pClient = new QTcpSocket();
}

SPMEControl::~SPMEControl()
{}

void SPMEControl::InitSpme()
{
}

void SPMEControl::IsConnected()
{
}

void SPMEControl::SetMeasType(eSpmeType type)
{
}

void SPMEControl::SetMeasCondition(int measID)
{
}

void SPMEControl::on_RecipeChanged(const QString& recipeName)
{
}

void SPMEControl::on_RecieveTestResult(QString msg)
{
}

void SPMEControl::SetAnalyzeCondition(int condition)
{
	QString str = QString("%1;SET-ANALYSISCONDITION;1;%2").arg(m_commandIndex++).arg(condition);
	int ret = m_pClient->write(str.toLocal8Bit());
	m_pClient->flush();
	//glog << "send setAnalyzeCondition:" << str << gendl;
}

void SPMEControl::MeasReference()
{
}

void SPMEControl::MeasSample()
{
}

void SPMEControl::AnalyzeSample()
{
}

void SPMEControl::SetIsMeasReference(bool needMeas)
{
}

bool SPMEControl::ProcessRecievedMsg(QString msg)
{
	QString msg1 = msg.replace("", "").replace("", "");
	QStringList list = msg1.split(';');
	if (list[0].toInt() == (m_commandIndex - 1))
	{
		if (list[1] == "MEASURE-REFERENCE" && list[2].toInt() == 1)
		{
			m_isRecieveReply = true;
		}
		if (list[1] == "MEASURE-SAMPLE" && list[2].toInt() == 1)
		{
			m_isRecieveReply = true;
		}
		if (list[1] == "ANALYZE-SAMPLE" && list[2].toInt() == 1)
		{
			m_isRecieveReply = true;
		}
		if (list[1] == "ANALYZE-SAMPLE" && list.size() >= 5 && m_spmeType == eSpmeType::OD)
		{
			m_OD = list[4].toDouble();
			glog << "Measure OD:" << m_OD << gendl;
		}
		if (list[1] == "ANALYZE-SAMPLE" && list.size() >= 7 && m_spmeType == eSpmeType::VIS)
		{
			m_thick = list[6].toDouble();
			glog << "Measure Thickness:" << m_thick << gendl;
		}
		if (list[1] == "ANALYZE-SAMPLE" && list.size() == 5 && m_spmeType == eSpmeType::Color)
		{
			QStringList sublist = list[4].split(',');
			if (sublist.size() >= 5)
			{
				m_color_x = sublist[3].toDouble();
				m_color_y = sublist[4].toDouble();
				m_color_Y = sublist[1].toDouble();
			}
			glog << "color_x,color_y,color_Y:" << m_color_x << "," << m_color_y << "," << m_color_Y << gendl;
		}
		if (list[1] == "SET-MEASCONDITION")
		{
			if (m_spmeType == eSpmeType::VIS)
			{
				SetAnalyzeCondition(2);
			}
			else
				SetAnalyzeCondition(1);
		}
	}

	return false;
}
