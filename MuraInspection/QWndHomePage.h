#pragma once

#include <QWidget>
#include <QTimer>
#include "ui_QWndHomePage.h"
#include "QWndInspectInfo.h"
#include "QWndImageMap.h"
#include "QWndProcessInfo.h"
#include "QWndHomePageGlassInfo.h"
#include "ShowLayoutWidget.h"

class QWndHomePage : public QWidget
{
	Q_OBJECT

signals:
	void sigPanelJudgeEdit();
public:
	QWndHomePage(QWidget *parent = nullptr);
	~QWndHomePage();

	void SetIcons();
	void InitWindow();
	void RefreshWidget();
private:
	Ui::QWndHomePageClass ui;
	QWndInspectInfo* m_pInspInfo = nullptr;
	QWndProcessInfo* m_pProcessInfo = nullptr;
	QWndImageMap* m_pImageShow = nullptr;
	ShowLayoutWidget* m_pShowMap = nullptr;
	QWndHomePageGlassInfo* m_pGlassInfo = nullptr;
	QTimer timer2000;
protected:
	void showEvent(QShowEvent* event) override;
public slots:
	void on_timertimeout();
	void on_SelectedDefectChangded(int index);

	void on_pb_StartInspectClicked(bool);
	void on_pb_PauseInspectClicked();
	void on_pb_StopInspectClicked(bool);
	void on_pb_NgMarkClicked();
	void on_pb_NgMarkOpenClicked();
	void on_pb_ManualStopClicked();
	void on_pb_CoaterStopClicked();
	void on_pb_AutoRecipeClicked();
	void on_pb_ReviewClicked();
	void on_pb_SingleStepClicked();

	void on_selectedDefectStation1(int index);
	void on_selectedDefectStation2(int index);

	void on_DefectTypeSelectChanged(QString, bool);

	void on_acqOperation_CurrentRecipe();
	void on_acqOperation_StopStep();
	void on_acqOperationExeCuteLog(QString strlog);
};
