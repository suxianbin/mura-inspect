#include "QWndRecipe.h"
#include "RecipeMgr.h"
#include "RecipeInfo.h"
#include "CameraParamMgr.h"
#include "GlassLayoutMgr.h"
#include "LightParamMgr.h"
#include "SpmeParamMgr.h"
#include "QWndMessageBox.h"
#pragma execution_character_set("utf-8")

QWndRecipe::QWndRecipe(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	initRightWidget();
	setStyleSheet();
	refreshSpmeParamListTable();
	initRecipeBindingList();
	initParamComboBox();

	connect(ui.pb_ModifyBatch, &QPushButton::clicked, this, &QWndRecipe::on_pb_ModifyBatchClicked);
	connect(ui.pb_RecipeNew, &QPushButton::clicked, this, &QWndRecipe::on_pb_RecipeNewClicked);
	connect(ui.pb_RecipeEdit, &QPushButton::clicked, this, &QWndRecipe::on_pb_RecipeEditClicked);
	connect(ui.pb_RecipeDelete, &QPushButton::clicked, this, &QWndRecipe::on_pb_RecipeDeleteClicked);
	connect(ui.pb_RecipeSearch, &QPushButton::clicked, this, &QWndRecipe::on_pb_RecipeSearchClicked);
	connect(ui.pb_RecipeSave, &QPushButton::clicked, this, &QWndRecipe::on_pb_RecipeSaveClicked);
	connect(ui.pb_BindingModifyBatch, &QPushButton::clicked, this, &QWndRecipe::on_pb_BindingModifyBatchClicked);
	connect(ui.pb_Binding, &QPushButton::clicked, this, &QWndRecipe::on_pb_BindingRecipeClicked);
	connect(ui.pb_BindingSearch, &QPushButton::clicked, this, &QWndRecipe::on_pb_BindingSearchClicked);
	connect(ui.tw_RecipeParamList, &QTableWidget::currentCellChanged, this, &QWndRecipe::on_ParamTableSeletedChanged);
}

QWndRecipe::~QWndRecipe()
{}

void QWndRecipe::loadConfig()
{
}

void QWndRecipe::setStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);border-radius:3px;}\
					 QPushButton:pressed {background-color:rgb(13,55,147);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	ui.pb_ModifyBatch->setStyleSheet(style);
	ui.pb_RecipeNew->setStyleSheet(style);
	ui.pb_RecipeEdit->setStyleSheet(style);
	ui.pb_RecipeDelete->setStyleSheet(style);
	ui.pb_RecipeSearch->setStyleSheet(style);
	ui.pb_RecipeSave->setStyleSheet(style);
	ui.pb_BindingModifyBatch->setStyleSheet(style);
	ui.pb_Binding->setStyleSheet(style);
	ui.pb_BindingSearch->setStyleSheet(style);
}

void QWndRecipe::refreshSpmeParamListTable()
{
	QStringList headers;
	headers << "参数名称" << "更新时间" << "备注信息";
	ui.tw_RecipeParamList->setColumnCount(headers.size());
	ui.tw_RecipeParamList->setHorizontalHeaderLabels(headers);
	ui.tw_RecipeParamList->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_RecipeParamList->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_RecipeParamList->setAlternatingRowColors(true); // 隔行变色
	ui.tw_RecipeParamList->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_RecipeParamList->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_RecipeParamList->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_RecipeParamList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_RecipeParamList->verticalHeader()->hide();
	ui.tw_RecipeParamList->setRowCount(RecipeMgr::Instance()->Size());

	int i = 0;
	for (auto var : RecipeMgr::Instance()->FindAll())
	{
		RecipeInfo* camera = dynamic_cast<RecipeInfo*>(var);
		QTableWidgetItem* item0 = new QTableWidgetItem(camera->Key);
		item0->setTextAlignment(Qt::AlignCenter);
		ui.tw_RecipeParamList->setItem(i, 0, item0);

		QTableWidgetItem* item1 = new QTableWidgetItem(camera->CreateTime);
		item1->setTextAlignment(Qt::AlignCenter);
		ui.tw_RecipeParamList->setItem(i, 1, item1);

		QTableWidgetItem* item2 = new QTableWidgetItem(camera->Remark);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_RecipeParamList->setItem(i, 2, item2);
		i++;
	}
}

void QWndRecipe::refreshParam(RecipeInfo* recipeParam)
{
	ui.le_RecipeName->setText(recipeParam->Key);
	ui.le_RecipeType->setText(recipeParam->RecipeType);
	ui.le_Remark->setText(recipeParam->Remark);

	ui.cmb_Camera->setCurrentIndex(0);
	for (int i = 0; i < ui.cmb_Camera->count(); i++)
	{

		QString str = ui.cmb_Camera->itemText(i);
		if (str == recipeParam->CameraInfoName)
		{
			ui.cmb_Camera->setCurrentIndex(i);
			break;
		}
	}
}

void QWndRecipe::initParamComboBox()
{
	ui.cmb_Camera->clear();
	ui.cmb_Camera->addItem("null");
	for (auto var : CameraParamMgr::Instance()->FindAll())
	{
		ui.cmb_Camera->addItem(var->Key);
	}
	
	ui.cmb_Layout->clear();
	ui.cmb_Layout->addItem("null");
	for (auto var : GlassLayoutMgr::Instance()->FindAll())
	{
		ui.cmb_Layout->addItem(var->Key);
	}

	ui.cmb_Light->clear();
	ui.cmb_Light->addItem("null");
	for (auto var : LightParamMgr::Instance()->FindAll())
	{
		ui.cmb_Light->addItem(var->Key);
	}

	ui.cmb_Algorithm->clear();
	ui.cmb_Algorithm->addItem("null");
	//for (auto var : LightParamMgr::Instance()->FindAll())
	//{
	//	ui.cmb_Layout->addItem(var->Key);
	//}

	ui.cmb_SPME->clear();
	ui.cmb_SPME->addItem("null");
	for (auto var : SpmeParamMgr::Instance()->FindAll())
	{
		ui.cmb_SPME->addItem(var->Key);
	}

	ui.cmb_Axis->clear();
	ui.cmb_Axis->addItem("null");
	//for (auto var : SpmeParamMgr::Instance()->FindAll())
	//{
	//	ui.cmb_Axis->addItem(var->Key);
	//}

	ui.cmb_PanelJudge->clear();
	ui.cmb_PanelJudge->addItem("null");
	//for (auto var : SpmeParamMgr::Instance()->FindAll())
	//{
	//	ui.cmb_PanelJudge->addItem(var->Key);
	//}
}

void QWndRecipe::initRecipeBindingList()
{
	QStringList headers;
	headers << "ID" << "Binding Name" << "Type";
	ui.tw_Binding->setColumnCount(headers.size());
	ui.tw_Binding->setHorizontalHeaderLabels(headers);
	ui.tw_Binding->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tw_Binding->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tw_Binding->setAlternatingRowColors(true); // 隔行变色
	ui.tw_Binding->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tw_Binding->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tw_Binding->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	ui.tw_Binding->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tw_Binding->verticalHeader()->hide();

	ui.tw_Binding->setRowCount((int)RecipeMgr::Instance()->BindingMap.size());
	int i = 0;
	for (auto var : RecipeMgr::Instance()->BindingMap)
	{
		QTableWidgetItem* item1 = new QTableWidgetItem(QString::number(i + 1));
		item1->setTextAlignment(Qt::AlignCenter);
		QTableWidgetItem* item2 = new QTableWidgetItem(var.second);
		item2->setTextAlignment(Qt::AlignCenter);
		ui.tw_Binding->setItem(i, 0, item1);
		ui.tw_Binding->setItem(i, 1, item2);
		i++;
	}
}

void QWndRecipe::initRightWidget()
{
	//初始化界面布局
	m_pMainDispFrameLayout = new QVBoxLayout(0);
	m_pMainDispFrameLayout->setMargin(0);
	m_pGlsCrackPage = new QWndGlassCrack();
	ui.frame->setLayout(m_pMainDispFrameLayout);
	m_pMainDispFrameLayout->addWidget(m_pGlsCrackPage);
	m_pGlsJudgePage = new QWndGlassJudge();
	m_pMainDispFrameLayout->addWidget(m_pGlsJudgePage);
	m_pPnlJudgePage = new QWndPanelJudge();
	m_pMainDispFrameLayout->addWidget(m_pPnlJudgePage);
	m_pCommonDftPage = new QWndCommonDefect();
	m_pMainDispFrameLayout->addWidget(m_pCommonDftPage);
	m_pDftClsifyPage = new QWndDefectClassification();
	m_pMainDispFrameLayout->addWidget(m_pDftClsifyPage);

	setAllButtonDefaultStyleSheet();
	hiddenAllWidget();
	setButtonStyleSheet(ui.pb_DefectClass);
	m_pDftClsifyPage->setHidden(false);

	connect(ui.pb_DefectClass, SIGNAL(clicked()), this, SLOT(on_pb_DefectClassClicked()));
	connect(ui.pb_GlassJudge, SIGNAL(clicked()), this, SLOT(on_pb_GlassJudgeClicked()));
	connect(ui.pb_PanelJudge, SIGNAL(clicked()), this, SLOT(on_pb_PanelJudgeClicked()));
	connect(ui.pb_CommonDefect, SIGNAL(clicked()), this, SLOT(on_pb_CommonDefectClicked()));
	connect(ui.pb_GlassCrack, SIGNAL(clicked()), this, SLOT(on_pb_GlassCrackClicked()));
}

void QWndRecipe::on_pb_ModifyBatchClicked()
{

}

void QWndRecipe::on_pb_RecipeNewClicked()
{
}

void QWndRecipe::on_pb_RecipeEditClicked()
{
}

void QWndRecipe::on_pb_RecipeDeleteClicked()
{
}

void QWndRecipe::on_pb_RecipeSearchClicked()
{
}

void QWndRecipe::on_pb_RecipeSaveClicked()
{
	if (ui.le_RecipeName->text().isEmpty())
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未选中有效配方!");
		int ret = messageBox->exec();
		return;
	}

	RecipeInfo* recipeParam = dynamic_cast<RecipeInfo*>(RecipeMgr::Instance()->FindBy(ui.le_RecipeName->text()));
	if (!recipeParam)
	{
		QWndMessageBox* messageBox = new QWndMessageBox("未找到配方!" + ui.le_RecipeName->text());
		int ret = messageBox->exec();
		return;
	}

	//ui.le_RecipeName->setText(recipeParam->Key);
	recipeParam->RecipeType = ui.le_RecipeType->text();
	recipeParam->Remark = ui.le_Remark->text();
	recipeParam->CreateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
	recipeParam->CameraInfoName = ui.cmb_Camera->currentText();
	recipeParam->LightInfoName = ui.cmb_Light->currentText();
	recipeParam->SpmeInfoName = ui.cmb_SPME->currentText();
	recipeParam->GlassLayoutName = ui.cmb_Layout->currentText();

	RecipeMgr::Instance()->Save("RecipeMgr.json");
}

void QWndRecipe::on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
	if (currentRow >= 0 && currentRow < ui.tw_RecipeParamList->rowCount())
	{
		if (currentRow != previousRow)
		{
			QTableWidgetItem* item = ui.tw_RecipeParamList->item(currentRow, 0);
			QString Name = item->text();
			RecipeInfo* recipeParam = dynamic_cast<RecipeInfo*>(RecipeMgr::Instance()->FindBy(Name));
			refreshParam(recipeParam);
		}
	}
	else
	{

	}

}

void QWndRecipe::on_pb_BindingModifyBatchClicked()
{
}

void QWndRecipe::on_pb_BindingRecipeClicked()
{
	int cur_row = ui.tw_Binding->currentRow();
	if (cur_row < 0)
	{
		return;
	}
	QString cur_text = ui.le_RecipeName->text();
	RecipeMgr::Instance()->BindingMap[cur_row + 1] = cur_text;
	RecipeMgr::Instance()->Save("RecipeMgr.json");
	QTableWidgetItem* item = ui.tw_Binding->item(cur_row, 1);
	item->setText(cur_text);
	ui.tw_Binding->setItem(cur_row, 1, item);
}

void QWndRecipe::on_pb_BindingSearchClicked()
{
}

void QWndRecipe::hiddenAllWidget()
{
	m_pGlsCrackPage->setHidden(true);
	m_pGlsJudgePage->setHidden(true);
	m_pPnlJudgePage->setHidden(true);
	m_pCommonDftPage->setHidden(true);
	m_pDftClsifyPage->setHidden(true);
}

void QWndRecipe::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(218, 226, 240);color:rgb(22, 22, 22);border-radius:3px;border:0px;}\
					QPushButton:pressed{ background-color:rgb(13,55,147); }\
					QPushButton:hover{ background-color:rgb(87, 153, 239);}";

	ui.pb_DefectClass->setStyleSheet(style);
	ui.pb_GlassJudge->setStyleSheet(style);
	ui.pb_PanelJudge->setStyleSheet(style);
	ui.pb_CommonDefect->setStyleSheet(style);
	ui.pb_GlassCrack->setStyleSheet(style);
}

void QWndRecipe::setButtonStyleSheet(QPushButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(10,45,121);border-radius:3px;border:0px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(10,45,121);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}

void QWndRecipe::on_pb_DefectClassClicked()
{
	setButtonStyleSheet(ui.pb_DefectClass);
	hiddenAllWidget();
	m_pDftClsifyPage->setHidden(false);
}

void QWndRecipe::on_pb_GlassJudgeClicked()
{
	setButtonStyleSheet(ui.pb_GlassJudge);
	hiddenAllWidget();
	m_pGlsJudgePage->setHidden(false);
}

void QWndRecipe::on_pb_PanelJudgeClicked()
{
	setButtonStyleSheet(ui.pb_PanelJudge);
	hiddenAllWidget();
	m_pPnlJudgePage->setHidden(false);
}

void QWndRecipe::on_pb_CommonDefectClicked()
{
	setButtonStyleSheet(ui.pb_CommonDefect);
	hiddenAllWidget();
	m_pCommonDftPage->setHidden(false);
}

void QWndRecipe::on_pb_GlassCrackClicked()
{
	setButtonStyleSheet(ui.pb_GlassCrack);
	hiddenAllWidget();
	m_pGlsCrackPage->setHidden(false);
}


