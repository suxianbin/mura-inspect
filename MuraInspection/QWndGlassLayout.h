#pragma once
#include <QWidget>
#include <QVBoxLayout>
#include "ui_QWndGlassLayout.h"
#include "GlassLayoutMgr.h"

class GlassLayout;
class ShowLayoutWidget;

class QWndGlassLayout : public QWidget
{
	Q_OBJECT

public:
	QWndGlassLayout(QWidget *parent = nullptr);
	~QWndGlassLayout();

private:
	Ui::QWndGlassLayoutClass ui;
	//���沼��
	QVBoxLayout* m_pMainDispFrameLayout = nullptr;
	ShowLayoutWidget* m_pShowLayout = nullptr;
	GlassLayout* m_layoutParam;
private:
	void refreshLayoutListTable();
	void refreshModelListTable();
	void setStyleSheet();
	void refreshParam(GlassLayout* param);
	void refreshLayoutWidget(GlassLayout* param);
	void refreshPanelTable(GlassLayout* param);
	void refreshModelComboBox();

	QVector<PanelLayout> getPanelLayouts();

	double panelPositionToGlassPosition(float xORy, const GlassLayout& _panelList, bool isX);
private slots:
	void on_LayoutTableSelectedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
	void on_ModelTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

	void on_pb_NewModelClicked();
	void on_pb_EditModelClicked();
	void on_pb_SaveModelClicked();
	void on_pb_DeleteModelClicked();

	void on_pb_GlassLayoutNewClicked();
	void on_pb_GlassLayoutSaveClicked();
	void on_pb_GlassLayoutEditClicked();
	void on_pb_GlassLayoutDeleteClicked();
	void on_pb_GlassLayoutSearchClicked();
	void on_pb_GlassLayoutBatchModifyClicked();
	void on_pb_DrawPanelClicked();
	void on_pb_ImportCSVClicked();
	void on_pb_ExportCSVClicked();
	void on_pb_LoadImgClicked();
	void on_pb_PanelSureClicked();
	void on_pb_PanelCancelClicked();
	void on_pb_DelPanelClicked();
};
