#pragma once
#include <QDialog>
#include "ui_QWndResultPanelJudgeEdit.h"
#include <QComboBox>

class QWndResultPanelJudgeEdit : public QDialog
{
	Q_OBJECT

public:
	QWndResultPanelJudgeEdit(QWidget *parent = nullptr);
	~QWndResultPanelJudgeEdit();
	void setRunTimeData();
private:
	Ui::QWndResultPanelJudgeEditClass ui;
	QMap<int, QComboBox*>ItemMap;
	void InitTable();

	void ComboBoxSetItems(QComboBox* comboBox, QStringList* items);
	void TableWidgetAddComboBox(QTableWidget* tableWidget, int x, int y, QStringList items, int comboBoxIdx);
private slots:
	void on_TableWidgetItemChanged(QTableWidgetItem* item);
	void on_ComboboxCurrentIndexChanged(int index);
};
