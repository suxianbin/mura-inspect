#pragma once

#include <QWidget>
#include "ui_QWndSPME.h"
#include "SpmeParam.h"

class SpmeParam;

class QWndSPME : public QWidget
{
	Q_OBJECT

public:
	QWndSPME(QWidget *parent = nullptr);
	~QWndSPME();
	void InitTable();
	void loadConfig();
	void refreshSpmeParamListTable();
private:
	Ui::QWndSPMEClass ui;
private:
	void setStyleSheet();

	void refreshParam(SpmeParam* param);

	QVector<SpmeMeasPoint> getMajorTableData();
	void setMajorTableData(QVector<SpmeMeasPoint>&data);

	int m_currentRow = -1;
private slots:
	void on_pb_SpmePrmNewClicked();
	void on_pb_SpmePrmEditClicked();
	void on_pb_SpmePrmDeleteClicked();
	void on_pb_SpmePrmSearchClicked();
	void on_pb_ModifyBatchClicked();
	void on_pb_SpmePrmSaveClicked();
	void on_btn_RecipeSetClicked();
	void on_btn_AnalyzeSetClicked();
	void on_btn_StartReferenceClicked();
	void on_btn_StartODClicked();
	void on_btn_AnalyzeClicked();
	void on_btn_ODReferencePosClicked();
	void on_pb_ColorReferencePosClicked();
	void on_pb_AddMinorPointClicked();
	void on_pb_DeleteMinorPointClicked();
	void on_pb_SaveMinorPointClicked();
	void on_pb_AddMajorPointClicked();
	void on_pb_DeleteMajorPointClicked();
	void on_pb_SaveMajorPointClicked();
	void on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
};
