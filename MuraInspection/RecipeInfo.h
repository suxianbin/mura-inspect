#pragma once
#include "Common/Base.h"
#include "Common/EntityMgr.h"
#include <QDate>
#include <QTime>

class RecipeInfo : public Base
{
public:
	RecipeInfo(QString key);
	~RecipeInfo();
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;

	QString RecipeName;
	QString RecipeType;
	int ProcessId;
	QString MachineStepID;
	QString CreateTime;
	QString Remark;

	QString AlgorithmName;  //算法配方
	QString GlassLayoutName;//Layout配方
	QString JudgeInfoName;  //判级配方
	QString AxisInfoName;   //分光计配方
	QString LightInfoName;  //光源配方
	QString SpmeInfoName;   //分光计配方
	QString CameraInfoName; //分光计配方
};