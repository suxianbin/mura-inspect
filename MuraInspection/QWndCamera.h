#pragma once

#include <QWidget>
#include "ui_QWndCamera.h"

class CameraParam;

class QWndCamera : public QWidget
{
	Q_OBJECT

public:
	QWndCamera(QWidget *parent = nullptr);
	~QWndCamera();
	void loadConfig();
	void refreshCameraParamListTable();
	void setStyleSheet();

	void refreshParam(CameraParam* param);
private:
	Ui::QWndCameraClass ui;
private slots:
	void on_pb_ModifyBatchClicked();
	void on_pb_CameraPrmNewClicked();
	void on_pb_CameraPrmEditClicked();
	void on_pb_CameraPrmDeleteClicked();
	void on_pb_CameraPrmSearchClicked();
	void on_pb_CameraPrmSaveClicked();
	void on_ParamTableSeletedChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

};
