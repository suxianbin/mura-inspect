#include "CameraControl.h"
#include "DevLog.h"

CameraControl::CameraControl(QObject *parent)
	: QObject(parent)
{
	m_pSerialPort = new QSerialPort();

}

CameraControl::~CameraControl()
{}

void CameraControl::ConnectCamera()
{
	if (m_pSerialPort->isOpen())
	{
		m_pSerialPort->close();
	}
	m_pSerialPort->open(QIODevice::ReadWrite);
	m_pSerialPort->setPortName(m_portName);
	m_pSerialPort->setBaudRate(m_baudrate);
	m_pSerialPort->setDataBits(QSerialPort::Data8);
	m_pSerialPort->setStopBits(QSerialPort::OneStop);
	m_pSerialPort->setParity(QSerialPort::NoParity);

	if (m_pSerialPort->open(QIODevice::ReadWrite))
	{
	}
	else
	{
		glog << "CameraControl Serial Port Open Fail!PortName:" << m_portName << ",Baudrate:"<< m_baudrate << gendl;
	}
}

void CameraControl::setPortName(QString name)
{
	m_portName = name;

}

void CameraControl::setBaudrate(QSerialPort::BaudRate baud)
{
	m_baudrate = baud;
}

void CameraControl::setExposure(int expo)
{
}

void CameraControl::setUserFFT(int index)
{
}

void CameraControl::setFFTFile(QString filePath)
{
}

