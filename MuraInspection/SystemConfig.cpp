#include "SystemConfig.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>

SystemConfig::SystemConfig()
{
}

SystemConfig::~SystemConfig()
{
}

bool SystemConfig::writeToFile(const QString& path)
{
    QJsonObject json;
    json["FtpEnable"] = FtpEnable;
    json["FtpAddress"] = FtpAddress;
    json["FtpPort"] = FtpPort;
    json["FtpUserName"] = FtpUserName;
    json["FtpPassword"] = FtpPassword;
    json["FtpLocalSavePath"] = FtpLocalSavePath;
    json["FtpUploadPath"] = FtpUploadPath;
    json["AdcEnable"] = AdcEnable;
    json["AdcHttp"] = AdcHttp;
    json["TmsEnable"] = TmsEnable;
    json["TmsHttp"] = TmsHttp;
    json["Station1GlassLayoutChecked"] = Station1GlassLayoutChecked;
    json["Station1GlassPanelChecked"] = Station1GlassPanelChecked;
    json["Station1GlassPanelIDDisplay"] = Station1GlassPanelIDDisplay;
    json["Station1GlassDefectChecked"] = Station1GlassDefectChecked;
    json["Station2GlassLayoutChecked"] = Station2GlassLayoutChecked;
    json["Station2GlassPanelChecked"] = Station2GlassPanelChecked;
    json["Station2GlassPanelIDDisplay"] = Station2GlassPanelIDDisplay;
    json["Station2GlassDefectChecked"] = Station2GlassDefectChecked;

    QJsonDocument doc(json);
    QDir dir(path);
    if (!dir.exists())
    {
        dir.mkpath(path);
    }
    QFile file(path + QString::fromStdString("SystemConfig.json"));
    file.open(QFile::WriteOnly);
    file.write(doc.toJson());
    file.close();
    return true;
}

bool SystemConfig::loadFromFile(const QString& path)
{
    QFile file(path + QString::fromStdString("SystemConfig.json"));
    if (file.open(QFile::ReadOnly))
    {
        QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
        if (!doc.isNull())
        {
            QJsonObject json = doc.object();
            FtpEnable = json["FtpEnable"].toBool();
            FtpAddress = json["FtpAddress"].toString();
            FtpPort = json["FtpPort"].toInt();
            FtpUserName = json["FtpUserName"].toString();
            FtpPassword = json["FtpPassword"].toString();
            FtpLocalSavePath = json["FtpLocalSavePath"].toString();
            FtpUploadPath = json["FtpUploadPath"].toString();
            AdcEnable = json["AdcEnable"].toBool();
            AdcHttp = json["AdcHttp"].toString();
            TmsEnable = json["TmsEnable"].toBool();
            TmsHttp = json["TmsHttp"].toString();
            Station1GlassLayoutChecked = json["Station1GlassLayoutChecked"].toBool();
            Station1GlassPanelChecked = json["Station1GlassPanelChecked"].toBool();
            Station1GlassPanelIDDisplay = json["Station1GlassPanelIDDisplay"].toBool();
            Station1GlassDefectChecked = json["Station1GlassDefectChecked"].toBool();
            Station2GlassLayoutChecked = json["Station2GlassLayoutChecked"].toBool();
            Station2GlassPanelChecked = json["Station2GlassPanelChecked"].toBool();
            Station2GlassPanelIDDisplay = json["Station2GlassPanelIDDisplay"].toBool();
            Station2GlassDefectChecked = json["Station2GlassDefectChecked"].toBool();
            return true;
        }
    }
    return false;
}
