#include "QWndSystemSet.h"

QWndSystemSet::QWndSystemSet(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	//初始化界面布局
	m_pMainDispFrameLayout = new QVBoxLayout(0);
	m_pMainDispFrameLayout->setMargin(0);
	m_pFTPPage = new QWndFtpClient();
	ui.frame->setLayout(m_pMainDispFrameLayout);
	m_pMainDispFrameLayout->addWidget(m_pFTPPage);
	m_pSoftCofigPage = new QWidget();
	m_pMainDispFrameLayout->addWidget(m_pSoftCofigPage);

	setAllButtonDefaultStyleSheet();
	hiddenAllWidget();
	setButtonStyleSheet(ui.pb_FTP);
	m_pFTPPage->setHidden(false);

	connect(ui.pb_FTP, SIGNAL(clicked()), this, SLOT(on_pb_FTPClicked()));
}

QWndSystemSet::~QWndSystemSet()
{}

void QWndSystemSet::hiddenAllWidget()
{
	m_pFTPPage->setHidden(true);
	m_pSoftCofigPage->setHidden(true);
}

void QWndSystemSet::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(10,45,121);color:rgb(221, 221, 221);border-radius:3px;border:0px;}\
					QPushButton:pressed{ background-color:rgb(13,55,147); }\
					QPushButton:hover{ background-color:rgb(87, 153, 239);}";

	ui.pb_FTP->setStyleSheet(style);
	ui.pb_SoftConfig->setStyleSheet(style);
}

void QWndSystemSet::setButtonStyleSheet(QPushButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(87, 153, 239);border-radius:3px;border:0px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}

void QWndSystemSet::on_pb_FTPClicked()
{
	setButtonStyleSheet(ui.pb_FTP);
	hiddenAllWidget();
	m_pFTPPage->setHidden(false);
}
