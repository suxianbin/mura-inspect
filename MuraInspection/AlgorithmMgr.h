#pragma once
#include "EntityMgr.h"
#include "AlgorithmParam.h"

class AlgorithmMgr : public EntityMgr
{
	//�����ṹ
	AlgorithmMgr();
	~AlgorithmMgr();
public:
	static AlgorithmMgr* Instance()
	{
		static AlgorithmMgr instance;
		return &instance;
	}
	bool Load(std::string name) override;
	bool Save(std::string name) override;

};

