#pragma once
#include "LightBase.h"
#include "OPTController.h"
#include "OPTErrorCode.h"

class OPTLight : public LightBase
{
public:
	OPTLight();
	~OPTLight();
	void SetIntensity(int ch, int intensity) override;
	void TurnOff(int ch) override;
private:
	OPTController_Handle* m_OPTControllerHanlde1;//��Դ1

	OPTController_Handle* m_OPTControllerHanlde2;
};

