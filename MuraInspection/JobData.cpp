﻿#include "JobData.h"
#include <QByteArray>
#pragma execution_character_set("UTF-8")

bool bytes2UShort(const QByteArray& arrData, int position, quint16& res)
{
	res = 0;
	bool f = false;
	if (arrData.size() > position + 1)
	{
		QByteArray ba = arrData.mid(position + 1, 1).toHex();
		int c = QString(ba).toInt(&f, 16);
		if (f)
		{
			c = c << 8;
			res += c;
			ba = arrData.mid(position + 0, 1).toHex();
			c = QString(ba).toInt(&f, 16);
			if (f)
			{
				res += c;
			}
		}
		return f;
	}
	return f;
}

QByteArray QString2Bytes(const QString& str, int len)
{
	QByteArray res;
	res.append(str.toUtf8().left(len));
	for (; res.size() < len;) 
	{
		res.append((char)0x00);
	}
	return res;
}

QByteArray UShort2Bytes(quint16 i)
{
	QByteArray bytes;
	char cs[2];
	cs[0] = i & 0xFF;
	cs[1] = (i >> 8) & 0xFF;

	bytes.append(cs[0]);
	bytes.append(cs[1]);

	return bytes;
}

void intToByte(const int& number, QByteArray& disData)
{
	QByteArray abyte0;
	abyte0.resize(4);
	abyte0[0] = (uchar)(0x000000ff & number);
	abyte0[1] = (uchar)((0x0000ff00 & number) >> 8);
	abyte0[2] = (uchar)((0x00ff0000 & number) >> 16);
	abyte0[3] = (uchar)((0xff000000 & number) >> 24);
	disData = abyte0;
}

#ifdef MURA_BOE_B12
void JobData::Array2JobData(const QByteArray& arrData, JobData& jobData)
{
	bool f = true;
	int p = 0;
	jobData.PRODID = arrData.mid(p, 10);
	p += 20;
	jobData.OperID = arrData.mid(p, 5);
	p += 10;
	jobData.LotID = arrData.mid(p, 10);
	p += 20;
	jobData.PPID01_1 = arrData.mid(p, 10);
	p += 104;
	f = f & bytes2UShort(arrData, p, jobData.JobType);
	p += 2;
	jobData.JobID = arrData.mid(p, 10);
	p += 20;
	f = f & bytes2UShort(arrData, p, jobData.LotSequenceNumber);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.SlotSequenceNumber);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.PropertyCode);
	p += 2;
	jobData.JobJudgeCode = arrData.mid(p, 2);
	p += 2;
	jobData.JobGradeCode = arrData.mid(p, 2);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.SubstrateType);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.ProcessingFlag);
	p += 8;
	f = f & bytes2UShort(arrData, p, jobData.InspectionFlag);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.SkipFlag);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.JobSize);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.GlassThickness);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.JobAngle);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.JobFlip);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.CuttingGlassType);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.InspectionJudgeData);
	p += 6;
	f = f & bytes2UShort(arrData, p, jobData.PairLotSequenceNumber);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.PairSlotSequenceNumber);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.PortNumber);
	f = f & bytes2UShort(arrData, p, jobData.InputMode);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.EVRouteRecipe);
	p += 2;
	f = f & bytes2UShort(arrData, p, jobData.ENRoutePathInfo);
	p += 4;
	f = f & bytes2UShort(arrData, p, jobData.JobChangeflag);
	p += 2;
	jobData.OptionValue = arrData.mid(p, 10);
	p += 20;
	jobData.Reserved = arrData.mid(p, 1);
	p += 2;
}

void JobData::JobData2Array(const JobData& jobData, QByteArray& arrData)
{
	arrData.append(QString2Bytes(jobData.PRODID, 20));
	arrData.append(QString2Bytes(jobData.OperID, 10));
	arrData.append(QString2Bytes(jobData.LotID, 20));
	arrData.append(QString2Bytes(jobData.PPID01_1, 104));
	arrData.append(UShort2Bytes(jobData.JobType));
	arrData.append(QString2Bytes(jobData.JobID, 20));
	arrData.append(UShort2Bytes(jobData.LotSequenceNumber));
	arrData.append(UShort2Bytes(jobData.SlotSequenceNumber));
	arrData.append(UShort2Bytes(jobData.PropertyCode));
	arrData.append(QString2Bytes(jobData.JobJudgeCode, 2));
	arrData.append(QString2Bytes(jobData.JobGradeCode, 2));
	arrData.append(UShort2Bytes(jobData.SubstrateType));
	arrData.append(UShort2Bytes(jobData.ProcessingFlag));
	arrData.append(UShort2Bytes(0));
	arrData.append(UShort2Bytes(0));
	arrData.append(UShort2Bytes(0));
	arrData.append(UShort2Bytes(jobData.InspectionFlag));
	arrData.append(UShort2Bytes(jobData.SkipFlag));
	arrData.append(UShort2Bytes(jobData.JobSize));
	arrData.append(UShort2Bytes(jobData.GlassThickness));
	arrData.append(UShort2Bytes(jobData.JobAngle));
	arrData.append(UShort2Bytes(jobData.JobFlip));
	arrData.append(UShort2Bytes(jobData.CuttingGlassType));
	arrData.append(UShort2Bytes(jobData.InspectionJudgeData));
	arrData.append(UShort2Bytes(0));
	arrData.append(UShort2Bytes(0));
	arrData.append(UShort2Bytes(jobData.PairLotSequenceNumber));
	arrData.append(UShort2Bytes(jobData.PairSlotSequenceNumber));
	arrData.append(UShort2Bytes(jobData.PortNumber));
	arrData.append(UShort2Bytes(jobData.InputMode));
	arrData.append(UShort2Bytes(jobData.EVRouteRecipe));
	arrData.append(UShort2Bytes(jobData.ENRoutePathInfo));
	arrData.append(UShort2Bytes(jobData.JobChangeflag));
	arrData.append(QString2Bytes(jobData.OptionValue, 20));
}
#endif // MURA_BOE_B12

