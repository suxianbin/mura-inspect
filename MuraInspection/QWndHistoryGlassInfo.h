#pragma once

#include <QWidget>
#include "ui_QWndHistoryGlassInfo.h"

class QWndHistoryGlassInfo : public QWidget
{
	Q_OBJECT

public:
	QWndHistoryGlassInfo(QWidget *parent = nullptr);
	~QWndHistoryGlassInfo();

private:
	Ui::QWndHistoryGlassInfoClass ui;
};
