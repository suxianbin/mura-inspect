#pragma once

#include <QWidget>
#include "ui_QWndDefectDeputyImage.h"

class QWndDefectDeputyImage : public QWidget
{
	Q_OBJECT

public:
	QWndDefectDeputyImage(QWidget *parent = nullptr);
	~QWndDefectDeputyImage();
	void updateDeputyImage();
private:
	Ui::QWndDefectDeputyImageClass ui;
};
