#pragma once
#include "Base.h"

class LightParam : public Base
{
public:
	LightParam(QString key);
	virtual ~LightParam();
	void write(QJsonObject& json)const override;
	void read(const QJsonObject& json) override;

	QString ParamName;
	QString Remark;
	QString CreateTime;



	//光源控制器1
	bool Chanel1Enable = true;
	int Chanel1Intensity = 120;

	bool Chanel2Enable = true;
	int Chanel2Intensity = 120;

	bool Chanel3Enable = true;
	int Chanel3Intensity = 120;

	bool Chanel4Enable = true;
	int Chanel4Intensity = 120;

	//光源控制器2
	bool Chanel5Enable = true;
	int Chanel5Intensity = 120;

	bool Chanel6Enable = true;
	int Chanel6Intensity = 120;

	bool Chanel7Enable = true;
	int Chanel7Intensity = 120;

	bool Chanel8Enable = true;
	int Chanel8Intensity = 120;
};

