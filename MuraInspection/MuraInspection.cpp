#pragma execution_character_set("utf-8")
#include "MuraInspection.h"
#include "QWndMessageBox.h"
#include <QProcess>
#include "HardwareConfig.h"
#include "RecipeMgr.h"
#include "RecipeInfo.h"
#include "CameraParamMgr.h"
#include "LightParamMgr.h"
#include "SpmeParamMgr.h"
#include "PanelModelMgr.h"
#include "GlassLayoutMgr.h"
#include "McLoop.h"
#include "SystemConfig.h"
#include "AlgorithmParam.h"
#include "AlgorithmMgr.h"

#define Version GetReleaseTime(__DATE__,__TIME__)

MuraInspection::MuraInspection(QWidget *parent)
    : QWidget(parent)
{
	m_splash = SplashScreen::getInstance();
	m_splash->show();
	m_splash->setStagePercent(5, "读取硬件配置文件");
	LoadHardwareConfig();
	m_splash->setStagePercent(15, "加载配方文件");
	LoadRecipe();
	m_splash->setStagePercent(35, "启动PLC逻辑");
	StartPLCLogic();
	m_splash->setStagePercent(50, "加载界面中...");
	QThread::msleep(300);

	m_splash->setStagePercent(70, "初始化硬件资源");
	QThread::msleep(300);

    ui.setupUi(this);
	GetSoftVersion();

	ui.pb_Spme->setVisible(false);
	this->setWindowIcon(QIcon(":/Icons/icon.png"));
    this->setWindowFlags(Qt::FramelessWindowHint);
	QString logo = "#frame{border-image:url(:/Icons/zkhy.png);background-color:rgb(13,55,147);}";
	ui.frame->setObjectName("frame");
	ui.frame->setStyleSheet(logo);

	QString btn_style = "color:rgb(221, 221, 221);background-color:rgb(13,55,147);border-radius:0px;border:0px;background-image:url(:/Icons/zkhy.png);}";
	ui.pushButton->setStyleSheet(btn_style);
	//ui.label_2->setText(QWndSystemParam::Instance()->SystemParam.Project);
	InitWindowHeader();
	InitSignalSlot();
	InitWindow();

	m_splash->close();
}

MuraInspection::~MuraInspection()
{}

void MuraInspection::GetSoftVersion()
{
	this->setWindowTitle(QString::fromStdString(Version));
	this->ui.label_Version->setText(QString::fromStdString(Version));
}

std::string MuraInspection::GetReleaseTime(const char* c_date, const char* c_time)
{
	std::string BOE_VERSION = "";

	#ifdef MURA_BOE_B12
		BOE_VERSION = "12 ";
	#elif MURA_BOE_B7
		BOE_VERSION = "7 ";
	#elif MURA_BOE_B17
		BOE_VERSION = "17 ";
	#elif MURA_BOE_B20
		BOE_VERSION = "20 ";
	#endif // MURA_BOE_B

	//编译时间拼接
	std::string dateStr(c_date);
	std::string timeStr(c_time);
	std::string concatenatedString = dateStr + " " + timeStr;
	const char* concatenatedChar = concatenatedString.c_str();

	std::tm tm;
	std::istringstream timestampStream(concatenatedChar);
	timestampStream >> std::get_time(&tm, "%b %d %Y %H:%M:%S");

	int month = tm.tm_mon + 1;	//月份是0~11
	int day = tm.tm_mday;
	int year = tm.tm_year % 100;	//年份取末两位
	int hour = tm.tm_hour;
	int min = tm.tm_min;
	int sec = tm.tm_sec;


	std::ostringstream ss;
	ss << "   Mura" << BOE_VERSION /*<< std::setw(2) << std::setfill('0') << year*/ << "2."
		<< std::setw(2) << std::setfill('0') << month << "." << std::setw(2) << std::setfill('0') << day;
		//<< "-" << std::setw(2) << std::setfill('0') << hour
		//<< ":" << std::setw(2) << std::setfill('0') << min
		//<< ":" << std::setw(2) << std::setfill('0') << sec;
	std::string str = ss.str();
	return str;
}


void MuraInspection::LoadRecipe()
{
	if (!RecipeMgr::Instance()->Load("RecipeMgr.json"))
	{
		RecipeMgr::Instance()->Add(new RecipeInfo("11"));
		RecipeMgr::Instance()->Add(new RecipeInfo("22"));
		RecipeMgr::Instance()->Add(new RecipeInfo("33"));
		RecipeMgr::Instance()->Save("RecipeMgr.json");
	}

	if (!CameraParamMgr::Instance()->Load("CameraMgr.json"))
	{
		CameraParamMgr::Instance()->Add(new CameraParam("R"));
		CameraParamMgr::Instance()->Add(new CameraParam("G"));
		CameraParamMgr::Instance()->Add(new CameraParam("B"));
		CameraParamMgr::Instance()->Add(new CameraParam("BM"));
		CameraParamMgr::Instance()->Save("CameraMgr.json");
	}

	if (!LightParamMgr::Instance()->Load("LightMgr.json"))
	{
		LightParamMgr::Instance()->Add(new LightParam("R"));
		LightParamMgr::Instance()->Add(new LightParam("G"));
		LightParamMgr::Instance()->Add(new LightParam("B"));
		LightParamMgr::Instance()->Add(new LightParam("BM"));
		LightParamMgr::Instance()->Save("LightMgr.json");
	}

	if (!SpmeParamMgr::Instance()->Load("SpmeMgr.json"))
	{
		SpmeMeasPoint p1 = SpmeMeasPoint(30, 100);
		SpmeMeasPoint p2 = SpmeMeasPoint(30, 200);
		SpmeMeasPoint p3 = SpmeMeasPoint(30, 300);
		SpmeParam* param1 = new SpmeParam("R_VIS");
		param1->MajorPoints.push_back(p1);
		param1->MajorPoints.push_back(p2);
		param1->MajorPoints.push_back(p3);

		SpmeParam* param2 = new SpmeParam("B_VIS");
		param2->MajorPoints.push_back(p1);
		param2->MajorPoints.push_back(p2);
		param2->MajorPoints.push_back(p3);

		SpmeParam* param3 = new SpmeParam("G_VIS");
		param3->MajorPoints.push_back(p1);
		param3->MajorPoints.push_back(p2);
		param3->MajorPoints.push_back(p3);

		SpmeParam* param4 = new SpmeParam("Color");
		param4->MajorPoints.push_back(p1);
		param4->MajorPoints.push_back(p2);
		param4->MajorPoints.push_back(p3);

		SpmeParam* param5 = new SpmeParam("OD");
		param5->MajorPoints.push_back(p1);
		param5->MajorPoints.push_back(p2);
		param5->MajorPoints.push_back(p3);

		SpmeParamMgr::Instance()->Add(param1);
		SpmeParamMgr::Instance()->Add(param2);
		SpmeParamMgr::Instance()->Add(param3);
		SpmeParamMgr::Instance()->Add(param4);
		SpmeParamMgr::Instance()->Add(param5);
		SpmeParamMgr::Instance()->Save("SpmeMgr.json");
	}

	if (!PanelModelMgr::Instance()->Load("PanelModelMgr.json"))
	{
		PanelModelMgr::Instance()->Add(new PanelModel("B12_1"));
		PanelModelMgr::Instance()->Add(new PanelModel("B12_2"));
		PanelModelMgr::Instance()->Add(new PanelModel("B12_3"));
		PanelModelMgr::Instance()->Add(new PanelModel("B12_4"));
		PanelModelMgr::Instance()->Save("PanelModelMgr.json");
	}

	if (!GlassLayoutMgr::Instance()->Load("GlassLayoutMgr.json"))
	{
		PanelMask* mask = new PanelMask();
		mask->x = 10;
		mask->y = 10;
		mask->width = 100;
		mask->height = 200;

		PanelLayout* panel = new PanelLayout();
		panel->panelMaskList.push_back(*mask);
		panel->x = 0;
		panel->y = 0;
		panel->width = 100;
		panel->height = 180;

		GlassLayout* layout = new GlassLayout("B12_1");
		layout->PanelLists.push_back(*panel);
		layout->PanelLists.push_back(*panel);
		layout->PanelLists.push_back(*panel);

		GlassLayout* layout2 = new GlassLayout("B12_2");
		layout2->PanelLists.push_back(*panel);
		layout2->PanelLists.push_back(*panel);
		layout2->PanelLists.push_back(*panel);

		GlassLayout* layout3 = new GlassLayout("B12_3");
		layout3->PanelLists.push_back(*panel);
		layout3->PanelLists.push_back(*panel);
		layout3->PanelLists.push_back(*panel);

		GlassLayoutMgr::Instance()->Add(layout);
		GlassLayoutMgr::Instance()->Add(layout2);
		GlassLayoutMgr::Instance()->Add(layout3);
		GlassLayoutMgr::Instance()->Save("GlassLayoutMgr.json");
	}

	if (!AlgorithmMgr::Instance()->Load("AlgorithmMgr.json"))
	{
		AlgorithmMgr::Instance()->Add(new AlgorithmParam("11"));
		AlgorithmMgr::Instance()->Add(new AlgorithmParam("22"));
		AlgorithmMgr::Instance()->Add(new AlgorithmParam("33"));
		AlgorithmMgr::Instance()->Save("AlgorithmMgr.json");
	}
}

void MuraInspection::SaveRecipe()
{
	RecipeMgr::Instance()->Save("RecipeMgr.json");
	CameraParamMgr::Instance()->Save("CameraMgr.json");
	LightParamMgr::Instance()->Save("LightMgr.json");
	SpmeParamMgr::Instance()->Save("SpmeMgr.json");
	PanelModelMgr::Instance()->Save("PanelModelMgr.json");
}

void MuraInspection::LoadHardwareConfig()
{
	HardwareConfig::Instance()->loadFromFile("MuraSystem/");
	SystemConfig::Instance()->loadFromFile();
}

void MuraInspection::InitWindow()
{
	//初始化图标
	this->setWindowIcon(QIcon(":/img/frames.png"));

	QString close_style = "color:rgb(221, 221, 221);background-color:rgb(10,45,121);border-radius:5px;}\
					 QPushButton:pressed {background-color:rgb(225, 0, 0);}\
					 QPushButton:hover {background-color:rgb(225, 0, 0);}";
	QString min_style = "color:rgb(221, 221, 221);background-color:rgb(10,45,121);border-radius:5px;}\
					 QPushButton:pressed {background-color:rgb(0, 0, 225);}\
					 QPushButton:hover {background-color:rgb(0, 0, 225);}";

	QString login_style = "color:rgb(221, 221, 221);background-color:rgb(10,45,121);border-radius:5px;background-image:url(:/img/exit.png);background-repeat: no-repeat;}\
					 QPushButton:pressed {background-image:url(:/img/exit.png);}\
					 QPushButton:hover {background-image:url(:/img/exit_select.png);}}";

	ui.pb_close->setStyleSheet(close_style);
	ui.pb_min->setStyleSheet(min_style);
	setButtonStyleSheet(ui.pb_HomePage);

	//初始化界面布局
	m_pMainDispFrameLayout = new QVBoxLayout(0);
	m_pMainDispFrameLayout->setMargin(0);
	m_pHomePage = new QWndHomePage();
	m_pHardwarePage = new QWndHardware();
	m_pSystemSetting = new QWndSystemSet();
	m_pAlgrithmPage = new QWndAlgorithmDebug();
	ui.fm_mainDispFrame->setLayout(m_pMainDispFrameLayout);
	m_pMainDispFrameLayout->addWidget(m_pHomePage);
	m_pMainDispFrameLayout->addWidget(m_pHardwarePage);
	m_pLayoutPage = new QWndGlassLayout();
	m_pMainDispFrameLayout->addWidget(m_pLayoutPage);
	m_pRecipePage = new QWndRecipe();
	m_pMainDispFrameLayout->addWidget(m_pRecipePage);
	m_pUserManagePage = new UserManagerUi();
	m_pMainDispFrameLayout->addWidget(m_pUserManagePage);
	m_pMainDispFrameLayout->addWidget(m_pSpmePage);
	m_pMainDispFrameLayout->addWidget(m_pAlgrithmPage);
	m_pMainDispFrameLayout->addWidget(m_pSystemSetting);
	hiddenAllWidget();
	m_pHomePage->setHidden(false);

	connect(ui.pb_close, SIGNAL(clicked()), this, SLOT(pb_closeClickedSlot()));
	connect(ui.pb_min, SIGNAL(clicked()), this, SLOT(pb_minClickedSlot()));

	connect(m_pHomePage, SIGNAL(sigPanelJudgeEdit()), this, SLOT(slot_resultPanelJugdeEdit()));

	m_pHistoryData = new QWndHistoryData();

	connect(m_pAlgrithmPage, SIGNAL(sig_acqCompleted()), this, SLOT(on_sig_acqCompleted()));
}

void MuraInspection::InitWindowHeader()
{
	QString style = "background-color:rgb(10,45,121);color:rgb(221, 221, 221);border-radius:5px;border:0px;padding-top: 10px;padding-bottom: 5px;}\
					QToolButton:pressed{ background-color:rgb(13,55,147); }\
					QToolButton:hover{ background-color:rgb(87, 153, 239);}";
	ui.pb_HomePage->setStyleSheet(style);
	ui.pb_HardwareDebug->setStyleSheet(style);
	ui.pb_AlgParam->setStyleSheet(style);
	ui.pb_Recipe->setStyleSheet(style);
	ui.pb_Layout->setStyleSheet(style);
	ui.pb_UserMgr->setStyleSheet(style);
	ui.pb_System->setStyleSheet(style);
	ui.pb_HistoryData->setStyleSheet(style);

	ui.pb_HomePage->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_HomePage->setIcon(QIcon(":/Icons/home.png"));
	ui.pb_HardwareDebug->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_HardwareDebug->setIcon(QIcon(":/Icons/ic_homepage_top_camera.png"));
	ui.pb_AlgParam->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_AlgParam->setIcon(QIcon(":/Icons/ic_homepage_top_algorithm.png"));
	ui.pb_Recipe->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_Recipe->setIcon(QIcon(":/Icons/ic_homepage_top_formula.png"));
	ui.pb_Layout->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_Layout->setIcon(QIcon(":/Icons/ic_homepage_top_layout.png"));
	ui.pb_UserMgr->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_UserMgr->setIcon(QIcon(":/Icons/ic_homepage_top_usermanagement.png"));
	ui.pb_System->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_System->setIcon(QIcon(":/Icons/system.png"));
	ui.pb_HistoryData->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_HistoryData->setIcon(QIcon(":/Icons/ic_homepage_top_history.png"));
	ui.pb_Spme->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.pb_Spme->setIcon(QIcon(":/Icons/ic_homepage_top_analyze.png"));

	//设置分光计是否启用
	//ui.pb_Spme->setVisible(QWndSystemParam::Instance()->SystemParam.SPMEDisplay);
	//ui.lb_StatusSPME->setVisible(QWndSystemParam::Instance()->SystemParam.SPMEDisplay);
	//ui.label_6->setVisible(QWndSystemParam::Instance()->SystemParam.SPMEDisplay);
}

void MuraInspection::InitSignalSlot()
{
	connect(ui.pb_HomePage, SIGNAL(clicked()), this, SLOT(pb_HomePageClickedSlot()));
	connect(ui.pb_HardwareDebug, SIGNAL(clicked()), this, SLOT(pb_HardwareDebugClickedSlot()));
	connect(ui.pb_AlgParam, SIGNAL(clicked()), this, SLOT(pb_AlgParamClickedSlot()));
	connect(ui.pb_Recipe, SIGNAL(clicked()), this, SLOT(pb_RecipeClickedSlot()));
	connect(ui.pb_Layout, SIGNAL(clicked()), this, SLOT(pb_LayoutClickedSlot()));
	connect(ui.pb_UserMgr, SIGNAL(clicked()), this, SLOT(pb_UserMgrClickedSlot()));
	connect(ui.pb_System, SIGNAL(clicked()), this, SLOT(pb_SystemClickedSlot()));
	connect(ui.pb_HistoryData, SIGNAL(clicked()), this, SLOT(pb_HistoryDataClickedSlot()));
	connect(ui.pb_Spme, SIGNAL(clicked()), this, SLOT(pb_SpmeClickedSlot()));
}

void MuraInspection::InitMenu()
{
	m_mainMenu = new MyMenu(0);
	//if (m_operation)
	//{
	//	QAction* action = m_operation->getAction();
	//	m_mainMenu->addMyAction(*action, "set");
	//}
	//m_mainMenu->addCustomMenu("login", ":/img/login.png", u8"用户登录");
	//m_mainMenu->addCustomMenu("userManage", ":/img/user.png", u8"用户管理");
	//m_mainMenu->addCustomMenu("logout", ":/img/logout.png", u8"用户登出");

	//connect(ui.pushButton, SIGNAL(clicked()), this, SLOT(on_pushButtonClicked()));

}

void MuraInspection::StartPLCLogic()
{
	McLoop::Instance()->start();
}

void MuraInspection::StopPLCLogic()
{
	McLoop::Instance()->terminate();
}

void MuraInspection::InitHardware()
{
}

void MuraInspection::pb_closeClickedSlot()
{
	this->close();
}

void MuraInspection::pb_minClickedSlot()
{
	showMinimized();
}

void MuraInspection::pb_HomePageClickedSlot()
{
	setButtonStyleSheet(ui.pb_HomePage);
	hiddenAllWidget();
	m_pHomePage->setHidden(false);
}

void MuraInspection::pb_HardwareDebugClickedSlot()
{
	setButtonStyleSheet(ui.pb_HardwareDebug);
	hiddenAllWidget();
	m_pHardwarePage->setHidden(false);
}

void MuraInspection::pb_AlgParamClickedSlot()
{
	setButtonStyleSheet(ui.pb_AlgParam);
	hiddenAllWidget();
	if (m_pAlgrithmPage)
	{
		m_pAlgrithmPage->setHidden(false);
	}
}

void MuraInspection::pb_RecipeClickedSlot()
{
	setButtonStyleSheet(ui.pb_Recipe);
	hiddenAllWidget();
	m_pRecipePage->setHidden(false);

}

void MuraInspection::pb_LayoutClickedSlot()
{
	setButtonStyleSheet(ui.pb_Layout);
	hiddenAllWidget();
	m_pLayoutPage->setHidden(false);
}

void MuraInspection::pb_UserMgrClickedSlot()
{
	setButtonStyleSheet(ui.pb_UserMgr);
	hiddenAllWidget();
	m_pUserManagePage->setHidden(false);
}

void MuraInspection::pb_SystemClickedSlot()
{
	setButtonStyleSheet(ui.pb_System);
	hiddenAllWidget();
	if (m_pSystemSetting)
	{
		m_pSystemSetting->setHidden(false);
	}
}

void MuraInspection::pb_SpmeClickedSlot()
{
	setButtonStyleSheet(ui.pb_Spme);
	hiddenAllWidget();
	if (m_pSpmePage)
	{
		m_pSpmePage->setHidden(false);
	}

}

void MuraInspection::pb_HistoryDataClickedSlot()
{
	//auto pro = new QProcess(this);
	//pro->start("MuraDataHistory.exe",QStringList());
	m_pHistoryData->showMaximized();
	//setButtonStyleSheet(ui.pb_HistoryData);
}

void MuraInspection::slot_runStepChanged(int station,int step)
{

}

void MuraInspection::slot_currentRecipeChanged()
{
}

void MuraInspection::slot_resultPanelJugdeEdit()
{

}

void MuraInspection::on_Timertimeout()
{
	QString onlineStyle = "background-color:rgba(149, 211, 190, 1);color:rgb(149, 211, 190);border-radius:10px;";
	QString offlineStyle = "background-color:rgba(219, 152, 155, 1);color:rgb(219, 152, 155);border-radius:10px;";
	//QString style = m_deviceControl->IsPlcConnected() ? onlineStyle : offlineStyle;
	//ui.lb_StatusPLC->setStyleSheet(style);
	//style = m_deviceControl->IsAoiConnected() ? onlineStyle : offlineStyle;
	//ui.lb_StatusAOI->setStyleSheet(style);
	//style = m_deviceControl->IsSpmeConnected() ? onlineStyle : offlineStyle;
	//ui.lb_StatusSPME->setStyleSheet(style);
}

void MuraInspection::on_autoTestTimerTimeout()
{
	//m_pHomePage->on_pb_ReviewClicked();

}

void MuraInspection::on_pushButtonClicked()
{
	m_mainMenu->exec(QCursor::pos());
}

void MuraInspection::on_sig_acqCompleted()
{
	m_pHomePage->RefreshWidget();
}

void MuraInspection::closeEvent(QCloseEvent * event)
{
	QWndMessageBox* messageBox = new QWndMessageBox("您确定要关闭当前软件吗?");
	int ret = messageBox->exec();
	if (ret == QDialog::Accepted)
	{
		StopPLCLogic();
		QThread::msleep(200);
		//SaveRecipe();
	}
	else
	{
		event->ignore();
	}
}

void MuraInspection::hiddenAllWidget()
{
	m_pHomePage->setHidden(true);
	if (m_pLayoutPage)
	{
		m_pLayoutPage->setHidden(true);
	}
	if (m_pRecipePage)
	{
		m_pRecipePage->setHidden(true);
	}
	if (m_pUserManagePage)
	{
		m_pUserManagePage->setHidden(true);
	}
	if (m_pSpmePage)
	{
		m_pSpmePage->setHidden(true);
	}
	if (m_pAlgrithmPage)
	{
		m_pAlgrithmPage->setHidden(true);
	}
	if (m_pSystemSetting)
	{
		m_pSystemSetting->setHidden(true);
	}
	if (m_pHardwarePage)
	{
		m_pHardwarePage->setHidden(true);
	}
}

void MuraInspection::setAllButtonDefaultStyleSheet()
{
	QString style = "background-color:rgb(10,45,121);color:rgb(221, 221, 221);border-radius:5px;border:0px;padding-top: 10px;padding-bottom: 5px;}\
					QToolButton:pressed{ background-color:rgb(13,55,147); }\
					QToolButton:hover{ background-color:rgb(87, 153, 239);}";

	ui.pb_HomePage->setStyleSheet(style);
	ui.pb_HardwareDebug->setStyleSheet(style);
	ui.pb_AlgParam->setStyleSheet(style);
	ui.pb_Layout->setStyleSheet(style);
	ui.pb_Recipe->setStyleSheet(style);
	ui.pb_UserMgr->setStyleSheet(style);
	ui.pb_System->setStyleSheet(style);
	ui.pb_HistoryData->setStyleSheet(style);
	ui.pb_Spme->setStyleSheet(style);
}

void MuraInspection::setButtonStyleSheet(QToolButton* button)
{
	QString style = "color:rgb(221, 221, 221);background-color:rgb(87, 153, 239);border-radius:5px;border:0px;padding-top: 10px;padding-bottom: 5px;}\
					 QPushButton:pressed {background-color:rgb(50, 49, 49);}\
					 QPushButton:hover {background-color:rgb(87, 153, 239);}";
	setAllButtonDefaultStyleSheet();
	button->setStyleSheet(style);
}
