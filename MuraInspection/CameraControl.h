#pragma once

#include <QObject>
#include <QSerialPort>

class CameraControl  : public QObject
{
	Q_OBJECT

public:
	CameraControl(QObject *parent);
	~CameraControl();
	void ConnectCamera();
	void setPortName(QString name);
	void setBaudrate(QSerialPort::BaudRate baud);

	void setExposure(int expo);
	void setUserFFT(int index);
	void setFFTFile(QString filePath);


private:
	QSerialPort* m_pSerialPort;
	QString m_portName;
	QSerialPort::BaudRate m_baudrate = QSerialPort::BaudRate::Baud9600;
};
