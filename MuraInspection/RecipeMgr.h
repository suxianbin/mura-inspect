#pragma once
#include "Common/EntityMgr.h"
#include <map>

class RecipeMgr : public EntityMgr
{
	//�����ṹ
	RecipeMgr();
	~RecipeMgr();
public:
	static RecipeMgr* Instance()
	{
		static RecipeMgr instance;
		return &instance;
	}
	bool Load(std::string name) override;
	bool Save(std::string name) override;

	std::map<int, QString>BindingMap;
	QString CurrentId;
	QString CurrentName;

};
