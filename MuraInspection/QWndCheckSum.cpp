#include "QWndCheckSum.h"
#include <QSettings>
#include <algorithm>

QWndCheckSum::QWndCheckSum(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ReadParam();
	InitTable();


}

QWndCheckSum::~QWndCheckSum()
{}

void QWndCheckSum::updateCheckSum()
{
	//m_LevelStatistical.clear();
	//for (int i = 0; i < ui.tableWidget->rowCount(); i++)
	//{
	//	ui.tableWidget->removeRow(0);
	//}

	//m_LevelStatistical = recipePlug->getCurrentRecipeLevelStaistical();
	////汇总检测数据
	//QVector<PanelRlt> panelRlts = _data.panelRlts;
	//for (const auto& panelrlt : panelRlts)
	//{
	//	for (const auto& defects : panelrlt.defects)
	//	{
	//		m_LevelStatistical.incrementCValue(defects.defect.type, defects.levelLabel);
	//		if (defects.isCommonDefect)
	//		{
	//			m_LevelStatistical.incrementCValue("Common", defects.levelLabel);
	//		}
	//	}
	//}
	//ui.tableWidget->setRowCount(m_LevelStatistical.size());
	//for (int i = 0; i < m_LevelStatistical.size(); i++)
	//{
	//	int j = 0;
	//	ui.tableWidget->setRowHeight(i, 35);
	//	QTableWidgetItem* item1 = new QTableWidgetItem(m_LevelStatistical[i].first);
	//	item1->setTextAlignment(Qt::AlignCenter);
	//	ui.tableWidget->setItem(i, 0, item1);
	//	for (auto var : m_LevelStatistical[i].second)
	//	{
	//		QTableWidgetItem* item = new QTableWidgetItem(QString::number(var.second));
	//		item->setTextAlignment(Qt::AlignCenter);
	//		ui.tableWidget->setItem(i, j + 1, item);
	//	
	//		++j;
	//	}
	//}


}

void QWndCheckSum::InitTable()
{
	ui.tableWidget->clear();
	DefectLevels.append("TOTAL");
	DefectTypeList.append("TOTAL");
	QStringList headers = DefectLevels;
	ui.tableWidget->setColumnCount(headers.size());
	ui.tableWidget->setRowCount(DefectTypeList.size());
	ui.tableWidget->setHorizontalHeaderLabels(headers);
	ui.tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);	//禁止编辑
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);			//最后一列自动拉伸
	ui.tableWidget->setAlternatingRowColors(true); // 隔行变色
	ui.tableWidget->setPalette(QPalette(Qt::gray)); // 设置隔行变色的颜色  gray灰色
	ui.tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
	ui.tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);//选中单行
	//ui.tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.tableWidget->verticalHeader()->hide();
	//ui.tableWidget->setShowGrid(false);
	ui.tableWidget->setShowGrid(true);
	for (int i = 0; i < ui.tableWidget->columnCount(); i++)
	{
		ui.tableWidget->setColumnWidth(i, 90);
	}
	QFont f;
	f.setPointSizeF(10);
	f.setFamily(u8"微软雅黑");
	for (int i = 0; i < ui.tableWidget->rowCount(); i++)
	{
		ui.tableWidget->setRowHeight(i, 35);
		QTableWidgetItem* item = new QTableWidgetItem(DefectTypeList[i]);
		item->setTextAlignment(Qt::AlignCenter);
		item->setFont(f);
		ui.tableWidget->setItem(i, 0, item);
	}

	QString style = "QTableWidget{background-color: rgb(230, 230, 230);alternate-background-color: rgb(180, 180, 180);gridline-color: rgb(80, 80, 80); gridline-width: 2px;}\
	 QHeaderView{\
	 color: rgb(130, 130, 130);\
	 font: 12pt;\
	 background-color: rgb(150, 150, 150);\
	 border: 0px solid rgb(144, 144, 144);\
	 border:0px solid rgb(191,191,191);\
	 border-left-color: rgba(255, 255, 255, 0);\
	 border-top-color: rgba(255, 255, 255, 0);\
	 border-radius:0px;\
	 min-height:50px;\
	 }";
	ui.tableWidget->setStyleSheet(style);
}

void QWndCheckSum::ReadParam()
{
	QString path = QCoreApplication::applicationDirPath() + "/MuraSystem/RecipePlug.ini";
	QSettings* setting = new QSettings(path, QSettings::IniFormat);
	setting->setIniCodec("UTF-8");

	DefectLevels.clear();
	DefectTypeList.clear();

	setting->beginGroup("DefectTypeList");
	QList<int> keys;
	// 将keys从字符串转换为整数并进行排序
	for (const QString& key : setting->childKeys()) {
		keys.append(key.toInt());
	}
	std::sort(keys.begin(), keys.end());
	// 使用排序后的整数作为key获取对应的值
	for (int key : keys) {
		DefectTypeList.append(setting->value(QString::number(key)).toString());
	}
	setting->endGroup();


	setting->beginGroup("DefectLevels");
	QList<int> keys_levels;
	// 将keys从字符串转换为整数并进行排序
	for (const QString& key : setting->childKeys()) 
	{
		keys_levels.append(key.toInt());
	}
	std::sort(keys_levels.begin(), keys_levels.end());
	// 使用排序后的整数作为key获取对应的值
	DefectLevels.append(u8"类 型");
	for (int key : keys_levels)
	{
		DefectLevels.append(setting->value(QString::number(key)).toString());
	}
	setting->endGroup();

	delete setting;
}
