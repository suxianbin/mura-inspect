#pragma once
#include <QString>

class SystemConfig
{
private:
	SystemConfig();
	~SystemConfig();
public:
	static SystemConfig* Instance()
	{
		static SystemConfig config;
		return &config;
	}
	//Ftp参数
	bool FtpEnable = true; //是否启用
	QString FtpAddress = "192.168.110.55";
	int	FtpPort = 21;
	QString	FtpUserName = "";
	QString FtpPassword = "";
	QString FtpLocalSavePath = "";
	QString	FtpUploadPath = "";

	//ADC参数
	bool AdcEnable = true; //是否启用
	QString AdcHttp = "192.168.110.55";

	//TMS参数
	bool TmsEnable = true; //是否启用
	QString TmsHttp = "192.168.110.55";

	//历史数据参数
	int DataKeepDays = 60;

	//主界面
	bool Station1GlassLayoutChecked = false;
	bool Station1GlassPanelChecked = false;
	bool Station1GlassPanelIDDisplay = false;
	bool Station1GlassDefectChecked = false;
	bool Station2GlassLayoutChecked = false;
	bool Station2GlassPanelChecked = false;
	bool Station2GlassPanelIDDisplay = false;
	bool Station2GlassDefectChecked = false;

	bool writeToFile(const QString& path = ".\\MuraSystem\\");
	bool loadFromFile(const QString& path = ".\\MuraSystem\\");
};

