#pragma once
#ifndef _IMAGE_UTIL_H_
#define _IMAGE_UTIL_H_

#include <QImage>
#include <qpixmap.h>
#include <QPen>
#include <opencv2/opencv.hpp>

class ImageUtil
{
public:

	/********************************
	 * 功能 转换Mat为QImage
	 * 返回
	 ********************************/
	static QImage cvMat2QImage(const cv::Mat&);

	/********************************
	 * 功能 mat转PixMap
	 * 输入 _mat
	 * 返回
	 ********************************/
	static QPixmap mat2QPixmap(const cv::Mat& _mat);

	/********************************
	 * 功能 PixMap转Mat
	 * 输入 Pixmap
	 * 返回
	 ********************************/
	static cv::Mat qPixmap2Mat(const QPixmap& _pixmap);

	/********************************
	 * 功能 根据path和笔宽计算shape 仅计算路径
	 * 输入 path 
	 * 输入 pen 3
	 * 返回 
	 ********************************/
	static QPainterPath shapeFromPath(const QPainterPath &path, const QPen &pen);


	/********************************
	 * 功能 根据path和笔宽计算shape 还包括闭合路径内的区域
	 * 输入 path 
	 * 输入 pen 
	 * 返回 
	 ********************************/
	static QPainterPath shapeFromPathContentsInclude(const QPainterPath& path, const QPen& pen);
};

#endif