#include "CalculateUtil.h"


bool CalculateUtil::QcvROIIsOk(cv::Mat& image, cv::Rect ROI)
{
	if (ROI.x < 0 || ROI.y < 0 || ROI.width < 0 || ROI.height < 0) {
		return false;
	}
	if (ROI.x > image.cols) {
		return false;
	}
	if (ROI.y > image.rows) {
		return false;
	}
	if (ROI.x + ROI.width > image.cols) {
		return false;
	}
	if (ROI.y + ROI.height > image.rows) {
		return false;
	}
	return true;
}

float CalculateUtil::calculatePt2LineDistance(cv::Point2f pt, cv::Point2f pt1, cv::Point2f pt2)
{
	return abs(((pt.x - pt1.x)*(pt2.y - pt1.y) - (pt2.x - pt1.x)*(pt.y - pt1.y)) / sqrt((pt2.x - pt1.x)*(pt2.x - pt1.x) + (pt2.y - pt1.y)*(pt2.y - pt1.y)));
}

int CalculateUtil::judgeDirection(const cv::Point2f& p0, const cv::Point2f& p1, const cv::Point2f& p2)
{
	cv::Vec3f H1 = cv::Vec3f(p1.x - p0.x, p1.y - p0.y, 0.0);
	cv::Vec3f H2 = cv::Vec3f(p2.x - p0.x, p2.y - p0.y, 0.0);
	float crossHz = H1[0] * H2[1] - H1[1] * H2[0];
	if (0 >= crossHz) {
		return 0;   //逆时针旋转角度小于PI
	}
	else {
		return 1;   //逆时针旋转角度大于PI
	}
}

float CalculateUtil::radian2Angle(float data) { return data * 180.0 / PI; } //弧度转角度

float CalculateUtil::getSpanAngle(const float& start, const float& end)
{
	float spanAngle = end - start;
	if (spanAngle < 0) {
		spanAngle += 2 * PI;
	}
	return radian2Angle(spanAngle);
}

float CalculateUtil::getPtAngleBaseCircleCenter(const cv::Point2f& center, const cv::Point2f& pt)                     
{
	double radian = atan2(pt.y - center.y, pt.x - center.x);
	if (radian < 0) {
		radian += 2 * PI;
	}
	return 	radian;
}

qreal CalculateUtil::calcSuitableScal(const QSizeF& imageSize, const QSize& viewSize)
{
	if(imageSize.width()==0|| imageSize.height()==0) {
		return 1;
	}
	qreal pixW = imageSize.width();
	qreal pixH = imageSize.height();
	qreal xScal = viewSize.width() / pixW;
	qreal yScal = viewSize.height()/ pixH;
	if (xScal > 1 && yScal > 1) {
		return qMax(xScal, yScal);
	}
	return qMin(xScal , yScal);
	
}

QRectF CalculateUtil::getAllowRect(const QRectF& _showSceneRect, qreal _scale, const QRectF& _allItemRect
  , const QSizeF& _viewSize)
{
	
	QRectF res(_showSceneRect);
	const qreal& sx = _showSceneRect.x();
	const qreal& sy = _showSceneRect.y();

	const qreal& itemx = _allItemRect.x();
	const qreal& itemy = _allItemRect.y();

	//保证scene的返回和视窗区大小一一致
	res.setWidth(_viewSize.width() / _scale);
	res.setHeight(_viewSize.height() / _scale);
	

	qreal viewH2Scene = res.height();


	
	if (sy < itemy) {
		//显示显示区域的上边缘
		
		//视窗区按比例放大后 高度尺寸小于 所有要显示范围尺寸
		if (viewH2Scene < _allItemRect.height()) {
			
			//图片的上边点最多能在视窗的中央  也就是视窗高度的-/2
			viewH2Scene = viewH2Scene / 2.0;
			if(sy< itemy- viewH2Scene) {
				res.setY(itemy - viewH2Scene);
			}
		}
		else {
			//全部图片的宽度能在视窗内看到  图片的下边 w靠近视窗的下边边
			if (sy < -(viewH2Scene - _allItemRect.height())) {
				res.setY(-(viewH2Scene - _allItemRect.height()-itemy));
			}
		}
	}
	else {
		//图片向上拖动 视窗下边缘大于图片小于图片上边缘后  最多能移动到 -图片长度比例的位置
		if (viewH2Scene < _allItemRect.height()) {
			viewH2Scene = viewH2Scene / 2.0;
			viewH2Scene = _allItemRect.height() - viewH2Scene+ itemy;
			if (sy > viewH2Scene) {
				res.setY(viewH2Scene);
			}
		}
		else {
			res.setY(itemy);
		}
	}

	qreal viewW2Scene = res.width();
	if (sx < itemx) {
		//图片向右拖动 小于图片左边缘后  最多能移动到 -图片长度比例的位置

		if (viewW2Scene < _allItemRect.width()) {
			//全部图片的宽不能能在视窗内看到  图片的左边点最多能在视窗的中央  也就是视窗宽度的-/2
			viewW2Scene = viewW2Scene / 2.0;
			if (sx < itemx-viewW2Scene) {
				res.setX(itemx -viewW2Scene);
			}
		}
		else {
			//全部图片的宽度能在视窗内看到  图片的右边 w靠近视窗的右边
			//如果x坐标最小为付的 窗口宽度成比例放大后减去图片的宽度
			if (sx < -(viewW2Scene - _allItemRect.width())) {
				res.setX(-(viewW2Scene - _allItemRect.width() -itemx));
			}
		}
	}
	else {
		if (viewW2Scene < _allItemRect.width()) {
			viewW2Scene = viewW2Scene / 2.0;
			//如果图片经过放大后 宽度大于窗体宽度 
			viewW2Scene = _allItemRect.width() - viewW2Scene+itemx;
			if (sx > viewW2Scene) {
				res.setX(viewW2Scene);
			}
		}
		else {
			res.setX(itemx);
		}
	}
	return res;
}

bool CalculateUtil::getThreePtCircle(const QPointF& pt1, const QPointF& pt2, const QPointF& pt3, QPointF& center
                                   , qreal& radius)
{
	
	qreal x1 = pt1.x(), x2 = pt2.x(), x3 = pt3.x();
	qreal y1 = pt1.y(), y2 = pt2.y(), y3 = pt3.y();
	qreal a = x1 - x2;
	qreal b = y1 - y2;
	qreal c = x1 - x3;
	qreal d = y1 - y3;
	qreal e = ((x1*  x1 - x2 * x2) + (y1*  y1 - y2 * y2)) / 2.0;
	qreal f = ((x1*  x1 - x3 * x3) + (y1*  y1 - y3 * y3)) / 2.0;
	qreal det = b * c - a * d;
	if (fabs(det) < 1e-5) {
		radius = -1;
		center = QPointF(0, 0);
		return false;
	}
	qreal x0 = -(d*  e - b * f) / det;
	qreal y0 = -(a*  f - c * e) / det;
	radius = hypot(x1 - x0, y1 - y0);
	center = QPointF(x0, y0);
	return true;
}

int CalculateUtil::isClockwise(const QPointF& p1, const QPointF& p2, const QPointF& p3)
{
	QPointF p12 (p2.x() - p1.x(), p2.y() - p1.y());
	QPointF p23(p3.x() - p2.x(), p3.y() - p2.y());
	int res = (p12.x())*(p23.y()) - (p12.y())*(p23.x());
	if(res>0) {
		return 1;
	}else if(res ==0) {
		return 0;
	}else {
		return -1;
	}
}

QList<QPointF> CalculateUtil::calculateArraw(const QPointF& _startP, const QPointF& _endP, const qreal& _scale)
{
	QList<QPointF> res;
	QPointF p = _endP - _startP;
	qreal l = ARROW_LENGTH / _scale;

	if(qPow(qPow(p.x(),2)+qPow(p.y(),2),0.5)>l) {
		qreal angle = atan2(p.y(), p.x());
		qreal a = 0.5;
		QPointF p1, p2;
		res << _endP;
		p1.setX(_endP.x() - l * cos(angle - a));
		p1.setY(_endP.y() - l * sin(angle - a));
		res << p1;

		p2.setX(_endP.x() - l * cos(angle + a));
		p2.setY(_endP.y() - l * sin(angle + a));
		res << p2;
	}
	return res;
}

qreal CalculateUtil::getAngle(const QPointF& point)
{
	if (point.x() == 0) {
		return point.y() > 0 ? 90 : 270;
	}
	qreal a = point.y() / point.x();
	a = qAtan(a);
	qreal ret = a * 180 / PI; //弧度转角度，方便调试
	if (point.x() < 0) {
		ret = 180 + ret;
	}
	if (ret > 360) {
		ret -= 360;
	}
	if (ret < 0) {
		ret += 360;
	}
	return ret;
}
