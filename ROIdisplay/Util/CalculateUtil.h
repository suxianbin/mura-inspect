#pragma once
#ifndef _CALCULATE_UTIL_H_
#define _CALCULATE_UTIL_H_
#pragma execution_character_set("utf-8")

#ifndef OUT
#define OUT
#endif

using UINT = unsigned int;

#ifndef PI
#define PI 3.1415926
#endif

#ifndef ARROW_LENGTH
#define ARROW_LENGTH 10
#endif


#include <opencv2/opencv.hpp>
#include <QObject>
#include <QRectF>

#include <qmath.h>
#include <QSizeF>

class CalculateUtil
{
public:
	/********************************
	 * 功能 判断Rol的尺寸能够在图片上显示
	 * 输入 image 图片
	 * 输入 ROI roi区域
	 * 返回
	 ********************************/
	static bool QcvROIIsOk(cv::Mat& image, cv::Rect ROI);


	/********************************
	 * 功能 计算点到直线的距离
	 * 输入 pt  点
	 * 输入 pt1 直线端点1
	 * 输入 pt2 直线端点2
	 * 返回 
	 ********************************/
	static float calculatePt2LineDistance(cv::Point2f pt, cv::Point2f pt1, cv::Point2f pt2);


	/********************************
	 * 功能 计算p0到p1的斜率是否大于p0到p2的斜率
	 * 输入 p0 基准点
	 * 输入 p1	点1
	 * 输入 p2	点2
	 * 返回 0 点1的斜率大于 点2  也就是 点2需要逆时针旋转  1相反
	 ********************************/
	static int judgeDirection(const cv::Point2f& p0, const cv::Point2f& p1, const cv::Point2f& p2);


	


	/********************************
	 * 功能 弧度转角度
	 * 输入 data 
	 * 返回 
	 ********************************/
	static float radian2Angle(float data);

	/********************************
	 * 功能 计算两个弧度差并返回角度
	 * 输入 start 
	 * 输入 end 
	 * 返回 
	 ********************************/
	static float getSpanAngle(const float& start, const float& end);

	/********************************
	 * 功能 计算点相对于正交圆心的方位角
	 * 输入 center 
	 * 输入 pt 
	 * 返回 
	 ********************************/
	static float getPtAngleBaseCircleCenter(const cv::Point2f& center, const cv::Point2f& pt);


	/********************************
	 * 功能 计算图像正好在视窗全部显示的放大缩小比例
	 * 输入 imageSize	图像大小
	 * 输入 viewSize	视窗大小
	 * 返回 缩放比例
	 ********************************/
	static qreal calcSuitableScal(const QSizeF& imageSize, const QSize& viewSize);



	/********************************
	 * 功能 获取允许拖动的范围
	 * 输入 _showSceneRect 需要显示的scene的范围
	 * 输入 _scale 缩放比例
	 * 输入 _allItemRect	所有Item的范围
	 * 输入 _viewSize 视图的范围
	 * 返回 
	 ********************************/
	static QRectF getAllowRect(const QRectF & _showSceneRect, qreal _scale, const QRectF& _allItemRect, const QSizeF& _viewSize);


	/********************************
	 * 功能 获取三点圆的半径
	 * 输入 pt1
	 * 输入 pt2
	 * 输入 pt3
	 * 输入 center 输出圆心
	 * 输入 radius 输出半径
	 * 返回 true 三点能够成圆
	 ********************************/
	static bool getThreePtCircle(const QPointF& pt1, const QPointF& pt2, const QPointF& pt3, QPointF& center, qreal& radius);//获取三点圆的半径和圆心
	

	/********************************
	 * 功能 获取角度  轴方向是0度角，顺时针为正角度
	 * 输入 point 
	 * 返回 
	 ********************************/
	static qreal getAngle(const QPointF& point);


	/********************************
	 * 功能 判断三点是顺时针还是逆时针
	 * 输入 p1 第一点
	 * 输入 p2 第二点
	 * 输入 p3 第三点
	 * 返回 1 顺时针 0一条线 -1 逆时针
	 ********************************/
	static int isClockwise(const QPointF& p1, const QPointF& p2, const QPointF& p3);


	/********************************
	 * 功能 计算箭头
	 * 输入 _startP 起始点
	 * 输入 _endP	结束点
	 * 输入 _scale	缩放比例
	 * 返回 箭头的三个点
	 ********************************/
	static QList<QPointF> calculateArraw(const QPointF& _startP, const QPointF& _endP,const qreal& _scale);
};
#endif

