#pragma once
#ifndef _ROIITEMCREATEFACTORY_FILE_H_
#define _ROIITEMCREATEFACTORY_FILE_H_
#include <QObject>
#include <QGraphicsItem>
#include <opencv2/opencv.hpp>

#include "DrawLineClass.h"
#include "DrawCircleItem.h"
#include "DrawPointItem.h"
#include "DrawPointsItem.h"
#include "ROIPtsLineItem.h"
#include "ROIArcItem.h"
#include "ROICircleItem.h"
#include "ROIPolygonItem.h"
#include "ROIRectangleItem.h"
#include "ROIRectItem.h"

using namespace  std;
class RoiItemCreateFactory 
{
public:

	
	/********************************
	 * 功能 构造绘制点Item
	 * 输入 _data 
	 * 返回 
	 ********************************/
	static DrawAbstractBaseItem<>* buildDrawPointItem(const cv::Point2f& _data);
	
	/********************************
	 * 功能 构造绘制多点Item
	 * 输入 _data 
	 * 返回 
	 ********************************/
	static DrawAbstractBaseItem<>* buildDrawPointsItem(const vector<cv::Point2f>& _data);

	
	/********************************
	 * 功能 构造ROI矩形Item
	 * 输入 data 
	 * 返回 
	 ********************************/
	static ROIAbstractBaseItem* buildRoiRectItem(const cv::Rect& data);
	/********************************
	 * 功能 获取ROI矩形数据
	 * 输入 pItem 
	 * 输入 res 输出数据
	 ********************************/
	static void getRoiRecData(ROIRectItem* pItem,OUT cv::Rect* res);
	
	/********************************
	 * 功能 构造多点ROIItem
	 * 输入 data 
	 * 返回 
	 ********************************/
	static ROIAbstractBaseItem* buildRoiPtsListItem(const vector<cv::Point2f>& data);

	/********************************
	 * 功能 获取ROI多点数据
	 * 输入 item 
	 * 输入 res 输出数据
	 ********************************/
	static void getRoiPtsListData(ROIPolygonItem* item, OUT vector<cv::Point2f>* res);

};
#endif