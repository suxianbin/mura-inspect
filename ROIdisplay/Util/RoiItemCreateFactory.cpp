#include "RoiItemCreateFactory.h"

DrawAbstractBaseItem<>* RoiItemCreateFactory::buildDrawPointItem(const cv::Point2f& _data)
{
	DrawPointItem* item = new DrawPointItem();
	item->setPos(_data.x, _data.y);
	return (DrawAbstractBaseItem<>*)item;
}

DrawAbstractBaseItem<>* RoiItemCreateFactory::buildDrawPointsItem(const vector<cv::Point2f>& _data)
{
	DrawPointsItem* item = new DrawPointsItem;
	QVector<QPointF> points;
	for(int i=0;i<_data.size();i++) {
		QPointF p;
		p.setX(_data[i].x);
		p.setY(_data[i].y);
		points << p;
	}
	item->setPolygon(points);
	return (DrawAbstractBaseItem<>*)item;
}


ROIAbstractBaseItem* RoiItemCreateFactory::buildRoiRectItem(const cv::Rect& data)
{
	ROIRectItem* pItem = new ROIRectItem;
	QRectF rect;
	rect.setTopLeft(QPoint(data.x, data.y));
	rect.setWidth(data.width);
	rect.setHeight(data.height);
	return pItem;
}

void RoiItemCreateFactory::getRoiRecData(ROIRectItem* pItem, OUT cv::Rect* res)
{
	QRectF data = pItem->getData();
	res->x = data.x();
	res->y = data.y();
	res->width = data.width();
	res->height = data.height();
}

ROIAbstractBaseItem* RoiItemCreateFactory::buildRoiPtsListItem(const vector<cv::Point2f>& data)
{
	ROIPolygonItem* pItem = new ROIPolygonItem;
	QList<QPointF> points;
	for(int i=0;i<data.size();i++) {

		points<<QPointF(data[i].x, data[i].y);
	}
	pItem->setData(points);
	return pItem;
}

void RoiItemCreateFactory::getRoiPtsListData(ROIPolygonItem* item, vector<cv::Point2f>* res)
{
	QList<QPointF> points = item->getData();
	res->clear();
	for(int i=0;i< points.size();i++) {
		res->push_back(cv::Point2f(points[i].x(), points[i].y()));
	}
}

