#include "ImageHistogramWidget.h"


ImageHistogramWidget::ImageHistogramWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	setWindowFlag(Qt::ToolTip, true);
	installEventFilter(this);
}

ImageHistogramWidget::~ImageHistogramWidget()
{
}

void ImageHistogramWidget::showEvent(QShowEvent* event)
{
	resize(200, 200);
	QWidget::showEvent(event);
}

