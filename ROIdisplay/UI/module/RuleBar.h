#ifndef RULEBAR
#define RULEBAR

#include <QtWidgets>


QT_BEGIN_NAMESPACE
class QFrame;
class QPaintEvent;
class QPainter;
class QGraphicsView;
QT_END_NAMESPACE

#define RULER_SIZE    16


/********************************
 * 功能 标尺
 ********************************/
class RuleBar : public QWidget
{
    Q_OBJECT
public:
    explicit RuleBar(QWidget * parent = Q_NULLPTR);
    /********************************
	 * 功能 初始化
	 * 输入 direction  标尺发方向
	 ********************************/
	void init(Qt::Orientation direction);
    /********************************
     * 功能 设置范围
     * 输入 lower 
     * 输入 upper 
     * 输入 max_size 
     ********************************/
    void setRange( double lower , double upper , double max_size );

    /********************************
     * 功能 更新鼠标位置
     * 输入 pos 
     ********************************/
    void updatePosition( const QPoint & pos );

    /**
	 * \brief 设置比例尺相关
	 * \param ratio 
	 * \param unit 
	 */
	void setRuleParam(const qreal& ratio, const QString& unit);
protected:
    void paintEvent(QPaintEvent *event) override;
	
    /********************************
     * 功能 绘制刻度
     * 输入 painter 
     ********************************/
    void drawTicker(QPainter * painter);
	
    /********************************
     * 功能 绘制鼠标位置
     * 输入 painter 
     ********************************/
    void drawPos(QPainter * painter) ;
private:
    Qt::Orientation   m_direction = Qt::Horizontal;					//标尺方向
    QPoint m_lastPos;												//最后位置
    QColor m_faceColor;
	qreal m_ratio = 1;												//像素比例
	QString m_unit;													//比例尺单位
    double m_lower;
    double m_upper;
    double m_maxsize;
};

#endif // RULEBAR

