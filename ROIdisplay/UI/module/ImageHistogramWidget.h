#pragma once

#include <QWidget>
#include "ui_ImageHistogramWidget.h"
/********************************
 * 功能 直方图显示窗口
 ********************************/
class ImageHistogramWidget : public QWidget
{
	Q_OBJECT

public:
	ImageHistogramWidget(QWidget *parent = Q_NULLPTR);
	~ImageHistogramWidget() override;

private:
	Ui::ImageHistogramWidget ui;

	void showEvent(QShowEvent* event) override;

};
