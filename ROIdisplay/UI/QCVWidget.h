#pragma once

#include <QWidget>

#include "ROIGraphicsView.h"
#include "rulebar.h"
#include "ui_QCVWidget.h"

class QCVWidget : public QWidget
{
	Q_OBJECT

signals:
	void sig_mouserMoved(const QPoint& point);
public:
	QCVWidget(QWidget *parent = Q_NULLPTR);
	~QCVWidget() override;

	/********************************
	 * 功能 设置背景图片
	 * 输入 img
	 ********************************/
	void setBackgroundImg(const cv::Mat& img);
	void setBackgroundImgMap(const QPixmap& img);

	/********************************
	 * 功能 添加Item
	 * 输入 pItem
	 ********************************/
	void addROIItem(ROIAbstractBaseItem* pItem);
	
	/********************************
	 * 功能 添加绘制Data
	 * 输入 pItem 
	 ********************************/
	void addDrawItem(DrawAbstractBaseItem<>* pItem);

	/********************************
	 * 功能 添加Item
	 * 输入 item 
	 ********************************/
	void addItem(QGraphicsItem* item);


	/********************************
	 * 功能 移除Item 不会析构
	 * 输入 item 
	 ********************************/
	void removeItem(QGraphicsItem* item);

	/********************************
	 * 功能 清除所有绘制的且会析构  item背景的除外
	 ********************************/
	void clearItem();

	void scaleToSuitableRect();
	//void setSuitableRect(const QRectF& rect);

	void resetSelectROI();
	void updateScene();


	/**
	 * \brief 设置比例尺是否显示
	 * \param visible
	 */
	void setRuleVisable(bool visible);

	/**
	 * \brief 设置比例尺相关参数
	 * \param ratio 比例 默认1
	 * \param unit 单位 默认空
	 */
	void setRuleParam(const qreal& ratio, const QString& unit);

	/**
	 * \brief 设置 视图是否可以移动
	 * \param can
	 */
	void setMouseEnable(bool can);

	/********************************
	 * 功能 设置视图范围 会对范围进行校验，不允许背景出范围
	 * 输入 _rect
	 ********************************/
	void scaleToRect(const QRectF& _rect);

	QGraphicsView* getGraphicsView();

private:
	Ui::QCVWidget ui;


private:
	void init();


};
