#include "QCVWidget.h"

QCVWidget::QCVWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	init();
}

QCVWidget::~QCVWidget()
{
}

void QCVWidget::setBackgroundImg(const cv::Mat& img)
{
	ui.view->setBackgroundImg(img);
}

void QCVWidget::setBackgroundImgMap(const QPixmap& img)
{
	ui.view->setBackgroundImgMap(img);
}

void QCVWidget::addROIItem(ROIAbstractBaseItem* pItem)
{
	ui.view->addROIItem(pItem);
}

void QCVWidget::addDrawItem(DrawAbstractBaseItem<>* pItem)
{
	ui.view->addDrawItem(pItem);
}

void QCVWidget::addItem(QGraphicsItem* item)
{
	ui.view->addItem(item);
}

void QCVWidget::removeItem(QGraphicsItem* item)
{
	ui.view->removeItem(item);
}


void QCVWidget::clearItem()
{
	ui.view->clearItem();
}

void QCVWidget::scaleToSuitableRect()
{
	ui.view->scaleToSuitableRect();
}

// void QCVWidget::setSuitableRect(const QRectF& rect)
// {
// 	ui.view->setSuitableRect(rect);
// }

void QCVWidget::resetSelectROI()
{
	ui.view->resetSelectROI();
}

void QCVWidget::updateScene()
{
	ui.view->viewport()->update();
}

void QCVWidget::setRuleVisable(bool visible)
{
	if(visible)
	{
		ui.rulebar_h->show();
		ui.rulebar_v->show();
		ui.widget->show();
	}else
	{
		ui.rulebar_h->hide();
		ui.rulebar_v->hide();
		ui.widget->hide();
	}
}

void QCVWidget::setRuleParam(const qreal& ratio, const QString& unit)
{
	ui.rulebar_h->setRuleParam(ratio, unit);
	ui.rulebar_v->setRuleParam(ratio, unit);
	ui.view->setPixelEquivalent(ratio, unit);
}

void QCVWidget::setMouseEnable(bool anble)
{
	ui.view->setMouseEnable(anble);
}

void QCVWidget::scaleToRect(const QRectF& _rect)
{
	ui.view->scaleToRect(_rect);
}

QGraphicsView* QCVWidget::getGraphicsView()
{
	return ui.view;
}

void QCVWidget::init()
{
	
	ui.rulebar_h->init(Qt::Horizontal);
	ui.rulebar_v->init(Qt::Vertical);

	connect(ui.view, &ROIGraphicsView::sig_sceneRectChenged,[=](const QRectF& rect)
	{
		ui.rulebar_h->setRange(rect.x(), rect.x() + rect.width(), rect.width());

		ui.rulebar_v->setRange(rect.y(), rect.y() + rect.height(), rect.height());
	});

	connect(ui.view, &ROIGraphicsView::sig_mouserMoved, [=](const QPoint& point)
	{
		ui.rulebar_h->updatePosition(point);
		ui.rulebar_v->updatePosition(point);
		emit sig_mouserMoved(point);
	});


	QPalette pal(ui.widget->palette());

	//设置背景黑色
	pal.setColor(QPalette::Background, Qt::gray	);
	ui.widget->setAutoFillBackground(true);
	ui.widget->setPalette(pal);
	QString useHelp = "鼠标右键拖动\n";
	useHelp += "鼠标滚轮放大/缩小\n";
	useHelp += "Ctrl+鼠标左键拖选放大\n";
	useHelp += "Ctrl+鼠标左键双击图形回到自适应\n";
	useHelp += "Ctrl可以查看鼠标位置的灰度值\n";
	useHelp += "Alt+鼠标左键拖选可以查看范围内的平均灰度值\n";
	useHelp += "Alt+鼠标左键拖选可以查看范围内的平均灰度值\n";
	useHelp += "鼠标右键可以操作ROI如完成绘制，增减控制点，清除绘图等\n";
	useHelp += "对于多边形ROI利用双击右键绘制完多点后，再次利用右键菜单可完成绘制\n";


	ui.label->setToolTip(useHelp);
	ui.label->installEventFilter(this);
}





