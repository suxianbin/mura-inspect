#pragma once
#ifndef _ROIDISPLAY_FILE_H_
#define _ROIDISPLAY_FILE_H_
#pragma execution_character_set("utf-8")

#include <QWidget>
#include <opencv2/opencv.hpp>
#include "ROIAbstractBaseItem.h"
#include "DrawAbstractBaseItem.h"
#include <QMouseEvent>
/********************************
 * 功能 显示 opencv 图片的自定义控件
 ********************************/
class Q_DECL_EXPORT ROIDisplay : public QWidget
{
	Q_OBJECT
signals:
	void sig_mouserMovedPoint(const QPoint& point);
public:
	explicit ROIDisplay(QWidget* parent = Q_NULLPTR);
	~ROIDisplay() override;

	/********************************
	 * 功能 更新图片  缩放及视窗区中点不会改变
	 * 输入 img
	 ********************************/
	void updataImage(const cv::Mat& img);
	void updataImageMap(const QPixmap& img);

	/********************************
	 * 功能 添加Item
	 * 输入 pItem
	 ********************************/
	void addROIItem(ROIAbstractBaseItem* pItem);

	/********************************
	 * 功能 添加绘制Data
	 * 输入 pItem
	 ********************************/
	void addDrawItem(DrawAbstractBaseItem<>* pItem);

	/********************************
	 * 功能 添加Item
	 * 输入 item 
	 ********************************/
	void addItem(QGraphicsItem* item);

	/********************************
	 * 功能 移除Item
	 * 输入 pItem 
	 ********************************/
	void removeItem(QGraphicsItem* pItem);

	/********************************
	 * 功能 清除所有绘制的item 并且析构
	 ********************************/
	void clearAll();

	/********************************
	  * 功能 清除绘制的Draw
	  ********************************/
	void clearDrawROI() ;

	/**
	 * \brief 放大缩小到适合的尺寸
	 */
	void scaleToSuitableRect();

	/**
	 * \brief 放大缩小到适合的尺寸
	 */
	void scaleToRect(const QRectF& distRect);

	/**
	 * \brief 设置比例尺是否显示
	 * \param visible 
	 */
	void setRuleVisable(bool visible);

	/**
	 * \brief 设置比例尺相关参数
	 * \param ratio 比例 默认1 
	 * \param unit 单位 默认空
	 */
	void setRuleParam(const qreal& ratio, const QString& unit);

	/**
	 * \brief 设置 鼠标可以使用
	 * \param can 
	 */
	void setMouseEnable(bool can);

	void updateScene();
	/**
	 * \brief 获取绘制视窗
	 * \return 
	 */
	QGraphicsView* getGraphicsView();
protected:
	class ROIDisplayHandle;
	ROIDisplayHandle* handle = Q_NULLPTR;
};
#endif