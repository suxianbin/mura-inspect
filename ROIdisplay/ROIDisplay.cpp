#include "ROIDisplay.h"
#include <QScrollArea>
#include <QScrollBar>
#include <QMessageBox>
#include <QInputDialog>
#include <QGraphicsItem>
#include <QLayout>
#include <QToolTip>
#include <QBoxLayout>
#include "MagnifyingRectItem.h"
#include "ROIGraphicsView.h"
#include "RoiItemCreateFactory.h"
#include "ROIRectangleItem.h"
#include "ROICircleItem.h"
#include "ROIArcItem.h"
#include "CalculateUtil.h"
#include "ImageUtil.h"
#include "ROIPolygonItem.h"
#include "RoiGraphicsScene.h"
#include "DrawLineClass.h"
#include "DrawCircleItem.h"
#include "DrawPointItem.h"
#include "DrawPointsItem.h"
#include "QCVWidget.h"


class ROIDisplay::ROIDisplayHandle
{
public:
	QCVWidget* m_view = Q_NULLPTR;
};

ROIDisplay::ROIDisplay(QWidget* parent) :
	QWidget(parent)
{
	handle         = new ROIDisplayHandle();
	handle->m_view = new QCVWidget(this);

	QHBoxLayout* playout = new QHBoxLayout(this);
	playout->addWidget(handle->m_view);
	playout->setMargin(0);
	setLayout(playout);

	connect(handle->m_view, &QCVWidget::sig_mouserMoved, this, &ROIDisplay::sig_mouserMovedPoint);
}

ROIDisplay::~ROIDisplay()
{
	delete handle;
}


void ROIDisplay::updataImage(const cv::Mat& img)
{
	if (img.empty()) 
	{
		return;
	}
	else
	{
		handle->m_view->setBackgroundImg(img);
	}
}

void ROIDisplay::updataImageMap(const QPixmap & img)
{
	if (img.isNull()) {
		//return;
	}
	else {
		handle->m_view->setBackgroundImgMap(img);
	}
}

void ROIDisplay::addROIItem(ROIAbstractBaseItem* pItem)
{
	handle->m_view->addROIItem(pItem);
}

void ROIDisplay::addDrawItem(DrawAbstractBaseItem<>* pItem)
{
	handle->m_view->addDrawItem(pItem);
}

void ROIDisplay::addItem(QGraphicsItem* item)
{
	handle->m_view->addItem(item);
}

void ROIDisplay::removeItem(QGraphicsItem* pItem)
{
	handle->m_view->removeItem(pItem);
}

void ROIDisplay::clearAll()
{
	handle->m_view->clearItem();
}


void ROIDisplay::clearDrawROI()
{
	handle->m_view->clearItem();
}

void ROIDisplay::scaleToSuitableRect()
{
	handle->m_view->scaleToSuitableRect();
	
}

void ROIDisplay::scaleToRect(const QRectF& distRect)
{
	handle->m_view->scaleToRect(distRect);
}

void ROIDisplay::setRuleVisable(bool visible)
{
	handle->m_view->setRuleVisable(visible);
}

void ROIDisplay::setRuleParam(const qreal& ratio, const QString& unit)
{
	handle->m_view->setRuleParam(ratio, unit);
}

void ROIDisplay::setMouseEnable(bool can)
{
	handle->m_view->setMouseEnable(can);
}

void ROIDisplay::updateScene()
{
	handle->m_view->updateScene();
}


QGraphicsView* ROIDisplay::getGraphicsView()
{
	return handle->m_view->getGraphicsView();
}





