#pragma once
#ifndef _MAGNIFYINGRECTITEM_FILE_H_
#define _MAGNIFYINGRECTITEM_FILE_H_
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QRect>
#include "KeepPenWidthGraphicsItem.h"


/********************************
 * 功能 此类用于拖选放大绘制的矩形框
 * 主要内容就是不会根据放大比例放大线宽
 * 且放宽和高不能为负
 ********************************/
class MagnifyingRectItem : public KeepPenWidthGraphicsItem<QGraphicsRectItem>
{

public:
	MagnifyingRectItem(QGraphicsItem *parent = Q_NULLPTR);
	~MagnifyingRectItem() override;
	/********************************
	 * 功能 会判断大小不能 使宽和高为负
	 * 输入 rect 
	 ********************************/
	void setRect(const QRectF &rect);
	
	/********************************
	 * 功能 会判断大小不能 使宽和高为负
	 ********************************/
	void setRect(qreal x, qreal y, qreal w, qreal h);

};
#endif