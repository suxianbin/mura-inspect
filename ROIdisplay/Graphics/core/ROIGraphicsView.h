#pragma once
#ifndef _ROIGRAPHICSVIEW_FILE_H_
#define _ROIGRAPHICSVIEW_FILE_H_
#pragma execution_character_set("utf-8")
#include <QApplication>
#include <QGraphicsView>
#include <QToolTip>
#include <QKeyEvent>
#include <QGraphicsProxyWidget>
#include <QInputDialog>

#include <opencv2/opencv.hpp>

#include "MagnifyingRectItem.h"
#include "RoiGraphicsScene.h"


class ImageHistogramWidget;

/********************************
 * 功能 ROIView
 ********************************/
class  ROIGraphicsView : public QGraphicsView
{
	Q_OBJECT
signals:
	/********************************
	 * 功能 sceneRect 改变
	 * 输入 rect 
	 ********************************/
	void sig_sceneRectChenged(const QRectF& rect);

	/********************************
	 * 功能 鼠标移动事件主要用于传递给标尺
	 * 输入 point 
	 ********************************/
	void sig_mouserMoved(const QPoint& point);

	/********************************
	 * 功能 缩放比例信号
	 * 输入 _scale 
	 ********************************/
	void sig_scaleChanged(const qreal& _scale);

public:
	ROIGraphicsView(QWidget *parent = Q_NULLPTR);
	~ROIGraphicsView() override;

	/********************************
	 * 功能 放大 或缩小
	 * 输入 sacle 
	 ********************************/
	void scale(qreal sacle);
	
	/********************************
	 * 功能 获取内容放大的比例
	 * 返回 
	 ********************************/
	qreal getScale();

	/********************************
	 * 功能 设置背景图片
	 * 输入 img 
	 ********************************/
	void setBackgroundImg(const cv::Mat& img);
	void setBackgroundImgMap(const QPixmap& img);

	/********************************
	 * 功能 设置视图范围 会对范围进行校验，不允许背景出范围
	 * 输入 _rect 
	 ********************************/
	void setSceneRect(const QRectF& _rect);
	
	/********************************
	 * 功能 设置视图范围  会对范围进行校验，不允许背景出范围
	 * 输入 x 
	 * 输入 y 
	 * 输入 w 
	 * 输入 h 
	 ********************************/
	void setSceneRect(qreal x, qreal y, qreal w, qreal h);

	/********************************
	 * 功能 添加RoiItem
	 * 输入 pItem 
	 ********************************/
	void addROIItem(ROIAbstractBaseItem* pItem);

	/********************************
	 * 功能 添加绘制Item
	 * 输入 pItem 
	 ********************************/
	void addDrawItem(DrawAbstractBaseItem<>* pItem);

	/********************************
	 * 功能 添加绘制item
	 * 输入 pItem 
	 ********************************/
	void addItem(QGraphicsItem* pItem);

	/********************************
	 * 功能 移除一个item
	 * 输入 pItem 
	 ********************************/
	void removeItem(QGraphicsItem* pItem);
	
	/********************************
	 * 功能 清除所有绘制的 item  背景等功能Item除外
	 ********************************/
	void clearItem();

	/********************************
	 * 功能 重置正在绘制Roi的状态 为正常状态
	 ********************************/
	void resetSelectROI();


	/********************************
	 * 功能 缩放到合适的尺寸
	 ********************************/
	void scaleToSuitableRect();

	/**
	 * \brief 缩放到合适尺寸
	 * \param rectf 
	 */
	void scaleToRect(const QRectF& rectf);


	/**
	 * \brief 设置是否能够使用鼠标拖动
	 * \param isCanMove 
	 */
	void setMouseEnable(bool enable);

	/**
	 * \brief 设置像素当量相关内容， 若unit不为空会显示相应实际坐标
	 * \param ratio 像素当量
	 * \param unit 单位
	 */
	void setPixelEquivalent(const qreal& ratio, const QString& unit);


	/************************************
	 * 功能: 屏幕上绘制直线  
	 * 参数: const QPointF & startPoint
	 * 参数: const QPointF & endPoint
	 * 返回: void
	*************************************/
	void drawLine(const QPointF& startPoint, const QPointF& endPoint);

	/************************************
	 * 功能: 屏幕上绘制十字线   
	 * 参数: const QPointF & centerPoint
	 * 返回: void
	*************************************/
	void drawCrossLine(const QPointF& centerPoint);

private:
	QGraphicsPixmapItem* m_backImgItem   = Q_NULLPTR;	//背景图片Item
	MagnifyingRectItem* m_magnifyingRect = Q_NULLPTR;	//拖选放大的显示效果Item

	QGraphicsTextItem* m_coordinateTextItem = nullptr;	//文本显示

	qreal m_scale = 1;									//缩放比例
	QPointF m_lastMouseRightPointF;						//鼠标右键按下后上一次的位置
	QPointF m_rightPressPointF;							//鼠标右键按下时的位置
	QPointF m_leftPressPointF;							//鼠标左键按下时的位置
	QPointF m_mousePos;									//鼠标位置
	cv::Mat m_orgMat;									//原始图像
	QPixmap m_showPixmap;								//显示出来的pixmap

	int m_controlType = 0;								//默认为0  1是放大 2是框选显示直方图
	ImageHistogramWidget* m_histogram = Q_NULLPTR;		//直方图显示
	RoiGraphicsScene* m_scene = Q_NULLPTR;
	QAction* m_clearAction = Q_NULLPTR;					//清除图像action

	QRectF m_showRect;//当前显示的矩形区域

	bool m_enableMouse = true;
	qreal m_ratio = 1;									//像素当量
	QString m_unit;										//单位
	QGraphicsLineItem* lineItem;
	bool hasImage = false;//标志来记录是否有图像
protected:
	
	/********************************
	 * 功能 初始化
	 ********************************/
	void init();

	/********************************
	 * 功能 显示鼠标位置的灰度相关数值
	 ********************************/
	void showToolTip();

	void wheelEvent(QWheelEvent* event) override;

	void mouseDoubleClickEvent(QMouseEvent* event) override;

	void mouseMoveEvent(QMouseEvent* event) override;

	void mousePressEvent(QMouseEvent* event) override;

	void mouseReleaseEvent(QMouseEvent* event) override;

	void keyPressEvent(QKeyEvent* event) override;

	void keyReleaseEvent(QKeyEvent* event) override;
	
	/********************************
	 * 功能 删除x 轴和Y轴不同放大比例的情况
	 * 输入 rx
	 * 输入 ry
	 ********************************/
	void scale(qreal rx, qreal ry) = delete;

	/********************************
	 * 功能 删除添加场景
	 * 输入 scene
	 ********************************/
	void setScene(QGraphicsScene *scene) = delete;


	void contextMenuEvent(QContextMenuEvent* event) override;

	/********************************
	 * 功能 可以根据图像大小显示显示初始比例
	 * 输入 _rect 
	 ********************************/
	void calculateItemRect(const QRectF& _rect);

};


#endif
