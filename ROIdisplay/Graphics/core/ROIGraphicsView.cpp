#include "ROIGraphicsView.h"
#include "ImageHistogramWidget.h"
#include "CalculateUtil.h"
#include "ImageUtil.h"

ROIGraphicsView::ROIGraphicsView(QWidget *parent)
	: QGraphicsView(parent)
{

	init();
}

ROIGraphicsView::~ROIGraphicsView()
{
	
}

void ROIGraphicsView::scale(qreal scale)
{
	if (m_scene != Q_NULLPTR) {
 		m_scale = scale * m_scale;
		
		QGraphicsView::scale(scale, scale);
		//QRectF rect = sceneRect();// mapToScene().boundingRect();
		QRectF rect = mapToScene(viewport()->rect()).boundingRect();
		setSceneRect(rect);
		emit sig_scaleChanged(m_scale);
	}
}

qreal ROIGraphicsView::getScale()
{
	return m_scale;
}

void ROIGraphicsView::setSceneRect(const QRectF& _rect)
{
	//计算可使用视窗范围
	const QRectF& rect = CalculateUtil::getAllowRect(_rect,m_scale, _rect, size());
	QGraphicsView::setSceneRect(rect);
	emit sig_sceneRectChenged(rect);
}

void ROIGraphicsView::setSceneRect(qreal x, qreal y, qreal w, qreal h)
{
	QRectF rect(x, y, w, h);
	setSceneRect(rect);
}

void ROIGraphicsView::addROIItem(ROIAbstractBaseItem* pItem)
{
	m_scene->addROIItem(pItem);
	calculateItemRect(pItem->mapRectToScene(pItem->boundingRect()));
}

void ROIGraphicsView::addDrawItem(DrawAbstractBaseItem<>* pItem)
{
	m_scene->addDrawItem(pItem);
	calculateItemRect(pItem->mapRectToScene(pItem->boundingRect()));

}

void ROIGraphicsView::addItem(QGraphicsItem* pItem)
{
	
	m_scene->addItem(pItem);
	calculateItemRect(pItem->mapRectToScene(pItem->boundingRect()));
}

void ROIGraphicsView::removeItem(QGraphicsItem* pItem)
{
	m_scene->removeItem(pItem);
}

void ROIGraphicsView::clearItem()
{
	m_scene->removeItem(m_backImgItem);
	m_scene->removeItem(m_magnifyingRect);
	
	m_scene->clearItem();
	if(!m_orgMat.empty()) {
		m_showRect.setX(0);
		m_showRect.setY(0);
		m_showRect.setHeight(m_orgMat.rows);
		m_showRect.setWidth(m_orgMat.cols);
	}
	
	m_scene->addItem(m_backImgItem);
	m_scene->addItem(m_magnifyingRect);
	hasImage = false; // 设置标志为 false，表示没有图像
}

void ROIGraphicsView::resetSelectROI()
{
	m_scene->resetSelectROI();
}

void ROIGraphicsView::scaleToSuitableRect()
{
	scale(1 / m_scale);
	//---2重新计算缩放比例
	m_showRect = m_scene->itemsBoundingRect();
	QSize s = m_showRect.size().toSize();
	scale(CalculateUtil::calcSuitableScal(s, size()));
	//---3移动视窗到0，0
	setSceneRect(m_showRect);
	
}

void ROIGraphicsView::scaleToRect(const QRectF& rectf)
{
	scale(1 / m_scale);
	//---2重新计算缩放比例
	QSizeF s1 = rectf.size().toSize();
	QSizeF s2 = size();
	qreal sc;
	if (s1.width() == 0 || s1.height() == 0) {
		sc =  1;
	}
	qreal xScal = s2.width() / s1.width();
	qreal yScal = s2.height() / s1.height();

	
	if (xScal > 1 && yScal > 1) {
		sc = qMin(xScal, yScal);
	}else
	{
		sc = qMin(xScal, yScal);
	}
	scale(sc);	//调节图片显示缩放大小
	//---3移动视窗到0，0
	//setSceneRect(rectf);
	QGraphicsView::setSceneRect(rectf);
	emit sig_sceneRectChenged(rectf);
}

void ROIGraphicsView::setMouseEnable(bool mouseEnable)
{
	m_enableMouse = mouseEnable;
}

void ROIGraphicsView::setPixelEquivalent(const qreal& ratio,const QString& unit)
{
	m_ratio = ratio;
	m_unit = unit;
}


void ROIGraphicsView::drawLine(const QPointF& startPoint, const QPointF& endPoint)
{
	if (m_scene != nullptr) {

		if (lineItem!= nullptr)
		{
			m_scene->removeItem(lineItem);
		}
		// 创建 QGraphicsLineItem 对象，表示直线
		lineItem = new QGraphicsLineItem(startPoint.x(), startPoint.y(), endPoint.x(), endPoint.y());

		// 设置直线的颜色、宽度等属性
		QPen pen;
		pen.setColor(Qt::red);  // 设置颜色为红色，可以根据需要修改
		pen.setWidth(2);        // 设置线宽为2，可以根据需要修改
		lineItem->setPen(pen);

		// 将直线添加到场景中
		m_scene->addItem(lineItem);
	}
}

void ROIGraphicsView::drawCrossLine(const QPointF& centerPoint)
{
	if (m_scene != nullptr && hasImage) {
		 // 移除之前的十字线，确保只有一条十字线存在
		QList<QGraphicsItem*> existingLines = m_scene->items();
		for (QGraphicsItem* item : existingLines) {
			if (item->data(Qt::UserRole).toBool()) {
				m_scene->removeItem(item);
				delete item;
			}
		}
		qreal imageWidth = m_orgMat.cols;
		qreal imageHeight = m_orgMat.rows;

		// 创建横线
		QGraphicsLineItem* horizontalLine = new QGraphicsLineItem(0, centerPoint.y(), imageWidth, centerPoint.y());
		horizontalLine->setPen(QPen(Qt::red, 5)); // 设置颜色和线宽
		horizontalLine->setZValue(1); // 设置 Z 值确保位于其他图形上方
		horizontalLine->setData(Qt::UserRole, QVariant(true)); // 使用 Qt::UserRole 标记十字线
		m_scene->addItem(horizontalLine);

		// 创建竖线
		QGraphicsLineItem* verticalLine = new QGraphicsLineItem(centerPoint.x(), 0, centerPoint.x(), imageHeight);
		verticalLine->setPen(QPen(Qt::red, 5));
		verticalLine->setZValue(1);
		verticalLine->setData(Qt::UserRole, QVariant(true));
		m_scene->addItem(verticalLine);
	}
}

void ROIGraphicsView::setBackgroundImg(const cv::Mat& img)
{
	m_orgMat = img;
	m_showPixmap = ImageUtil::mat2QPixmap(img);
	m_backImgItem->setPixmap(m_showPixmap);
	calculateItemRect(QRectF(0,0, img.cols, img.rows));
	hasImage = true; // 设置标志为 true，表示有图像
}

void ROIGraphicsView::setBackgroundImgMap(const QPixmap& img)
{
	m_backImgItem->setPixmap(img);
	calculateItemRect(QRectF(0, 0, img.height(), img.width()));
	hasImage = true; // 设置标志为 true，表示有图像
}

void ROIGraphicsView::wheelEvent(QWheelEvent* event)
{
	if(scene()==Q_NULLPTR) {
		return;
	}
	
	/**********缩放计算*****************/
	if(m_enableMouse)
	{
		QRectF sceneRectF = sceneRect();
		if (sceneRectF.width() != 0 && sceneRectF.height() != 0) {

			// 获取当前鼠标相对于view的位置;
			qreal s = 0.2;
			if (event->angleDelta().y() <= 0) {
				// 向下滚动，缩小;
				s = -s;
			}
			scale(1 + s);
		}
	}
}

void ROIGraphicsView::mouseDoubleClickEvent(QMouseEvent* event)
{
	if (QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
		if (m_enableMouse)
		{
			scaleToSuitableRect();
		}
	}
	QGraphicsView::mouseDoubleClickEvent(event);
}

void ROIGraphicsView::mouseMoveEvent(QMouseEvent* event)
{
	m_mousePos = event->pos();
	//拖动
	if (m_enableMouse)
	{
		if (event->buttons() == Qt::LeftButton &&!m_scene->blockMouse()) {
			QPointF disPointF = mapToScene(m_lastMouseRightPointF.toPoint()) - mapToScene(event->pos());
			// qreal disx = disPointF.rx() /m_scale;
			// qreal disy = disPointF.ry() / m_scale;
			const QPointF& topLeft = sceneRect().topLeft();
			setSceneRect(topLeft.x() + disPointF.rx(), topLeft.y() + disPointF.ry(), size().width() / m_scale, size().height() / m_scale);
			m_lastMouseRightPointF = event->pos();
		}

		//按着Ctrl键和鼠标左键框选放大计算  
		if (event->buttons() == Qt::LeftButton&&QApplication::queryKeyboardModifiers() == Qt::ControlModifier && !m_scene->blockMouse()) {
			if (scene() != Q_NULLPTR) {
				QPointF pf1 = m_leftPressPointF.toPoint();
				pf1 = mapToScene(pf1.toPoint());
				QPointF pf2 = mapToScene(event->pos());
				pf2 = pf2 - pf1;
				m_magnifyingRect->setRect(0, 0, pf2.x(), pf2.y());
				m_controlType = 1;
			}
		}
	}
	
	// //按着Alt键和鼠标左键框选放大计算平均灰度和显示直方图 
	// else if (event->buttons() == Qt::LeftButton&&QApplication::queryKeyboardModifiers() == Qt::AltModifier) {
	// 	if (scene() != Q_NULLPTR) {
	// 		QPointF pf1 = m_leftPressPointF.toPoint();
	// 		pf1 = mapToScene(pf1.toPoint());
	// 		QPointF pf2 = mapToScene(event->pos());
	// 		pf2 = pf2 - pf1;
	// 		m_magnifyingRect->setRect(0, 0, pf2.x(), pf2.y());
	// 		m_controlType = 2;
	// 	}
	// }
	//ctrl显示灰度信息
	//if (QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
	//	showToolTip();
	//}

	//十字线
	//drawCrossLine(mapToScene(m_mousePos.toPoint()));

	//文本
	//if (hasImage)
	//{
	//	QPointF currentPos = mapToScene(m_mousePos.toPoint());
	//	QString coordinateText = QString("坐标(%1,%2)").arg(QString::number((int)round(currentPos.x()), 10)).arg(QString::number((int)round(currentPos.y()), 10));
	//	// 如果有像素当量和单位信息，也可以添加到文本中
	//	if (!m_unit.isEmpty()) {
	//		coordinateText += QString(" (%1,%2)").arg(QString::number(currentPos.x() * m_ratio, 'f', 2) + m_unit).arg(QString::number(currentPos.y() * m_ratio, 'f', 2) + m_unit);
	//	}
	//	m_coordinateTextItem->setPlainText(coordinateText);
	//}
	//

	emit sig_mouserMoved(m_mousePos.toPoint());
	QGraphicsView::mouseMoveEvent(event);

}

void ROIGraphicsView::mousePressEvent(QMouseEvent* event)
{
	if(m_enableMouse)
	{
		if (event->button() == Qt::RightButton) {
			if (m_histogram->isVisible()) {
				m_histogram->hide();
			}
			//拖选放大操作
			m_leftPressPointF = event->pos();
			QPointF point = mapToScene(event->pos());
			m_magnifyingRect->setRect(0, 0, 0, 0);
			m_magnifyingRect->setPos(point);
			m_magnifyingRect->show();
		}

		if (event->button() == Qt::LeftButton) {
			m_rightPressPointF = event->pos();
			m_lastMouseRightPointF = event->pos();
			setCursor(Qt::OpenHandCursor);
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void ROIGraphicsView::mouseReleaseEvent(QMouseEvent* event)
{
	if(m_enableMouse)
	{
		setCursor(Qt::ArrowCursor);
		if (!(event->buttons() & Qt::RightButton)) {
			if (m_magnifyingRect->isVisible() &&
				m_magnifyingRect->rect().width() > 10
				&& m_magnifyingRect->rect().height() > 10) {
				if (m_controlType == 1) {
					//放大
					qreal s = (qreal)sceneRect().width() / m_magnifyingRect->rect().width();
					scale(s);
					setSceneRect(m_magnifyingRect->scenePos().x(), m_magnifyingRect->scenePos().y(),
						size().width() / m_scale, size().height() / m_scale);
				}
				else if (m_controlType == 2) {
					QPoint p(width() - 200 - 50, 50);
					p = mapToGlobal(p);
					m_histogram->move(p.x(), p.y());
					m_histogram->show();
				}

			}
			m_magnifyingRect->hide();
			m_controlType = 0;
		}
	}


	QGraphicsView::mouseReleaseEvent(event);
}

void ROIGraphicsView::keyPressEvent(QKeyEvent* event)
{
	if (event->modifiers() & Qt::ControlModifier) {
		showToolTip();
	}
	QGraphicsView::keyPressEvent(event);
}

void ROIGraphicsView::keyReleaseEvent(QKeyEvent* event)
{
	bool f = event->modifiers() & Qt::ControlModifier;
	if (!f) {
		QToolTip::hideText();
	}
	if(!(event->modifiers() & Qt::AltModifier)) {
		if(m_histogram->isVisible()) {
			m_histogram->hide();
		}
	}
	QGraphicsView::keyReleaseEvent(event);
}


void ROIGraphicsView::contextMenuEvent(QContextMenuEvent* event)
{
	QMenu menu;
	bool f = false;
	QList<QAction*> actions = m_scene->getActions(mapToScene(event->pos()));
	menu.addActions(actions);
	if (menu.actions().size() > 0) {
		menu.exec(event->globalPos());
	}else
	{
		QGraphicsView::contextMenuEvent(event);
	}
	//
}

void ROIGraphicsView::calculateItemRect(const QRectF& _rect)
{
	
	bool f = false;
	if (m_showRect.left() > _rect.left()|| m_showRect.width()==0) {
		m_showRect.setLeft(_rect.left() );
		f = true;
	}
	if (m_showRect.right() < _rect.right()) {
		m_showRect.setRight(_rect.right() );
		f = true;
	}
	if (m_showRect.top() >_rect.top()|| m_showRect.height() ==0) {
		m_showRect.setTop(_rect.top() );
		f = true;
	}
	if (m_showRect.bottom() < _rect.bottom()) {
		m_showRect.setBottom(_rect.bottom() );
		f = true;
	}
	if (f) {
		scale(CalculateUtil::calcSuitableScal(m_showRect.size().toSize(), size()));
	}
}



void ROIGraphicsView::init()
{
	//设置图形抗锯齿模式
	setRenderHint(QPainter::Antialiasing);
	//设置不保持绘制状态例如 pen和 bursh
	// setOptimizationFlags(QGraphicsView::DontSavePainterState);
	//设置更新模式
	setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
	//设置鼠标放大缩小转换的锚点为鼠标点
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	//设置修改视图重设尺寸的锚点为鼠标点
	setResizeAnchor(QGraphicsView::AnchorUnderMouse);
	//鼠标在不按下的情况也能监控move
	setMouseTracking(true);
	//默认禁用滚动条
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//默认基准点在左上角
	setAlignment(Qt::AlignCenter);

	//背景的照片Item
	m_backImgItem = new QGraphicsPixmapItem();

	//拖选放大的item
	m_magnifyingRect = new MagnifyingRectItem();
	m_magnifyingRect->hide();
	//直方图组件
	m_histogram = new ImageHistogramWidget(this);
	m_histogram->hide();
	//绘制的场景
	m_scene = new RoiGraphicsScene(m_scale,this);
	m_scene->addItem(m_backImgItem);
	m_scene->addItem(m_magnifyingRect);
	QGraphicsView::setScene(m_scene);
	connect(this, &ROIGraphicsView::sig_scaleChanged, m_scene, &RoiGraphicsScene::slot_viewScaleChanged);


	m_clearAction = new QAction();
	m_clearAction->setText("清除绘制图像");
	connect(m_clearAction, &QAction::triggered, [=]()
	{
		clearItem();
	});

	 //初始化坐标文本显示
	m_coordinateTextItem = new QGraphicsTextItem();
	m_scene->addItem(m_coordinateTextItem);
	m_coordinateTextItem->setPos(100, 50);  // 设置文本显示的初始位置
	m_coordinateTextItem->setDefaultTextColor(Qt::green);  // 设置文本颜色
	m_coordinateTextItem->setFont(QFont("Arial", 100));
}

void ROIGraphicsView::showToolTip()
{
	//鼠标点转化为图上的左边点
	QPointF currentPos = mapToScene(m_mousePos.toPoint());

	//需要将原图坐标系转换为glass坐标系 ljx2022520

	QString str;
	str	+= QString(" 坐标(%1,%2)").arg(QString::number((int)round(currentPos.x()), 10)).arg(QString::number((int)round(currentPos.y()), 10));
	if(m_unit!="")
	{
		str += QString(" (%1,%2)").arg(QString::number(currentPos.x()*m_ratio,'f',2)+m_unit).arg(QString::number(currentPos.y()*m_ratio,'f', 2) + m_unit);
	}
	if (currentPos.x() >= 0 && currentPos.x() < m_orgMat.cols
		&&currentPos.y() >= 0 && currentPos.y() < m_orgMat.rows) {
		const cv::Vec3b& data = m_orgMat.at<cv::Vec3b>(currentPos.y(), currentPos.x());
		int b = data[0];
		int g = data[1];
		int r = data[2];
		str += QString("RGB:%1,%2,%3").arg(r).arg(g).arg(b);
		int gray = m_orgMat.at<uchar>(currentPos.y(), currentPos.x());
		str	+= QString(" 灰度值:%1").arg(gray);

	}
	
	QPoint currentTextPos = mapToGlobal(m_mousePos.toPoint());
	//冒泡框
	QToolTip::showText(currentTextPos, str);
}

