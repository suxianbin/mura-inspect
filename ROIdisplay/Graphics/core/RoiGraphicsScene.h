#pragma once
#ifndef _ROIGRAPHICSSCENE_FILE_H_
#define _ROIGRAPHICSSCENE_FILE_H_
#pragma execution_character_set("utf-8")
#include <QGraphicsScene>
#include "ROIAbstractBaseItem.h"
#include "DrawAbstractBaseItem.h"


/********************************
 * 功能 可以绘制RoiGraphics的Scene
 ********************************/
class RoiGraphicsScene : public QGraphicsScene
{
	Q_OBJECT
signals:
	void sig_viewScaleChanged(const qreal& _scale);

public slots:
	/********************************
	 * 功能 视窗缩放比例处理
	 ********************************/
	void slot_viewScaleChanged(const qreal&_scale);
public:
	RoiGraphicsScene(const qreal& _scale,QObject *parent = Q_NULLPTR);
	~RoiGraphicsScene() override;



	
	void addItem(QGraphicsItem *item);

	
	/********************************
	 * 功能 设置绘制的ROIItem
	 * 输入 _pItem 
	 ********************************/
	void addROIItem(ROIAbstractBaseItem* _pItem);
	
	/********************************
	 * 功能 添加绘制效果
	 * 输入 _pItem 
	 ********************************/
	void addDrawItem(DrawAbstractBaseItem<>* _pItem);


	void removeItem(QGraphicsItem *item);


	QList<ROIAbstractBaseItem*> getItemByState(ROIItemState state);


	/********************************
	 * 功能 清除Roi和DrawItem
	 ********************************/
	void clearItem();

	/********************************
	 * 功能 重置RoiItem的状态
	 ********************************/
	void resetSelectROI();

	QList<QAction*> getActions(const QPointF& _point);

	bool blockMouse();

	

private:
	QList<ROIAbstractBaseItem*> m_roiItems;						//绘制ROIitem
	QList<DrawAbstractBaseItem<>*> m_drawItems;							//绘制item
	qreal m_viewScale =1;
private:


	/********************************
	 * 功能 鼠标按下事件 会将按下鼠标的位置传递给正在绘制的RoiItem
	 * 输入 event 
	 ********************************/
	void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
	/********************************
	 * 功能 鼠标移动事件，会将鼠标位置记录在正在绘制的RoiItem中；
	 * 输入 event 
	 ********************************/
	void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;

	/********************************
	 * 功能 右键菜单事项
	 * 输入 event 
	 ********************************/
	//void contextMenuEvent(QGraphicsSceneContextMenuEvent* event) override;


	void setItemNormal(QList<ROIAbstractBaseItem*> _items);

private:
};
#endif