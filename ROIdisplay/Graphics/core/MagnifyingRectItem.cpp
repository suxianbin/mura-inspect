#include "MagnifyingRectItem.h"


MagnifyingRectItem::MagnifyingRectItem(QGraphicsItem *parent)
	: KeepPenWidthGraphicsItem<QGraphicsRectItem>(parent)
{
	/*
	 * 矩形框默认的样式
	 * 虚线边框，不会随着放大而增加线粗
	 * 半透明填充
	 * 
	 */
	QPen p;
	p.setWidth(2);
	p.setColor(Qt::darkGray);
	p.setStyle(Qt::DotLine);
	
	QBrush b;
	
	QColor color = Qt::gray;
	color.setAlpha(90);
	b.setColor(color);
	b.setStyle(Qt::SolidPattern);


	setPen(p);
	setBrush(b);
}

MagnifyingRectItem::~MagnifyingRectItem()
{
}

void MagnifyingRectItem::setRect(const QRectF& rect)
{
	setRect(rect.x(), rect.y(), rect.width(), rect.height());
}

void MagnifyingRectItem::setRect(qreal x, qreal y, qreal w, qreal h)
{
	if (w < 0) {
		w = 0;
	}
	if (h < 0) {
		h = 0;
	}
	KeepPenWidthGraphicsItem<QGraphicsRectItem>::setRect(x, y, w, h);
}
