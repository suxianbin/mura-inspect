#pragma once
#ifndef _ROIABSTRACTBASEITEM_FILE_H_
#define _ROIABSTRACTBASEITEM_FILE_H_
#pragma execution_character_set("utf-8")
#include <QObject>
#include <QGraphicsItem>
#include <QMenu>
#include "HandleControlItem.h"

enum ROIItemState
{
	NomaleState= 0,				//正常状态  没有选择
	SelectState,				//选中状态
	DrawingState				//绘制状态
};
/********************************
 * 功能 roi绘制基础抽象类
 ********************************/
class Q_DECL_EXPORT ROIAbstractBaseItem : public QObject,public QGraphicsPathItem
{
	Q_OBJECT
signals:
	/********************************
	 * 功能 roi绘制 完成，
	 ********************************/
	void sig_roiComplete();

	/********************************
	 * 功能 右键双击信号
	 ********************************/
	void sig_mouseRightDoubleClick();

	/********************************
	 * 功能 Roi被修改
	 ********************************/
	void sig_roiChanged();


	void sig_setStateSelected();

public slots:
	/********************************
	 * 功能 控制点移动事件处理槽函数
	 * 输入 index 索引
	 * 输入 type	类型
	 * 输入 event 鼠标事件
	 ********************************/
	virtual void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) = 0;


	void slot_scaleChanged(const qreal& _scale);
public:
	
	ROIAbstractBaseItem(QGraphicsItem* parent = Q_NULLPTR);
	~ROIAbstractBaseItem() override;
	
	/********************************
	 * 功能 每次点击界面添加的点
	 * 输入 _point 
	 ********************************/
	virtual void addPoint(const QPointF& _point) =0;
	
	/********************************
	 * 功能 鼠标移动事件
	 * 输入 event 
	 ********************************/
	virtual void mouseMove(QGraphicsSceneMouseEvent* event);

	/********************************
	 * 功能 设置Roi状态
	 * 输入 _state 
	 ********************************/
	void setState(ROIItemState _state);
	
	/********************************
	 * 功能 获取roi状态
	 * 返回 
	 ********************************/
	ROIItemState getState();


	/********************************
	 * 功能 
	 * 输入 _pen 
	 ********************************/
	void setPen(const QPen& _pen) ;
	
	/********************************
	 * 功能 
	 * 返回 
	 ********************************/
	QPen pen() const;

	/********************************
	 * 功能 获取右键菜单可重写
	 * 返回 
	 ********************************/
	virtual QList<QAction*> getActions();
	
	/********************************
	 * 功能 刷新区域范围
	 * 返回 
	 ********************************/
	QRectF boundingRect() const override;
	
	/********************************
	 * 功能 碰撞检测路径
	 * 返回 
	 ********************************/
	QPainterPath shape() const override;

	bool blockMouse();

protected:
	ROIItemState m_state;								//ROI绘制状态
	QPointF m_mouserPos;								//鼠标位置 scene坐标系
	QPen m_pen;											//绘制的笔   利用双笔画缓存减少绘制时频繁计算笔画的宽度
	QPen m_orgPen;										//设置的原始笔
	QList<HandleControlItem*> m_controlItems;			//控制点列表
	QList<QAction*> m_actions;							//右键菜单列表
	qreal m_viewScale = 1;
protected:

	/********************************
	 * 功能 获取主要绘制是碰撞检测路径
	 * 返回 
	 ********************************/
	virtual QPainterPath getMyShap() const = 0;

	/********************************
	 * 功能 获取控制点列表
	 * 返回 
	 ********************************/
	QList<HandleControlItem*> getControlItemList() const;

	/********************************
	 * 功能 添加控制点
	 * 输入 item 
	 ********************************/
	void addControlItem(HandleControlItem* item);

	/********************************
	 * 功能 移除控制点
	 * 输入 item 
	 ********************************/
	void removeControlItem(HandleControlItem* item);

	

	/********************************
	 * 功能 获取控制点根据索引
	 * 输入 _index 
	 * 返回 
	 ********************************/
	HandleControlItem* getItemByIndex(const int& _index) const;
	

	/********************************
	 * 功能 
	 ********************************/
	virtual void viewScaleChangedEvent();
};
#endif