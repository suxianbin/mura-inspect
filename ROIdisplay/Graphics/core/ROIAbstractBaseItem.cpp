#include "ROIAbstractBaseItem.h"
#include "ROIGraphicsView.h"
#include "KeepPenWidthGraphicsItem.h"


ROIAbstractBaseItem::ROIAbstractBaseItem(QGraphicsItem* parent):QGraphicsPathItem(parent)
{
	// setFlag(QGraphicsItem::ItemContainsChildrenInShape, true);

	setState(DrawingState);
	QPen p;
	p.setWidth(2);
	p.setColor(Qt::green);
	p.setStyle(Qt::SolidLine);
	setPen(p);
	//完成绘制action
	// QAction* actionFinish = new QAction(this);
	// actionFinish->setText("完成绘制");
	// connect(actionFinish,&QAction::triggered,[=]()
	// {
	// 	setState(NomaleState);
	// 	emit sig_roiComplete();
	// });
	// m_actions << actionFinish;
};

ROIAbstractBaseItem::~ROIAbstractBaseItem()
{
	
}

void ROIAbstractBaseItem::mouseMove(QGraphicsSceneMouseEvent* event)
{
	if(m_state==DrawingState|| (m_state == SelectState)) {
		//提示界面将要刷新
		//prepareGeometryChange();//[注:2023/8/23 20:00 LJX ]注释 测试测量崩溃，注释掉后暂时解决
		m_mouserPos = event->scenePos();
		update();//替代上面的实时更新效果
	}
	QGraphicsItem::mouseMoveEvent(event);
}

void ROIAbstractBaseItem::setState(ROIItemState _state)
{
	if (_state == SelectState) {
		emit sig_setStateSelected();
	}
	m_state = _state;
	QList<HandleControlItem*> items = getControlItemList();
	if(m_state==NomaleState) {
		for(int i=0;i<items.size();i++) {
			items[i]->hide();
		}
	}else {
		for (int i = 0; i < items.size(); i++) {
			items[i]->show();
		}
	}
	if(m_state==SelectState) {
		//setToolTip("右键可以完成此ROI的绘制");
	}else {
		setToolTip("");

	}
}

ROIItemState ROIAbstractBaseItem::getState()
{
	return m_state;
}


void ROIAbstractBaseItem::setPen(const QPen& _pen)
{
	m_orgPen = _pen;
	m_pen = m_orgPen;
	m_pen.setWidthF(m_orgPen.widthF() / m_viewScale);
}

QPen ROIAbstractBaseItem::pen()const
{
	return m_pen;
}

QList<QAction*> ROIAbstractBaseItem::getActions()
{
	return m_actions;
}


QRectF ROIAbstractBaseItem::boundingRect() const
{
	QRectF res = shape().boundingRect();
	return res;
}

QPainterPath ROIAbstractBaseItem::shape() const
{
	QPainterPath  path = getMyShap();
	path = mapFromItem(this, path);
	const QList<HandleControlItem*>& points = getControlItemList();
	for (int i = 0; i < points.size(); i++) {
		//mapFromItem 把子类的矩形坐标转到父类的坐标系
		path.addPath(mapFromItem(points[i], points[i]->shape()));
	}
	return path;
}

bool ROIAbstractBaseItem::blockMouse()
{
	bool f = false;
	if (m_state == DrawingState)
	{
		f = true;
	}

	if(m_state==SelectState)
	{
		QPointF p2 = mapFromScene(m_mouserPos);
		for (int i = 0; i < m_controlItems.size(); i++)
		{
			QPointF p3 = mapToItem(m_controlItems[i],p2);
			if (m_controlItems[i]->contains(p3))
			{
				f = true;
				break;
			}
		}
	}
	return f;
}


QList<HandleControlItem*> ROIAbstractBaseItem::getControlItemList() const
{
	return m_controlItems;
}

void ROIAbstractBaseItem::addControlItem(HandleControlItem* item)
{
	QObject::connect(item, &HandleControlItem::sig_controlItemChanged, [=](int index, int type, QGraphicsSceneMouseEvent* event)
	{
		if(m_state ==SelectState) {
			//prepareGeometryChange();//[注:2023/8/24 10:14 LJX ]
			slot_controlItemChanged(index, type, event);
			emit sig_roiChanged();
			update();
		}
	});

	QObject::connect(item, &HandleControlItem::sig_mouseRightDoubleClick, this,&ROIAbstractBaseItem::sig_mouseRightDoubleClick);

	m_controlItems.push_back(item);
}

void ROIAbstractBaseItem::removeControlItem(HandleControlItem* item)
{
	m_controlItems.removeOne(item);
	item->deleteLater();
	item = Q_NULLPTR;
}



HandleControlItem* ROIAbstractBaseItem::getItemByIndex(const int& _index) const
{
	for (auto item = m_controlItems.begin(); item != m_controlItems.end(); ++item) {
		HandleControlItem*  it = item.i->t();
		if (it->index() == _index) {
			return item.i->t();
		}
	}
	return Q_NULLPTR;
}


void ROIAbstractBaseItem::viewScaleChangedEvent()
{
}


void ROIAbstractBaseItem::slot_scaleChanged(const qreal& _scale)
{
	m_viewScale = _scale;
	m_pen.setWidthF(m_orgPen.widthF() / m_viewScale);
	viewScaleChangedEvent();
}
