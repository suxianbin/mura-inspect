#include "HandleControlItem.h"
#include "ImageUtil.h"

HandleControlItem::HandleControlItem(const QPointF& _point, QGraphicsItem* parent, int _index, eControlItemType _type)
	:QGraphicsPathItem(parent), m_parentItem(parent), m_index(_index), m_type(_type)
{
	setPos(_point);
	init();
}

HandleControlItem::~HandleControlItem()
{
}

void HandleControlItem::setPixMap(const QPixmap& _image, const QSize& size)
{
	m_pixmap = _image;
	m_pixmapRect.setWidth(size.width());
	m_pixmapRect.setHeight(size.height());
	m_pixmapRect.setX(-size.width() / 2);
	m_pixmapRect.setY(-size.height() / 2);
}

int HandleControlItem::index()
{
	return m_index;
}

void HandleControlItem::setIndex(int _index)
{
	m_index = index();
}

void HandleControlItem::init()
{
	m_brush.setStyle(Qt::SolidPattern);
	m_brush.setColor(Qt::blue);
	m_pen.setColor(Qt::red);
	m_pen.setWidthF(1);
	m_pen.setStyle(Qt::SolidLine);
	setPen(m_pen);
	setBrush(m_brush);
	hide();
	m_topLeft.setX(-HandleControlItemSize / 2);
	m_topLeft.setY(-HandleControlItemSize / 2);
	m_size = HandleControlItemSize;
	setAcceptHoverEvents(true);
	//忽略放大缩小等时间
	setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
}

void HandleControlItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if (parentItem() != Q_NULLPTR) {
		painter->setPen(m_pen);
		painter->setBrush(m_brush);
		QPointF  points[4];
		switch (m_type) {
		case eControlItemType::Nomal:
			//正常控制点圆形
			painter->drawEllipse(m_topLeft.rx(), m_topLeft.ry(), m_size, m_size);
			break;
		case eControlItemType::Rotate:
			//画旋转圆弧
			painter->drawArc(m_topLeft.rx() * 2, m_topLeft.ry() * 2, m_size * 2, m_size * 2, 270, 180);
			break;
		case eControlItemType::Base:
			//菱形
			points[0] = QPointF(0, m_topLeft.ry());//上
			points[1] = QPointF(-m_topLeft.rx(), 0);//右
			points[2] = QPointF(0, -m_topLeft.ry());//下
			points[3] = QPointF(m_topLeft.rx(), 0);//左
			painter->drawPolygon(points, 4, Qt::OddEvenFill);
			break;
		case eControlItemType::Image:
			painter->drawPixmap(m_pixmapRect, m_pixmap, m_pixmap.rect());
			break;
		default:
			//画正方形
			painter->drawRect(m_topLeft.rx(), m_topLeft.ry(), m_size, m_size);
			break;
		}
	}
}

eControlItemType HandleControlItem::getType()
{
	return m_type;
}

// void HandleControlItem::move(qreal x, qreal y)
// {
// 	setPos(x,y);
// }

void HandleControlItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	m_pen.setColor(Qt::yellow);
	m_brush.setColor(Qt::yellow);
	// QGraphicsPathItem::mousePressEvent(event);
}

void HandleControlItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
	//左键按下可移动
	if ((event->buttons()&Qt::LeftButton)) {
		emit sig_controlItemChanged(m_index, (int)m_type, event);
		scene()->setFocus();
	}
	 //QGraphicsPathItem::mouseMoveEvent(event);
}

void HandleControlItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
	m_pen.setColor(Qt::red);
	m_brush.setColor(Qt::blue);
	// QGraphicsPathItem::mouseReleaseEvent(event);
}

void HandleControlItem::hoverEnterEvent(QGraphicsSceneHoverEvent* e)
{
	//设置鼠标形状
	switch (m_type) {
	case eControlItemType::Base:
		setCursor(Qt::SizeAllCursor);
		break;
	case eControlItemType::Nomal:
		setCursor(Qt::PointingHandCursor);
		break;
	case eControlItemType::Image:
		setCursor(Qt::SizeAllCursor);
		break;
	default:
		;
	}
	QGraphicsPathItem::hoverEnterEvent(e);
}

void HandleControlItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* e)
{
	//还原鼠标形状
	setCursor(Qt::ArrowCursor);
	QGraphicsPathItem::hoverLeaveEvent(e);
}

void HandleControlItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
{
	if (event->button() == Qt::RightButton) {
		emit sig_mouseRightDoubleClick();
	}
}

QPainterPath HandleControlItem::shape() const
{
	QPainterPath path;
	QVector<QPointF> points;

	QPointF point = m_topLeft;
	qreal size = m_size;
	switch (m_type) {
	case eControlItemType::Nomal:
		path.addEllipse(point.rx(), point.ry(), size, size);
		break;
	case eControlItemType::Rotate:
		//画旋转圆弧
		path.addEllipse(point.x() * 2, point.y() * 2, size * 2, size * 2);
		break;
	case eControlItemType::Base:
		points << QPointF(0, point.y());//上
		points << QPointF(-point.x(), 0);//右
		points << QPointF(0, -point.y());//下
		points << QPointF(point.x(), 0);//左
		path.addPolygon(QPolygonF(points));
		break;
	case eControlItemType::Image:
		path.addRect(m_pixmapRect);
	default:
		//画正方形
		path.addRect(point.x(), point.y(), size, size);
		break;
	}
	// return ImageUtil::shapeFromPathContentsInclude(path, pen());
	return path;
}

QRectF HandleControlItem::boundingRect() const
{
	return shape().boundingRect();
}