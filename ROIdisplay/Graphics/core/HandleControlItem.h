#pragma once
#ifndef _HANDLECONTROLITEM_FILE_H_
#define _HANDLECONTROLITEM_FILE_H_
#include <QObject>
#include <QGraphicsRectItem>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QDebug>

#define HandleControlItemSize 10

/********************************
 * 功能 控制点的类型
 ********************************/
enum class eControlItemType
{
	Nomal = 0,			//用于图形形状控制的点  圆形
	Rotate,				//旋转
	Base,				//基准点一般用于移动位置
	Image				//显示图片

};

/********************************
 * 功能 ROI的控制Item
 ********************************/
class Q_DECL_EXPORT HandleControlItem :public QObject, public QGraphicsPathItem
{
	Q_OBJECT
signals:
	/********************************
	 * 功能 控制点移动信号
	 * 输入 index 控制点的索引号
	 * 输入 type 控制点类型
	 * 输入 event 移动的鼠标事件
	 ********************************/
	void sig_controlItemChanged(int index,int type, QGraphicsSceneMouseEvent* event);

	/********************************
	 * 功能 鼠标右键双击信号
	 ********************************/
	void sig_mouseRightDoubleClick();
public:
	/********************************
	 * 功能 ROI item的控制点
	 * 输入 _point 在父类中的点
	 * 输入 parent 父Item
	 * 输入 _index 索引
	 * 输入 _type 类型
	 ********************************/
	HandleControlItem(const QPointF& _point, QGraphicsItem *parent, int _index = -1,eControlItemType _type = eControlItemType::Nomal);
	~HandleControlItem() override;

	/********************************
	 * 功能 设置显示
	 * 输入 _image 
	 * 输入 size 
	 ********************************/
	void setPixMap(const QPixmap& _image, const QSize& size);

	/********************************
	 * 功能 获取控制点索引
	 * 返回 
	 ********************************/
	int index();
	
	/********************************
	 * 功能 设置控制点索引
	 * 输入 _index 
	 ********************************/
	void setIndex(int _index);

	/********************************
	 * 功能 获取控制点边界，用于鼠标移入移出 ，点击等判定
	 * 返回 
	 ********************************/
	QPainterPath shape() const override;

	/********************************
	 * 功能 
	 * 返回 
	 ********************************/
	QRectF boundingRect() const override;
	
	/********************************
	 * 功能 获取控制点类型
	 * 返回 
	 ********************************/
	eControlItemType getType();
protected:
	QMap<int, QVariant> m_dataMap;					//控制点 自定义数据
	int m_index = 0;								//索引号
	eControlItemType m_type;							//类型
	QBrush m_brush;									//绘制的刷子
	QPen m_pen;										//绘制的笔
	QPointF m_topLeft;								//绘制点的左上角左边一般为size的一半  用于绘制的图像中心在设置的点上。
	qreal m_size;									//大小根据放大缩小变化
	QGraphicsItem* m_parentItem = Q_NULLPTR;		//父类
	QPixmap m_pixmap;
	QRect m_pixmapRect;
private:
	/********************************
	 * 功能 初始化方法
	 ********************************/
	void init();

	/********************************
	 * 功能 绘制事件重写
	 * 输入 painter 
	 * 输入 option 
	 * 输入 widget 
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
	
	/********************************
	 * 功能 鼠标按下事件
	 * 输入 event 
	 ********************************/
	void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
	
	/********************************
	 * 功能 鼠标移动事件
	 * 输入 event 
	 ********************************/
	void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
	
	/********************************
	 * 功能 鼠标松开事件
	 * 输入 event 
	 ********************************/
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
	
	/********************************
	 * 功能 鼠标移入事件
	 * 输入 e 
	 ********************************/
	void hoverEnterEvent(QGraphicsSceneHoverEvent *e) override;
	
	/********************************
	 * 功能 鼠标移出事件
	 * 输入 e 
	 ********************************/
	void hoverLeaveEvent(QGraphicsSceneHoverEvent *e) override;
	
	/********************************
	 * 功能 鼠标双击事件
	 * 输入 event 
	 ********************************/
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) override;
};
#endif