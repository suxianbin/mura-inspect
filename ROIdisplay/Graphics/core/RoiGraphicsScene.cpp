#include "RoiGraphicsScene.h"



RoiGraphicsScene::RoiGraphicsScene(const qreal& _scale, QObject *parent)
	: QGraphicsScene(parent), m_viewScale(_scale)
{
	setBackgroundBrush(Qt::darkGray);
	
}

RoiGraphicsScene::~RoiGraphicsScene()
{
}

void RoiGraphicsScene::addItem(QGraphicsItem* item)
{
	bool f = true;
	DrawAbstractBaseItem<>* dItem = dynamic_cast<DrawAbstractBaseItem<>*>(item);
	if (dItem != Q_NULLPTR) {
		addDrawItem(dItem);
		f = false;
	}

	ROIAbstractBaseItem* rItem = dynamic_cast<ROIAbstractBaseItem*>(item);
	if (rItem != Q_NULLPTR) {
		addROIItem(rItem);
		f = false;
	}
	if(f) {
		QGraphicsScene::addItem(item);
	}
	
}


void RoiGraphicsScene::addROIItem(ROIAbstractBaseItem* _pItem)
{
	resetSelectROI();
	m_roiItems << _pItem;
	_pItem->slot_scaleChanged(m_viewScale);
	connect(this, &RoiGraphicsScene::sig_viewScaleChanged, 
		_pItem, &ROIAbstractBaseItem::slot_scaleChanged);
	connect(_pItem, &ROIAbstractBaseItem::sig_setStateSelected,
		[=]()
		{
			resetSelectROI();
		});

	QGraphicsScene::addItem(_pItem);
}

void RoiGraphicsScene::addDrawItem(DrawAbstractBaseItem<>* _pItem)
{
	m_drawItems << _pItem;
	QGraphicsScene::addItem((QGraphicsItem*)_pItem);
}

void RoiGraphicsScene::removeItem(QGraphicsItem* item)
{
	DrawAbstractBaseItem<>* dItem = dynamic_cast<DrawAbstractBaseItem<>*>(item);
	if (dItem != Q_NULLPTR) {
		m_drawItems.removeOne(dItem);
	}

	ROIAbstractBaseItem* rItem = dynamic_cast<ROIAbstractBaseItem*>(item);
	if (rItem != Q_NULLPTR) {
		m_roiItems.removeOne(rItem);
	}
	if (item != Q_NULLPTR)
	{
		QGraphicsScene::removeItem(item);
		QGraphicsScene::update();
		//QGraphicsScene::clear();
	}
}


void RoiGraphicsScene::clearItem()
{
	clear();
	m_roiItems.clear();
	m_drawItems.clear();
}

void RoiGraphicsScene::resetSelectROI()
{
	QList<ROIAbstractBaseItem*> items = getItemByState(SelectState);
	setItemNormal(items);
}

QList<QAction*> RoiGraphicsScene::getActions(const QPointF& _point)
{
	QList<QAction*> res;
	for (int i = 0; i < m_roiItems.size(); i++) {
		ROIAbstractBaseItem* item = m_roiItems[i];
		const QPointF& p = item->mapFromScene(_point);
		QRectF r = item->boundingRect();
		if (r.contains(p) && item->getState() !=NomaleState) {
			res<< item->getActions();
		}
	}
	return res;
}

bool RoiGraphicsScene::blockMouse()
{
	bool f = false;

	foreach(ROIAbstractBaseItem* item, m_roiItems)
	{
		if(item->blockMouse())
		{
			f = true;
			break;
		}
	}
	return f;
}


void RoiGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	QList<ROIAbstractBaseItem*> items = getItemByState(DrawingState);
	if (event->button() == Qt::LeftButton&& items .size()>0) {
		if (items.size() > 0) {
			foreach(ROIAbstractBaseItem* item, items)
			{
				item->addPoint(event->scenePos());
			}
		}
	}
	QGraphicsScene::mousePressEvent(event);
}

void RoiGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{

	QList<ROIAbstractBaseItem*> itemsDrawing = getItemByState(DrawingState);
	QList<ROIAbstractBaseItem*> itemsSelect = getItemByState(SelectState);

	if (event->button() == Qt::NoButton && itemsDrawing.size() > 0) {
		QList<ROIAbstractBaseItem*> itemsDrawingCopy = itemsDrawing; // Create a copy of the list
		foreach(ROIAbstractBaseItem * item, itemsDrawingCopy) {
			item->mouseMove(event);
		}
	}
	if (event->button() == Qt::LeftButton && itemsSelect.size() > 0) {
		QList<ROIAbstractBaseItem*> itemsSelectCopy = itemsSelect; // Create a copy of the list
		foreach(ROIAbstractBaseItem * item, itemsSelectCopy) {
			item->mouseMove(event);
		}
	}

	//[注:2023/8/23 16:30 LJX ] 为了解决测量崩溃，注释掉下面的代码换成上面的代码，是否有效还需测试；
	/*QList<ROIAbstractBaseItem*> itemsDrawing = getItemByState(DrawingState);
	if (event->button() == Qt::NoButton && itemsDrawing.size() > 0) {
		foreach(ROIAbstractBaseItem * items, itemsDrawing)
		{
			items->mouseMove(event);
		}
	}

	itemsDrawing.clear();
	QList<ROIAbstractBaseItem*> itemsSelect = getItemByState(SelectState);
	if (event->button() == Qt::LeftButton && itemsSelect.size() > 0) {
		foreach(ROIAbstractBaseItem * item, itemsSelect)
		{
			item->mouseMove(event);
		}
	}
	itemsSelect.clear();
	*/
	QGraphicsScene::mouseMoveEvent(event);
}

void RoiGraphicsScene::setItemNormal(QList<ROIAbstractBaseItem*> _items)
{
	foreach(ROIAbstractBaseItem* item, _items)
	{
		item->setState(NomaleState);
	}
}

QList<ROIAbstractBaseItem*> RoiGraphicsScene::getItemByState(ROIItemState state)
{
	QList<ROIAbstractBaseItem*> res;
	foreach(ROIAbstractBaseItem* item , m_roiItems)
	{
		if(item->getState()== state) {
			res << item;
		}
	}
	return res;
}

void RoiGraphicsScene::slot_viewScaleChanged(const qreal& _scale)
{
	m_viewScale = _scale;
	emit sig_viewScaleChanged(_scale);
}
