#pragma once
#ifndef _DRAWABSTRACTBASEITEM_FILE_H_
#define _DRAWABSTRACTBASEITEM_FILE_H_
#include <QObject>
#include <QPen>
#include "KeepPenWidthGraphicsItem.h"

/********************************
 * 功能 绘制图形的基本类
 * 提供了设置绘制颜色和保证视觉线宽的绘制
 ********************************/
template <typename BaseType = QAbstractGraphicsShapeItem>
class  DrawAbstractBaseItem :public KeepPenWidthGraphicsItem<BaseType>//,public DrawBaseItem
{
public:

	explicit DrawAbstractBaseItem(QGraphicsItem* parent = Q_NULLPTR) :KeepPenWidthGraphicsItem<BaseType>(parent)
	{
		QPen p;
		p.setWidth(2);
		p.setStyle(Qt::SolidLine);
		//setPen(p);
	};

	virtual void setColor(const QColor&_color)
	{
		//QPen p = pen();
		//p.setColor(_color);
		//setPen(p);
	};
};
#endif