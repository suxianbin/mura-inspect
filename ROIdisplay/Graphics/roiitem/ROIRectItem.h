#pragma once
#ifndef _ROIRECTITEM_FILE_H_
#define _ROIRECTITEM_FILE_H_
#include <QObject>
#include "ROIAbstractBaseItem.h"


class Q_DECL_EXPORT ROIRectItem : public ROIAbstractBaseItem
{
	Q_OBJECT

public:
	ROIRectItem(QGraphicsItem *parent = Q_NULLPTR);
	~ROIRectItem() override;

	/********************************
	* 功能 控制点点击处理
	* 输入 _point
	********************************/
	void addPoint(const QPointF& _point) override;
	void setData(const QRectF& rect);
	QRectF getData();


public slots:
	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;

private:
	int m_addPointIndex = 0;
	qreal m_width =0;					//宽度
	qreal m_height = 0;					//高度

private:
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	/********************************
	 * 功能 获取本身的shap
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;
};
#endif