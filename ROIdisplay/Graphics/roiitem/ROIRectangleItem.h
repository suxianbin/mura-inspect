#pragma once
#ifndef _ROIRECTANGLEITEM_FILE_H_
#define _ROIRECTANGLEITEM_FILE_H_
#include <QObject>
#include <QGraphicsItem>
#include "ROIAbstractBaseItem.h"

#define TRIANGLESIZE 5
/********************************
 * 功能 扫描矩形Roi
 ********************************/
struct ROIRectangleData
{
	QPointF p1;
	QPointF p2;
	qreal   offset;      //以pt1pt2为轴线，向该轴线两侧法线方向的扩展偏移量
	bool   isClockwise;   //true表示沿着pt1 pt2的方向的顺时针方向扫描，为false则是逆时针方向扫描
};

class Q_DECL_EXPORT ROIRectangleItem : public ROIAbstractBaseItem
{
	Q_OBJECT
public:
	ROIRectangleItem();
	~ROIRectangleItem() override;

	/********************************
	 * 功能 控制点点击处理
	 * 输入 _point
	 ********************************/
	void addPoint(const QPointF& _point) override;
	void setData(const ROIRectangleData& _data);
	ROIRectangleData getData();
public slots:
	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;

private:
	int m_addPointIndex = 0;
	qreal m_rotate = 0;				//旋转角度
	qreal m_width = 0;				//宽度
	qreal m_height = 0;				//高度
	bool m_positive = false;		//扫描矩形的方向，延基准点到第一个点的右边是正方向


	QVector<QPointF> m_arrow;

private:
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	/********************************
	 * 功能 计算旋转角度和宽度
	 * 输入 pointf 
	 ********************************/
	void calcWidthAndRote(const QPointF& pointf);

	
	/********************************
	 * 功能 获取本身的shap
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;

	void viewScaleChangedEvent() override;
};
#endif