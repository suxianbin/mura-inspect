#pragma once
#ifndef _ROIPOLYGONITEM_FILE_H_
#define _ROIPOLYGONITEM_FILE_H_
#include <QObject>
#include <QGraphicsItem>
#include <QToolTip>
#include "ROIAbstractBaseItem.h"

/********************************
 * 功能 Roi 多点区域
 ********************************/
class Q_DECL_EXPORT ROIPolygonItem : public ROIAbstractBaseItem
{
	Q_OBJECT

public:
	ROIPolygonItem();
	~ROIPolygonItem() override;

	/********************************
	 * 功能 添加节点
	 * 输入 _point
	 ********************************/
	void addPoint(const QPointF& _point) override;

	void setData(const QList<QPointF>& points);
	QList<QPointF> getData();

	/********************************
	 * 功能 重写获取控制菜单针对多边形提供新的选项
	 * 返回 
	 ********************************/
	QList<QAction*> getActions() override;
public slots:
	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;

private:
	int m_addPointIndex = 0;									
	QAction* m_removeOneAction = Q_NULLPTR;				//移除控制点菜单
	// QAction* m_finishAction = Q_NULLPTR;				//完成菜单 		

	/********************************
	 * 功能 重写绘制
	 * 输入 painter 
	 * 输入 option 
	 * 输入 widget 
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	/********************************
	 * 功能 获取本身的shap
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;

	

	// void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
};
#endif