#pragma once
#ifndef _ROIARCITEM_FILE_H_
#define _ROIARCITEM_FILE_H_
#pragma execution_character_set("utf-8")
#include <QObject>
#include <QtMath>
#include "ROIAbstractBaseItem.h"

#ifndef OUT
#define OUT
#endif

struct ROIArcData                     
{
	QPointF center;						//圆心
	qreal radius = -1;						//半径
	qreal startAngle =-1;					//起始角度
	qreal endAngle=-1;						//终止角度
	qreal offset=-1;						//扫描半径
	bool isPositive =false;					//true 正向 向外扩 false 负向 向内收缩
};
/********************************
 * 功能 扫描弧形RoiItem
 ********************************/
class Q_DECL_EXPORT ROIArcItem : public ROIAbstractBaseItem
{
	Q_OBJECT

public:
	ROIArcItem();
	~ROIArcItem() override;


	/********************************
	 * 功能 每次点击界面添加的点
	 * 输入 _point
	 ********************************/
	void addPoint(const QPointF& _point) override;
	
	void setData(const ROIArcData& _data);
	ROIArcData getData();

public slots:
	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;

private:
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	/********************************
	 * 功能 计算点到圆心的距离与 半径的差
	 * 输入 _point 
	 ********************************/
	void calculateRadiusDiffer(const QPointF& _point);

	/********************************
	 * 功能 获取中轴线第四点的位置
	 * 返回
	 ********************************/
	QPointF getCenterLinePoint();


	/********************************
	 * 功能 弧线
	 * 输入 _endPoint 
	 ********************************/
	void calcArcParameter(const QPointF& _endPoint) ;

	/********************************
	 * 功能 获取本身的shap
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;


	/********************************
	 * 功能 计算圆心到经过点到最外弧的连线的终点
	 * 输入 index  经过控制点的索引
	 * 返回 
	 ********************************/
	void getCenterToEnd(int index);

	/********************************
	 * 功能 计算弧的范围
	 * 输入 radius 
	 * 输入 _rect 
	 ********************************/
	void calcArcRect(const qreal& radius, OUT QRectF& _rect);

	void calcArrow();
	
	void viewScaleChangedEvent() override;

private:
	int m_addPointIndex = 0;					//当前绘制点的序号
	qreal m_radius = 0;							//扫描基准圆半径
	QPointF m_centPoint;						//圆心
	qreal m_radiusDiffer = 0;					//扫描半径 和基准圆半径的差值
	qreal m_startAngle = 0;						//起始角度
	qreal m_midAngle = 0;						//中间一点的角度
	qreal m_endAngle = 0;						//结束点的角度
	qreal m_arcLen = 0;							//绘制的弧长
	QRectF m_baseRect;							//绘制弧的基础范围
	QRectF m_externalRect;						//绘制外扩弧的范围
	QRectF m_insideRect;						//绘制内缩弧的范围
	int m_isClockwise = 0;						//判断按照顺序的三个点是否是顺时针旋转 1顺时针 ，0一条线 ，-1逆时针
	bool m_positive = false;							//第四点远离中心 为正 靠近圆心为负

	QVector<QPointF> m_externPoint;				//圆心经过前三个控制点到外弧上的焦点，用于绘制直线
	QVector<QPointF> m_arrows;					//箭头
};
#endif