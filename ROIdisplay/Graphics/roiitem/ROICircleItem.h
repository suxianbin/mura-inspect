#pragma once
#ifndef _ROICIRCLEITEM_FILE_H_
#define _ROICIRCLEITEM_FILE_H_
#pragma execution_character_set("utf-8")
#include <QObject>


#include "ROIAbstractBaseItem.h"

struct ROICircleData						//环形ROI
{
	QPointF center;
	qreal radius =-1;					//基准半径
	qreal offset = -1;					//偏移量，即内半径为radius-offset,外半径为radius+offset
	bool isPostive = true;			//是否是正向偏移，向外偏移
};


/********************************
 * 功能 环形扫描Item
 ********************************/
class Q_DECL_EXPORT ROICircleItem : public ROIAbstractBaseItem
{
	Q_OBJECT
public:
	ROICircleItem();
	~ROICircleItem() override;

	/********************************
	 * 功能 添加节点
	 * 输入 _point 
	 ********************************/
	void addPoint(const QPointF& _point) override;

	void setData(const ROICircleData& data);
	ROICircleData getData();
public slots:
	
	/********************************
	 * 功能 节点移动处理
	 * 输入 index 
	 * 输入 type 
	 * 输入 event 
	 ********************************/
	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;
private:
	/********************************
	 * 功能 重写绘制事件
	 * 输入 painter 
	 * 输入 option 
	 * 输入 widget 
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	/********************************
	 * 功能 计算扫描环与基准圆的半径差
	 * 输入 _point 
	 ********************************/
	void calculateRadiusDiffer(const QPointF& _point);

	/********************************
	 * 功能 获取中轴线第四点的位置
	 * 返回 
	 ********************************/
	QPointF getCenterLinePoint() const;

	/********************************
	 * 功能 获取本身的shap
	 * 返回
	 ********************************/
	QPainterPath getMyShap() const override;


	/********************************
	 * 功能 计算圆心经过第二点到外弧的交点和第四点的箭头
	 ********************************/
	void calcLineEndPoint();

	void calcArrows();

	void viewScaleChangedEvent() override;
private:
	int m_addPointIndex = 0;
	qreal m_radius;				//扫描基准圆半径
	QPointF m_centPoint;
	qreal m_radiusDiffer;			//扫描半径 和基准圆半径的差值
	bool m_positive;				//第四点远离中心 为正 靠近圆心为负

	QPointF m_lineEndPoint;			//连线的终点

	QVector<QPointF> m_arrow;
};
#endif