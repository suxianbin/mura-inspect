#include "ROICircleItem.h"
#include "CalculateUtil.h"
#include "ImageUtil.h"


ROICircleItem::ROICircleItem()
	: ROIAbstractBaseItem()
{
}

ROICircleItem::~ROICircleItem()
{
	
}


void ROICircleItem::addPoint(const QPointF& _point)
{
	if (m_addPointIndex == 0) {
		//第一个点
		HandleControlItem* item0 = new HandleControlItem(QPointF(0, 0), this, 0);
		addControlItem(item0);
		setPos(_point);
		item0->show();

		//第二个点 显示鼠标位置
		HandleControlItem* item1 = new HandleControlItem(QPointF(0, 0), this, 1);
		addControlItem(item1);
		item1->show();

		m_addPointIndex++;
	} else if (m_addPointIndex == 1) {
		//第二个点 确定位置
		QPointF p = mapFromScene(_point);
		getItemByIndex(1)->setPos(p);


		//第三个点 显示鼠标位置
		HandleControlItem* item2 = new HandleControlItem(QPointF(0, 0), this, 2);
		addControlItem(item2);
		item2->show();

		// 圆心base点 有三个点就可以显示圆心点了
		HandleControlItem* itembase = new HandleControlItem(QPointF(0, 0), this, -1, eControlItemType::Base);
		addControlItem(itembase);
		itembase->show();

		m_addPointIndex++;
	} else if (m_addPointIndex == 2) {
		//第三个个点 确定位置
		QPointF p = mapFromScene(_point);
		getItemByIndex(2)->setPos(p);

		//第二个点 确定圆心半径等信息
		CalculateUtil::getThreePtCircle(getItemByIndex(0)->pos(), getItemByIndex(1)->pos(), p, m_centPoint, m_radius);
		getItemByIndex(-1)->setPos(m_centPoint);

		//第四个点  扫用范围用的同心圆
		HandleControlItem* item3 = new HandleControlItem(QPointF(0, 0), this, 3);
		addControlItem(item3);
		item3->show();
		
		m_addPointIndex++;
	} else if (m_addPointIndex == 3) {
		//第四个点 确定位置
		QPointF p = mapFromScene(_point);
		//计算 两个圆的半径
		calculateRadiusDiffer(p);
		//第四个点的位置
		getItemByIndex(3)->setPos(getCenterLinePoint());
		//计算中点连线
		calcLineEndPoint();
		
		setState(SelectState);
		m_addPointIndex++;
		// emit sig_roiComplete();
	}
}

void ROICircleItem::setData(const ROICircleData& data)
{
	if (data.offset > 0 && data.radius > 0) {
		//上
		QPointF p0 = data.center;
		p0.setY(p0.y() - data.radius);
		addPoint(p0);

		//右
		QPointF p1 = data.center;
		p1.setX(p1.x() + data.radius);
		addPoint(p1);

		//下
		QPointF p2 = data.center;
		p2.setY(p2.y() + data.radius);
		addPoint(p2);

		//上
		QPointF p3 = data.center;
		if (data.isPostive) {
			p0.setY(p0.y() - data.radius - data.offset);
		} else {
			p0.setY(p0.y() - data.radius + data.offset);

		}
		addPoint(p3);
	}
}

ROICircleData ROICircleItem::getData()
{
	ROICircleData data;
	data.radius    = m_radius;
	data.center    = mapToScene(m_centPoint);
	data.isPostive = m_positive;
	data.offset    = m_radiusDiffer;
	return data;
}


void ROICircleItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF point = event->scenePos();
	if (index != -1) {
		point = mapFromScene(point);
		getItemByIndex(index)->setPos(point);
		//第二个点 确定圆心半径等信息
		CalculateUtil::getThreePtCircle(getItemByIndex(0)->pos(), getItemByIndex(1)->pos(), getItemByIndex(2)->pos()
		                              , m_centPoint, m_radius);
		calculateRadiusDiffer(getItemByIndex(3)->pos());
		getItemByIndex(-1)->setPos(m_centPoint);
		getItemByIndex(3)->setPos(getCenterLinePoint());
		
		//计算中点连线
		calcLineEndPoint();
		calcArrows();
	} else {
		//圆心位置移动
		QPointF basePoint = getItemByIndex(-1)->pos();
		basePoint         = point - basePoint;
		setPos(basePoint);
	}
}

void ROICircleItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	QPointF point = mapFromScene(m_mouserPos);
	painter->setPen(pen());
	if (m_addPointIndex == 1) {
		//确定了第一点 用鼠标位置做第二点
		painter->drawLine(0, 0, point.rx(), point.ry());
		getItemByIndex(1)->setPos(point);
		
	} else if (m_addPointIndex == 2) {
		//确定了 1 2点用鼠标位置做第三点，计算出 圆心和半径
		CalculateUtil::getThreePtCircle(getItemByIndex(0)->pos(), getItemByIndex(1)->pos(),
		                                point, m_centPoint, m_radius);

		//第三点缓存位置
		getItemByIndex(2)->setPos(point);
		getItemByIndex(-1)->setPos(m_centPoint);
		
	} else if (m_addPointIndex == 3) {
		
		//计算扫描的offset
		calculateRadiusDiffer(point);
		getItemByIndex(3)->setPos(getCenterLinePoint());

		//计算中点连线
		calcLineEndPoint();
		
	}


	//提前计算 减少在绘制时的计算
	if (m_addPointIndex >= 2) {
		//根据前三个点画圆
		painter->drawEllipse(m_centPoint, m_radius, m_radius);
	}
		
	if (m_addPointIndex >=3) {
		//绘制扫描圆
		painter->drawEllipse(m_centPoint, m_radius + m_radiusDiffer, m_radius + m_radiusDiffer);
		painter->drawEllipse(m_centPoint, m_radius - m_radiusDiffer, m_radius - m_radiusDiffer);

		//绘制中点连线
		painter->drawLine(m_centPoint, m_lineEndPoint);

		QBrush b(pen().color());
		painter->setBrush(b);
		painter->drawPolygon(m_arrow);
	}
}

void ROICircleItem::calculateRadiusDiffer(const QPointF& _point)
{
	//点到圆心的距离
	QPointF p = _point - m_centPoint;
	qreal r   = pow(pow(p.x(), 2) + pow(p.y(), 2), 0.5);
	if (r > m_radius) {
		m_positive = true;
		//最多为二倍半径
		if (r > m_radius * 2) {
			m_radiusDiffer = m_radius;
		} else {
			m_radiusDiffer = r - m_radius;
		}
	} else {
		m_positive     = false;
		m_radiusDiffer = m_radius - r;
	}
}

QPointF ROICircleItem::getCenterLinePoint() const
{
	//计算第四个点的位置  位于圆心到第二个点的连线上  x3-x2/x2-x1 = r2/r1    y3-y2/y2-y1 = r2/r1
	QPointF p4 = getItemByIndex(1)->pos() - m_centPoint;
	if (m_positive) {
		p4.setX((m_radiusDiffer * p4.rx() + m_radius * p4.rx()) / m_radius);
		p4.setY((m_radiusDiffer * p4.ry() + m_radius * p4.ry()) / m_radius);
	} else {
		// p4.setX((m_radiusDiffer*p4.rx() + m_radius * p4.rx()) / m_radius);
		// p4.setY((m_radiusDiffer*p4.ry() + m_radius * p4.ry()) / m_radius);
		p4.setX(p4.rx() * (m_radius - m_radiusDiffer) / m_radius);
		p4.setY(p4.ry() * (m_radius - m_radiusDiffer) / m_radius);
	}
	return p4 + m_centPoint;
}

QPainterPath ROICircleItem::getMyShap() const
{
	QPainterPath path;
	if (m_addPointIndex > 1) {
		path.addEllipse(m_centPoint, m_radius, m_radius);

	}
	if (m_addPointIndex > 2) {
		path.addEllipse(m_centPoint, m_radius + m_radiusDiffer, m_radius + m_radiusDiffer);
		path.addEllipse(m_centPoint, m_radius - m_radiusDiffer, m_radius - m_radiusDiffer);

	}
	return ImageUtil::shapeFromPath(path, m_pen);
}

void ROICircleItem::calcLineEndPoint()
{
	m_lineEndPoint = getItemByIndex(1)->pos() - m_centPoint;
	m_lineEndPoint.setX((m_radiusDiffer * m_lineEndPoint.rx() + m_radius * m_lineEndPoint.rx()) / m_radius);
	m_lineEndPoint.setY((m_radiusDiffer * m_lineEndPoint.ry() + m_radius * m_lineEndPoint.ry()) / m_radius);
	m_lineEndPoint = m_lineEndPoint + m_centPoint;

	calcArrows();
}

void ROICircleItem::calcArrows()
{
	//计算第四点的箭头
	if (m_positive) {
		m_arrow = CalculateUtil::calculateArraw(m_centPoint,
			getItemByIndex(3)->pos(), m_viewScale).toVector();
	}
	else {
		m_arrow = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(),
			getItemByIndex(3)->pos(), m_viewScale).toVector();
	}
}

void ROICircleItem::viewScaleChangedEvent()
{
	if(m_addPointIndex>=3) {
		calcArrows();
	}
}
