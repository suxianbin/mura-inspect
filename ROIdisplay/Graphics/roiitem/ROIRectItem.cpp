#include "ROIRectItem.h"
#include "ImageUtil.h"
#include "CalculateUtil.h"

ROIRectItem::ROIRectItem(QGraphicsItem *parent)
	: ROIAbstractBaseItem(parent)
{
	HandleControlItem* baseItem = new HandleControlItem(QPoint(0,0),this,0, eControlItemType::Base);
	addControlItem(baseItem);
	setState(DrawingState);
	
}

ROIRectItem::~ROIRectItem()
{
}

void ROIRectItem::addPoint(const QPointF& _point)
{
	if(m_addPointIndex==0) {
		setPos(_point);
		getItemByIndex(0)->show();
		m_addPointIndex++;
		HandleControlItem* pItem = new HandleControlItem(QPoint(0, 0), this, 1);
		addControlItem(pItem);
		pItem->show();
	}else if(m_addPointIndex==1) {
		QPointF p = mapFromScene(_point);
		getItemByIndex(1)->setPos(p);
		m_height = p.ry();
		m_width = p.rx();
		setState(SelectState);
		m_addPointIndex++;
		// emit sig_roiComplete();
	}
}

void ROIRectItem::setData(const QRectF& rect)
{
	addPoint(rect.topLeft());
	addPoint(rect.bottomRight());
}

QRectF ROIRectItem::getData()
{
	QRectF rect;
	rect.setTopLeft(pos());
	rect.setWidth(m_width);
	rect.setHeight(m_height);
	return rect;
}

void ROIRectItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF p = event->scenePos();
	if(index==0) {
		setPos(p);
	}else if(index ==1) {
		p = mapFromScene(p);
		getItemByIndex(1)->setPos(p);
		m_height = p.ry();
		m_width = p.rx();
	}
}

void ROIRectItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	painter->setPen(pen());
	if(m_addPointIndex==1) {
		QPointF p = mapFromScene(m_mouserPos);
		getItemByIndex(1)->setPos(p);
		m_height = p.ry();
		m_width = p.rx();
		painter->drawRect(0, 0, m_width, m_height);
	}else if(m_addPointIndex==2) {
		painter->drawRect(0, 0, m_width, m_height);
	}
}

QPainterPath ROIRectItem::getMyShap() const
{
	QPainterPath path;
	QRectF rect;
	rect.setWidth(m_width);
	rect.setHeight(m_height);
	path.addRect(rect);
	return ImageUtil::shapeFromPath(path, pen());
}
