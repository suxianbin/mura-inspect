#include "ROIRectangleItem.h"
#include "ImageUtil.h"
#include "CalculateUtil.h"


ROIRectangleItem::ROIRectangleItem()
	: ROIAbstractBaseItem()
{
	HandleControlItem* handleItemStart = new HandleControlItem(QPointF(0, 0), this, 0, eControlItemType::Base);
	addControlItem(handleItemStart);
	handleItemStart->hide();
}

ROIRectangleItem::~ROIRectangleItem()
{
}

void ROIRectangleItem::addPoint(const QPointF& _point)
{
	if (m_addPointIndex == 0) {
		//点击第一次，绘制基准位置点位
		//基础点位
		setPos(_point);
		getItemByIndex(0)->show();
		m_addPointIndex++;
		//添加一个绘制控制点
		HandleControlItem* handleItem = new HandleControlItem(QPointF(0, 0), this, 1);
		addControlItem(handleItem);
		handleItem->show();
	}
	else if (m_addPointIndex == 1) {
		//点击第二次  绘制完中心基准线，开始绘制矩形框
		//获取第一个控制点位，
		HandleControlItem* handleItem = getItemByIndex(1);

		QPointF point = _point - pos();
		//计算旋转角度
		calcWidthAndRote(point);
		setRotation(m_rotate);

		point = mapFromScene(_point);
		m_width = point.rx();
		//设置第二个点的位置 ，因为已经旋转过角度了  只用设置宽度就可以
		handleItem->setPos(m_width, 0);

		m_addPointIndex++;
		//添加第三个点的位置
		HandleControlItem* handleItem2 = new HandleControlItem(QPointF(0, 0), this, 2);
		addControlItem(handleItem2);
		handleItem2->show();
	}
	else if (m_addPointIndex == 2) {
		//第三次点击，因为已经旋转过角度了 只用设置高度就可以
		HandleControlItem* handleItem = getItemByIndex(2);
		QPointF point = mapFromScene(_point);
		m_positive = point.y() > 0 ? true : false;
		m_height = abs(point.y());
		handleItem->setPos(m_width, point.y());
		m_addPointIndex++;

		m_arrow = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(), getItemByIndex(2)->pos(), m_viewScale).toVector();

		setState(SelectState);
		// emit sig_roiComplete();
	}
}

void ROIRectangleItem::setData(const ROIRectangleData& _data)
{
	if (_data.offset > 0 && _data.offset < 999999999) {
		addPoint(_data.p1);
		addPoint(_data.p2);
		QPointF p = mapFromScene(_data.p2);
		if (_data.isClockwise) {
			p.setY(p.y() + _data.offset);
		}
		else {
			p.setY(p.y() - _data.offset);
		}
		p = mapToScene(p);
		addPoint(p);
	}
}

ROIRectangleData ROIRectangleItem::getData()
{
	ROIRectangleData data;
	data.p1 = pos();
	data.p2 = mapToScene(getItemByIndex(1)->pos());
	data.offset = m_height;
	data.isClockwise = m_positive;
	return data;
}

void ROIRectangleItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF scenePoint = event->scenePos();
	if (index == 0) {
		setPos(scenePoint);
	}
	else if (index == 1) {
		QPointF point = scenePoint - pos();

		//计算旋转角度
		calcWidthAndRote(point);
		setRotation(m_rotate);
		point = mapFromScene(scenePoint);
		m_width = point.rx();
		auto it = getItemByIndex(1);
		it->setPos(m_width, 0);

		auto it2 = getItemByIndex(2);
		qreal y = m_height;
		if (!m_positive) {
			y = -y;
		}
		it2->setPos(m_width, y);
	}
	else if (index == 2) {
		QPointF point = mapFromScene(scenePoint);
		auto it = getItemByIndex(2);
		m_positive = point.y() > 0 ? true : false;
		m_height = abs(point.y());

		it->setPos(m_width, point.y());
	}
	m_arrow = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(), getItemByIndex(2)->pos(), m_viewScale).toVector();
}

void ROIRectangleItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	QPointF pointf = mapFromScene(m_mouserPos);
	painter->setPen(pen());
	QBrush b = painter->brush();
	b.setStyle(Qt::NoBrush);
	painter->setBrush(b);
	if (m_addPointIndex == 0) {
		return;
	}
	else if (m_addPointIndex == 1) {
		//绘制基准线
		painter->drawLine(QPoint(0, 0), pointf);
		getItemByIndex(1)->setPos(pointf);
	}
	else if (m_addPointIndex == 2) {
		//直接将点转化到item的坐标系上，因为item已经旋转过了，所以绘制的
		pointf = mapToItem(this, pointf);
		m_positive = pointf.ry() > 0 ? true : false;
		m_height = abs(pointf.ry());

		//设置第三个点的位置
		getItemByIndex(2)->setPos(m_width, pointf.ry());
		//计算箭头
		m_arrow = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(), getItemByIndex(2)->pos(), m_viewScale).toVector();
	}

	if (m_addPointIndex >= 2) {
		//绘制基准线
		painter->drawLine(QPoint(0, 0), QPoint(m_width, 0));
		//绘制矩形
		painter->drawRect(0, -m_height, m_width, 2 * m_height);
		//绘制箭头
		QBrush b;
		b.setColor(pen().color());
		b.setStyle(Qt::SolidPattern);
		painter->setBrush(b);
		painter->drawPolygon(m_arrow);
	}
}

void ROIRectangleItem::calcWidthAndRote(const QPointF& pointf)
{
	m_rotate = atan2(pointf.y(), pointf.x());//旋转角度
	m_rotate = m_rotate * 180 / PI;
	qreal k = pointf.y() / pointf.x();
	k = -1 / k;//法线斜率
	m_width = powf(powf(pointf.x(), 2) + powf(pointf.y(), 2), 0.5);		//矩形宽度
}

QPainterPath ROIRectangleItem::getMyShap() const
{
	QPainterPath path;
	path.moveTo(0, 0);
	path.lineTo(m_width, 0);
	HandleControlItem* it2 = getItemByIndex(1);
	path.addRect(0, -m_height, m_width, m_height * 2);
	if (m_addPointIndex >= 1) {
		path.addPolygon(m_arrow);
	}
	return ImageUtil::shapeFromPath(path, pen());
}

void ROIRectangleItem::viewScaleChangedEvent()
{
	if (m_addPointIndex >= 2) {
		m_arrow = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(), getItemByIndex(2)->pos(), m_viewScale).toVector();
	}
}