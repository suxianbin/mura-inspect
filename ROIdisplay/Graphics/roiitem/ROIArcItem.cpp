#include "ROIArcItem.h"
#include "CalculateUtil.h"
#include "ImageUtil.h"


ROIArcItem::ROIArcItem()
	: ROIAbstractBaseItem()
{
	//三个外扩点
	m_externPoint << QPointF(0, 0) << QPointF(0, 0) << QPointF(0, 0);

}

ROIArcItem::~ROIArcItem()
{
	
}

void ROIArcItem::addPoint(const QPointF& _point)
{
	if (m_addPointIndex == 0) {
		//第一个点
		HandleControlItem* item0 = new HandleControlItem(QPointF(0, 0), this, 0);
		addControlItem(item0);
		setPos(_point);
		item0->show();
	

		//第二个点 显示鼠标位置
		HandleControlItem* item1 = new HandleControlItem(QPointF(0, 0), this, 1);
		addControlItem(item1);
		item1->show();
		
		m_addPointIndex++;
	} else if (m_addPointIndex == 1) {
		//第二个点 确定位置
		QPointF p = mapFromScene(_point);
		getItemByIndex(1)->setPos(p);


		//第三个点 显示鼠标位置
		HandleControlItem* item2 = new HandleControlItem(QPointF(0, 0), this, 2);
		addControlItem(item2);
		item2->show();
		
		// 圆心base点 有三个点就可以显示圆心点了
		HandleControlItem* itembase = new HandleControlItem(QPointF(0, 0), this, -1, eControlItemType::Base);
		addControlItem(itembase);
		itembase->show();

		m_addPointIndex++;
	} else if (m_addPointIndex == 2) {
		//第三个个点 确定位置
		QPointF p = mapFromScene(_point);
		getItemByIndex(2)->setPos(p);

		//第二个点 确定圆心半径等信息
		CalculateUtil::getThreePtCircle(getItemByIndex(0)->pos(), getItemByIndex(1)->pos(), p, m_centPoint, m_radius);
		getItemByIndex(-1)->setPos(m_centPoint);
		//计算弧的属性
		calcArcParameter(p);

		//计算基础准弧的范围
		calcArcRect(m_radius, m_baseRect);
		
		//第四个点  扫用范围用的同心圆
		HandleControlItem* item3 = new HandleControlItem(QPointF(0, 0), this, 3);
		addControlItem(item3);
		item3->show();
		
		m_addPointIndex++;
	} else if (m_addPointIndex == 3) {
		//第四个点 确定位置
		QPointF p = mapFromScene(_point);
		calculateRadiusDiffer(p);
		getItemByIndex(3)->setPos(getCenterLinePoint());

		//计算另外两个弧的范围
		qreal widthEx = m_radius + m_radiusDiffer;
		qreal widthIn = m_radius - m_radiusDiffer;


		//计算扫描弧的范围
		calcArcRect(widthEx, m_externalRect);
		calcArcRect(widthIn, m_insideRect);
		
		
		getCenterToEnd(0);
		getCenterToEnd(1);
		getCenterToEnd(2);

		calcArrow();

		//计算 两个圆的半径  以及计算 第四个点的位置
		m_addPointIndex++;
		setState(SelectState);

	}
}

void ROIArcItem::setData(const ROIArcData& _data)
{
	//由圆心和半径及起始角度计算第一个点的位置就是
	if(_data.offset>0&&_data.endAngle>0&& _data.radius>0&& _data.startAngle>0) {
		QPointF p0;
		p0.setY(qSin(_data.startAngle)*_data.radius);
		p0.setX(cos(_data.startAngle)*_data.radius);
		p0 = p0 + _data.center;
		addPoint(p0);

		qreal midAngle = (_data.startAngle + _data.endAngle) / 2;
		QPointF p1;
		p1.setY(qSin(midAngle)*_data.radius);
		p1.setX(cos(midAngle)*_data.radius);
		p1 = p1 + _data.center;
		addPoint(p1);


		QPointF p2;
		p2.setY(qSin(_data.endAngle)*_data.radius);
		p2.setX(qCos(_data.endAngle)*_data.radius);
		p2 = p2 + _data.center;
		addPoint(p2);

		QPointF p3;
		qreal radius = 0;
		if (_data.isPositive) {
			radius = _data.radius + _data.offset;
		}
		else {
			radius = _data.radius - _data.offset;
		}
		p3.setY(qSin(_data.endAngle)*radius);
		p3.setX(qCos(_data.endAngle)*radius);
		p3 = p3 + _data.center;
		addPoint(p3);
	}
}

ROIArcData ROIArcItem::getData()
{
	ROIArcData data;
	data.radius = m_radius;
	data.startAngle = m_startAngle;
	data.endAngle = m_endAngle;
	data.center = mapToScene(m_centPoint);
	data.isPositive = m_positive;
	data.offset = m_radiusDiffer;
	return data;
}

void ROIArcItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF point = event->scenePos();
	if (index != -1&&index!=3) {
		point = mapFromScene(point);
		getItemByIndex(index)->setPos(point);
		//第二个点 确定圆心半径等信息
		CalculateUtil::getThreePtCircle(getItemByIndex(0)->pos(), getItemByIndex(1)->pos(), getItemByIndex(2)->pos(), m_centPoint, m_radius);
		calculateRadiusDiffer(getItemByIndex(3)->pos());
		getItemByIndex(-1)->setPos(m_centPoint);


		//基准弧
		calcArcRect(m_radius, m_baseRect);
		//计算另外两个弧的范围
		qreal widthEx = m_radius + m_radiusDiffer;
		qreal widthIn = m_radius - m_radiusDiffer;
		//计算扫描弧的范围
		calcArcRect(widthEx, m_externalRect);
		calcArcRect(widthIn, m_insideRect);

		
		getCenterToEnd(0);
		getCenterToEnd(1);
		getCenterToEnd(2);

		calcArrow();

	}else if (index == 3) {
		QPointF p = mapFromScene(point);
		calculateRadiusDiffer(p);
		getItemByIndex(3)->setPos(getCenterLinePoint());

		//计算另外两个弧的范围
		qreal widthEx = m_radius + m_radiusDiffer;
		qreal widthIn = m_radius - m_radiusDiffer;
		//计算扫描弧的范围
		calcArcRect(widthEx, m_externalRect);
		calcArcRect(widthIn, m_insideRect);
		
		getCenterToEnd(0);
		getCenterToEnd(1);
		getCenterToEnd(2);

		calcArrow();

	}
	else {
		//圆心位置移动
		QPointF basePoint = getItemByIndex(-1)->pos();
		basePoint = point - basePoint;
		setPos(basePoint);
	}
}

void ROIArcItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	painter->setPen(pen());
	QPointF point = mapFromScene(m_mouserPos);
	if (m_addPointIndex == 1) {
		//确定了第一点 用鼠标位置做第二点
		painter->drawLine(0, 0, point.rx(), point.ry());
		getItemByIndex(1)->setPos(point);
	} else if (m_addPointIndex == 2) {
		//确定了 1 2点用鼠标位置做第三点，计算出 圆心和半径
		CalculateUtil::getThreePtCircle(getItemByIndex(0)->pos(), getItemByIndex(1)->pos(), point, m_centPoint
		                              , m_radius);
		//第三点缓存位置
		getItemByIndex(2)->setPos(point);
		getItemByIndex(-1)->setPos(m_centPoint);

		
		//计算弧角度参数
		calcArcParameter( getItemByIndex(2)->pos());
		//基准弧
		calcArcRect(m_radius, m_baseRect);

		getCenterToEnd(0);
		getCenterToEnd(1);
		getCenterToEnd(2);
	}
	else if (m_addPointIndex == 3) {
		//根据前三个点画圆
		calculateRadiusDiffer(point);
		getItemByIndex(3)->setPos(getCenterLinePoint());
		point = point - m_centPoint;
		//计算另外两个弧的范围
		//计算另外两个弧的范围
		qreal widthEx = m_radius + m_radiusDiffer;
		qreal widthIn = m_radius - m_radiusDiffer;
		//计算扫描弧的范围
		calcArcRect(widthEx, m_externalRect);
		calcArcRect(widthIn, m_insideRect);


		calcArrow();
		
	} 
	if(m_addPointIndex>=2) {
		painter->drawArc(m_baseRect, m_startAngle, m_arcLen);
	}
	
	
	if (m_addPointIndex >= 3) {

		//绘制外弧
		painter->drawArc(m_externalRect, m_startAngle, m_arcLen);
		//绘制内弧
		painter->drawArc(m_insideRect, m_startAngle, m_arcLen);
		//绘制圆心到外弧的直线
		painter->drawLine(m_centPoint, m_externPoint[0]);
		painter->drawLine(m_centPoint, m_externPoint[1]);
		painter->drawLine(m_centPoint, m_externPoint[2]);

		//绘制箭头
		QBrush b(pen().color());
		painter->setBrush(b);
		painter->drawPolygon(m_arrows);
	}
}

void ROIArcItem::calculateRadiusDiffer(const QPointF& _point)
{
	//点到圆心的距离
	QPointF p = _point - m_centPoint;
	qreal r   = pow(pow(p.x(), 2) + pow(p.y(), 2), 0.5);
	if (r > m_radius) {
		m_positive = true;
		//最多为二倍半径
		if (r > m_radius * 2) {
			m_radiusDiffer = m_radius;
		} else {
			m_radiusDiffer = r - m_radius;
		}
	} else {
		m_positive     = false;
		m_radiusDiffer = m_radius - r;
	}
}

QPointF ROIArcItem::getCenterLinePoint()
{
	//计算第四个点的位置  位于圆心到第二个点的连线上  x3-x2/x2-x1 = r2/r1    y3-y2/y2-y1 = r2/r1
	QPointF p4 = getItemByIndex(1)->pos() - m_centPoint;
	if (m_positive) {
		p4.setX((m_radiusDiffer * p4.rx() + m_radius * p4.rx()) / m_radius);
		p4.setY((m_radiusDiffer * p4.ry() + m_radius * p4.ry()) / m_radius);
	} else {
		p4.setX(p4.rx() * (m_radius - m_radiusDiffer) / m_radius);
		p4.setY(p4.ry() * (m_radius - m_radiusDiffer) / m_radius);
	}

	return p4 + m_centPoint;
}

void ROIArcItem::calcArcParameter(const QPointF& _endPoint)
{
	//默认的起始点都是第一个点 pos()0,0
	QPointF p1 = getItemByIndex(0)->pos() - m_centPoint;
	QPointF p2 = getItemByIndex(1)->pos() - m_centPoint;
	QPointF p3 = _endPoint - m_centPoint;

	m_startAngle  = CalculateUtil::getAngle(p1);
	m_midAngle    = CalculateUtil::getAngle(p2);
	m_endAngle    = CalculateUtil::getAngle(p3);
	qreal rAngle  = 0;
	m_isClockwise = CalculateUtil::isClockwise(p1, p2, p3);
	if (m_isClockwise > 0) {
		//顺时针旋转 
		rAngle = m_endAngle - m_startAngle;
		if (rAngle < 0) {
			rAngle = rAngle + 360;
		} else if (rAngle > 360) {
			rAngle = rAngle - 360;
		}
		rAngle = -abs(rAngle) * 16;
	} else if (m_isClockwise < 0) {
		//逆时针选装
		rAngle = m_startAngle - m_endAngle;
		if (rAngle < 0) {
			rAngle = rAngle + 360;
		} else if (rAngle > 360) {
			rAngle = rAngle - 360;
		}
		rAngle = rAngle * 16;
		rAngle = abs(rAngle);
	}

	//经过测试 绘制的角度 90度为12点方向 0度为3点方向，因为我们的坐标系 y轴向下为正，因此需要做个转换
	m_startAngle = -m_startAngle * 16;
	//以x的正轴为0度角  正跨度为逆时针旋转，负跨度为顺时针选装
	m_arcLen = rAngle;

}

QPainterPath ROIArcItem::getMyShap() const
{
	QPainterPath path;
	
	if(m_addPointIndex>1) {
		path.arcTo(m_baseRect, m_startAngle, m_arcLen);
		
	}
	if(m_addPointIndex>2) {
		path.arcTo(m_externalRect, m_startAngle, m_arcLen);
		path.arcTo(m_insideRect, m_startAngle, m_arcLen);
		path.moveTo(m_centPoint);

		QPointF p = getItemByIndex(0)->pos() - m_centPoint;
		p.setX((m_radiusDiffer * p.rx() + m_radius * p.rx()) / m_radius);
		p.setY((m_radiusDiffer * p.ry() + m_radius * p.ry()) / m_radius);
		p = p + m_centPoint;
		path.lineTo(p);
		
		p = getItemByIndex(2)->pos() - m_centPoint;
		p.setX((m_radiusDiffer * p.rx() + m_radius * p.rx()) / m_radius);
		p.setY((m_radiusDiffer * p.ry() + m_radius * p.ry()) / m_radius);
		p = p + m_centPoint;

		path.lineTo(p);
	}
	return ImageUtil::shapeFromPath(path,pen());
}

void ROIArcItem::getCenterToEnd(int index)
{
	QPointF p = getItemByIndex(index)->pos() - m_centPoint;
	p.setX((m_radiusDiffer * p.rx() + m_radius * p.rx()) / m_radius);
	p.setY((m_radiusDiffer * p.ry() + m_radius * p.ry()) / m_radius);
	p = p + m_centPoint;
	m_externPoint[index] = p;
}

void ROIArcItem::calcArcRect(const qreal& radius, QRectF& _rect)
{
	_rect.setLeft(m_centPoint.rx() - radius);
	_rect.setTop(m_centPoint.ry() - radius);
	_rect.setWidth(radius * 2);
	_rect.setHeight(radius * 2);
}

void ROIArcItem::viewScaleChangedEvent()
{
	if(m_addPointIndex>=3) {
		
		calcArrow();
	}
}



void ROIArcItem::calcArrow()
{
	if (m_positive) {
		m_arrows = CalculateUtil::calculateArraw(m_centPoint,
			getItemByIndex(3)->pos(), m_viewScale).toVector();
	}
	else {
		m_arrows = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(),
			getItemByIndex(3)->pos(), m_viewScale).toVector();

	}
}
