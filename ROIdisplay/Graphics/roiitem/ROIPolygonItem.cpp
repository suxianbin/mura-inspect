#include "ROIPolygonItem.h"
#include "CalculateUtil.h"
#include "ImageUtil.h"


ROIPolygonItem::ROIPolygonItem()
	: ROIAbstractBaseItem()
{
	HandleControlItem* item = new HandleControlItem(QPointF(0, 0), this, m_addPointIndex);
	addControlItem(item);
	item->show();
	QAction* addPointAction = new QAction(this);
	addPointAction->setText("添加一个点");
	m_actions << addPointAction;
	connect(addPointAction, &QAction::triggered, [=]()
	{
		
		m_addPointIndex++;
		HandleControlItem* item = new HandleControlItem(QPointF(0, 0), this, m_addPointIndex);
		addControlItem(item);
		item->show();
	});

	m_removeOneAction = new QAction(this);
	m_removeOneAction->setText("移除一个点");
	connect(m_removeOneAction, &QAction::triggered, [=]()
	{
		setState(SelectState);
		HandleControlItem* item = getItemByIndex(m_addPointIndex);
		removeControlItem(item);
		m_addPointIndex--;
	});


	connect(this,&ROIAbstractBaseItem::sig_mouseRightDoubleClick,[=]()
	{
		if(m_state==DrawingState) {
			setState(SelectState);
		}
	});
}

ROIPolygonItem::~ROIPolygonItem()
{
}



void ROIPolygonItem::addPoint(const QPointF& _point)
{
	if(m_state== DrawingState) {
		if(m_addPointIndex==0) {
			setPos(_point);
			getItemByIndex(0)->setPos(0,0);
		}else {
			QPointF p = mapFromScene(_point);
			getItemByIndex(m_addPointIndex)->setPos(p);
		}
		m_addPointIndex++;
		HandleControlItem* item = new HandleControlItem(QPointF(0, 0), this, m_addPointIndex);
		addControlItem(item);
		item->show();
	}
}

void ROIPolygonItem::setData(const QList<QPointF>& points)
{
	for(int i=0;i<points.size();i++) {
		addPoint(points[i]);
	}
	setState(SelectState);
}

QList<QPointF> ROIPolygonItem::getData()
{
	QList<QPointF> res;
	auto list = getControlItemList();
	for(int i=0;i<list.size();i++) {
		res << mapToScene(list[i]->pos());
	}
	return res;
}

QList<QAction*> ROIPolygonItem::getActions()
{
	QList<QAction*> res = m_actions;
	// if(m_state==DrawingState) {
	// 	res << m_finishAction;
	// }
	if(m_addPointIndex>=2) {
		res << m_removeOneAction;
	}
	return res;
}


void ROIPolygonItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF p = event->scenePos();
	p = mapFromScene(p);
	getItemByIndex(index)->setPos(p);
}

void ROIPolygonItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	painter->setPen(pen());
	if (m_state == DrawingState) {
		QPointF p = mapFromScene(m_mouserPos);
		getItemByIndex(m_addPointIndex)->setPos(p);
	}
	if(m_addPointIndex>0&&m_addPointIndex<2) {
		painter->drawLine(getItemByIndex(0)->pos(), getItemByIndex(1)->pos());
	}else {
		QVector<QPointF> points;
		for(int i=0;i<=m_addPointIndex;i++) {
			points << getItemByIndex(i)->pos();
		}
		painter->drawPolygon(points);

	}

	if(m_state==DrawingState) {
		QToolTip::showText(QCursor::pos(),"利用鼠标右键双击可结束绘制" );
	}

}

QPainterPath ROIPolygonItem::getMyShap() const
{
	QPainterPath path;
	QVector<QPointF> points;
	for (int i = 0; i <= m_addPointIndex; i++) {
		points << getItemByIndex(i)->pos();
	}
	path.addPolygon(points);
	return ImageUtil::shapeFromPath(path, pen());
}



