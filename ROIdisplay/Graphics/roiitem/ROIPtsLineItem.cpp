#include "ROIPtsLineItem.h"
#include "CalculateUtil.h"
#include "ImageUtil.h"


ROIPtsLineItem::ROIPtsLineItem()
	: ROIAbstractBaseItem()
{
	HandleControlItem* handleItemStart = new HandleControlItem(QPointF(0, 0), this, 0, eControlItemType::Base);
	addControlItem(handleItemStart);
	handleItemStart->hide();

	
}

ROIPtsLineItem::~ROIPtsLineItem()
{
	
}

void ROIPtsLineItem::addPoint(const QPointF& _point)
{
	if (m_addPointIndex == 0) {
		setPos(_point);
		HandleControlItem* handleItemEnd = new HandleControlItem(QPointF(0, 0), this, 1);
		addControlItem(handleItemEnd);
		handleItemEnd->show();

		getItemByIndex(0)->show();
		m_addPointIndex = 1;

	}
	else if (m_addPointIndex == 1) {
		m_addPointIndex = 2;
		QPointF p = mapFromScene(_point);
		getItemByIndex(1)->setPos(p);
		calcArrows();
		setState(SelectState);
		// emit sig_roiComplete();
	}
}

void ROIPtsLineItem::setArrow(bool f)
{
	m_isDoubleArrow = f;
}

void ROIPtsLineItem::setData(const ROIPtsLineData& data)
{
	addPoint(data.p1);
	addPoint(data.p2);
}

ROIPtsLineData ROIPtsLineItem::getData()
{
	ROIPtsLineData res;
	res.p1 = pos();
	res.p2 = res.p1 + getItemByIndex(1)->pos();

	
	return res; 
}

void ROIPtsLineItem::slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event)
{
	QPointF point = event->scenePos();
	if(type == (int)eControlItemType::Base) {
		point =mapFromItem(this,point);
		setPos(point);
		getItemByIndex(1)->show();

	}else if(type == (int)eControlItemType::Nomal) {
		point = mapFromItem(this,point);
		point = point - pos();
		auto it = getItemByIndex(1);
		it->setPos(point);
		calcArrows();
	}
}

void ROIPtsLineItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	painter->setPen(pen());
	QPointF p1(0, 0);
	QPointF p2;
	if (m_addPointIndex == 1) {
		p2 = mapFromScene(m_mouserPos);
		getItemByIndex(1)->setPos(p2);

		calcArrows();
	}
	if(m_addPointIndex>0) {
		p2 = getItemByIndex(1)->pos();
		painter->drawLine(p1,p2);
		if (m_isDoubleArrow) {
			QBrush b(pen().color());
			painter->setBrush(b);
			painter->drawPolygon(m_arrow1);
			painter->drawPolygon(m_arrow1);
		}
	}
	
	
}

QPainterPath ROIPtsLineItem::getMyShap() const
{
	QPainterPath path;
	if(m_addPointIndex>0) {
		path.moveTo(0, 0);
		HandleControlItem* it2 = getItemByIndex(1);
		path.lineTo(it2->pos());
		if (m_isDoubleArrow) {
			path.addPolygon(m_arrow1);
			path.addPolygon(m_arrow2);
		}
	}
	return ImageUtil::shapeFromPath(path, pen());
}

void ROIPtsLineItem::calcArrows()
{
	if (m_isDoubleArrow) {
		m_arrow1 = CalculateUtil::calculateArraw(getItemByIndex(0)->pos(),
			getItemByIndex(1)->pos(),m_viewScale).toVector();
		m_arrow1 = CalculateUtil::calculateArraw(getItemByIndex(1)->pos(),
			getItemByIndex(0)->pos(), m_viewScale).toVector();
	}
}

void ROIPtsLineItem::viewScaleChangedEvent()
{
	calcArrows();
}





