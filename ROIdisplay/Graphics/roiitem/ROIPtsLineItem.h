#pragma once
#ifndef _ROIPTSLINEITEM_FILE_H_
#define _ROIPTSLINEITEM_FILE_H_
#pragma execution_character_set("utf-8")
#include <QObject>
#include <QGraphicsItem>
#include "ROIAbstractBaseItem.h"


struct ROIPtsLineData
{
	QPointF p1;
	QPointF p2;
};

/********************************
 * 功能 ROi两点连线
 ********************************/
class Q_DECL_EXPORT ROIPtsLineItem : public ROIAbstractBaseItem
{
public:
	ROIPtsLineItem();
	~ROIPtsLineItem() override;


	/********************************
	 * 功能 控制点点击处理
	 * 输入 _point 
	 ********************************/
	void addPoint(const QPointF& _point) override;

	/********************************
	 * 功能 是否绘制双箭头
	 * 输入 f 
	 ********************************/
	void setArrow(bool f);
	
	/********************************
	 * 功能 根据数据绘制
	 * 输入 data 
	 ********************************/
	void setData(const ROIPtsLineData& data);

	/********************************
	 * 功能 获取数据
	 * 返回 
	 ********************************/
	ROIPtsLineData getData();
	
public slots:
	/********************************
	 * 功能 重写 控制点移动事件
	 * 输入 index 
	 * 输入 type 
	 * 输入 event 
	 ********************************/
	void slot_controlItemChanged(int index, int type, QGraphicsSceneMouseEvent* event) override;


private:
	int m_addPointIndex = 0;
	bool m_isDoubleArrow = false;

	QVector<QPointF> m_arrow1;
	QVector<QPointF> m_arrow2;
private:
	/********************************
	 * 功能 重写绘制事件
	 * 输入 painter 
	 * 输入 option 
	 * 输入 widget 
	 ********************************/
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	/********************************
	 * 功能 获取本身的shap
	 * 返回 
	 ********************************/
	QPainterPath getMyShap() const override;

	void calcArrows();

	void viewScaleChangedEvent() override;
};
#endif
