#pragma once
#include <QObject>
#include <QGraphicsEllipseItem>
#include "DrawAbstractBaseItem.h"

class Q_DECL_EXPORT DrawCircleItem : public DrawAbstractBaseItem<QGraphicsEllipseItem>
{

public:
	DrawCircleItem(QGraphicsItem *parent = Q_NULLPTR);
	~DrawCircleItem() override;
};
