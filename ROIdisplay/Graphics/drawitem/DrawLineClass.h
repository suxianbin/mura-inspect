#pragma once

#include <QObject>
#include <QGraphicsItem>
#include "DrawAbstractBaseItem.h"

class Q_DECL_EXPORT DrawLineClass : public DrawAbstractBaseItem<QGraphicsLineItem>
{

public:
	DrawLineClass(QGraphicsItem *parent = Q_NULLPTR);
	~DrawLineClass() override;

};
