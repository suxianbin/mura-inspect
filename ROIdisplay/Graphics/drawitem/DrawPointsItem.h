#pragma once


#include <QGraphicsItem>
#include <QObject>
#include "DrawAbstractBaseItem.h"

class Q_DECL_EXPORT DrawPointsItem : public DrawAbstractBaseItem<QGraphicsPolygonItem>
{

public:
	DrawPointsItem(QGraphicsItem *parent =  Q_NULLPTR);
	~DrawPointsItem() override;
};
