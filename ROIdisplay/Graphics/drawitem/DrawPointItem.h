#pragma once
#include <QGraphicsItem>
#include <QGraphicsEllipseItem>
#include <QObject>
#include "DrawAbstractBaseItem.h"

#define POINTSIZE 6
class Q_DECL_EXPORT DrawPointItem : public DrawAbstractBaseItem<QGraphicsEllipseItem>
{
	
public:
	DrawPointItem(QGraphicsItem *parent = Q_NULLPTR);
	~DrawPointItem() override;
	void setColor(const QColor& _color) override;


};
