#include "DrawPointItem.h"


DrawPointItem::DrawPointItem(QGraphicsItem *parent)
	: DrawAbstractBaseItem<QGraphicsEllipseItem>(parent)
{
	//设置 忽略选装、 放大 、缩小、剪裁等
	setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
	//计算左上角
	setRect(-POINTSIZE, -POINTSIZE, POINTSIZE * 2, POINTSIZE * 2);
	QBrush brush;
	brush.setStyle(Qt::SolidPattern);
}

DrawPointItem::~DrawPointItem()
{
}

void DrawPointItem::setColor(const QColor& _color)
{
	QPen p = pen();
	p.setColor(_color);
	QBrush b = brush();
	b.setColor(_color);
	setPen(p);
	setBrush(b);
}