#pragma once

#include <QObject>
#include <QGraphicsRectItem>
#include "DrawAbstractBaseItem.h"


class Q_DECL_EXPORT DrawRectItem : public DrawAbstractBaseItem<QGraphicsRectItem>
{

public:
	DrawRectItem(QGraphicsItem *parent = Q_NULLPTR);
	~DrawRectItem() override;
};
