#pragma once

#include <QtWidgets/QWidget>
#include "ui_MuraHistoryData.h"

class MuraHistoryData : public QWidget
{
    Q_OBJECT

public:
    MuraHistoryData(QWidget *parent = nullptr);
    ~MuraHistoryData();

private:
    Ui::MuraHistoryDataClass ui;
};
